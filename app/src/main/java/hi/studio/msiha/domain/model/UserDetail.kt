package hi.studio.msiha.domain.model

import android.os.Parcelable
import hi.studio.msiha.data.constant.TYPE_ODHA
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class UserDetail(
    var id: Int? = null,
    var email: String? = null,
    var addressKtp: String? = null,
    var address: String? = null,
    var placeBirth: String? = null,
    var dateBirth: String? = null,
    var gender: String? = null,
    var phone: String? = null,
    var pmoName: String? = null,
    var pmoRelationship: String? = null,
    var pmoPhone: String? = null,
    var pmoEmail: String? = null,
    var patientType: String? = null,
    var isActive: Boolean? = null,
    var createdAt: String? = null,
    var updatedAt: String? = null
) : Parcelable {

    fun isOdha(): Boolean {
        return patientType?.equals(TYPE_ODHA) ?: false
    }

    fun age(): Int {
        return Date().year - Date(dateBirth?.toLong() ?: 0L).year
    }

}