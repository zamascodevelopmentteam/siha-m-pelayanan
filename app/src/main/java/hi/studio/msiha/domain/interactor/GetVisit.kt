package hi.studio.msiha.domain.interactor

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.request.IkhtisarRequest
import hi.studio.msiha.data.datasource.remote.request.MedicineLeftRequest
import hi.studio.msiha.data.datasource.remote.response.*
import hi.studio.msiha.data.repositoryImpl.VisitRepositoryImpl
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either

class GetVisit(private val repo: VisitRepositoryImpl) {

    fun getIkhtisar(
        idVisit: Int,
        callback: (Either<IkhtisarRequest, Throwable>) -> Unit = {}
    ) = repo.getIkhtisar(idVisit, callback)

    fun createVisitHIVNegative(
        ordinal: Int,
        idPatient: Int,
        date: String,
        callback: (Either<Visit, Throwable>) -> Unit = {}
    ) = repo.createVisitHIVNegative(ordinal, idPatient, date, callback)



    fun createVisitODHA(
        ordinal: Int? = null,
        patientId: Int? = null,
        date: String? = null,
        treatementStartDate: String? = null,
        noRegNas: String? = null,
        tglKonfirmasiHivPos: String? = null,
        tglKunjungan: String? = null,
        tglRujukMasuk: String? = null,
        kelompokPopulasi: String? = null,
        statusTb: String? = null,
        tglPengobatanTb: String? = null,
        statusFungsional: String? = null,
        stadiumKlinis: String? = null,
        ppk: Boolean? = null,
        tglPemberianPpk: String? = null,
        statusTbTpt: String? = null,
        tglPemberianObat: String? = null,
        obatArv1: Int? = null,
        jmlHrObatArv1: Int? = null,
        obatArv2: Int? = null,
        jmlHrObatArv2: Int? = null,
        obatArv3: Int? = null,
        jmlHrObatArv3: Int? = null,
        lsmPenjangkauId: Int? = null,
        namaReagen: Int? = null,
        jmlReagen: Int? = null,
        hasilLab: Int? = null,
        hasilLabLain: Int? = null,
        notifikasiPasangan: String? = null,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.createVisitODHA(
        ordinal,
        patientId,
        date,
        treatementStartDate,
        noRegNas,
        tglKonfirmasiHivPos,
        tglKunjungan,
        tglRujukMasuk,
        kelompokPopulasi,
        statusTb,
        tglPengobatanTb,
        statusFungsional,
        stadiumKlinis,
        ppk,
        tglPemberianPpk,
        statusTbTpt,
        tglPemberianObat,
        obatArv1,
        jmlHrObatArv1,
        obatArv2,
        jmlHrObatArv2,
        obatArv3,
        jmlHrObatArv3,
        lsmPenjangkauId,
        namaReagen,
        jmlReagen,
        hasilLab,
        hasilLabLain,
        notifikasiPasangan,
        callback
    )

    fun updateVisit(
        id: Int,
        bundle: Bundle,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.updateVisitDetail(id, bundle, callback)

    fun giveRecipe(
        visitId: Int,
        recipeId: Int,
        patientId: Int,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.giveRecipe(visitId, recipeId, patientId, callback)

    fun getVisitDetail(
        idVisit: Int,
        callback: (Either<Visit, Throwable>) -> Unit = {}
    ) = repo.getVisitDetail(idVisit, callback)

    fun updateIkhtisar(
        ordinal: Int,
        ikhtisarRequest: IkhtisarRequest,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.updateIkhtisar(ordinal, ikhtisarRequest, callback)

    fun getTodayVisit(
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit
    ) = repo.getTodayVisit(callback)

    fun getTodayVisitWithKeyword(
        keyword:String,
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit
    ) = repo.getTodayVisitWithKeyword(keyword,callback)

    fun getPlanVisit(
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit
    ) = repo.getPlanVisit(callback)

    fun getHistoryVisit(
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit
    ) = repo.getVisitHistory(callback)

    fun createGeneralVisit(
        patientId: Int,
        callback: (Either<CreateVisitResponse, Throwable>) -> Unit
    ) = repo.createGeneralVisit(patientId,callback)

    fun getVisitAllDetail(
        visitId:Int,
        callback: (Either<VisitAllDetailResponse, Throwable>) -> Unit
    ) = repo.getVisitAllDetail(visitId, callback)

    fun getPreviousTreatment(
        visitId:Int,
        callback: (Either<VisitAllDetailResponse, Throwable>) -> Unit
    ) = repo.getPreviousTreatment(visitId,callback)

    fun createTreatment(
        visitId:Int,
        callback: (Either<String, Throwable>) -> Unit
    ) = repo.createTreatment(visitId,callback)

    fun saveTreatmentData(
        visitId:Int,
        visitData: VisitData,
        callback: (Either<String, Throwable>) -> Unit
    ) = repo.saveTreatment(visitId,visitData,callback )

    fun endVisit(
        visitId: Int,
        callback: (Either<String,Throwable>) -> Unit
    )= repo.endVisit(visitId,callback)

    fun updateSisaObat(
        visitId: Int,
        medicineLeftRequest: MedicineLeftRequest,
        callback: (Either<DefaultResponse, Throwable>) -> Unit
    ) = repo.updateSisaObat(visitId,medicineLeftRequest,callback)
}