package hi.studio.msiha.domain.interactor

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.request.ODHARequest
import hi.studio.msiha.data.mapper.PatientMapper
import hi.studio.msiha.data.repositoryImpl.PatientRepositoryImpl
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Recipe
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either

class GetPatient(private val repo: PatientRepositoryImpl) {
    fun getPatient(
        page: Int,
        limit: Int,
        callback: (Either<List<Patient>, Throwable>) -> Unit = {}
    ) =
        repo.getPatients(page, limit, callback)

    fun createPatient(bundle: Bundle, callback: (Either<String, Throwable>) -> Unit = {}) =
        repo.createPatient(PatientMapper.toRequest(bundle), callback)

    fun updatePatient(id: Int, bundle: Bundle, callback: (Either<String, Throwable>) -> Unit = {}) =
        repo.updatePatient(id, PatientMapper.toRequest(bundle), callback)

    fun updatePatient(
        id: Int,
        patient: Patient,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) =
        repo.updatePatient(id, PatientMapper.toRequest(patient), callback)

    fun getVisitHistories(
        idPatient: Int,
        type:String,
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit = {}
    ) =
        repo.getVisitHistory(idPatient,type, callback)

    fun activatePatient(
        id: Int,
        password: String,
        active: Boolean,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.activatePatient(id, password, active, callback)

    fun getRecipes(
        idPatient: Int,
        page: Int,
        limit: Int,
        callback: (Either<List<Recipe>, Throwable>) -> Unit = {}
    ) =
        repo.getRecipes(idPatient, page, limit, callback)

    fun updateToODHA(
        idPatient: Int,
        request: ODHARequest,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.updateToODHA(idPatient, request, callback)

    fun getPatientByNIK(nik: String, callback: (Either<Patient?, Throwable>) -> Unit = {}) =
        repo.getPatientByNik(nik, callback)

    fun getPatientById(nik: Int, callback: (Either<Patient?, Throwable>) -> Unit = {}) =
        repo.getPatientById(nik, callback)

}