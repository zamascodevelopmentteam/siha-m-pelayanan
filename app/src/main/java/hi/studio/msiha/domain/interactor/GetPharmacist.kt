package hi.studio.msiha.domain.interactor

import hi.studio.msiha.data.datasource.remote.request.FarmasiOrderRequest
import hi.studio.msiha.data.repositoryImpl.PharmacistRepositoryImpl
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.utils.Either

class GetPharmacist(private val repo: PharmacistRepositoryImpl) {
    fun getListReagen(page: Int, limit: Int, callback: (Either<List<Order>, Throwable>) -> Unit) =
        repo.getListReagen(page, limit, callback)

    fun addMedicine(medicine: Medicine, callback: (Either<MedicineRsp, Throwable>) -> Unit) =
        repo.createMedicine(medicine, callback)

    fun getListMedicine(
        type: String,
        page: Int,
        limit: Int,
        callback: (Either<List<MedicineRsp>, Throwable>) -> Unit
    ) =
        repo.getListMedicine(type, page, limit, callback)

    fun updateMedicine(medicine: Medicine, callback: (Either<MedicineRsp, Throwable>) -> Unit) =
        repo.updateMedicine(medicine, callback)

    fun createOrder(order: FarmasiOrderRequest, callback: (Either<Order, Throwable>) -> Unit) =
        repo.createOrder(order, callback)

    fun confirmMedicine(id: Int, callback: (Either<String, Throwable>) -> Unit) =
        repo.confirmMedicine(id, callback)

    fun getMedicineDetail(id: Int, callback: (Either<MedicineRsp, Throwable>) -> Unit) =
        repo.getMedicineDetail(id, callback)
}