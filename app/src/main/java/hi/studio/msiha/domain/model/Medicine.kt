package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Medicine(
    val id:Int?=0,
    val name:String?="",
    val codeName:String?="",
    val type:String?="",
    val picture:String?="",
    val medicineType:String?="",
    val ingredients:String?="",
    val usage:String?="",
    val createdAt:String?="",
    val updatedAt:String?="",
    val medicineStocks: MedicineStocks?=null
) : Parcelable {
    fun toMedicineRsp():MedicineRsp{
        val medicineStock= ArrayList<MedicineStocks>()
        this.medicineStocks?.let { medicineStock.add(it) }
        return MedicineRsp(
            id?:0,
            name?:"",
            codeName?:"",
            type?:"",
            picture?:"",
            medicineType?:"",
            ingredients,
            usage,
            createdAt?:"",
            updatedAt?:"",
            MedicineStocks()
        )
    }
}