package hi.studio.msiha.domain.interactor

import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import hi.studio.msiha.data.repositoryImpl.ExamRepositoryImpl
import hi.studio.msiha.domain.model.Exam
import hi.studio.msiha.utils.Either

class GetExam(private val repo: ExamRepositoryImpl) {
    fun getExam(
        id: Int,
        callback: (Either<Exam?, Throwable>) -> Unit = {}
    ) = repo.getExam(id, callback)

    fun getExamByVisit(
        idVisit: Int,
        callback: (Either<Exam?, Throwable>) -> Unit = {}
    ) = repo.getExamByVisit(idVisit, callback)

    fun getHivHistoryByVisit(
        visitId:Int,
        callback: (Either<ArrayList<HivTestHistory>?, Throwable>) -> Unit
    ) = repo.getHivHistoryByVisit(visitId,callback)

    fun getVlHistoryByVisit(
        visitId:Int,
        callback: (Either<ArrayList<VlTestHistory>?, Throwable>) -> Unit
    ) = repo.getVlHistoryByVisit(visitId,callback)

    fun getImsHistoryByVisit(
        visitId:Int,
        callback: (Either<ArrayList<ImsTestHistory>?, Throwable>) -> Unit
    ) = repo.getImsHistoryByVisit(visitId,callback)

    fun createHIVExam(
        visitId:Int,
        callback: (Either<String?, Throwable>) -> Unit = {}
    ) = repo.createHIVExam(visitId, callback)

    fun createIMSExam(
        visitId:Int,
        callback: (Either<String?, Throwable>) -> Unit = {}
    ) = repo.createIMSExam(visitId, callback)

    fun createVLCExam(
        visitId:Int,
        callback: (Either<String?, Throwable>) -> Unit = {}
    ) = repo.createVLExam(visitId, callback)

    fun updateHIVExam(
        id: Int,
        request: HIVExamRequest,
        callback: (Either<String?, Throwable>) -> Unit = {}
    ) = repo.updateHIVExam(id, request, callback)

    fun updateIMSExam(
        id: Int,
        request: IMSExamRequest,
        callback: (Either<String?, Throwable>) -> Unit = {}
    ) = repo.updateIMSExam(id, request, callback)

    fun updateVLCExam(
        id: Int,
        request: VLCExamRequest,
        callback: (Either<String?, Throwable>) -> Unit = {}
    ) = repo.updateVLExam(id, request, callback)
}