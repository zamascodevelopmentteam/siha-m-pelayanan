package hi.studio.msiha.domain.model

class OrderMedicine(
    val medicineId:Int,
    val medicine:Medicine
)