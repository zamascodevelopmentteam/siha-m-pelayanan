package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Reagen(
    val id: Int? = null,
    val name: String? = null,
    val codeName: String? = null,
    val medicineType: String? = null,
    val stockUnitType: String? = null,
    val packageUnitType: String? = null,
    val sum: String? = null
) : Parcelable