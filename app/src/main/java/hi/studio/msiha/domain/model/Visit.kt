package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Visit(
    val id: Int? = null,
    val ordinal: Int? = null,
    val visitType: String? = null,
    val visitDate: String? = null,
    val createdBy: Int? = null,
    val createdAt: String? = null,
    val updatedAt: String? = null,
    val userId: Int? = null,
    val upkId: Int? = null,
    val patientId: Int? = null
) : Parcelable