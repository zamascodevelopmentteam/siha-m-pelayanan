package hi.studio.msiha.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PrescriptionMedicine(
    val id:String,
    val amount:Int?,
    val jumlahHari:Int?,
    val notes:String?,
    val createdBy:String?,
    val updatedBy:String?,
    val createdAt:String?,
    val updatedAt:String?,
    val deletedAt:String?,
    val prescriptionId:String?,
    val medicineId:Int?,
    var medicineName:String?="",
    @SerializedName("medicine")
    var medicine: Medicine?=null
) : Parcelable{
    fun toMedicineInstruction():MedicineInstruction{
        return MedicineInstruction(medicineId?:0,medicine?.name?:"",amount?:0,"",notes,medicine?.codeName?:"",jumlahHari?:0)
    }
}