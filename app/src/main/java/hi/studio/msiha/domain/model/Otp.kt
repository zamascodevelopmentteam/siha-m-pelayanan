package hi.studio.msiha.domain.model

data class Otp(
    val id: Int = 0,
    val valid: Boolean = false
)