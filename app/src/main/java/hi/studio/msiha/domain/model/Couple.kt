package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Couple(
    val id: Int,
    val name: String,
    val age: Int,
    val gender: String,
    val relationship: String
) : Parcelable