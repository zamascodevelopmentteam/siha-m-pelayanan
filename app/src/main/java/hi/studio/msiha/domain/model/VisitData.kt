package hi.studio.msiha.domain.model

import android.os.Parcelable
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VisitData(
    val id:Int,
    var ordinal:Int,
    var treatmentStartDate:String?=null,
    var tglKonfirmasiHivPos: String? = null,
    var tglKunjungan: String? = null,
    var tglRujukMasuk: String? = null,
    var kelompokPopulasi: String? = null,
    var statusTb: String? = null,
    var tglPengobatanTb: String? = null,
    var statusFungsional: String? = null,
    var stadiumKlinis: String? = null,
    var ppk: Boolean? = null,
    var tglPemberianPpk: String? = null,
    var statusTbTpt: String? = null,
    var tglPemberianTbTpt:String?=null,
    var tglPemberianObat: String? = null,
    var lsmPenjangkauId: Int? = null,
    var notifikasiPasangan: String? = null,
    var couples: List<ODHAVisitRequest.Couple?>?=null,
    var weight:Int?=0,
    var height:Int?=0,
    var lsmPenjangkau:String?=null,
    var noRegNas:String?=null,
    var akhirFollowUp:String?=null,
    var tglRujukKeluar:String?=null,
    var tglMeninggal:String?=null,
    var tglBerhentiArv:String?=null
) : Parcelable