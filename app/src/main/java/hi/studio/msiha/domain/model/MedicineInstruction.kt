package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class MedicineInstruction(
    val medicineId:Int,
    val name:String,
    val stockQty:Int,
    val unit:String,
    val notes:String?,
    val code:String,
    val jumlahHari:Int
) : Parcelable