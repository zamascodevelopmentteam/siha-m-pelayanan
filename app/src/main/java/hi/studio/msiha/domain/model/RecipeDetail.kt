package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecipeDetail(
    val id: Int = 0,
    val recipeNo: String = "",
    val notes: String = "",
    val givenAt: String = "",
    val userId: Int = 0,
    val recipeMedicine: List<RecipeMedicine>
) : Parcelable