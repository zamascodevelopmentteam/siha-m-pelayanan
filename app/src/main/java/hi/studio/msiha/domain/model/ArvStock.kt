package hi.studio.msiha.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ArvStock (
    val id:Int?=0,
    val name:String?="",
    val codename: String?="",
    @SerializedName("medicine_type")
    val medicineType:String?,
    @SerializedName("is_lini_1")
    val isLini1:Boolean?,
    @SerializedName("is_lini_2")
    val isLini2:Boolean?,
    @SerializedName("is_alkes")
    val isAlkes:Boolean?,
    @SerializedName("is_anak")
    val isAnak:Boolean?,
    @SerializedName("is_cd_vl")
    val isCdVl:Boolean?,
    @SerializedName("is_preventif")
    val isPreventid:Boolean?,
    @SerializedName("is_r1")
    val isR1:Boolean?,
    @SerializedName("is_r2")
    val isR2:Boolean?,
    @SerializedName("is_r3")
    val isR3:Boolean?,
    @SerializedName("is_sifilis")
    val isSifilis:Boolean?,
    @SerializedName("package_quantity")
    val packageQuantity:Int?=0,
    @SerializedName("stock_unit_type")
    val stockUnitType:String?="",
    @SerializedName("package_unit_type")
    val packageUnitType:String?="",
    @SerializedName("stock_qty")
    val stockQuantity:Int?=0,
    val position:Int?=0
) : Parcelable