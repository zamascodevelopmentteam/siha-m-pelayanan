package hi.studio.msiha.domain.interactor

import hi.studio.msiha.data.datasource.remote.request.ARVRequest
import hi.studio.msiha.data.datasource.remote.request.MedicinePrescriptionRequest
import hi.studio.msiha.data.repositoryImpl.RecipeRepositoryImpl
import hi.studio.msiha.domain.model.*
import hi.studio.msiha.utils.Either

class GetRecipe(private val repo: RecipeRepositoryImpl) {
    fun createRecipe(
        nik: String,
        date: Long,
        notes: String,
        medicines: List<RecipeMedicine>,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.createRecipe(nik, date, notes, medicines, callback)

    fun getRecipeDetail(
        idRecipe: Int,
        callback: (Either<RecipeDetail?, Throwable>) -> Unit = {}
    ) = repo.getRecipeDetail(idRecipe, callback)

    fun confirmRecipe(
        idRecipe: Int,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.confirmRecipe(idRecipe, callback)

    fun createArvRecipe(
        request: ARVRequest,
        callback: (Either<String, Throwable>) -> Unit = {}
    ) = repo.createArvRecipe(request, callback)

    fun getArvReagen(
        callback: (Either<List<Reagen>, Throwable>) -> Unit = {}
    ) = repo.getReagens("ARV", "NULL", callback)

    fun getNonArvReagen(
        exam: String,
        callback: (Either<List<Reagen>, Throwable>) -> Unit = {}
    ) = repo.getReagens("NON_ARV", exam, callback)

    fun getArvStock(
        callback: (Either<List<ArvStock>, Throwable>) -> Unit
    ) = repo.getArv(callback)

    fun getReagenStock(
        callback: (Either<List<ArvStock>, Throwable>) -> Unit
    ) = repo.getReagen(callback)

    fun createPrescription(
        med:MedicinePrescriptionRequest,
        visitId:Int,
        callback:(Either<String,Throwable>) ->Unit ={}
    ) = repo.createPrescription(med,visitId,callback)

    fun giveMedicine(
        visitId:Int,
        callback: (Either<String, Throwable>) -> Unit
    ) = repo.giveMedicine(visitId,callback)

    fun getLastGivenMedicine(
        patientId:Int,
        callback: (Either<Prescription, Throwable>) -> Unit
    ) = repo.getLastGivenPrescription(patientId,callback)
}