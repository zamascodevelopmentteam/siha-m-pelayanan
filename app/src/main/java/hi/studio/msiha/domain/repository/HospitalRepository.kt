package hi.studio.msiha.domain.repository

import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Statistic
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.utils.Either

interface HospitalRepository {
    fun getHospitalDetail(id: Int, callback: (Either<Hospital, Throwable>) -> Unit)
    fun getStatistic(callback: (Either<Statistic, Throwable>) -> Unit)
    fun getTodayVisit(callback: (Either<List<TodayVisit>, Throwable>) -> Unit)
    fun getVisits(page: Int, limit: Int, callback: (Either<List<Patient>, Throwable>) -> Unit)
    fun getHospitals(callback: (Either<List<Hospital>, Throwable>) -> Unit)
}