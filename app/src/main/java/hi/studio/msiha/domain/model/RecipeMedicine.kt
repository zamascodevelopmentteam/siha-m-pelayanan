package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecipeMedicine(
    val medicineId: Int = 0,
    val name: String = "",
    val rule: String? = "",
    val amount: Int = 0,
    val detail: MedicineRsp? = null,
    var confirmed: Boolean = false
) : Parcelable