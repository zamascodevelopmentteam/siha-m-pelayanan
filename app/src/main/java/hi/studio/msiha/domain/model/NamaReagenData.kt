package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class NamaReagenData(
    val name:String?=null,
    val codeName:String?=null
) : Parcelable