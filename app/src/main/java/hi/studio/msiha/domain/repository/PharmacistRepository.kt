package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.datasource.remote.request.FarmasiOrderRequest
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.utils.Either

interface PharmacistRepository {
    fun getListReagen(
        page:Int,
        limit:Int,
        callback: (Either<List<Order>, Throwable>) -> Unit)

    fun createMedicine(
        request: Medicine,
        callback: (Either<MedicineRsp, Throwable>) -> Unit)

    fun getListMedicine(
        type:String,
        page:Int,
        limit:Int,
        callback: (Either<List<MedicineRsp>, Throwable>) -> Unit)

    fun updateMedicine(
        request: Medicine,
        callback: (Either<MedicineRsp, Throwable>) -> Unit)

    fun createOrder(
        request:FarmasiOrderRequest,
        callback: (Either<Order, Throwable>) -> Unit)

    fun confirmMedicine(
        id: Int,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getMedicineDetail(
        id: Int,
        callback: (Either<MedicineRsp, Throwable>) -> Unit
    )
}