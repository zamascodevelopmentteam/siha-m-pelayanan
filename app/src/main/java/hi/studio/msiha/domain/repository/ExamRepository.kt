package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import hi.studio.msiha.domain.model.Exam
import hi.studio.msiha.utils.Either

interface ExamRepository {
    fun getExam(id: Int, callback: (Either<Exam?, Throwable>) -> Unit)
    fun getExamByVisit(idVisit: Int, callback: (Either<Exam?, Throwable>) -> Unit)
    fun createHIVExam(visitId: Int, callback: (Either<String, Throwable>) -> Unit)
    fun createIMSExam(visitId:Int, callback: (Either<String, Throwable>) -> Unit)
    fun createVLExam(visitId:Int, callback: (Either<String, Throwable>) -> Unit)
    fun updateHIVExam(
        id: Int,
        request: HIVExamRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun updateIMSExam(
        id: Int,
        request: IMSExamRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun updateVLExam(
        id: Int,
        request: VLCExamRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getHivHistoryByVisit(visitId: Int, callback: (Either<ArrayList<HivTestHistory>?, Throwable>) -> Unit)
    fun getImsHistoryByVisit(visitId: Int, callback: (Either<ArrayList<ImsTestHistory>?, Throwable>) -> Unit)
    fun getVlHistoryByVisit(visitId: Int, callback: (Either<ArrayList<VlTestHistory>?, Throwable>) -> Unit)
}