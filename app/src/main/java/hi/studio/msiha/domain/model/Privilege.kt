package hi.studio.msiha.domain.model

data class Privilege(
    val menu: Int,
    val allow: Boolean = true
)