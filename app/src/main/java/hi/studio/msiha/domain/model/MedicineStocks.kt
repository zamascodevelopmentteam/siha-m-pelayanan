package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class MedicineStocks(
    val cupboardCode: String = "",
    val stock: Int = 0,
    val hospitalId: Int = 0
) : Parcelable