package hi.studio.msiha.domain.interactor

import hi.studio.msiha.data.repositoryImpl.AuthRepositoryImpl
import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.utils.Either

class GetAuth(private val repo: AuthRepositoryImpl) {
    fun login(nik: String, pass: String, callback: (Either<AuthResult, Throwable>) -> Unit = {}) =
        repo.login(nik, pass, callback)

    fun register(
        nik: String,
        name: String,
        password: String,
        regnas: String,
        role: String,
        avatar: String,
        callback: (Either<AuthResult, Throwable>) -> Unit = {}
    ) = repo.register(nik, name, password, regnas, role, avatar, callback)

    fun logout(callback: (Either<String, Throwable>) -> Unit = {}) =
        repo.logout(callback)
}