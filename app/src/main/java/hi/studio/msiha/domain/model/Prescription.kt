package hi.studio.msiha.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Prescription(
    val id:Int,
    val prescriptionNumber:String?,
    val paduanObatIds:String?,
    val paduanObatStr:String?,
    val notes:String?,
    val isDraft:Boolean,
    val createdBy:String?,
    val regimentId:String,
    val createdAt:String?,
    val updatedAt:String?,
    val deletedAt:String?,
    val patientId:String?,
    @SerializedName("statusPaduan")
    val statusPaduan:String?,
    @SerializedName("prescriptionMedicines")
    val prescriptionMedicines:List<PrescriptionMedicine>?
) : Parcelable