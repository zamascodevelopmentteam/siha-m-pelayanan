package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.datasource.remote.request.ODHARequest
import hi.studio.msiha.data.datasource.remote.request.RegisterPatientRequest
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Recipe
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either

interface PatientRepository {
    fun getPatients(page: Int, limit: Int, callback: (Either<List<Patient>, Throwable>) -> Unit)
    fun createPatient(
        request: RegisterPatientRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun updatePatient(
        id: Int,
        request: RegisterPatientRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getVisitHistory(idVisit: Int, type:String,callback: (Either<List<VisitHistory>, Throwable>) -> Unit)

    fun activatePatient(
        id: Int,
        password: String,
        active: Boolean,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getRecipes(
        idPatient: Int,
        page: Int,
        limit: Int,
        callback: (Either<List<Recipe>, Throwable>) -> Unit
    )

    fun updateToODHA(
        id: Int,
        request: ODHARequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getPatientByNik(nik: String, callback: (Either<Patient?, Throwable>) -> Unit)
    fun getPatientById(nik: Int, callback: (Either<Patient?, Throwable>) -> Unit)
}