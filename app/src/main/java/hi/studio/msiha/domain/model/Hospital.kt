package hi.studio.msiha.domain.model

data class Hospital(
    val id:Int,
    val name:String,
    val createdAt:String,
    val updatedAt:String
)