package hi.studio.msiha.domain.model

data class TodayVisit(
    val ordinal: String = "",
    val user: User = User()
)