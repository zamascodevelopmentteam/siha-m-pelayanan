package hi.studio.msiha.domain.model

data class Statistic(
    val visitEstimation: Int = 0,
    val visitThisDay: Int = 0,
    val visitThisMonth: Int = 0,
    val examThisDay: Int = 0
)