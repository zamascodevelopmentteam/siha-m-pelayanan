package hi.studio.msiha.domain.model

import android.os.Parcelable
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import hi.studio.msiha.data.datasource.remote.response.TestHiv
import hi.studio.msiha.data.datasource.remote.response.TestIm
import hi.studio.msiha.data.datasource.remote.response.TestVl
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VisitHistory(
    val id: Int? = null,
    val ordinal: Int? = null,
    val visitType: String? = null,
    val visitDate: String? = null,
    val checkOutDate: String? = null,
    val createdBy: Int? = null,
    val updatedBy: String? = null,
    val createdAt: String? = null,
    val updatedAt: String? = null,
    val deletedAt: String? = null,
    val userId: Int? = null,
    val upkId: Int? = null,
    val patientId: Int? = null,
    val patient:Patient?=null,
    val isComplete:Boolean?=false,
    val isGenerated:Boolean?=false,
    val treatment: ODHAVisitRequest?=null,
    val testHiv:TestHiv?=null,
    val testIms:TestIm?=null,
    val testVl:TestVl?=null,
    val isOnTransit: Boolean? =null,
    val isCompletedForLab:Boolean?=false,
    val isCompletedForRR:Boolean?=false,
    val isCompletedForPharma:Boolean?=false,
    val needConfirmation:Boolean?=false,
    val prescription: Prescription?=null,
    val upk:UPK?=null
    //val upk
    //val petugasRR
) : Parcelable