package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Patient(
    val id: Int? = null,
    var nik: String? = null,
    var name: String? = null,
    var addressKTP: String? = null,
    var addressDomicile: String? = null,
    var dateBirth: String? = null,
    var gender: String? = null,
    var phone: String? = null,
    var statusPatient: String? = null,
    var weight: Int? = null,
    var height: Int? = null,
    var namaPmo: String? = null,
    var hubunganPmo: String? = null,
    var noHpPmo: String? = null,
    var createdBy: Int? = null,
    var createdAt: String? = null,
    var updatedAt: String? = null,
    var userId: Int? = null,
    var upkId: Int? = null,
    var email:String?=null,
    var user:User?=null
) : Parcelable