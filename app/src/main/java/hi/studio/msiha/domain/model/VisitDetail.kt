package hi.studio.msiha.domain.model

import android.os.Parcelable
import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import kotlinx.android.parcel.Parcelize

@Parcelize
class VisitDetail(
    val id: Int? = null,
    val ordinal: Int? = null,
    val visitType: String? = null,
    val visitDate: String? = null,
    val checkOutDate: String? = null,
    val createdBy: Int? = null,
    val updatedBy: String? = null,
    val createdAt: String? = null,
    val updatedAt: String? = null,
    val deletedAt: String? = null,
    val userId: Int? = null,
    val upkId: Int? = null,
    val patientId: Int? = null,
    val patient:Patient?=null,
    val treatment:ODHAVisitRequest?=null,
    val testHiv:HIVExamRequest?=null,
    val testIms:IMSExamRequest?=null,
    val testVL:VLCExamRequest?=null,
    val prescription: Prescription
) : Parcelable