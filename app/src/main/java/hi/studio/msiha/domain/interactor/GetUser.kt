package hi.studio.msiha.domain.interactor

import hi.studio.msiha.data.repositoryImpl.UserRepositoryImpl
import hi.studio.msiha.domain.model.Otp
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.Either

class GetUser(private val repo: UserRepositoryImpl) {
    fun getProfile(id: Int, callback: (Either<User, Throwable>) -> Unit = {}) =
        repo.getProfile(id, callback)

    fun getUserByNIK(nik: String, callback: (Either<User?, Throwable>) -> Unit = {}) =
        repo.getUserByNik(nik, callback)

    fun getOtp(idUser: Int, callback: (Either<Otp, Throwable>) -> Unit = {}) =
        repo.getOtp(idUser, callback)

    fun submitOtp(idOtp: Int, code: String, callback: (Either<Otp, Throwable>) -> Unit = {}) =
        repo.submitOtp(idOtp, code, callback)

    fun updateProfile(user: User, callback: (Either<User, Throwable>) -> Unit = {}) =
        repo.updateProfile(user, callback)
}