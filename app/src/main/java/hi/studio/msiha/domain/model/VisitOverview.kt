package hi.studio.msiha.domain.model

data class VisitOverview(
    val treatementStartDate: String? = null,
    val noRegNas: String? = null
)