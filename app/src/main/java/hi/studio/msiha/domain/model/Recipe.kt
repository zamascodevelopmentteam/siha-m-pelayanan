package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Recipe(
    val id: Int = 0,
    val recipeNo: String = "",
    val totalMedicine: Int = 0
) : Parcelable