package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UPK(
    var id:Int?=0,
    var name:String?=""
) : Parcelable