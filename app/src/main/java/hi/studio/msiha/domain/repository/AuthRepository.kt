package hi.studio.msiha.domain.repository

import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.utils.Either

interface AuthRepository {
    fun login(nik: String, pass: String, callback: (Either<AuthResult, Throwable>) -> Unit)
    fun forgotPassword(nik: String)
    fun register(
        nik: String,
        name: String,
        password: String,
        regnas: String,
        role: String,
        avatar: String,
        callback: (Either<AuthResult, Throwable>) -> Unit
    )

    fun logout(callback: (Either<String, Throwable>) -> Unit)
}