package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    var id: Int? = null,
    var nik: String? = null,
    var isActive: Boolean? = null,
    var name: String? = null,
    var role: String? = null,
    var logisticrole: String? = null,
    var avatar: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var createdBy: Int? = null,
    var updatedBy: Int? = null,
    var createdAt: String? = null,
    var updatedAt: String? = null,
    var provinceId: Int? = null,
    var upkId: Int? = null,
    var sudinKabKotumId: Int? = null
) : Parcelable
