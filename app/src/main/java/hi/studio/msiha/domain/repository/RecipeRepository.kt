package hi.studio.msiha.domain.repository

import hi.studio.msiha.data.datasource.remote.request.ARVRequest
import hi.studio.msiha.data.datasource.remote.request.MedicinePrescriptionRequest
import hi.studio.msiha.domain.model.*
import hi.studio.msiha.utils.Either

interface RecipeRepository {
    fun giveMedicine(
        visitId:Int,
        callback: (Either<String, Throwable>) -> Unit)

    fun createPrescription(
         medicinePrescriptionRequest: MedicinePrescriptionRequest,
         visitId:Int,
         callback: (Either<String, Throwable>) -> Unit
    )

    fun createRecipe(
        nik: String,
        date: Long,
        notes: String,
        medicines: List<RecipeMedicine>,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getRecipeDetail(
        idRecipe: Int,
        callback: (Either<RecipeDetail?, Throwable>) -> Unit
    )

    fun confirmRecipe(
        idRecipe: Int,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun createArvRecipe(
        request: ARVRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getReagens(
        type: String,
        exam: String,
        callback: (Either<List<Reagen>, Throwable>) -> Unit
    )

    fun getArv(
        callback: (Either<List<ArvStock>, Throwable>) -> Unit
    )

    fun getReagen(callback: (Either<List<ArvStock>, Throwable>) -> Unit)

    fun getLastGivenPrescription(
        patientId:Int,
        callback: (Either<Prescription, Throwable>) -> Unit
    )
}