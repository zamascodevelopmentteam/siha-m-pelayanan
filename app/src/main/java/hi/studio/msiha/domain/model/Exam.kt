package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Exam(
    val id: Int? = null,
    val examOrdinal: String? = null,
    val examRequestDate: String? = null,
    val examDate: String? = null,
    val receiveResultDate: String? = null,
    val reasonTestHiv: String? = null,
    val hivTestType: String? = null,
    val hivExaminationMethod: String? = null,
    val reagen1: String? = null,
    val nameReagen1: String? = null,
    val resultReagen1: String? = null,
    val reagen2: String? = null,
    val nameReagen2: String? = null,
    val resultReagen2: String? = null,
    val reagen3: String? = null,
    val nameReagen3: String? = null,
    val resultReagen3: String? = null,
    val nameReagenNat: String? = null,
    val resultTestNat: String? = null,
    val nameReagenPcr: String? = null,
    val resultTestPcr: String? = null,
    val nameReagenElisa: String? = null,
    val resultTestElisa: String? = null,
    val nameReagenWesternBlot: String? = null,
    val resultTestWesternBlot: String? = null,
    val conclusionResultTestHiv: String? = null,
    val hivResult: String? = null,
    val sifilisResult: String? = null,
    val testLab: String? = null,
    val resultTestLab: String? = null,
    val rprIms: Int? = null,
    val imsResult: String? = null,
    val createdAt: String? = null,
    val updatedAt: String? = null,
    val visitId: Int? = null,
    val userId: Int? = null,
    val hospitalId: Int? = null,
    val vlTestType: String? = null,
    val vlReagen: String? = null,
    val vlReagenAmount: Int? = null,
    val vlTestResult: Int? = null
) : Parcelable