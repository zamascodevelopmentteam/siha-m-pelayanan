package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Arv(
    var id: Int? = null,
    var name: String? = null,
    var days: Int? = null,
    var medicineAmount: Int? = null,
    var medicineUnit: String? = null,
    var type: String? = null,
    var rules: String? = null,
    var stock: Int? = null
) : Parcelable