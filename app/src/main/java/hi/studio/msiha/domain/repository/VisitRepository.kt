package hi.studio.msiha.domain.repository

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.request.IkhtisarRequest
import hi.studio.msiha.data.datasource.remote.request.MedicineLeftRequest
import hi.studio.msiha.data.datasource.remote.response.*
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either

interface VisitRepository {


    fun getVisitAllDetail(
        visitId:Int,
        callback: (Either<VisitAllDetailResponse, Throwable>) -> Unit
    )
    fun createGeneralVisit(
        patientId: Int,
        callback: (Either<CreateVisitResponse, Throwable>) -> Unit
    )

    fun createVisitHIVNegative(
        ordinal: Int,
        idPatient: Int,
        date: String, callback: (Either<Visit, Throwable>) -> Unit
    )

    fun createVisitODHA(
        ordinal: Int?,
        patientId: Int?,
        date: String?,
        treatementStartDate: String?,
        noRegNas: String?,
        tglKonfirmasiHivPos: String?,
        tglKunjungan: String?,
        tglRujukMasuk: String?,
        kelompokPopulasi: String?,
        statusTb: String?,
        tglPengobatanTb: String?,
        statusFungsional: String?,
        stadiumKlinis: String?,
        ppk: Boolean?,
        tglPemberianPpk: String?,
        statusTbTpt: String?,
        tglPemberianObat: String?,
        obatArv1: Int?,
        jmlHrObatArv1: Int?,
        obatArv2: Int?,
        jmlHrObatArv2: Int?,
        obatArv3: Int?,
        jmlHrObatArv3: Int?,
        lsmPenjangkauId: Int?,
        namaReagen: Int?,
        jmlReagen: Int?,
        hasilLab: Int?,
        hasilLabLain: Int?,
        notifikasiPasangan: String?,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun updateVisitDetail(id: Int, bundle: Bundle, callback: (Either<String, Throwable>) -> Unit)
    fun giveRecipe(
        visitId: Int,
        recipeId: Int,
        patientId: Int,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getVisitDetail(idVisit: Int, callback: (Either<Visit, Throwable>) -> Unit)
    fun getIkhtisar(idVisit: Int, callback: (Either<IkhtisarRequest, Throwable>) -> Unit)
    fun updateIkhtisar(
        ordinal: Int,
        visit: IkhtisarRequest,
        callback: (Either<String, Throwable>) -> Unit
    )

    fun getTodayVisit(callback: (Either<List<VisitHistory>, Throwable>) -> Unit)
    fun getTodayVisitWithKeyword(keyword:String,callback: (Either<List<VisitHistory>, Throwable>) -> Unit)
    fun getPlanVisit(callback: (Either<List<VisitHistory>, Throwable>) -> Unit)
    fun getVisitHistory(callback: (Either<List<VisitHistory>, Throwable>) -> Unit)
    fun createTreatment(visitId:Int,callback:(Either<String,Throwable>) -> Unit)
    fun saveTreatment(visitId:Int,visitData: VisitData,callback:(Either<String,Throwable>) -> Unit)
    fun endVisit(visitId: Int, callback: (Either<String, Throwable>) -> Unit)
    fun getPreviousTreatment(visitId: Int, callback: (Either<VisitAllDetailResponse, Throwable>) -> Unit)
    fun updateSisaObat(visitId: Int, medicineLeftRequest: MedicineLeftRequest, callback: (Either<DefaultResponse, Throwable>) -> Unit)
}