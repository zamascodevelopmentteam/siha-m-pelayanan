package hi.studio.msiha.domain.repository

import hi.studio.msiha.domain.model.Otp
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.Either

interface UserRepository {
    fun getProfile(id: Int, callback: (Either<User, Throwable>) -> Unit)
    fun getUserByNik(nik: String, callback: (Either<User?, Throwable>) -> Unit)
    fun getOtp(idUser: Int, callback: (Either<Otp, Throwable>) -> Unit)
    fun submitOtp(idOtp: Int, code: String, callback: (Either<Otp, Throwable>) -> Unit)
    fun updateProfile(user: User, callback: (Either<User, Throwable>) -> Unit)
}