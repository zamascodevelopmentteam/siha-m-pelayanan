package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MedicineRsp(
    val id: Int = 0,
    val name: String = "",
    val codeName: String = "",
    val type: String = "",
    val picture: String? = "",
    val medicineType: String = "",
    val ingredients: String? = "",
    val usage: String? = "",
    val createdAt: String = "",
    val updatedAt: String = "",
    val medicineStock: MedicineStocks = MedicineStocks()
) : Parcelable