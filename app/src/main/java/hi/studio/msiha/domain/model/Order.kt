package hi.studio.msiha.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Order(
    val id:Int,
    val invoiceNumber:String,
    val transactionDate:Long,
    val confirm:Boolean,
    val to:String,
    val amount:Int,
    val createdAt:String,
    val updatedAt:String,
    val hospitalId:Int,
    val orderMedicines:ArrayList<Medicine>?
) : Parcelable