package hi.studio.msiha.domain.interactor

import hi.studio.msiha.data.repositoryImpl.HospitalRepositoryImpl
import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Statistic
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.utils.Either

class GetHospital(private val repo: HospitalRepositoryImpl) {
    fun getStatistic(callback: (Either<Statistic, Throwable>) -> Unit) = repo.getStatistic(callback)

    fun getTodayVisit(callback: (Either<List<TodayVisit>, Throwable>) -> Unit) =
        repo.getTodayVisit(callback)

    fun getVisits(page: Int, limit: Int, callback: (Either<List<Patient>, Throwable>) -> Unit) =
        repo.getVisits(page, limit, callback)

    fun getHospitalById(id:Int, callback:(Either<Hospital, Throwable>) -> Unit)=
        repo.getHospitalDetail(id,callback)

    fun getHospitals(callback: (Either<List<Hospital>, Throwable>) -> Unit) =
        repo.getHospitals(callback)
}