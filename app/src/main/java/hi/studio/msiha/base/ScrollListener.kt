package hi.studio.msiha.base

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class ScrollListener : RecyclerView.OnScrollListener() {
    private var loading = false
    private var prevTotal = 0
    private var maxItemVisible = 5

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount = recyclerView.childCount
        val totalItemCount = recyclerView.layoutManager?.itemCount ?: 0
        val firstVisibleItem =
            (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        if (loading) {
            if (totalItemCount > prevTotal) {
                loading = false
                prevTotal = totalItemCount
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + maxItemVisible)) {
            loading = true
            onLoadMore()
        }
//        if ((recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == recyclerView.adapter?.itemCount ?: 0 - 1) {
//            onLoadMore()
//        }
    }

    abstract fun onLoadMore()
}