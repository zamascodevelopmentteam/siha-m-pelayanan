package hi.studio.msiha.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

abstract class BaseAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    AutoUpdatableAdapter {

    private lateinit var callback: Unit
    protected var data: MutableList<T> by Delegates.observable(ArrayList()) { prop, old, new ->
        compareDiffUtil(prop, old, new)
    }
    var stopFetch = false

    val all: List<T>
        get() = data

    private var onItemClick: OnItemClick<T>? = null
    private var onScrollList: OnScrollList? = null

    @LayoutRes
    protected abstract fun setView(viewType: Int): Int

    protected abstract fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder

    protected abstract fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: T,
        callback: OnItemClick<T>?
    )

    protected abstract fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<T>,
        new: MutableList<T>
    )

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(setView(viewType), parent, false)
        return itemViewHolder(parent.context, view, viewType)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        if (!stopFetch) {
            when (position) {
                0 -> {
                    onScrollList?.onScroll(
                        isReachTop = true,
                        isReachMiddle = false,
                        isReachBottom = false
                    )
                }
                data.size - 1 -> {
                    onScrollList?.onScroll(
                        isReachTop = false,
                        isReachMiddle = false,
                        isReachBottom = true
                    )
                }
                else -> {
                    onScrollList?.onScroll(
                        isReachTop = false,
                        isReachMiddle = true,
                        isReachBottom = false
                    )
                }
            }
        }
        onBindViewHolder(holder, position, itemCount, get(position), onItemClick)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun add(t: T) {
        data.add(t)
        notifyItemInserted(itemCount - 1)
    }

    fun add(t: T, position: Int) {
        data.add(position, t)
        notifyItemInserted(position)
    }

    fun update(t: T, position: Int) {
        data[position] = t
        notifyItemChanged(position)
    }

    fun addAll(data: List<T>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun remove(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    operator fun get(position: Int): T {
        return data[position]
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    fun itemCallback(callback: (view: View, item: T, position: Int) -> Unit) {
        onItemClick = object : OnItemClick<T> {
            override fun onClick(view: View, item: T, position: Int) {
                callback(view, item, position)
            }
        }
    }

    fun scrollCallback(callback: (isReachTop: Boolean, isReachMiddle: Boolean, isReachBottom: Boolean) -> Unit) {
        onScrollList = object : OnScrollList {
            override fun onScroll(
                isReachTop: Boolean,
                isReachMiddle: Boolean,
                isReachBottom: Boolean
            ) {
                callback(isReachTop, isReachMiddle, isReachBottom)
            }
        }
    }

    interface OnItemClick<T> {
        fun onClick(view: View, item: T, position: Int = 0)
    }

    interface OnScrollList {
        fun onScroll(isReachTop: Boolean, isReachMiddle: Boolean, isReachBottom: Boolean)
    }
}

interface AutoUpdatableAdapter {
    fun <T> RecyclerView.Adapter<*>.autoNotify(
        oldList: List<T>,
        newList: List<T>,
        compare: (T, T) -> Boolean
    ) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return compare(oldList[oldItemPosition], newList[newItemPosition])
            }

            override fun areContentsTheSame(
                oldItemPosition: Int,
                newItemPosition: Int
            ): Boolean {
                return oldList[oldItemPosition] == newList[newItemPosition]
            }

            override fun getOldListSize() = oldList.size

            override fun getNewListSize() = newList.size
        })

        diff.dispatchUpdatesTo(this)
    }
}

abstract class BaseViewHolder<T>(itemView: View) :
    RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = this.itemView

    abstract fun onBind(index: Int, count: Int, item: T, callback: BaseAdapter.OnItemClick<T>?)
}