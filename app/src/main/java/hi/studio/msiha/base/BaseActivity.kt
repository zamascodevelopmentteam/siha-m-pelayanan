package hi.studio.msiha.base

import android.app.ProgressDialog
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.data.constant.KEY_FCM_TOKEN
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast

abstract class BaseActivity : AppCompatActivity() {
    protected lateinit var handler: Handler
    private var doubleBackToExitPressedOnce: Boolean = false

    @LayoutRes
    protected abstract fun setView(): Int

    protected abstract fun initView(savedInstanceState: Bundle?)

    private var loading: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (setView() != 0) {
            when (AppSIHA.instance.getUser()?.role) {
                KEY_ROLE_RR -> setTheme(R.style.rr)
                KEY_ROLE_LAB ->setTheme(R.style.lab)
                KEY_ROLE_PHARMACIST -> setTheme(R.style.pharmacist)
                else -> 0
            }
            setContentView(setView())
        }
        handler = Handler()
        initView(savedInstanceState)
        getFcmToken()
    }

    private fun getFcmToken(){
        //if(!Hawk.contains(KEY_FCM_TOKEN)){
        //Log.d("fcm","Token ")
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("fcm token", "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token

                    // Log and toast
                    val msg = "token fcm is : $token"
                    Hawk.put(KEY_FCM_TOKEN,token)
                    Log.d("fcm token", msg)
                    //Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                })
        //}
    }

    protected fun doubleBackPressed(message: String) {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(this, getString(R.string.exit_message), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }

        handler.postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    private fun initLoading() {
        loading = indeterminateProgressDialog(message = "Mohon tunggu...")
        loading?.setCancelable(false)
    }

    fun showError(e: Throwable?) {
        showLoading(false)
        e?.let {
            toast(it.localizedMessage)
        }
    }

    fun showLoading(show: Boolean = true) {
        if (loading == null) {
            initLoading()
        }
        if (show) {
            loading?.apply {
                if (!isShowing) show()
            }
        } else {
            loading?.apply {
                if (isShowing) dismiss()
            }
        }
    }

    fun showMessage(msg: String) {
        showLoading(false)
        toast(msg)
    }

    fun showImportantMessage(msg: String) {
        showLoading(false)
        longToast(msg)
    }


    override fun onDestroy() {
        loading?.dismiss()
        super.onDestroy()
    }
}