package hi.studio.msiha

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import androidx.multidex.MultiDex
//import com.google.firebase.analytics.FirebaseAnalytics
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.constant.*
import hi.studio.msiha.di.*
import hi.studio.msiha.domain.model.Privilege
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.ui.auth.AuthActivity
import hi.studio.msiha.utils.DebugTree
import hi.studio.msiha.utils.PreferenceHelper.get
import hi.studio.msiha.utils.PreferenceHelper.set
import hi.studio.msiha.utils.extention.clear
import hi.studio.msiha.utils.extention.getObject
import hi.studio.msiha.utils.extention.putObject
import org.jetbrains.anko.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class AppSIHA : Application() {
    private val preferences: SharedPreferences by inject()
    private val privileges = mutableMapOf(
        Pair(KEY_ROLE_LAB, Privilege(R.menu.main_lab_menu)),
        Pair(KEY_ROLE_RR, Privilege(R.menu.main_rr_menu)),
        Pair(KEY_ROLE_PHARMACIST, Privilege(R.menu.main_pharmacist_menu)),
        Pair(KEY_ROLE_PATIENT, Privilege(0, false))
    )

    companion object {
        @JvmStatic
        lateinit var instance: AppSIHA
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree("_siha"))
        }
        startKoin {
            androidContext(this@AppSIHA)
            modules(
                listOf(
                    appModule,
                    remoteModule,
                    repoModule,
                    roomModule,
                    viewModelModule,
                    useCaseModule
                )
            )
        }
        Hawk.init(this).build()
        //FirebaseAnalytics.getInstance(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun isInternetAvailable(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun setToken(token: String) {
        preferences[KEY_TOKEN] = token
    }

    fun getToken(): String? {
        val token: String? = preferences[KEY_TOKEN]
        return "Bearer $token"
    }

    fun setRefreshToken(token: String) {
        preferences[KEY_REFRESH_TOKEN] = token
    }

    fun getRefreshToken(): String? {
        return preferences[KEY_REFRESH_TOKEN]
    }

    fun setUser(user: User) {
        preferences.putObject(KEY_USER, user)
    }

    fun getUser(): User? {
        return preferences.getObject(KEY_USER, User::class.java)
    }

    fun getPrivilege(): Privilege? {
        val user = getUser()
        return privileges[user?.role]
    }

    fun isAllow(): Boolean {
        return getPrivilege()?.allow ?: false
    }

    fun clearData() {
        Timber.d("data cleared")
        preferences.clear()
    }

    fun logout() {
        preferences.clear()
        startActivity(
            intentFor<AuthActivity>()
                .newTask()
                .singleTop()
                .clearTop()
                .clearTask()
        )
    }
}