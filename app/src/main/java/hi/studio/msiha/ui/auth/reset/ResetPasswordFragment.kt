package hi.studio.msiha.ui.auth.reset

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment

class ResetPasswordFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_reset_password
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }
}