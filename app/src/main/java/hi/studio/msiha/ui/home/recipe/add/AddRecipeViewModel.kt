package hi.studio.msiha.ui.home.recipe.add

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.constant.MEDICINE_CURE
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.RecipeMedicine
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class AddRecipeViewModel(
    private val pharmacist: GetPharmacist,
    private val getRecipe: GetRecipe
) : BaseViewModel() {
    val medicinesResult = MutableLiveData<List<MedicineRsp>>().toSingleEvent()
    val addRecipeResult = MutableLiveData<String>().toSingleEvent()

    fun loadMedicine(page: Int, limit: Int) {
        isLoading.postValue(true)
        pharmacist.getListMedicine(MEDICINE_CURE, page, limit) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> medicinesResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createRecipe(
        nik: String,
        date: Long,
        notes: String,
        medicines: List<RecipeMedicine> = emptyList()
    ) {
        isLoading.postValue(true)
        getRecipe.createRecipe(nik, date, notes, medicines) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> addRecipeResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}