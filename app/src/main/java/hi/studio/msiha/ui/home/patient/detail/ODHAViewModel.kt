package hi.studio.msiha.ui.home.patient.detail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.request.ODHARequest
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.model.Otp
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ODHAViewModel(private val getPatient: GetPatient, private val getUser: GetUser) :
    BaseViewModel() {

    val odhaResult = MutableLiveData<String>().toSingleEvent()
    val otpResult = MutableLiveData<Otp>().toSingleEvent()

    fun updateToODHA(id: Int, request: ODHARequest) {
        isLoading.postValue(true)
        getPatient.updateToODHA(id, request) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> odhaResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getOtp(userId: Int) {
        isLoading.postValue(true)
        getUser.getOtp(userId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> otpResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}