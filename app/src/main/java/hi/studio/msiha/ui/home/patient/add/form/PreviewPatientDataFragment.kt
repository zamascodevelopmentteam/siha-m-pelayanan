package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.wizard.WizardStep

class PreviewPatientDataFragment : BaseFragment(), WizardStep,
    AddPatientFragment.OnAddPatientParentListener {

    companion object {
        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            PreviewPatientDataFragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = Bundle()
        set(value) {}

    override fun invalidateStep(): Boolean {
        return true
    }

    override fun setView(): Int {
        return R.layout.fragment_preview_patient_data
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }

    override fun onPreviewData(bundle: Bundle?) {
        super.onPreviewData(bundle)
        bundle?.apply {
            val nik = getString(PersonalDataFragment.KEY_PERSONAL_NIK)
            val name = getString(PersonalDataFragment.KEY_PERSONAL_NAME)
            val address = getString(PersonalDataFragment.KEY_PERSONAL_ADDRESS)
            val dob = getString(PersonalDataFragment.KEY_PERSONAL_DOB)
            val age = getInt(PersonalDataFragment.KEY_PERSONAL_AGE, 0)
            val domicile = getString(PersonalDataFragment.KEY_PERSONAL_DOMICILE)
            val gender = getString(PersonalDataFragment.KEY_PERSONAL_GENDER)
            val phone = getString(PersonalDataFragment.KEY_PERSONAL_PHONE)
            val email = getString(PersonalDataFragment.KEY_PERSONAL_EMAIL)
            val patientType = getString(PersonalDataFragment.KEY_PERSONAL_TYPE)
            val pmoName = getString(PMODataFragment.KEY_PMO_NAME)
            val pmoEmail = getString(PMODataFragment.KEY_PMO_EMAIL)
            val pmoRelation = getString(PMODataFragment.KEY_PMO_RELATION)
            val pmoPhone = getString(PMODataFragment.KEY_PMO_PHONE)
        }
    }
}