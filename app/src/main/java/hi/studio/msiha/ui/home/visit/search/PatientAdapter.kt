package hi.studio.msiha.ui.home.visit.search

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.Patient
import kotlin.reflect.KProperty

class PatientAdapter : BaseAdapter<Patient>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_patient
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return PatientViewHolder(context, view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: Patient,
        callback: OnItemClick<Patient>?
    ) {
        when (holder) {
            is PatientViewHolder -> holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<Patient>,
        new: MutableList<Patient>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}