package hi.studio.msiha.ui.home.pharmacist.stock.addmedicine

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class AddMedicineViewModel(private val getPharmacist: GetPharmacist):BaseViewModel(){
    val createMedicineResult = MutableLiveData<MedicineRsp>().toSingleEvent()

    fun createMedicine(medicine: Medicine) {
        isLoading.postValue(true)
        getPharmacist.addMedicine(medicine) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> createMedicineResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}