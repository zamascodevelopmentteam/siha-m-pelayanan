package hi.studio.msiha.ui.home.pharmacist.recipe

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.RecipeDetail
import hi.studio.msiha.domain.model.RecipeMedicine
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.pharmacist.dashboard.PharmacistDashboardFragment
import hi.studio.msiha.ui.home.recipe.add.AddRecipeActivity
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.fragment_patient_recipe.*
import kotlinx.android.synthetic.main.layout_patient_header.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class PatientRecipeFragment : BaseFragment() {
    private val viewModel by viewModel<PatientRecipeViewModel>()
    private lateinit var medicineAdapter: MedicineAdapter
    private var recipeDetail: RecipeDetail? = null
    private var popUp: CheckStockBottomSheet? = null
    private var user: User? = null
    private var medicine: RecipeMedicine? = null
    private var position: Int = 0

    override fun setView(): Int {
        return R.layout.fragment_patient_recipe
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        setupAdapter()
        arguments?.let {
            recipeDetail = it[PharmacistDashboardFragment.KEY_RECIPE_DETAIL] as RecipeDetail?
            displayData(recipeDetail)
        }

        confirmBtn.setOnClickListener {
            val popUpRecipe = ConfirmRecipeBottomSheet()
            popUpRecipe.setCallback {
                recipeDetail?.let {
                    viewModel.confirmRecipe(it.id)
                }
            }
            popUpRecipe.isCancelable = false
            popUpRecipe.show(childFragmentManager, "confirm_recipe")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_pharmacist_patient_recipe, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_edit) {
            requireActivity().startActivity<AddRecipeActivity>(
                Pair(
                    PatientDetailActivity.KEY_PATIENT,
                    user
                ),
                Pair(
                    PharmacistDashboardFragment.KEY_RECIPE_DETAIL,
                    recipeDetail
                )
            )
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@PatientRecipeFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
            })

            isError.observe(this@PatientRecipeFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            isMessage.observe(this@PatientRecipeFragment, Observer {
                (requireActivity() as MainActivity).showMessage(it)
            })

            profileResult.observe(this@PatientRecipeFragment, Observer {
                user = it
                it?.apply {
                    userAvatarLbl.showFromUrl(avatar)
                    nameLbl.text = name
                    nikLbl.text = nik
                }
            })

            confirmMedicineResult.observe(this@PatientRecipeFragment, Observer { result ->
                recipeDetail?.let {
                    popUp?.dismiss()
                    medicineAdapter.update(result.first.apply {
                        confirmed = true
                    }, result.second)
                    val data = medicineAdapter.all.filter {
                        !it.confirmed
                    }
                    confirmBtn.isEnabled = data.isEmpty()
                }
            })

            confirmRecipeResult.observe(this@PatientRecipeFragment, Observer {
                showMessage(it)
                findNavController().navigateUp()
            })

            medicineDetailResult.observe(this@PatientRecipeFragment, Observer {
                it?.let {
                    popUp = CheckStockBottomSheet(it)
                    popUp?.setCallback {
                        medicine?.let { m ->
                            viewModel.confirmMedicine(m, position)
                        }
                    }
                    popUp?.isCancelable = false
                    popUp?.show(childFragmentManager, "check_stock")
                }
            })
        }
    }

    private fun setupAdapter() {
        medicineAdapter = MedicineAdapter()
        medicineList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = medicineAdapter
        }
        medicineAdapter.itemCallback { v: View, item: RecipeMedicine, position: Int ->
            when (v.id) {
                R.id.checkStockBtn -> {
                    item.detail?.apply {
                        medicine = item
                        this@PatientRecipeFragment.position = position
                        viewModel.getMedicineDetail(id)
                    }
                }
            }
        }
    }

    private fun displayData(recipeDetail: RecipeDetail?) {
        recipeDetail?.apply {
            viewModel.getProfile(userId)
            medicineAdapter.addAll(recipeMedicine)
        }
    }
}