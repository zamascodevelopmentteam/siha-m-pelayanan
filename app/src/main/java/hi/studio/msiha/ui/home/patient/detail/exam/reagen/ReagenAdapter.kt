package hi.studio.msiha.ui.home.patient.detail.exam.reagen

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.Reagen
import kotlin.reflect.KProperty

class ReagenAdapter : BaseAdapter<Reagen>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_choose_reagen
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return ReagenViewHolder(context, view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: Reagen,
        callback: OnItemClick<Reagen>?
    ) {
        when (holder) {
            is ReagenViewHolder -> holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<Reagen>,
        new: MutableList<Reagen>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}