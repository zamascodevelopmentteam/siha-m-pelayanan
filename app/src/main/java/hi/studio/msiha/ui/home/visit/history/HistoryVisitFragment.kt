package hi.studio.msiha.ui.home.visit.history



import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.AppSIHA

import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.data.constant.KEY_PAGE_LIMIT
import hi.studio.msiha.data.constant.KEY_QR_RESULT
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.PatientFragment
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.visit.todayvisit.TodayVisitAdapter
import hi.studio.msiha.ui.home.visit.todayvisit.TodayVisitViewModel
import hi.studio.msiha.ui.home.visit.visitplan.VisitPlanAdapter
import hi.studio.msiha.ui.scan.ScanActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_today_visit.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class HistoryVisitFragment : BaseFragment() {


    private val viewModel by viewModel<HistoryVisitViewModel>()
    private lateinit var listAdapter: HistoryVisitAdapter
    private var limit = KEY_PAGE_LIMIT

    override fun setView(): Int {
        return R.layout.fragment_today_visit
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(false)
        requireActivity().actionBtn.setOnClickListener {
            KEY_ROLE_LAB
            findNavController().navigate(R.id.patientSearchFragment)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        getAllVisit()
        setupListAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_lab_patient, menu)
        val scan = menu.findItem(R.id.action_scan)
        scan.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_scan -> {
                var intent =  Intent(
                    this.activity,
                    ScanActivity::class.java
                ).putExtra(
                    ScanActivity.KEY_SCAN_TYPE,
                    ScanActivity.TYPE_VISIT
                )

                intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivityForResult(intent, PatientFragment.RC_PATIENT)
            }
            R.id.action_history ->{
                findNavController().navigate(R.id.historyVisitFragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getAllVisit(){
        viewModel.getHistoryVisit(0,limit)
    }

    private fun observeLiveData(){
        viewModel.apply {
            isLoading.observe(this@HistoryVisitFragment, Observer {
                refreshLayout.post {
                    refreshLayout.isRefreshing = it
                }
            })

            isError.observe(this@HistoryVisitFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getHistoryVisitResult.observe(this@HistoryVisitFragment, Observer {
                Log.d("testing","size : ${it.size}")
                if(it.isEmpty()){
                    Log.d("testing"," invisible size : ${it.size}")
                    rvVisit.visibility=View.INVISIBLE
                    emptyVisitLayout.visibility=View.VISIBLE
                }else{
                    Log.d("testing"," visible size : ${it.size}")
                    rvVisit.visibility=View.VISIBLE
                    emptyVisitLayout.visibility=View.GONE
                    listAdapter.addAll(it)
                }
            })

            createVisitResult.observe(viewLifecycleOwner,Observer{
                it?.let {
                    Log.d("testing", "test ${it.message}")
                    Log.d("test","testing ${it.data.ordinal}")
                    val bundle = bundleOf(PatientDetailFragment.KEY_VISIT_HISTORY to it.data)
                    //Bundle().putParcelable(PatientDetailFragment.KEY_VISIT_HISTORY, it.data)
                    if (it.data.visitType == "TEST") {
                        findNavController().navigate(R.id.visitDetailFragment2,bundle)
                    } else {
                        findNavController().navigate(R.id.visitDetailRRFragment2,bundle)
                    }
                }
            })
        }
    }

    private fun setupListAdapter(){
        listAdapter = HistoryVisitAdapter()
        rvVisit.apply {
            adapter=listAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            layoutManager = LinearLayoutManager(activity)
        }
//
//        listAdapter.itemCallback { _: View, visitHistory: VisitHistory, _: Int ->
//            val patient = visitHistory.patient
//            requireActivity().startActivity<PatientDetailActivity>(
//                Pair(
//                    PatientDetailActivity.KEY_PATIENT,
//                    patient
//                ),
//                Pair(
//                    PatientDetailActivity.KEY_MODE,
//                    PatientDetailActivity.MODE_VISIT
//                )
//            )
//        }

        rvVisit.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
                limit += KEY_PAGE_LIMIT
                viewModel.getHistoryVisit(0, limit)
            }
        })
    }


}

