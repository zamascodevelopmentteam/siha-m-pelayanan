package hi.studio.msiha.ui.auth.login

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetAuth
import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class LoginViewModel(private val getAuth: GetAuth) : BaseViewModel() {
    val loginResult = MutableLiveData<AuthResult>().toSingleEvent()

    fun login(nik: String, pass: String) {
        isLoading.postValue(true)
        getAuth.login(nik, pass) {
            when (it) {
                is Either.Left -> loginResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}