package hi.studio.msiha.ui.home.order

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment

class OrderFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_order
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }
}