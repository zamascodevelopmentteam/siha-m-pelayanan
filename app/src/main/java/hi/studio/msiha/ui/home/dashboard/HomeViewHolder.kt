package hi.studio.msiha.ui.home.dashboard

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_patient_home.view.*

class HomeViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<TodayVisit>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: TodayVisit,
        callback: BaseAdapter.OnItemClick<TodayVisit>?
    ) {
        item.user.apply {
            view.itemPatientAvatar.showFromUrl(avatar, context)
            view.itemPatientName.text = name
            view.itemPatientVisitCount.text = "Kunjungan ke ${item.ordinal}"
        }

        view.itemPatientDetailBtn.setOnClickListener {
            callback?.onClick(it, item, index)
        }
    }
}