package hi.studio.msiha.ui.home.patient.detail

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_HIV_RESULT
import hi.studio.msiha.data.constant.KEY_IMS_RESULT
import hi.studio.msiha.data.constant.KEY_QR_RESULT
import hi.studio.msiha.data.constant.KEY_VL_RESULT
import hi.studio.msiha.domain.model.ODHAVisit
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.LocalSharedViewModel
import hi.studio.msiha.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_visit_detail_rr.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class VisitDetailRRFragment : BaseFragment() {
    private var visitHistory: VisitHistory? = null
    private var patient: Patient? = null
    private val viewModel by viewModel<VisitDetailViewModel>()
    private val locaViewModel by sharedViewModel<LocalSharedViewModel>()
    private var visitDetail:VisitDetail?=null

    companion object {
        val odhaData = ODHAVisit()
        const val KEY_ORDINAL = "ordinal"
        const val KEY_PATIENT_ID = "patient_id"
        const val KEY_DATE = "date"
        const val KEY_VISIT_DETAIL = "visit_detail"
        const val KEY_ID = "user id"
        const val KEY_HOSPITAL_ID = "hospital id"
        const val KEY_EXAM_REQUEST = "exam_request"
        const val KEY_EXAM_CHECK = "exam_check"
        const val KEY_EXAM_RESULT = "exam_result"
        const val KEY_EXAM_TYPE = "exam_type"
        const val TYPE_HIV = "HIV"
        const val TYPE_IMS = "IMS"
        const val TYPE_VL = "VL"
        const val KEY_ID_VISIT = "id_visit"
        const val KEY_HIV_TEST="hiv_test"
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_detail_rr
    }

    @SuppressLint("SetTextI18n")
    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            patient = getParcelable(PatientDetailActivity.KEY_PATIENT)
            visitHistory = getParcelable(PatientDetailFragment.KEY_VISIT_HISTORY)
            visitHistory?.apply {
                ordinalLbl.text = "Data Kunjungan ke-${treatment?.ordinal}"
                patient?.let {
                    odhaData.ordinal = ordinal
                    odhaData.patientId = it.id
                    odhaData.date = visitDate
                }
                if(this.checkOutDate!=null){
                    saveBtn.visibility=View.INVISIBLE
                }else{
                    saveBtn.visibility=View.VISIBLE
                }
                viewModel.getVisitDetailByVisitId(this.id?:0)
            }
            if(visitDetail?.testHiv==null){
                hivExamBtn.text="Tambah"
            }else{
                hivExamBtn.text="Ubah"
            }
            if(visitDetail?.testIms==null){
                imsExamBtn.text="Tambah"
            }else{
                imsExamBtn.text="Ubah"
            }
            if(visitDetail?.testVL==null){
                vlcExamBtn.text="Tambah"
            }else{
                vlcExamBtn.text="Ubah"
            }
        }

        visitDataBtn.setOnClickListener {
            Hawk.put(KEY_VISIT_DETAIL,visitDetail)
            Log.d("cisitDetail","hei ${visitDetail?.treatment}")
            if(visitDetail?.treatment==null){
                showConfirmationDialog("Data Perawatan")
            }else {
                if (findNavController().currentDestination?.id == R.id.visitDetailRRFragment2) {
                    findNavController().navigate(R.id.visitEditFragment2, arguments)
                } else
                    findNavController().navigate(R.id.visitEditFragment, arguments)
            }
        }

//        overviewBtn.setOnClickListener {
//            if(findNavController().currentDestination?.id == R.id.visitDetailRRFragment2)
//                findNavController().navigate(R.id.overviewFragment2, arguments)
//            else
//                findNavController().navigate(R.id.overviewFragment, arguments)
//
//        }

        hivExamBtn.setOnClickListener {
            if(visitDetail?.testHiv==null){
                showConfirmationDialog("Test HIV")
            }else {
                Hawk.put(KEY_HIV_RESULT, visitDetail?.testHiv)
                if (findNavController().currentDestination?.id == R.id.visitDetailRRFragment2)
                    findNavController().navigate(R.id.HIVExamPreviewFragment, arguments)
                else
                    findNavController().navigate(
                        R.id.HIVExamFragmentRR,
                        updateArgument(ExamFragment.TYPE_HIV)
                    )
            }
        }

        imsExamBtn.setOnClickListener {
            if(visitDetail?.testIms==null){

                showConfirmationDialog("Test IMS")
            }else {
                Hawk.put(KEY_IMS_RESULT, visitDetail?.testIms)
                if (findNavController().currentDestination?.id != R.id.visitDetailRRFragment2)
                //findNavController().navigate(R.id.IMSExamPreviewFragment2, arguments)
                    findNavController().navigate(
                        R.id.IMSExamFragmentRR,
                        updateArgument(ExamFragment.TYPE_IMS)
                    )
                else
                    findNavController().navigate(R.id.IMSExamPreviewFragment, arguments)
            }
        }

        vlcExamBtn.setOnClickListener {
            if(visitDetail?.testVL==null){
               showConfirmationDialog("Test VL/CD4")
            }else {
                Hawk.put(KEY_VL_RESULT, visitDetail?.testVL)
                if (findNavController().currentDestination?.id != R.id.visitDetailRRFragment2)
                    findNavController().navigate(
                        R.id.VLCExamFragmentRR,
                        updateArgument(ExamFragment.TYPE_VL)
                    )
                else
                    findNavController().navigate(R.id.VLCExamFragment, arguments)
            }
        }

        medicineLeftBtn.setOnClickListener {
            if(visitDetail?.treatment==null){
                context?.let { it1 ->
                    MaterialDialog(it1).show {
                        title(text="Data perawatan tidak ditemukan")
                        message(text="Harap mengisi data perawatan terlebih dahulu,sebelum mengakses sisa obat")
                        positiveButton(text="Kembali") { dialog ->
                            this.dismiss()
                        }
                    }
                }
            }else {
                if (findNavController().currentDestination?.id == R.id.visitDetailRRFragment2) {
                    arguments?.apply {
                        putParcelable(KEY_VISIT_DETAIL, visitDetail)
                    }
                    findNavController().navigate(R.id.medicineLeftFragment2, arguments)
                } else
                    findNavController().navigate(R.id.medicineLeftFragment, arguments)
            }
        }

        recipeBtn.setOnClickListener {
            if(visitDetail?.treatment==null){
                context?.let { it1 ->
                    MaterialDialog(it1).show {
                        title(text="Data perawatan tidak ditemukan")
                        message(text="Harap mengisi data perawatan terlebih dahulu,sebelum membuat resep")
                        positiveButton(text="Kembali") { dialog ->
                            this.dismiss()
                        }
                    }
                }
            }else {
                val bundle = bundleOf(KEY_VISIT_DETAIL to visitDetail)
                if (findNavController().currentDestination?.id != R.id.visitDetailRRFragment2)
                    if (visitHistory?.checkOutDate == null) {
                        findNavController().navigate(R.id.addARVFragment, arguments?.apply {
                            putParcelable(PatientDetailFragment.KEY_VISIT_HISTORY, visitHistory)
                            putParcelable(KEY_VISIT_DETAIL, visitDetail)
                        })
                    } else {
                        findNavController().navigate(R.id.recipeRRFragment, arguments?.apply {
                            putParcelable(PatientDetailFragment.KEY_VISIT_HISTORY, visitHistory)
                            putParcelable(KEY_VISIT_DETAIL, visitDetail)
                        })
                    }
                else
                    findNavController().navigate(R.id.recipeRRFragment, bundle)
            }
        }

        saveBtn.setOnClickListener {
            //Timber.d(odhaData.toString())
            observeEndVisit()
            viewModel.endVisit(visitHistory?.id?:0)
        }

        //checkDataState()

        if(Hawk.contains(KEY_QR_RESULT)) Hawk.delete(KEY_QR_RESULT)
    }


    private fun showConfirmationDialog(title:String){
        context?.let { it1 ->
            MaterialDialog(it1)
                .title(text="Apakah anda yakin ingin menambah $title")
                .positiveButton(text="Iya") {
                    when(title){
                        "Data Perawatan" -> visitHistory?.id?.let { it -> viewModel.createTreatment(it) }
                        "Test HIV"-> viewModel.createHivExam(visitHistory?.id?:0)
                        "Test IMS"-> viewModel.createImsExam(visitHistory?.id?:0)
                        "Test VL/CD4"-> viewModel.createVlcExam(visitHistory?.id?:0)
                    }

                }
                .negativeButton(text="Tidak"){
                    it.dismiss()
                }
                .show()
        }
    }
    private fun updateArgument(type: String?): Bundle? {
        val bundle = Bundle().apply {
            patient?.apply {
                putInt(ExamFragment.KEY_ID, id ?: 0)
                putInt(ExamFragment.KEY_HOSPITAL_ID, upkId ?: 0)
            }
            visitHistory?.apply {
                id?.let {
                    putInt(ExamFragment.KEY_ID_VISIT, id)
                }
            }
//            putLong(ExamFragment.KEY_EXAM_REQUEST, examRequestedCheckDateLbl.tag as Long?:0L)
//            putLong(ExamFragment.KEY_EXAM_CHECK, examCheckDateLbl.tag as Long?:0L)
//            putLong(ExamFragment.KEY_EXAM_RESULT, examResultDateLbl.tag as Long?:0L)
            putString(ExamFragment.KEY_EXAM_TYPE, type)
        }
        return arguments?.apply {
            putBundle(ExamFragment.KEY_EXAM_DATA, bundle)
        }
    }

    override fun onResume() {
        super.onResume()
        if(visitHistory!=null){
            viewModel.getVisitDetailByVisitId(visitHistory!!.id?:0)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeEndVisit(){
        viewModel.endVisitResult.observe(viewLifecycleOwner,Observer{
            viewModel.endVisitResult.removeObservers(viewLifecycleOwner)
            findNavController().navigateUp()
        })
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@VisitDetailRRFragment, Observer {
                try{
                    (requireActivity() as PatientDetailActivity).showLoading(it)
                }catch (e:Exception){
                    (requireActivity() as MainActivity).showLoading(it)
                }
            })

            isError.observe(this@VisitDetailRRFragment, Observer {
                try{
                    (requireActivity() as PatientDetailActivity).showError(it)
                }catch (e:Exception){
                    (requireActivity() as MainActivity).showError(it)
                }
            })

            isMessage.observe(this@VisitDetailRRFragment, Observer {
                try{
                    (requireActivity() as PatientDetailActivity).showMessage(it)
                }catch (e:Exception){
                    (requireActivity() as MainActivity).showMessage(it)
                }
            })

            visitCompleteResult.observe(this@VisitDetailRRFragment, Observer {
                findNavController().navigateUp()
            })

            getVisitDetailResult.observe(viewLifecycleOwner,Observer{
                visitDetail=it

                Hawk.put(KEY_VISIT_DETAIL,it)
                if(it.treatment!=null && it.checkOutDate==null){
                    visitDataBtn.text = "Ubah"
                    visitDataBtn.setBackgroundResource(R.drawable.button_bg_primary)
                    visitDataBtn.setTextColor(Color.WHITE)
                }else if(it.treatment!=null && it.checkOutDate!=null){
                    visitDataBtn.text = "Lihat"
                }
                if(visitDetail?.testHiv==null){
                    hivExamBtn.text="Tambah"
                }else{
                    hivExamBtn.text="Ubah"
                }
                if(visitDetail?.testIms==null){
                    imsExamBtn.text="Tambah"
                }else{
                    imsExamBtn.text="Ubah"
                }
                if(visitDetail?.testVL==null){
                    vlcExamBtn.text="Tambah"
                }else{
                    vlcExamBtn.text="Ubah"
                }
            })

            createTreatmentResult.observe(viewLifecycleOwner,Observer{
                //viewModel.getVisitDetailByVisitId(visitHistory?.id?:0)
                if (findNavController().currentDestination?.id == R.id.visitDetailRRFragment2) {
                    findNavController().navigate(R.id.visitEditFragment2, arguments)
                } else
                    findNavController().navigate(R.id.visitEditFragment, arguments)
            })

            createHivExamResult.observe(viewLifecycleOwner,Observer{
                Hawk.put(KEY_HIV_RESULT,visitDetail?.testHiv)
                if(findNavController().currentDestination?.id == R.id.visitDetailRRFragment2)
                    findNavController().navigate(R.id.HIVExamPreviewFragment, arguments)
                else
                    findNavController().navigate(R.id.HIVExamFragmentRR, updateArgument(ExamFragment.TYPE_HIV))
            })

            createImsExamResult.observe(viewLifecycleOwner,Observer{
                Hawk.put(KEY_IMS_RESULT, visitDetail?.testIms)
                if (findNavController().currentDestination?.id != R.id.visitDetailRRFragment2)
                //findNavController().navigate(R.id.IMSExamPreviewFragment2, arguments)
                    findNavController().navigate(
                        R.id.IMSExamFragmentRR,
                        updateArgument(ExamFragment.TYPE_IMS)
                    )
                else
                    findNavController().navigate(R.id.IMSExamPreviewFragment, arguments)
            })

            createVlcExamResult.observe(viewLifecycleOwner,Observer{
                Hawk.put(KEY_VL_RESULT,visitDetail?.testVL)
                if(findNavController().currentDestination?.id != R.id.visitDetailRRFragment2)
                //findNavController().navigate(R.id.VLExamPreviewFragment2, arguments)
                    findNavController().navigate(R.id.VLCExamFragmentRR, updateArgument(ExamFragment.TYPE_VL))
                else
                    findNavController().navigate(R.id.VLCExamFragment, arguments)
            })
        }

        locaViewModel.apply {
            getOverview().observe(this@VisitDetailRRFragment, Observer {
                it?.apply {
                    odhaData.noRegNas = noRegNas
                    odhaData.treatementStartDate = treatementStartDate
                }
                //checkDataState()
            })

            getVisitData().observe(this@VisitDetailRRFragment, Observer {
                it?.apply {
                    odhaData.tglKonfirmasiHivPos = tglKonfirmasiHivPos
                    odhaData.tglKunjungan = tglKunjungan
                    odhaData.tglRujukMasuk = tglRujukMasuk
                    odhaData.kelompokPopulasi = kelompokPopulasi
                    odhaData.statusTb = statusTb
                    odhaData.tglPengobatanTb = tglPengobatanTb
                    odhaData.statusFungsional = statusFungsional
                    odhaData.stadiumKlinis = stadiumKlinis
                    odhaData.ppk = ppk
                    odhaData.tglPemberianPpk = tglPemberianPpk
                    odhaData.statusTbTpt = statusTbTpt
                    odhaData.tglPemberianObat = tglPemberianObat
                    odhaData.lsmPenjangkauId = lsmPenjangkauId
                    odhaData.notifikasiPasangan = notifikasiPasangan
                }
                //checkDataState()
            })
        }
    }

//    private fun checkDataState() {
//        if (odhaData.noRegNas?.isNotBlank() == true) {
//            overviewBtn.text = "Ubah"
//            overviewBtn.setBackgroundResource(R.drawable.button_bg_primary)
//            overviewBtn.setTextColor(Color.WHITE)
//        } else if (odhaData.tglKonfirmasiHivPos?.isNotBlank() == true) {
////            visitDataBtn.text = "Ubah"
////            visitDataBtn.setBackgroundResource(R.drawable.button_bg_primary)
////            visitDataBtn.setTextColor(Color.WHITE)
//        }
//    }
}