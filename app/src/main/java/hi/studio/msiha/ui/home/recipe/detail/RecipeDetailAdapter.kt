package hi.studio.msiha.ui.home.recipe.detail

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.RecipeMedicine
import kotlin.reflect.KProperty

class RecipeDetailAdapter : BaseAdapter<RecipeMedicine>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_recipe_medicine_label_only
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return RecipeDetailViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: RecipeMedicine,
        callback: OnItemClick<RecipeMedicine>?
    ) {
        if (holder is RecipeDetailViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<RecipeMedicine>,
        new: MutableList<RecipeMedicine>
    ) {
        autoNotify(old, new) { o, n ->
            o.medicineId == n.medicineId
        }
    }
}