package hi.studio.msiha.ui.home.patient.detail

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.*
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.KEY_PATIENT
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.MODE_PATIENT
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.MODE_VISIT
import hi.studio.msiha.utils.extention.showFromUrl
import hi.studio.msiha.utils.extention.toFormat
import kotlinx.android.synthetic.main.fragment_patient_detail.*
import kotlinx.android.synthetic.main.item_patient.*
import kotlinx.android.synthetic.main.layout_lab_visits.*
import kotlinx.android.synthetic.main.layout_rr_patient.*
import kotlinx.android.synthetic.main.layout_rr_visit.*
import kotlinx.android.synthetic.main.layout_visit_history.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class PatientDetailFragment : BaseFragment() {
    private val viewModel by viewModel<PatientDetailViewModel>()
    private lateinit var visitAdapter: VisitHistoryAdapter
    private var patient: Patient? = null
    private var isActive=true
    private var mode="HISTORY"

    companion object {
        const val KEY_VISIT_HISTORY = "visit_history"
    }

    override fun setView(): Int {
        return R.layout.fragment_patient_detail
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        clearTestHistory()
        Log.d("test patient","call detail")
        (activity as? AppCompatActivity)?.supportActionBar?.show()
        arguments?.apply {
            patient = getParcelable(KEY_PATIENT)
            viewModel.getPatientByNik(patient?.nik!!)
            patient?.apply {
                Timber.d(patient.toString())
                nameLbl.text = name
                nikLbl.text = nik
                patientGenderLbl.text=gender
                userAvatarImg.showFromUrl(null)
                if(patient!!.nik!=null && AppSIHA.instance.getUser()?.role== KEY_ROLE_RR) viewModel.getPatientByNik(patient!!.nik!!)
                val status = when (statusPatient) {
//                    TYPE_HIV_POSITIF -> "HIV Positif"
//                    TYPE_ODHA -> "Pasien ODHA"
//                    else -> "HIV Negatif"
                    TYPE_HIV_POSITIF -> "ODHA"
                    TYPE_ODHA -> "ODHA Dalam Pengobatan"
                    TYPE_UNKNOWN->"Belum Tahu"
                    else -> "HIV Negatif"
                }
                hivStatusLbl.text = "Status: $status"
                if (statusPatient == TYPE_ODHA) {
                    odhaExamLayout.visibility = View.VISIBLE
                    odhaExamLayout2.visibility = View.VISIBLE
                    isActive=this.user?.isActive?:true
                    odhaBtn.text="verifikasi"
                    odhaBtn.visibility=View.VISIBLE
                    if(odhaBtn!=null && isActive){
                        odhaBtn.visibility=View.GONE
                    }
                } else {
                    odhaExamLayout.visibility = View.GONE
                    odhaExamLayout2.visibility = View.GONE
                }
            }
            when (AppSIHA.instance.getUser()?.role) {
                KEY_ROLE_RR -> {
                    when (getInt(PatientDetailActivity.KEY_MODE)) {
                        MODE_PATIENT -> initRRPatient()
                        MODE_VISIT -> {
                            initRRVisit()
                            mode="TODAY"
                        }
                    }
                }
                KEY_ROLE_LAB -> {
                    mode="TODAY"
                    initLabPatient()
                }
                KEY_ROLE_PHARMACIST -> {
                    mode="ALL"
                    initPharmacistPatient()
                }
            }
        }

        setupAdapter()



        detailBtn.setOnClickListener {
            findNavController().navigate(R.id.patientDataFragment, arguments)
        }

        odhaBtn.setOnClickListener {
            if(patient?.user?.isActive==false && patient?.statusPatient== TYPE_ODHA){
                patient?.id?.let { it1 -> viewModel.getOtp(it1) }

            }else
                findNavController().navigate(R.id.ODHAFragment, arguments)
        }

        addTestBtn.setOnClickListener{
         // Toast.makeText(context,"Test create",Toast.LENGTH_SHORT).show()
            context?.let { it1 ->
                MaterialDialog(it1)
                    .title(text="Apakah anda yakin ingin menambah kunjungan Lab?")
                    .positiveButton(text="Iya") {
                        when (patient?.statusPatient) {
                            TYPE_ODHA -> {
                                patient?.id?.let {
                                    val calendar = Calendar.getInstance()
                                    val date = calendar.time.toFormat("yyyy-MM-dd")
                                    viewModel.createVisitByPatientId(it)
                                    //viewModel.createVisitOdha(visitAdapter.itemCount + 1, it, date)
                                }
                            }
                            else -> {
                                patient?.id?.let { it1 -> viewModel.createVisitByPatientId(it1) }
                                //                    findNavController().navigate(R.id.addVisitFragment, arguments?.apply {
                                //                        putInt(
                                //                            AddVisitFragment.KEY_ORDINAL,
                                //                            visitAdapter.itemCount
                                //                        )
                                //                    })
                            }
                        }
                    }
                    .negativeButton(text="Tidak"){
                        it.dismiss()
                    }
                    .show()
            }
        }

        addVisitBtn.setOnClickListener {
            Timber.d(patient.toString())
            context?.let { it1 ->
                MaterialDialog(it1)
                    .title(text="Apakah anda yakin ingin menambah kunjungan")
                    .positiveButton(text="Iya") {
                        when (patient?.statusPatient) {
                            TYPE_ODHA -> {
                                patient?.id?.let {
                                    val calendar = Calendar.getInstance()
                                    val date = calendar.time.toFormat("yyyy-MM-dd")
                                    viewModel.createVisitByPatientId(it)
                                    //viewModel.createVisitOdha(visitAdapter.itemCount + 1, it, date)
                                }
                            }
                            else -> {
                                patient?.id?.let { it1 -> viewModel.createVisitByPatientId(it1) }
            //                    findNavController().navigate(R.id.addVisitFragment, arguments?.apply {
            //                        putInt(
            //                            AddVisitFragment.KEY_ORDINAL,
            //                            visitAdapter.itemCount
            //                        )
            //                    })
                            }
                        }
                    }
                    .negativeButton(text="Tidak"){
                      it.dismiss()
                    }
                    .show()
            }
//
        }

        hivExamBtn.setOnClickListener(hivClickListener)
        hivExam2Btn.setOnClickListener(hivClickListener)
        imsExamBtn.setOnClickListener(imsClickListener)
        imsExam2Btn.setOnClickListener(imsClickListener)
        vlcExamBtn.setOnClickListener(vlClickListener)
        vlcExam2Btn.setOnClickListener(vlClickListener)

//        overviewBtn.setOnClickListener {
//            findNavController().navigate(R.id.overviewFragment, arguments)
//        }
    }

    private fun clearTestHistory(){
        if(Hawk.contains(KEY_IMS_RESULT)) Hawk.delete(KEY_IMS_RESULT)
        if(Hawk.contains(KEY_HIV_RESULT)) Hawk.delete(KEY_HIV_RESULT)
        if(Hawk.contains(KEY_VL_RESULT)) Hawk.delete(KEY_VL_RESULT)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        if(Hawk.contains(KEY_MEDICINE_INS)) {
            Log.d("key_medicine","delete coy")
            Hawk.delete(KEY_MEDICINE_INS)
        }else{
            Log.d("key_medicine","no delete coy")
        }
    }

    private val hivClickListener = View.OnClickListener {
        findNavController().navigate(R.id.examHistoryFragment, arguments?.apply {
            putString("exam", "HIV")
            //putParcelable(KEY_PATIENT,patient)
        })
    }

    private val imsClickListener = View.OnClickListener {
        findNavController().navigate(R.id.examHistoryFragment, arguments?.apply {
            putString("exam", "IMS")
        })
    }

    private val vlClickListener = View.OnClickListener {
        findNavController().navigate(R.id.examHistoryFragment, arguments?.apply {
            putString("exam", "VLC")
        })
    }

    private fun initRRPatient() {
        detailBtn.visibility = View.VISIBLE
        //layoutRRPatient.visibility = View.VISIBLE
        layoutRRPatient.visibility = View.GONE
        layoutRRVisit.visibility = View.VISIBLE
        layoutLabVisits.visibility = View.GONE
        layoutVisitHistory.visibility = View.VISIBLE
        odhaBtn.visibility = View.VISIBLE
        patient?.let {

            Log.d("patient","patient status :${it.statusPatient}")
            Log.d("patient","patient active? :${it.user?.isActive}")
            if (it.statusPatient == TYPE_HIV_POSITIF || (it.statusPatient==TYPE_ODHA && !isActive))   {
                odhaBtn.visibility = View.VISIBLE
            } else {
                odhaBtn.visibility = View.GONE
            }
            viewModel.getVisitHistories(it.id ?: 0,mode)
        }
    }

    private fun initRRVisit() {
        detailBtn.visibility = View.VISIBLE
        layoutRRPatient.visibility = View.GONE
        layoutRRVisit.visibility = View.VISIBLE
        layoutLabVisits.visibility = View.GONE
        layoutVisitHistory.visibility = View.VISIBLE
        patient?.let {
            if (it.statusPatient == TYPE_HIV_POSITIF || (it.statusPatient==TYPE_ODHA && it.user?.isActive==false))   {
                odhaBtn.visibility = View.VISIBLE
            } else {
                odhaBtn.visibility = View.GONE
            }
            viewModel.getVisitHistories(it.id ?: 0,mode)
        }
    }

    private fun initLabPatient() {
        detailBtn.visibility = View.GONE
        layoutRRPatient.visibility = View.VISIBLE
        layoutRRVisit.visibility = View.GONE
        layoutLabVisits.visibility = View.VISIBLE
        layoutVisitHistory.visibility = View.VISIBLE
        patient?.let {
            viewModel.getVisitHistories(it.id ?: 0,mode)
        }
    }

    private fun initPharmacistPatient() {
        detailBtn.visibility = View.GONE
        layoutRRPatient.visibility = View.GONE
        layoutRRVisit.visibility = View.GONE
        layoutLabVisits.visibility = View.GONE
        layoutVisitHistory.visibility = View.VISIBLE
        patient?.let {
            viewModel.getVisitHistories(it.id ?: 0,mode)
        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@PatientDetailFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@PatientDetailFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            visitHistoriesResult.observe(this@PatientDetailFragment, Observer {
                it?.let {
                    Log.d("testing","add all item")
                    visitAdapter.addAll(it)
                    if (it.isEmpty()) {
                        emptyVisitLayout.visibility = View.VISIBLE
                        visitList.visibility=View.GONE
                    }else{
                        emptyVisitLayout.visibility=View.GONE
                        visitList.visibility=View.VISIBLE
                    }
                } ?: run {
                    emptyVisitLayout.visibility = View.VISIBLE
                }
            })

            visitResult.observe(this@PatientDetailFragment, Observer {
                viewModel.getVisitHistories(patient?.id ?: 0,mode)
            })

            createVisitResult.observe(viewLifecycleOwner,Observer{
                viewModel.getVisitHistories(patient?.id ?: 0,mode)
            })

            getPatientByNikResult.observe(this@PatientDetailFragment,Observer{
                patient = it
                if(odhaBtn!=null){
                    if (it.statusPatient == TYPE_HIV_POSITIF || (it.statusPatient==TYPE_ODHA && it.user?.isActive==false))   {
                        odhaBtn.visibility = View.VISIBLE
                    } else {
                        odhaBtn.visibility = View.GONE
                    }
                }
                Hawk.put(KEY_PATIENT,it)
            })

            getOtpResult.observe(viewLifecycleOwner,Observer{
                findNavController().navigate(R.id.otpFragment, arguments)
            })
        }
    }

    private fun setupAdapter() {
        visitAdapter = VisitHistoryAdapter(patient?.statusPatient== TYPE_ODHA?:false)
        visitList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = visitAdapter
        }
        visitAdapter.itemCallback { v: View, item: VisitHistory, _: Int ->
            when (v.id) {
                R.id.visitPreviewBtn -> {
                    Hawk.put(KEY_VISIT_HISTORY,item)
                    when (AppSIHA.instance.getUser()?.role) {
                        KEY_ROLE_RR -> {
                            //if (patient?.statusPatient == TYPE_ODHA) {
                            if (item.visitType=="TREATMENT") {
                                findNavController().navigate(
                                    R.id.visitDetailRRFragment,
                                    arguments?.apply {
                                        putParcelable(KEY_VISIT_HISTORY, item)
                                    })
                            } else {
                                findNavController().navigate(
                                    R.id.visitDetailFragment,
                                    arguments?.apply {
                                        putParcelable(KEY_VISIT_HISTORY, item)
                                    })
                            }
                        }
                        KEY_ROLE_LAB -> {
                            if (item.checkOutDate == null) {
                                findNavController().navigate(R.id.examFragment, arguments?.apply {
                                    putParcelable(KEY_VISIT_HISTORY, item)
                                })
                            } else {
                                findNavController().navigate(
                                    R.id.visitDetailFragment,
                                    arguments?.apply {
                                        putParcelable(KEY_VISIT_HISTORY, item)
                                    })
                            }
                        }
                        KEY_ROLE_PHARMACIST -> {
                            if(item.checkOutDate ==null) {
                                findNavController().navigate(R.id.addARVFragment, arguments?.apply {
                                    putParcelable(KEY_VISIT_HISTORY, item)
                                })
                            }else{
                                findNavController().navigate(R.id.recipeRRFragment, arguments?.apply {
                                    putParcelable(KEY_VISIT_HISTORY, item)
                                })
                            }
                        }
                    }
                }
            }
        }
    }
}