package hi.studio.msiha.ui.home.pharmacist.stock.addmedicine


import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineStocks
import hi.studio.msiha.utils.extention.getSelectedContent
import kotlinx.android.synthetic.main.fragment_add_medicine.*
import org.jetbrains.anko.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


/**
 * A simple [Fragment] subclass.
 */
class AddMedicineFragment : BaseFragment(),
    PopupMenu.OnMenuItemClickListener {

    private var addType: String = "CURE"
    private var hospitalId = 0

    private val viewModel by viewModel<AddMedicineViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_add_medicine
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        val user = AppSIHA.instance.getUser()
        user?.let {
            hospitalId = user.upkId ?: 0
        }
        setupSwitchFormButton()
        setupUI()
        setupBackButton()
        setupSubmitButton()

    }

    override fun onResume() {
        super.onResume()
        observeViewModel()
    }

    private fun setupBackButton() {
        imgBackButton.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun setupSubmitButton() {

        buttonAddMedicine.setOnClickListener {
            try {
                viewModel.createMedicine(
                    Medicine(
                        id = 0,
                        name = editTextMedicineName.text.toString(),
                        codeName = editTextMedicineKode.text.toString(),
                        type = if (addType == "CURE") {
                            editTextMedicineType.text.toString()
                        } else {
                            spinnerReagen.getSelectedContent()
                        },
                        picture = "",
                        medicineType = addType,
                        ingredients = editTextKandungan.text.toString(),
                        usage = editTextPenggunaan.text.toString(),
                        medicineStocks = MedicineStocks(
                            cupboardCode = editTextCabinetCode.text.toString(),
                            stock = editTextStock.text.toString().toInt(),
                            hospitalId = hospitalId
                        ),
                        createdAt = "",
                        updatedAt = ""
                    )
                )
            } catch (e: NumberFormatException) {
                Toast.makeText(context, "Stock harus angka", Toast.LENGTH_SHORT).show()
                findNavController().navigateUp()
            }
        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@AddMedicineFragment, Observer {

            })

            createMedicineResult.observe(this@AddMedicineFragment, Observer {
                it?.let {
                    requireActivity().toast("Success")
                    findNavController().navigateUp()
                }
            })
        }
    }

    private fun setupUI() {
        val bundle = this.arguments
        val mode = bundle?.getInt("mode")
        addType = if (mode == 0) {
            "CURE"
        } else {
            "REAGEN"
        }
        initFormView(addType)
    }

    private fun setupSwitchFormButton() {
        imgSwitch.setOnClickListener {
            PopupMenu(activity, textToolbarTitle).apply {
                setOnMenuItemClickListener(this@AddMedicineFragment)
                if (addType == "CURE") {
                    inflate(R.menu.menu_item_add_medicine)
                } else {
                    inflate(R.menu.menu_item_add_reagen)
                }
                show()
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.addMedicine -> {
                switchView(addType)
                true
            }
            else -> false
        }
    }

    private fun switchView(oldView: String) {
        addType = if (oldView == "CURE") {
            "REAGEN"
        } else {
            "CURE"
        }
        initFormView(addType)
    }

    private fun initFormView(mode: String) {
        if (mode == "CURE") {
            textToolbarTitle.text = "Tambahkan Obat"
            editTextMedicineName.hint = "Nama Obat"
            editTextMedicineKode.hint = "Kode Obat"
            editTextMedicineType.visibility = View.VISIBLE
            spinnerReagen.visibility = View.GONE
            textStok.text = "Stok Obat"
            textDetailObat.text = "Detail Obat"
            editTextKandungan.hint = "Kandungan Obat"
        } else {
            Timber.d("this is no modicine $mode")
            textToolbarTitle.text = "Tambahkan Reagen"
            editTextMedicineName.hint = "Nama Reagen"
            editTextMedicineKode.hint = "Kode Reagen"
            editTextMedicineType.visibility = View.GONE
            spinnerReagen.visibility = View.VISIBLE
            textStok.text = "Stok Reagen"
            textDetailObat.text = "Detail Reagen"
            editTextKandungan.hint = "Kandungan Reagen"
        }
    }
}
