package hi.studio.msiha.ui.home.patient.detail.exam.history

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import hi.studio.msiha.ui.home.patient.detail.exam.VlExamListViewHolder
import kotlin.reflect.KProperty


class VlcExamHistoryAdapter:
    BaseAdapter<VlTestHistory>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_exam_history
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return VlExamListViewHolder(context, view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: VlTestHistory,
        callback: OnItemClick<VlTestHistory>?
    ) {
        if (holder is VlExamListViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<VlTestHistory>,
        new: MutableList<VlTestHistory>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}