package hi.studio.msiha.ui.home.stock

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment

class StockFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_stock
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
    }
}