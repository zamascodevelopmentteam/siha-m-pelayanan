package hi.studio.msiha.ui.home.pharmacist.order.createorder

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_create_order.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateOrderFragment : BaseFragment(),
PopupMenu.OnMenuItemClickListener{

    private var addType="CURE"

    private val viewModel by viewModel<CreateOrderViewModel>()


    override fun setView(): Int {
        return R.layout.fragment_create_order
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        loadMedicine(addType)
        observeLiveData()
        handleSwitchButton()
        textToolbarTitle.text = "Pesan Obat"
        imgBackButton.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun handleSwitchButton() {
        imgSwitch.setOnClickListener {
            PopupMenu(activity, textToolbarTitle).apply {
                setOnMenuItemClickListener(this@CreateOrderFragment)
                if(addType=="CURE"){
                    inflate(R.menu.menu_item_pesan_reagen)
                }else{
                    inflate(R.menu.menu_item_pesan_obat)
                }
                show()
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.orderMedicine -> {
                switchView(addType)
                true
            }
            else -> false
        }
    }

    private fun switchView(oldView:String){
        if(oldView=="CURE"){
            addType= "REAGEN"
            textToolbarTitle.text = "Pesan Reagen"
        }else{
            addType="CURE"
            textToolbarTitle.text = "Pesan Obat"
        }

        loadMedicine(addType)
    }

    private fun loadMedicine(type:String){
        viewModel.getListMedicine(type)
    }


    private fun observeLiveData() {
        viewModel.apply {
            isLoading.observe(this@CreateOrderFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
            })

            isError.observe(this@CreateOrderFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getMedicineListResult.observe(this@CreateOrderFragment, Observer {
                if (it.isNotEmpty()) {
                    recyclerMedicine.visibility = View.VISIBLE
                    layoutEmptyState.visibility = View.INVISIBLE
                } else {
                    layoutEmptyState.visibility = View.VISIBLE
                    recyclerMedicine.visibility = View.INVISIBLE
                }
            })
        }
    }
}
