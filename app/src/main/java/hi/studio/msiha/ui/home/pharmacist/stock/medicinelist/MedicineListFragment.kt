package hi.studio.msiha.ui.home.pharmacist.stock.medicinelist


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.pharmacist.stock.AdapterMedicine
import kotlinx.android.synthetic.main.fragment_medicine_list.*
import kotlinx.android.synthetic.main.fragment_medicine_list.layoutEmptyState
import kotlinx.android.synthetic.main.fragment_reagen_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * A simple [Fragment] subclass.
 */
class MedicineListFragment : BaseFragment() {

    private val viewModel by viewModel<MedicineListViewModel>()
    private lateinit var medicineListAdapter: AdapterMedicine

    override fun setView(): Int {
        return R.layout.fragment_medicine_list
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        viewModel.getListMedicine()
        observeLiveData()
    }

    private fun observeLiveData(){
        viewModel.apply {
            isLoading.observe(this@MedicineListFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
                layoutEmptyState.visibility=View.INVISIBLE
            })

            isError.observe(this@MedicineListFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getMedicineListResult.observe(this@MedicineListFragment, Observer {
                if(it.isNotEmpty()) {
                    recyclerMedicine.visibility=View.VISIBLE
                    layoutEmptyState.visibility=View.INVISIBLE
                    medicineListAdapter.addAll(it)
                }else{
                    layoutEmptyState.visibility=View.VISIBLE
                    recyclerMedicine.visibility=View.INVISIBLE
                }
            })
        }
    }

    private fun setupAdapter() {
        medicineListAdapter = AdapterMedicine()
        recyclerMedicine.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = medicineListAdapter
            addItemDecoration(DividerItemDecoration(
                recyclerMedicine.context,
                RecyclerView.VERTICAL
            ))
        }
        medicineListAdapter.itemCallback { _: View, item: ArvStock, _: Int ->
//            val bundle = Bundle()
//            bundle.putParcelable("medicine",item)
//            findNavController().navigate(R.id.medicineDetailFragment,bundle)
        }
        recyclerMedicine.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
            }
        })
    }




}
