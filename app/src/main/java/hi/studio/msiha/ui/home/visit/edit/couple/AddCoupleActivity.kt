package hi.studio.msiha.ui.home.visit.edit.couple

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.afollestad.vvalidator.form
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import kotlinx.android.synthetic.main.activity_add_couple.*
import kotlinx.android.synthetic.main.default_toolbar.*

class AddCoupleActivity : BaseActivity() {
    companion object {
        const val KEY_DATA = "data"
    }

    override fun setView(): Int {
        return R.layout.activity_add_couple
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        supportActionBar?.apply {
            title = "Tambahkan Pasangan"
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
        validateForm()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun validateForm() {
        form {
            input(nameTxt, "name") {
                isNotEmpty().description(R.string.empty_field)
            }
            input(ageTxt, "age") {
                isNotEmpty().description(R.string.empty_field)
                isNumber().greaterThan(0).description("Umur harus lebih dari 0")
            }
            val genderArray = getResources().getStringArray(R.array.gender)
            val relationshipArray = getResources().getStringArray(R.array.relationship)
            submitWith(confirmBtn) {
                if (it.success()) {
                    setResult(
                        Activity.RESULT_OK,
                        Intent().putExtra(
                            KEY_DATA,
                            ODHAVisitRequest.Couple(
                                0,
                                it["name"]?.asString() ?: "",
                                it["age"]?.asInt() ?: 0,
                                genderArray[it["gender"]?.asInt() ?: 0],
                                relationshipSpn.selectedItem as String
                            )
                        )
                    )
                    finish()
                }
            }
        }
    }
}