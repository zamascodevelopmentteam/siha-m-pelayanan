package hi.studio.msiha.ui.home.patient.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.data.datasource.remote.request.ODHARequest
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.ui.home.visit.edit.form.OtpFragment
import kotlinx.android.synthetic.main.fragment_odha_activation.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ODHAFragment : BaseFragment() {
    private val viewModel by viewModel<ODHAViewModel>()
    private var patient: Patient? = null

    companion object {
        const val KEY_PERSONAL_EMAIL = "personal_email"
        const val KEY_PMO_NAME = "pmo_name"
        const val KEY_PMO_RELATION = "pmo_relation"
        const val KEY_PMO_PHONE = "pmo_phone"
        const val KEY_PMO_EMAIL = "pmo_email"
    }

    override fun setView(): Int {
        return R.layout.fragment_odha_activation
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            patient = getParcelable<Patient>(PatientDetailActivity.KEY_PATIENT)
        }

        emailTxt.setText(patient?.phone)
        pmoNameTxt.setText(patient?.namaPmo)
        pmoRelationShipTXt.setText(patient?.hubunganPmo)
        pmoPhoneTxt.setText(patient?.noHpPmo)

        saveBtn.setOnClickListener {
            val result = validate()
            if (result.success()) {
                patient?.let {
                    viewModel.updateToODHA(
                        it.id ?: 0, ODHARequest(
                            TYPE_ODHA,
                            emailTxt.text.toString(),
                            emailTxt.text.toString(),
                            pmoNameTxt.text.toString(),
                            pmoRelationShipTXt.text.toString(),
                            pmoPhoneTxt.text.toString()
                        )
                    )
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@ODHAFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@ODHAFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            odhaResult.observe(this@ODHAFragment, Observer {
//                patient?.id?.let {
//                    getOtp(it)
//                }
                val bundle = Bundle().apply {
                    putParcelable(OtpFragment.KEY_PATIENT, patient)
                    putInt(OtpFragment.KEY_OTP_ID, 10)
                }
                findNavController().navigate(R.id.otpFragment, bundle)
            })

//            otpResult.observe(this@ODHAFragment, Observer {
//                val bundle = Bundle().apply {
//                    putParcelable(OtpFragment.KEY_PATIENT, patient)
//                    putInt(OtpFragment.KEY_OTP_ID, it.id)
//                }
//            })
        }
    }

    private fun validate(): FormResult {
        val form = form {
            input(R.id.emailTxt, KEY_PERSONAL_EMAIL) {
                isNotEmpty().description(R.string.empty_field)
            }
//            input(R.id.emailTxt, KEY_PMO_NAME) {
//                isNotEmpty().description(R.string.empty_field)
//            }
//            input(R.id.emailTxt, KEY_PMO_EMAIL) {
//                isNotEmpty().description(R.string.empty_field)
//            }
//            input(R.id.emailTxt, KEY_PMO_PHONE) {
//                isNotEmpty().description(R.string.empty_field)
//            }
//            input(R.id.emailTxt, KEY_PMO_RELATION) {
//                isNotEmpty().description(R.string.empty_field)
//            }
        }
        return form.validate()
    }
}