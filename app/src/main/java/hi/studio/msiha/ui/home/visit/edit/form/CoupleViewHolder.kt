package hi.studio.msiha.ui.home.visit.edit.form

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import kotlinx.android.synthetic.main.item_couple.view.*

class CoupleViewHolder(private val view: View) : BaseViewHolder<ODHAVisitRequest.Couple>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: ODHAVisitRequest.Couple,
        callback: BaseAdapter.OnItemClick<ODHAVisitRequest.Couple>?
    ) {
        item.apply {
            view.nameLbl.text = name
            view.ageLbl.text = "$age Tahun"
            view.relationshipLbl.text = relationship
            view.genderLbl.text = gender
        }
    }
}