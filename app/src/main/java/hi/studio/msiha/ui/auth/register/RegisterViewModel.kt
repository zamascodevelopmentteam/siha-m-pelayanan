package hi.studio.msiha.ui.auth.register

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetAuth
import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class RegisterViewModel(private val getAuth: GetAuth) : BaseViewModel() {
    val registerResult = MutableLiveData<AuthResult>().toSingleEvent()

    //todo Change avatar using File
    fun register(
        nik: String,
        name: String,
        password: String,
        regnas: String,
        role: String,
        avatar: String
    ) {
        isLoading.postValue(true)
        getAuth.register(nik, name, password, regnas, role, avatar) {
            when (it) {
                is Either.Left -> registerResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}