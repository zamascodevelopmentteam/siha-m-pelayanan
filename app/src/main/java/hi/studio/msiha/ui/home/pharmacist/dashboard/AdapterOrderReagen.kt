package hi.studio.msiha.ui.home.pharmacist.dashboard

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.domain.model.User
import kotlin.reflect.KProperty

class AdapterOrderReagen : BaseAdapter<Order>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_farmasi_dashboard_reagen
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return ReagenViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: Order,
        callback: OnItemClick<Order>?
    ) {
        if (holder is ReagenViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<Order>,
        new: MutableList<Order>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}