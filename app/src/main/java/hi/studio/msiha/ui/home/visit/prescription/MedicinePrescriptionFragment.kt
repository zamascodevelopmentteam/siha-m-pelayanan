package hi.studio.msiha.ui.home.visit.prescription


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.ui.home.pharmacist.recipe.arv.ArvAdapter
import kotlinx.android.synthetic.main.fragment_create_recipe.*

/**
 * A simple [Fragment] subclass.
 */
class MedicinePrescriptionFragment : Fragment() {

    private lateinit var adapter: ArvAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_medicine_prescription, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPrecriptionList()
    }

    private fun setupPrecriptionList(){
        adapter = ArvAdapter(false)
        arvList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@MedicinePrescriptionFragment.adapter
        }
    }


}
