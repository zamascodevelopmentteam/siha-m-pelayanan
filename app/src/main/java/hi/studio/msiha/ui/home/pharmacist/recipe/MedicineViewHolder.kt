package hi.studio.msiha.ui.home.pharmacist.recipe

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.RecipeMedicine
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_pharmacy_recipe_medicine.view.*

class MedicineViewHolder(private val view: View) : BaseViewHolder<RecipeMedicine>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: RecipeMedicine,
        callback: BaseAdapter.OnItemClick<RecipeMedicine>?
    ) {
        item.apply {
            view.medicineImg.showFromUrl(detail?.picture ?: "")
            view.medicineNameLbl.text = detail?.name
            view.medicineAmountLbl.text = "$amount"
            view.medicineRuleLbl.text = rule
            if (confirmed) {
                view.checkStockBtn.visibility = View.GONE
                view.checkStockDoneImg.visibility = View.VISIBLE
            } else {
                view.checkStockBtn.visibility = View.VISIBLE
                view.checkStockDoneImg.visibility = View.GONE
            }
        }

        view.checkStockBtn.setOnClickListener {
            callback?.onClick(it, item, index)
        }
    }
}