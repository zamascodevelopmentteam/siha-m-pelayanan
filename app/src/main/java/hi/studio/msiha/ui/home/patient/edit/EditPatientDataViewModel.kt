package hi.studio.msiha.ui.home.patient.edit

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class EditPatientDataViewModel(private val getPatient: GetPatient) : BaseViewModel() {

    val updatePatientResult = MutableLiveData<String>().toSingleEvent()

    fun updatePatient(id: Int, patient: Patient) {
        isLoading.postValue(true)
        getPatient.updatePatient(id, patient) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> updatePatientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }

    }
}