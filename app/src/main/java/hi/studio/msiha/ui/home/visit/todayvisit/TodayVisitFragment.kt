package hi.studio.msiha.ui.home.visit.todayvisit


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.data.constant.KEY_PAGE_LIMIT
import hi.studio.msiha.data.constant.KEY_QR_RESULT
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.PatientFragment
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.scan.ScanActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_today_visit.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class TodayVisitFragment : BaseFragment() {


    private val viewModel by viewModel<TodayVisitViewModel>()
    private lateinit var listAdapter:TodayVisitAdapter
    private var limit = KEY_PAGE_LIMIT

    override fun setView(): Int {
        return R.layout.fragment_today_visit
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        if(AppSIHA.instance.getUser()?.role== KEY_ROLE_LAB){
            requireActivity().actionBtn.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_search_black_24dp) })
            requireActivity().actionBtn.setOnClickListener {
                context?.let { it1 ->
                    MaterialDialog(it1).show {
                        title(text="Cari Kunjungan")
                        input(hint="Nik atau nama",allowEmpty = true) { dialog, text ->
                            // Text submitted with the action button
                            viewModel.getTodayVisitWithKeyword(text.toString())
                        }
                        positiveButton(text="Cari")
                        negativeButton(text="Batal"){
                            viewModel.getTodayVisit(0,limit)
                            this.dismiss()
                        }
                    }
                }
            }
        }else {
            requireActivity().actionBtn.setOnClickListener {
                KEY_ROLE_LAB
                findNavController().navigate(R.id.patientSearchFragment)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(Hawk.contains(KEY_QR_RESULT)){
            try {
                val patient: Patient = Hawk.get(KEY_QR_RESULT)
                patient.id?.let { viewModel.createVisit(it) }
            }catch(e:Exception){
                
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        getAllVisit()
        setupListAdapter()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.menu_lab_patient, menu)
        val scan = menu.findItem(R.id.action_scan)
        val search = menu.findItem(R.id.action_search)
        search.isVisible= AppSIHA.instance.getUser()?.role != KEY_ROLE_LAB
        scan.isVisible = !(AppSIHA.instance.getUser()?.role == KEY_ROLE_LAB
                || AppSIHA.instance.getUser()?.role == KEY_ROLE_PHARMACIST)
        val searchView = search.actionView as SearchView
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                viewModel.getTodayVisitWithKeyword(p0.toString())
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0 != null) {
                    if(p0.isEmpty()){
                        viewModel.getTodayVisitWithKeyword(p0.toString())
                    }
                }
                return false
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_scan -> {
                var intent =  Intent(
                    this.activity,
                    ScanActivity::class.java
                ).putExtra(
                    ScanActivity.KEY_SCAN_TYPE,
                    ScanActivity.TYPE_VISIT
                )

                intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivityForResult(intent,PatientFragment.RC_PATIENT)
            }
            R.id.action_history ->{
                findNavController().navigate(R.id.historyVisitFragment)
            }
            R.id.action_visit_plan->{
                findNavController().navigate(R.id.visitPlanFragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PatientFragment.RC_PATIENT && resultCode == Activity.RESULT_OK) {
            data?.let { it ->
                val patient = it.getParcelableExtra(PatientDetailActivity.KEY_PATIENT) as Patient?
                patient?.let {
                    it.id?.toInt()?.let { it1 -> viewModel.createVisit(it1) }
                }
            }
        }
    }

    private fun getAllVisit(){
        viewModel.getTodayVisit(0,limit)
    }

    private fun observeLiveData(){
        viewModel.apply {
            isLoading.observe(this@TodayVisitFragment, Observer {
                refreshLayout.post {
                    refreshLayout.isRefreshing = it
                }
            })

            isError.observe(this@TodayVisitFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getTodayVisitResult.observe(this@TodayVisitFragment, Observer {
                if(it.isEmpty()){
                    rvVisit.visibility=View.INVISIBLE
                    emptyVisitLayout.visibility=View.VISIBLE
                }else{
                    rvVisit.visibility=View.VISIBLE
                    emptyVisitLayout.visibility=View.GONE
                    listAdapter.addAll(it)
                }
            })

            createVisitResult.observe(viewLifecycleOwner,Observer{
                it?.let {
                    val bundle = bundleOf(PatientDetailFragment.KEY_VISIT_HISTORY to it.data)
                    if (it.data.visitType == "TEST") {
                        findNavController().navigate(R.id.visitDetailFragment2,bundle)
                    } else {
                        findNavController().navigate(R.id.visitDetailRRFragment2,bundle)
                    }
                }
            })
        }
    }

    private fun setupListAdapter(){
        listAdapter = TodayVisitAdapter()
        rvVisit.apply {
            adapter=listAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            layoutManager = LinearLayoutManager(activity)
        }

        listAdapter.itemCallback { _: View, visitHistory: VisitHistory, _: Int ->
            val patient = visitHistory.patient
            requireActivity().startActivity<PatientDetailActivity>(
                Pair(
                    PatientDetailActivity.KEY_PATIENT,
                    patient
                ),
                Pair(
                    PatientDetailActivity.KEY_MODE,
                    PatientDetailActivity.MODE_VISIT
                )
            )
        }

        rvVisit.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
                limit += KEY_PAGE_LIMIT
                viewModel.getTodayVisit(0, limit)
            }
        })
    }

}
