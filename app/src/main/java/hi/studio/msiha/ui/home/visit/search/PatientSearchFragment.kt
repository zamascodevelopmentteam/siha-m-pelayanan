package hi.studio.msiha.ui.home.visit.search

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.PatientFragment
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.scan.ScanActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_patient.patientList
import kotlinx.android.synthetic.main.fragment_search_patient.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class  PatientSearchFragment : BaseFragment() {
    private val viewModel by viewModel<PatientSearchViewModel>()
    private lateinit var patientAdapter: PatientAdapter
    private var currentText = ""

    override fun setView(): Int {
        return R.layout.fragment_search_patient
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        searchNikTxt.doAfterTextChanged {
            if (!currentText.equals(it.toString(), true)
                && it.toString().isNotBlank()
            ) {
                currentText = it.toString()
                viewModel.getPatientByNIK(it.toString())
            } else if (it.toString().isBlank()) {
                currentText = ""
                patientAdapter.clear()
            }
        }

        requireActivity().actionBtn.setOnClickListener {
            startActivityForResult(
                Intent(
                    requireContext(),
                    ScanActivity::class.java
                ).putExtra(
                    ScanActivity.KEY_SCAN_TYPE,
                    ScanActivity.TYPE_PATIENT_DETAIL
                ),
                PatientFragment.RC_PATIENT
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PatientFragment.RC_PATIENT && resultCode == Activity.RESULT_OK) {
            data?.let {
                val patient = it.getParcelableExtra(PatientDetailActivity.KEY_PATIENT) as Patient?
                patient?.let {
                    goToDetail(patient, PatientDetailActivity.MODE_VISIT)
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            patientResult.observe(this@PatientSearchFragment, Observer {
                it?.let {
                    patientAdapter.clear()
                    patientAdapter.add(it)
                }
            })

            isError.observe(this@PatientSearchFragment, Observer {
                if(it.message=="patient_has_checked_in_today") {
                    Toast.makeText(
                        context,
                        "Pasien telah check-in hari ini",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })

            createVisitResult.observe(viewLifecycleOwner, Observer {
                it?.let {
                    Log.d("testing", "test ${it.message}")
                    Log.d("test","testing ${it.data.ordinal}")
                    val bundle = bundleOf(PatientDetailFragment.KEY_VISIT_HISTORY to it.data)
                    //Bundle().putParcelable(PatientDetailFragment.KEY_VISIT_HISTORY, it.data)
                    if (it.data.visitType == "TEST") {
                        findNavController().navigate(R.id.visitDetailFragment2,bundle)
                    } else {
                        findNavController().navigate(R.id.visitDetailRRFragment2,bundle)
                    }
                }
            })
        }

    }

    private fun setupAdapter() {
        patientAdapter = PatientAdapter()
        patientList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = patientAdapter
        }

        patientAdapter.itemCallback { v: View, patient: Patient, _: Int ->
            //goToDetail(patient, PatientDetailActivity.MODE_VISIT)
            //TODO add visit
            viewModel.createVisit(patient.id?:0)
        }

        patientList.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {

            }
        })
    }

    private fun goToDetail(patient: Patient?, mode: Int) {
        patient?.let {
            requireActivity().startActivity<PatientDetailActivity>(
                Pair(
                    PatientDetailActivity.KEY_PATIENT,
                    it
                ),
                Pair(
                    PatientDetailActivity.KEY_MODE,
                    mode
                )
            )
        }
    }
}