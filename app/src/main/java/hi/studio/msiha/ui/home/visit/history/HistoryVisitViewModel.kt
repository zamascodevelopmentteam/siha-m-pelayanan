package hi.studio.msiha.ui.home.visit.history

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.response.CreateVisitResponse
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class HistoryVisitViewModel(private val getVisit: GetVisit): BaseViewModel(){
    val getHistoryVisitResult = MutableLiveData<List<VisitHistory>>().toSingleEvent()
    val createVisitResult = MutableLiveData<CreateVisitResponse>().toSingleEvent()

    fun getHistoryVisit(page: Int, limit: Int) {
        isLoading.postValue(true)
        getVisit.getHistoryVisit{
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getHistoryVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}