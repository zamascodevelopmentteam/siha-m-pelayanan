package hi.studio.msiha.ui.home.visit.preview

import android.os.Bundle
import androidx.lifecycle.Observer
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.domain.model.VisitHistory
import kotlinx.android.synthetic.main.default_toolbar.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class VisitPreviewActivity : BaseActivity() {
    private val viewModel by viewModel<VisitPreviewViewModel>()

    companion object {
        const val KEY_VISIT_HISTORY = "visit history"
    }

    override fun setView(): Int {
        return R.layout.activity_visit_preview
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        supportActionBar?.apply {
            title = "Informasi Kunjungan"
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onStart() {
        super.onStart()
        observeViewModel()
        intent?.apply {
            val history = getParcelableExtra<VisitHistory>(KEY_VISIT_HISTORY)
            history?.let { it ->
                it.id?.let {
                    viewModel.getVisits(it)
                }
            }
        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@VisitPreviewActivity, Observer {
                showLoading(it)
            })

            isError.observe(this@VisitPreviewActivity, Observer {
                showError(it)
            })

            visitsResult.observe(this@VisitPreviewActivity, Observer {
                it?.let {
                    observeVisit(it)
                }
            })
        }
    }

    private fun observeVisit(visit: Visit) {
        visit.apply {

        }
    }
}