package hi.studio.msiha.ui.home.patient

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.data.constant.KEY_PAGE_LIMIT
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.add.PatientFormActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.scan.ScanActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_patient.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class PatientFragment : BaseFragment() {
    private val viewModel by viewModel<PatientViewModel>()
    private lateinit var patientAdapter: PatientAdapter
    private var limit = KEY_PAGE_LIMIT

    companion object {
        const val RC_PATIENT = 234
    }

    override fun setView(): Int {
        return R.layout.fragment_patient
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        setHasOptionsMenu(true)
        requireActivity().actionBtn.setOnClickListener {
            requireActivity().startActivity(Intent(context,PatientFormActivity::class.java))
        }
        refreshLayout.setOnRefreshListener {
            limit = KEY_PAGE_LIMIT
            viewModel.getPatients(0, limit)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getPatients(0, limit)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        if (AppSIHA.instance.getUser()?.role == KEY_ROLE_LAB
            || AppSIHA.instance.getUser()?.role == KEY_ROLE_RR
        ) {
            menu.clear()
            inflater.inflate(R.menu.menu_search, menu)
            setupSearchView(menu.findItem(R.id.action_search))
        } else {
            menu.clear()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_scan -> {
                startActivityForResult(
                    Intent(
                        requireContext(),
                        ScanActivity::class.java
                    ).putExtra(
                        ScanActivity.KEY_SCAN_TYPE,
                        ScanActivity.TYPE_PATIENT_DETAIL
                    ),
                    RC_PATIENT
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@PatientFragment, Observer {
                refreshLayout.post {
                    refreshLayout.isRefreshing = it
                }
            })

            isError.observe(this@PatientFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            patientsResult.observe(this@PatientFragment, Observer {
                observePatients(it)
            })

            patientResult.observe(this@PatientFragment, Observer {
                goToDetail(it, PatientDetailActivity.MODE_PATIENT)
            })
        }
    }

    private fun setupSearchView(menuItem: MenuItem?) {
        menuItem?.let {
            val searchView = it.actionView as SearchView
            searchView.queryHint = "Cari NIK"
            searchView.inputType = InputType.TYPE_CLASS_NUMBER
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let { q ->
                        if (q.isNotEmpty()) {
                            viewModel.getPatientByNIK(q)
                        }
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })
        }
    }

    private fun setupAdapter() {
        patientAdapter = PatientAdapter()
        patientList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = patientAdapter
        }
        patientAdapter.itemCallback { v: View, patient: Patient, _: Int ->
            goToDetail(patient, PatientDetailActivity.MODE_PATIENT)
        }
        patientList.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
                limit += KEY_PAGE_LIMIT
                viewModel.getPatients(0, limit)
            }
        })
    }

    private fun observePatients(data: List<Patient>) {
        patientAdapter.addAll(data)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_PATIENT && resultCode == Activity.RESULT_OK) {
            data?.let { it ->
                val patient = it.getParcelableExtra(PatientDetailActivity.KEY_PATIENT) as Patient?
                patient?.let {
                    goToDetail(it, PatientDetailActivity.MODE_PATIENT)
                }
            }
        }
    }

    private fun goToDetail(patient: Patient?, mode: Int) {
        patient?.let {
            requireActivity().startActivity<PatientDetailActivity>(
                Pair(
                    PatientDetailActivity.KEY_PATIENT,
                    it
                ),
                Pair(
                    PatientDetailActivity.KEY_MODE,
                    mode
                )
            )
        }
    }
}