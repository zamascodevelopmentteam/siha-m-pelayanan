package hi.studio.msiha.ui.home.pharmacist.stock


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayout
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_pharmacist_stock.*

/**
 * A simple [Fragment] subclass.
 */
class PharmacistStockFragment : BaseFragment() {

    override fun setView(): Int {
        return R.layout.fragment_pharmacist_stock
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupViewPager()
        setupTabLayout()
        setupFab(0)
    }

    private fun setupViewPager() {
        personalPager.adapter = childFragmentManager.let { PharmacistStockPagerAdapter(it) }
        tabLayout.setupWithViewPager(personalPager)
    }

    private fun setupTabLayout() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    setupFab(tab.position)
                }
            }
        })
    }

    private fun setupFab(pos: Int) {
        val bundle = Bundle()
        bundle.putInt("mode", pos)
        requireActivity().actionBtn.setOnClickListener {
            findNavController().navigate(R.id.addMedicineFragment, bundle)
        }
    }
}
