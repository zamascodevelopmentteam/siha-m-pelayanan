package hi.studio.msiha.ui.home.patient.detail.exam.reagen

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_choose_reagen.*

class ReagenViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<Reagen>(view), LayoutContainer {
    override val containerView: View?
        get() = view

    override fun onBind(
        index: Int,
        count: Int,
        item: Reagen,
        callback: BaseAdapter.OnItemClick<Reagen>?
    ) {
        item.apply {
            reagenImg.showFromUrl(null)
            reagenNameLbl.text = name
            reagenTypeLbl.text = medicineType
            reagenStockLbl.text = "Stok: $sum"
            if(this.sum=="0"){
                chooseBtn.visibility=View.GONE
            }else{
                chooseBtn.visibility=View.VISIBLE
            }
        }

        chooseBtn.setOnClickListener {
            callback?.onClick(it, item, index)
        }
    }
}