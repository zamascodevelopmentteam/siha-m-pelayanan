package hi.studio.msiha.ui.home.patient.detail.exam

import android.content.Context
import android.util.Log
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.constant.TYPE_HIV_POSITIF
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.TestHiv
import hi.studio.msiha.utils.extention.toDateFormat
import kotlinx.android.synthetic.main.item_exam_history.view.*
import kotlinx.android.synthetic.main.item_patient.view.*


class HivExamListViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<HivTestHistory>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: HivTestHistory,
        callback: BaseAdapter.OnItemClick<HivTestHistory>?
    ) {
        Log.d("testing","patient id : ${item.id} - ${item.testHiv?.aasanTest}")
        item.apply {
            view.examTitleLbl.text = "Kunjungan ke -${item.ordinal}"
            view.examDateLbl.text="${item.testHiv?.tanggalTest.toDateFormat()}"
        }

        view.examPreviewBtn.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}