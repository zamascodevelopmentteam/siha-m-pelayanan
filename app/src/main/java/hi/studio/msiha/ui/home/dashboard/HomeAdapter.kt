package hi.studio.msiha.ui.home.dashboard

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.TodayVisit
import kotlin.reflect.KProperty

class HomeAdapter : BaseAdapter<TodayVisit>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_patient_home
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return HomeViewHolder(context, view)
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<TodayVisit>,
        new: MutableList<TodayVisit>
    ) {
        autoNotify(old, new) { o, n ->
            o.user.id == n.user.id
        }
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: TodayVisit,
        callback: OnItemClick<TodayVisit>?
    ) {
        if (holder is HomeViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

}