package hi.studio.msiha.ui.home.patient.detail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetExam
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.ODHAVisit
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class VisitDetailViewModel(
    private val getVisit: GetVisit,
    private val getExam: GetExam
) : BaseViewModel() {
    val visitCompleteResult = MutableLiveData<String>().toSingleEvent()
    val endVisitResult = MutableLiveData<String>().toSingleEvent()
    val createTreatmentResult = MutableLiveData<String>().toSingleEvent()
    val createHivExamResult = MutableLiveData<String>().toSingleEvent()
    val createImsExamResult = MutableLiveData<String>().toSingleEvent()
    val createVlcExamResult = MutableLiveData<String>().toSingleEvent()
    val getVisitDetailResult = MutableLiveData<VisitDetail>().toSingleEvent()

    fun getVisitDetailByVisitId(visitId:Int){
        isLoading.postValue(true)
        getVisit.getVisitAllDetail(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getVisitDetailResult.postValue(it.left.data)
                is Either.Right ->isError.postValue(it.right)
            }
        }

    }

    fun createVisitOdha(data: ODHAVisit) {
        isLoading.postValue(true)
        data.apply {
            getVisit.createVisitODHA(
                ordinal,
                patientId,
                date,
                treatementStartDate,
                noRegNas,
                tglKonfirmasiHivPos,
                tglKunjungan,
                tglRujukMasuk,
                kelompokPopulasi,
                statusTb,
                tglPengobatanTb,
                statusFungsional,
                stadiumKlinis,
                ppk,
                tglPemberianPpk,
                statusTbTpt,
                tglPemberianObat,
                obatArv1,
                jmlHrObatArv1,
                obatArv2,
                jmlHrObatArv2,
                obatArv3,
                jmlHrObatArv3,
                lsmPenjangkauId,
                namaReagen,
                jmlReagen,
                hasilLab,
                hasilLabLain,
                notifikasiPasangan
            ) {
                isLoading.postValue(false)
                when (it) {
                    is Either.Left -> visitCompleteResult.postValue(it.left)
                    is Either.Right -> isError.postValue(it.right)
                }
            }
        }
    }

    fun endVisit(visitId: Int) {
        isLoading.postValue(true)
        getVisit.endVisit(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> endVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }

    fun createTreatment(visitId: Int){
        isLoading.postValue(true)
        getVisit.createTreatment(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createTreatmentResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }

    fun createHivExam(visitId:Int){
        isLoading.postValue(true)
        getExam.createHIVExam(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createHivExamResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }

    fun createImsExam(visitId:Int){
        isLoading.postValue(true)
        getExam.createIMSExam(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createImsExamResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }

    fun createVlcExam(visitId: Int){
        isLoading.postValue(true)
        getExam.createVLCExam(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createVlcExamResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }
}