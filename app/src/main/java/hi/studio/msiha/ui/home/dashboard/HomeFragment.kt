package hi.studio.msiha.ui.home.dashboard

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.domain.model.Statistic
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.fragment_main.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment() {
    private val viewModel by viewModel<HomeViewModel>()
    private lateinit var patientAdapter: HomeAdapter

    override fun setView(): Int {
        return R.layout.fragment_main
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        val user = AppSIHA.instance.getUser()
        user?.let {
            userAvatarImg.showFromUrl(it.avatar)
            userNameLbl.text = "Hi ${it.name} ${when (it.role) {
                KEY_ROLE_RR -> "RR"
                KEY_ROLE_LAB -> "Lab"
                KEY_ROLE_PHARMACIST -> "Farmasi"
                else -> ""
            }
            }"
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getStatistic()
        viewModel.getTodayVisit()
    }

    private fun setupAdapter() {
        patientAdapter = HomeAdapter()
        patientList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = patientAdapter
        }
        patientAdapter.itemCallback { v: View, todayVisit: TodayVisit, _: Int ->
            when (v.id) {
                R.id.itemPatientDetailBtn -> {
                    activity?.startActivity<PatientDetailActivity>(
                        Pair(
                            PatientDetailActivity.KEY_PATIENT,
                            todayVisit.user
                        ),
                        Pair(
                            PatientDetailActivity.KEY_MODE,
                            when (AppSIHA.instance.getUser()?.role) {
                                KEY_ROLE_RR -> PatientDetailActivity.MODE_PATIENT
                                else -> 0
                            }
                        )
                    )
                }
            }
        }
        patientList.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
            }
        })
    }

    private fun observeViewModel() {
        viewModel.apply {
            statisticResult.observe(this@HomeFragment, Observer {
                observeStatistic(it)
            })

            todayVisitResult.observe(this@HomeFragment, Observer {
                observeTodayVisit(it)
            })

            isError.observe(this@HomeFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            isLoading.observe(this@HomeFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
            })
        }
    }

    private fun observeStatistic(statistic: Statistic) {
        statistic.apply {
            patientEstimationLbl.text = "Perkiraan Pasien ODHA Bulan Ini : $visitEstimation"
            patientCheckInMonthLbl.text = "$visitThisMonth Pasien"
            patientCheckInTodayLbl.text = "$visitThisDay Pasien"
            patientTransitLbl.text = "$examThisDay Pasien"
        }
    }

    private fun observeTodayVisit(visits: List<TodayVisit>) {
        patientAdapter.addAll(visits)
    }
}