package hi.studio.msiha.ui.home.pharmacist.recipe

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseBottomSheetFragment
import kotlinx.android.synthetic.main.pop_up_recipe_confirmed.*

class ConfirmRecipeBottomSheet : BaseBottomSheetFragment() {
    private var callback: () -> Unit = {}

    override fun setView(): Int {
        return R.layout.pop_up_recipe_confirmed
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        confirmBtn.setOnClickListener {
            callback()
        }

        closeBtn.setOnClickListener {
            dismiss()
        }
    }

    fun setCallback(callback: () -> Unit) {
        this.callback = callback
    }
}