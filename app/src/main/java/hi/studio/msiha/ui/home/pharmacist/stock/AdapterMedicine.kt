package hi.studio.msiha.ui.home.pharmacist.stock

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.ui.home.pharmacist.stock.medicinelist.MedicineViewHolder
import kotlin.reflect.KProperty


class AdapterMedicine : BaseAdapter<ArvStock>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_farmasi_stok_obat
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return MedicineViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: ArvStock,
        callback: OnItemClick<ArvStock>?
    ) {
        if (holder is MedicineViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<ArvStock>,
        new: MutableList<ArvStock>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}