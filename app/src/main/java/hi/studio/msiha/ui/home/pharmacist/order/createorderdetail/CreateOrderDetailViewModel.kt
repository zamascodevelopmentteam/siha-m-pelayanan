package hi.studio.msiha.ui.home.pharmacist.order.createorderdetail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.request.FarmasiOrderRequest
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class CreateOrderDetailViewModel(private val getPharmacist: GetPharmacist):BaseViewModel(){
    val getCreateOrderResult = MutableLiveData<Order>().toSingleEvent()

    fun createOrder(order: FarmasiOrderRequest) {
        isLoading.postValue(true)
        getPharmacist.createOrder(order) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getCreateOrderResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}