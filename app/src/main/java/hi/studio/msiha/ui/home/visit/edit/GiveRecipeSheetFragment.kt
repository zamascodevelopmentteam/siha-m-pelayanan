package hi.studio.msiha.ui.home.visit.edit

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseBottomSheetFragment
import kotlinx.android.synthetic.main.pop_up_give_recipe.*

class GiveRecipeSheetFragment : BaseBottomSheetFragment() {
    private var callback: (yes: Boolean) -> Unit = {}

    override fun setView(): Int {
        return R.layout.pop_up_give_recipe
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        noBtn.setOnClickListener {
            callback(false)
        }
        yesBtn.setOnClickListener {
            callback(true)
        }
    }

    fun setCallback(callback: (yes: Boolean) -> Unit) {
        this.callback = callback
    }
}