package hi.studio.msiha.ui.home.patient.detail.exam

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.afollestad.vvalidator.util.onItemSelected
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_VL_RESULT
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.detail.ExamViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.KEY_VISIT_DETAIL
import hi.studio.msiha.ui.home.patient.detail.exam.reagen.ReagenChooseActivity
import hi.studio.msiha.utils.extention.selectItem
import kotlinx.android.synthetic.main.fragment_exam_vlc.*
import kotlinx.android.synthetic.main.fragment_exam_vlc.saveBtn
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.widget.ArrayAdapter





class VLCExamFragment : BaseFragment() {
    private val viewModel by viewModel<ExamViewModel>()
    private var qty = 0

    companion object {
        const val KEY_REAGEN = "reagen_vl"
        const val KEY_REAGEN_AMOUNT = "reagen_amount_vl"
        const val KEY_TEST_TYPE = "vl_test_type"
        const val KEY_TEST_RESULT = "vl_result"
        const val KEY_EXAM_QUERY_PARAM = "exam_query_param"
    }

    override fun setView(): Int {
        return R.layout.fragment_exam_vlc
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {

        reagenTxt.setOnClickListener(reagenChooseListener)
        saveBtn.setOnClickListener {
            val visitHistory: VisitHistory =
                arguments?.get(PatientDetailFragment.KEY_VISIT_HISTORY) as VisitHistory
            when {
                reagenTxt.tag.toString().isEmpty() -> Toast.makeText(context,"Anda belum memilih Reagen", Toast.LENGTH_SHORT).show()
                reagenAmountTxt.text.toString().isEmpty() -> Toast.makeText(context,"Anda belum memilih Jumlah Reagen", Toast.LENGTH_SHORT).show()
                testLabResultTxt.text.toString().isEmpty() && testLabResultSpn.selectedItemPosition==0 -> Toast.makeText(context,"Anda belum memasukkan hasil test",Toast.LENGTH_SHORT).show()
                else -> {
                    var testResult = if(testLabResultSpn.selectedItemPosition>0){
                        testLabResultSpn.selectedItem as String
                    }else{
                        testLabResultTxt.text.toString()
                    }
                    val vlExam = VLCExamRequest(
                        ordinal = visitHistory.ordinal,
                        visitId = visitHistory.id,
                        testType = testLabSpn.selectedItem.toString(),
                        nameReagen1 = reagenTxt.tag.toString().toInt(),
                        qty = reagenAmountTxt.text.toString().toInt(),
                        resultTest = testResult
                    )
                    Hawk.put(KEY_VL_RESULT, vlExam)
                    viewModel.updateVLCExam(vlExam)
                }
            }
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
        populateView()
    }

    private fun populateView(){
        var vl:VLCExamRequest?=null
        if(Hawk.contains(KEY_VL_RESULT)){
             vl = Hawk.get(KEY_VL_RESULT)

        }else if(Hawk.contains(KEY_VISIT_DETAIL)){
             vl = Hawk.get<VisitDetail>(KEY_VISIT_DETAIL).testVL
        }
        populateView(vl)
    }

    private fun populateView(vl:VLCExamRequest?){
        if(vl!=null){
            if(vl.testType!=null){
                testLabSpn.selectItem(vl.testType)
                if(vl.testType=="CD4"){
                    val adapter = ArrayAdapter.createFromResource(
                        context,
                        R.array.result_cd4,
                        android.R.layout.simple_spinner_item
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    testLabResultSpn.adapter = adapter
                        when(vl.resultTest){
                            "INVALID"->testLabResultSpn.setSelection(1)
                            "ERROR"->{
                                Log.d("error ih","error cd4")
                                testLabResultSpn.setSelection(2,false)
                            }
                            "NO RESULT"->testLabResultSpn.setSelection(3)
                            else ->  testLabResultTxt.setText("${vl.resultTest}")
                        }


                }else{
                    val adapter = ArrayAdapter.createFromResource(
                        context,
                        R.array.result_vl,
                        android.R.layout.simple_spinner_item
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    testLabResultSpn.adapter = adapter
                        when (vl.resultTest) {
                            "NOT DETECTED" -> testLabResultSpn.setSelection(1, true)
                            "INVALID" -> testLabResultSpn.setSelection(2)
                            "ERROR" -> testLabResultSpn.setSelection(3)
                            "NO RESULT" -> testLabResultSpn.setSelection(4)
                            else -> testLabResultTxt.setText("${vl.resultTest}")
                        }

                }
            }

            if(vl.nameReagen1!=null){
                reagenTxt.text = vl.namaReagenData?.name
                reagenTxt.tag=vl.nameReagen1
            }
            if(vl.qty!=null){
                reagenAmountTxt.setText("${vl.qty}")
            }
        }

        testLabSpn.onItemSelected {
            if(it==1){
                val adapter = ArrayAdapter.createFromResource(
                    context,
                    R.array.result_cd4,
                    android.R.layout.simple_spinner_item
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                testLabResultSpn.adapter = adapter

            }else{
                val adapter = ArrayAdapter.createFromResource(
                    context,
                    R.array.result_vl,
                    android.R.layout.simple_spinner_item
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                testLabResultSpn.adapter = adapter

            }
        }
//
//        testLabResultSpn.onItemSelected {
//            if(it==0){
//                inputAngkaWrapper.visibility=View.VISIBLE
//            }else{
//                inputAngkaWrapper.visibility=View.GONE
//            }
//        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@VLCExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@VLCExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            examResult.observe(this@VLCExamFragment, Observer {
                findNavController().navigateUp()
            })
        }
    }

    private val reagenChooseListener = View.OnClickListener {
        Hawk.put(HIVExamFragment.KEY_REAGEN_TYPE,"is_cd_vl")
        startActivityForResult(
            Intent(requireContext(), ReagenChooseActivity::class.java)
                .putExtra("type", IMSExamFragment.KEY_REAGEN_IMS),
            IMSExamFragment.RC_CHOOSE_REAGEN
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            data?.apply {
                val medicineName = getStringExtra("name")
                val medicineId = getIntExtra("id", 0)
                qty = getIntExtra("qty", 0)
                reagenAmountTxt.setText("$qty")
                updateButton(
                    when (requestCode) {
                        IMSExamFragment.RC_CHOOSE_REAGEN -> reagenTxt
                        else -> null
                    }, medicineId, "$medicineName($qty)"
                )
            }
        }
    }

    private fun updateButton(view: AppCompatTextView?, id: Int, name: String) {
        view?.apply {
            text = name
            tag = id
        }
    }

}