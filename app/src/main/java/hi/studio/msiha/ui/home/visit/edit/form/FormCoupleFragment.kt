package hi.studio.msiha. ui.home.visit.edit.form

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.KEY_VISIT_DETAIL
import hi.studio.msiha.ui.home.visit.edit.couple.AddCoupleActivity
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_couple.*
import java.util.*

class FormCoupleFragment : BaseFragment(), WizardStep {
    private lateinit var adapter: CoupleAdapter
    private val bundle = Bundle()
    private lateinit var visitDetail:VisitDetail

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        val couples: ArrayList<ODHAVisitRequest.Couple>? = adapter.all as ArrayList<ODHAVisitRequest.Couple>
        Log.d("tag","kopel size : ${couples?.size}")

        bundle.apply {
            Log.d("error couples","size : adapterAll ${adapter.all.size}")
            Log.d("notifikasi","error : ${statusNotificationSpn.selectedItem.toString()}")
            putString(KEY_NOTIFICATION_STATUS, statusNotificationSpn.selectedItem.toString())
            putParcelableArrayList(KEY_COUPLES, couples)
        }
        return true
    }

    companion object {
        const val KEY_NOTIFICATION_STATUS = "status_notification"
        const val KEY_COUPLES = "couples"
        const val RC_COUPLE = 123

        @JvmStatic
        fun newInstance() =
            FormCoupleFragment().apply {
            }
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_couple
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {

        setupAdapter()
        setupCoupleSelectionBehaviour()
        addCoupleBtn.setOnClickListener {
            startActivityForResult(
                Intent(requireContext(), AddCoupleActivity::class.java),
                RC_COUPLE
            )
        }
        if(Hawk.contains(KEY_VISIT_DETAIL)) {
            visitDetail = Hawk.get(KEY_VISIT_DETAIL)
            if(visitDetail.checkOutDate!=null){
                addCoupleBtn.visibility=View.GONE
                statusNotificationSpn.isEnabled=false
            }
            if(!visitDetail.treatment?.notifikasiPasangan.isNullOrEmpty()) {
                val arrays = context?.resources?.getStringArray(R.array.status_notification)
                val idx = arrays?.indexOf(visitDetail?.treatment?.notifikasiPasangan?.capitalize())?:-1
                if(idx!=-1) statusNotificationSpn.setSelection(idx)
                if(idx==1){
                    coupleList.visibility=View.VISIBLE
                    addCoupleBtn.visibility=View.VISIBLE
                    tvTitle.visibility=View.VISIBLE
                    Log.d("test couple","notif ${visitDetail?.treatment?.couples?.size}")
                    if (visitDetail.treatment?.couples?.isNotEmpty()!!) {
                        adapter.addAll(visitDetail.treatment?.couples as List<ODHAVisitRequest.Couple>)
//                        for(kopel in visitDetail?.treatment?.couples!!){
//                            if(kopel!=null){
//                                adapter.add()
//                            }
//                        }
                    }
                }
            }
        }
    }

    private fun setupCoupleSelectionBehaviour(){
        statusNotificationSpn.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val arrays = context?.resources?.getStringArray(R.array.status_notification)
                visitDetail.treatment?.notifikasiPasangan= arrays?.get(position) ?:""
                if(Hawk.contains(KEY_VISIT_DETAIL)) {
                    Hawk.put(KEY_VISIT_DETAIL,visitDetail)
                }
                if(position==1){
                    coupleList.visibility=View.VISIBLE
                    addCoupleBtn.visibility=View.VISIBLE
                    tvTitle.visibility=View.VISIBLE
                }else{
                    coupleList.visibility=View.INVISIBLE
                    addCoupleBtn.visibility=View.INVISIBLE
                    tvTitle.visibility=View.INVISIBLE

                }
            }
        }
    }

     fun setupAdapter() {
        adapter = CoupleAdapter()

        coupleList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@FormCoupleFragment.adapter
        }
         if(Hawk.contains(KEY_COUPLES)){
            val couples = Hawk.get<List<ODHAVisitRequest.Couple>>(KEY_COUPLES)
             adapter.addAll(couples)
         }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_COUPLE && resultCode == Activity.RESULT_OK) {
            data?.apply {
                val couple = getParcelableExtra<ODHAVisitRequest.Couple>(AddCoupleActivity.KEY_DATA)
                couple?.let {
                    adapter.add(it)
                    visitDetail.treatment?.couples=adapter.all
                    Hawk.put(KEY_VISIT_DETAIL,visitDetail)
                    Hawk.put(KEY_COUPLES,adapter.all)
                }
            }
        }
    }
}