package hi.studio.msiha.ui.home.patient.edit

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.TYPE_HIV_NEGATIVE
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.KEY_PATIENT
import hi.studio.msiha.utils.extention.getAge
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.extention.showDatePicker
import kotlinx.android.synthetic.main.fragment_edit_patient_data.*
import kotlinx.android.synthetic.main.fragment_edit_patient_personal_data.*
import kotlinx.android.synthetic.main.fragment_edit_patient_pmo_data.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class EditPatientDataFragment : BaseFragment() {

    private val viewModel by viewModel<EditPatientDataViewModel>()
    private var patient: Patient? = null
    private var updatedPatient: Patient? = null


    override fun setView(): Int {
        return R.layout.fragment_edit_patient_data
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        patient = arguments?.getParcelable(KEY_PATIENT)
        if(Hawk.contains(KEY_PATIENT)){
            Log.d("test","patient exist")
            patient= Hawk.get(KEY_PATIENT)
        }else{
            Log.d("test","patient not exist")
        }
        updatedPatient = patient?.copy()

        setupTabLayout()
        fillPatientPersonalInfo()
        if (patient?.statusPatient == TYPE_ODHA) {
            fillPatientPmoInfo()
        }

        dateOfBornTxt.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, db: String, s1: String ->
                ageTxt.setText(calendar.getAge().toString())
                dateOfBornTxt.setText(s1)
                dateOfBornTxt.tag = db
                updatedPatient?.dateBirth = db
            }
        }

        saveBtn.setOnClickListener {
            if (tabLayout?.selectedTabPosition == 0) {
                updatePatientPersonalData()
            } else {
                updatePatientPmoData()
            }
        }
    }

    private fun setupTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_personal_data)))
        if (patient?.statusPatient == TYPE_ODHA) {
            tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_pmo_data)))
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        editPatientPersonalDataLayout.visibility = View.VISIBLE
                        editPatientPmoDataLayout.visibility = View.GONE
                    }
                    1 -> {
                        editPatientPersonalDataLayout.visibility = View.GONE
                        editPatientPmoDataLayout.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun fillPatientPersonalInfo() {
        val nik = patient?.nik
        val name = patient?.name.orEmpty()
        val address = patient?.addressKTP.orEmpty()
        val domicile = patient?.addressDomicile.orEmpty()
        val dob = patient?.dateBirth
        val gender = patient?.gender.orEmpty()
        val phone = patient?.phone.orEmpty()
        val email = patient?.email.orEmpty()

        nikTxt.setText(nik)
        nameTxt.setText(name)
        addressCardIdTxt.setText(address)
        addressDomicileTxt.setText(domicile)
        genderSpn.selectItem(gender)
        phoneTxt.setText(phone)
        if (patient?.statusPatient == TYPE_ODHA){
            odhaStatusSpn.isEnabled=false
        }
        val cal = dob?.split("-")
        val calendar = Calendar.getInstance()
        if(cal!=null){
            try{
                calendar.set(cal!![0].toInt(),cal!![1].toInt(),cal!![2].toInt())
                ageTxt.setText(calendar.getAge().toString())
            }catch(e:Exception){

            }
        }


        dateOfBornTxt.setText(dob)
        dateOfBornTxt.tag=dob
        odhaStatusSpn.setSelection(if (patient?.statusPatient == TYPE_ODHA) 1 else 0)
        emailTxt.setText(email)
    }

    private fun fillPatientPmoInfo() {
        val name = patient?.namaPmo.orEmpty()
        val phone = patient?.noHpPmo.orEmpty()
        val relation = patient?.hubunganPmo
        pmoNameTxt.setText(name)
        pmoPhoneTxt.setText(phone)
        pmoRelationShipTXt.setText(relation)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.apply {
            updatePatientResult.observe(this@EditPatientDataFragment, Observer {
                (requireActivity() as PatientDetailActivity).showMessage(it)
                requireActivity().finish()
            })

            isLoading.observe(this@EditPatientDataFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })
        }
    }

    private fun updatePatientPersonalData() {
        if (validatePatientPersonalData()) {
            updatedPatient?.apply {
                nik = nikTxt.text.toString()
                name = nameTxt.text.toString()
                addressKTP = addressCardIdTxt.text.toString()
                addressDomicile = addressDomicileTxt.text.toString()
                gender = genderSpn.selectedItem.toString()
                phone = phoneTxt.text.toString()
                statusPatient = if (odhaStatusSpn.selectedItem.toString() == "Bukan ODHA") {
                    TYPE_HIV_NEGATIVE
                } else {
                    TYPE_ODHA
                }
                email=emailTxt.text.toString()
            }
            updatedPatient?.id?.let {
                viewModel.updatePatient(it, updatedPatient!!)
            }
        } else {
            showMessage("Mohon isi semua input form tersedia")
        }
    }

    private fun updatePatientPmoData() {
        if (validatePatientPmoData()) {
            updatedPatient?.apply {
                namaPmo = pmoNameTxt.text.toString()
                noHpPmo = pmoPhoneTxt.text.toString()
                hubunganPmo = pmoRelationShipTXt.text.toString()
            }
            updatedPatient?.id?.let {
                viewModel.updatePatient(it, updatedPatient!!)
            }
        } else {
            showMessage("Mohon isi semua input form PMO yang tersedia")
        }
    }

    private fun validatePatientPersonalData(): Boolean {
        var valid = true
        if (nikTxt.text.isNullOrEmpty()) valid = false
        if (nameTxt.text.isNullOrEmpty()) valid = false
        if (addressCardIdTxt.text.isNullOrEmpty()) valid = false
        if (addressDomicileTxt.text.isNullOrEmpty()) valid = false
        if (dateOfBornTxt.text.isNullOrEmpty()) valid = false
        if (ageTxt.text.isNullOrEmpty()) valid = false
        if (genderSpn.selectedItem.toString().toLowerCase() != "laki-laki" &&
            genderSpn.selectedItem.toString().toLowerCase() != "perempuan"
        ) {
            valid = false
        }
        if (phoneTxt.text.isNullOrEmpty()) valid = false

        return valid
    }

    private fun validatePatientPmoData(): Boolean {
        var valid = true
        if (pmoNameTxt.text.isNullOrEmpty()) valid = false
        if (pmoPhoneTxt.text.isNullOrEmpty()) valid = false
        if (pmoRelationShipTXt.text.isNullOrEmpty()) valid = false

        return valid
    }
}