package hi.studio.msiha.ui.home.patient.detail

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.*
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.extention.showDate
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.showFromUrl
import hi.studio.msiha.utils.extention.toFormat
import kotlinx.android.synthetic.main.fragment_exam.*
import kotlinx.android.synthetic.main.item_patient.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class ExamFragment : BaseFragment() {
    private val viewModel by viewModel<ExamViewModel>()
    private var patient: Patient? = null
    private var visitHistory: VisitHistory? = null

    companion object {
        const val KEY_EXAM_DATA = "exam_data"
        const val KEY_ID = "user id"
        const val KEY_HOSPITAL_ID = "hospital id"
        const val KEY_ORDINAL = "ordinal"
        const val KEY_EXAM_REQUEST = "exam_request"
        const val KEY_EXAM_CHECK = "exam_check"
        const val KEY_EXAM_RESULT = "exam_result"
        const val KEY_EXAM_TYPE = "exam_type"
        const val TYPE_HIV = "HIV"
        const val TYPE_IMS = "IMS"
        const val TYPE_VL = "VL"
        const val KEY_ID_VISIT = "id_visit"
        const val KEY_HIV_TEST="hiv_test"
    }

    override fun setView(): Int {
        return R.layout.fragment_exam
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            patient = getParcelable(PatientDetailActivity.KEY_PATIENT)
            visitHistory = getParcelable(PatientDetailFragment.KEY_VISIT_HISTORY)

            patient?.apply {
                nameLbl.text = name
                nikLbl.text = nik
                userAvatarImg.showFromUrl(null)
                val status = when (statusPatient) {
                    TYPE_HIV_POSITIF -> "HIV Positif"
                    TYPE_ODHA -> "Pasien ODHA"
                    else -> "HIV Negatif"
                }
                hivStatusLbl.text = "Status: $status"
            }
            visitHistory?.apply {
                ordinalLbl.text = "Kunjungan ke-$ordinal"
                Log.d("testing exam","visit history $visitHistory")
                id?.let { viewModel.getVisitAllDetail(it) }


                if(visitType=="TREATMENT"){
                    if(testVl!=null) {
                        layoutVlc.visibility = View.VISIBLE
                    }
                    finishExamBtn.visibility = View.GONE
                }else{
                    layoutVlc.visibility = View.GONE
                    finishExamBtn.visibility = View.VISIBLE
                }
            }
        }

        val calendar = Calendar.getInstance()
        examRequestedCheckDateLbl.text = calendar.time.toFormat()
        examRequestedCheckDateLbl.tag = calendar.timeInMillis
        examCheckDateLbl.text = calendar.time.toFormat()
        examCheckDateLbl.tag = calendar.timeInMillis
        examResultDateLbl.text = calendar.time.toFormat()
        examResultDateLbl.tag = calendar.timeInMillis

        examRequestedCheckDateLbl.setOnClickListener(calendarListener)
        examCheckDateLbl.setOnClickListener(calendarListener)
        examResultDateLbl.setOnClickListener(calendarListener)

        vlcFormBtn.setOnClickListener {
            findNavController().navigate(R.id.VLCExamFragment, updateArgument(TYPE_VL))
        }

        hivFormBtn.setOnClickListener {
            findNavController().navigate(R.id.HIVExamFragment, updateArgument(TYPE_HIV))
        }

        imsFormBtn.setOnClickListener {
            findNavController().navigate(R.id.IMSExamFragment, updateArgument(TYPE_IMS))
        }

        finishExamBtn.setOnClickListener {
            //Timber.d(VisitDetailRRFragment.odhaData.toString())
            observeEndVisit()
            viewModel.endVisit(visitHistory?.id?:0)
        }

    }

    private fun observeEndVisit(){
        viewModel.endVisitResult.observe(viewLifecycleOwner,Observer{
            viewModel.endVisitResult.removeObservers(viewLifecycleOwner)
            findNavController().navigateUp()
        })
    }

    private fun updateArgument(type: String?): Bundle? {
        val bundle = Bundle().apply {
            patient?.apply {
                putInt(KEY_ID, id ?: 0)
                putInt(KEY_HOSPITAL_ID, upkId ?: 0)
            }
            visitHistory?.apply {
                id?.let {
                    putInt(KEY_ID_VISIT, id)
                }
            }
            putLong(KEY_EXAM_REQUEST, examRequestedCheckDateLbl.tag as Long? ?: 0L)
            putLong(KEY_EXAM_CHECK, examCheckDateLbl.tag as Long? ?: 0L)
            putLong(KEY_EXAM_RESULT, examResultDateLbl.tag as Long? ?: 0L)
            putString(KEY_EXAM_TYPE, type)
        }
        return arguments?.apply {
            putBundle(KEY_EXAM_DATA, bundle)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@ExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@ExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            examResult.observe(this@ExamFragment, Observer {
                findNavController().navigateUp()
            })

            getExamResult.observe(this@ExamFragment, Observer {
                it.apply {
                    examRequestedCheckDateLbl.showDate(examRequestDate?.toLong() ?: 0L)
                    examRequestedCheckDateLbl.tag = examRequestDate?.toLong() ?: 0L
                    examCheckDateLbl.showDate(examDate?.toLong() ?: 0L)
                    examCheckDateLbl.tag = examDate?.toLong() ?: 0L
                    examResultDateLbl.showDate(receiveResultDate?.toLong() ?: 0L)
                    examResultDateLbl.tag = receiveResultDate?.toLong() ?: 0L
                    updateButton(hivFormBtn, hivResult != null)
                    updateButton(imsFormBtn, imsResult != null)
                    updateButton(vlcFormBtn, vlTestResult != null)
                }
            })

            getVisitAllDetailResult.observe(viewLifecycleOwner, Observer {
                val visitDetail = it.data
                if(visitDetail.testHiv!=null){
                    hivFormBtn.text="Hasil"
                    Hawk.put(KEY_HIV_RESULT,visitDetail.testHiv)
                    layoutHivWrapper.visibility=View.VISIBLE
                    hivSeparator.visibility=View.VISIBLE
                }else{
                    layoutHivWrapper.visibility=View.GONE
                    hivSeparator.visibility=View.GONE
                }
                if(visitDetail.testIms!=null){
                    imsFormBtn.text="Hasil"
                    Hawk.put(KEY_IMS_RESULT,visitDetail.testIms)
                    layoutIMSWrapper.visibility=View.VISIBLE
                    imsSeparator.visibility=View.VISIBLE
                }else{
                    layoutIMSWrapper.visibility=View.GONE
                    imsSeparator.visibility=View.GONE
                }

                if(visitDetail.testVL!=null){
                    vlcFormBtn.text="Hasil"
                    Hawk.put(KEY_VL_RESULT,visitDetail.testVL)
                    layoutVlc.visibility = View.VISIBLE
                }else{
                    layoutVlc.visibility = View.GONE
                }
                hivStatusLbl.text=visitDetail.patient?.statusPatient
            })
        }
    }

    private val calendarListener = View.OnClickListener {
        showDatePicker(requireContext()) { calendar: Calendar, s: String, s1: String ->
            if (it is TextView) {
                it.text = s1
                it.tag = calendar.timeInMillis
            }
        }
    }

    private fun updateButton(view: Button, isPreview: Boolean) {
        if (isPreview) {
            view.setBackgroundResource(R.drawable.button_bg_primary_outline)
            view.text = "Ubah Form"
            view.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
        } else {
            view.setBackgroundResource(R.drawable.button_bg_primary)
            view.text = "Isi Form"
            view.setTextColor(Color.WHITE)
        }
    }
}