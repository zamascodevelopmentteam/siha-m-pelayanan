package hi.studio.msiha.ui.home.pharmacist.stock.medicinelist

import android.view.View
import com.bumptech.glide.request.RequestOptions
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.GlideApp
import kotlinx.android.synthetic.main.item_farmasi_stok_obat.view.*

class MedicineViewHolder(private val view: View) : BaseViewHolder<ArvStock>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: ArvStock,
        callback: BaseAdapter.OnItemClick<ArvStock>?
    ) {
        val requestOptions = RequestOptions()
        requestOptions.circleCrop()
        GlideApp.with(view)
            .load(R.drawable.ic_pill)
            .apply(requestOptions)
            .into(view.imgMedicine)
        view.textMedicineName.text = item.name
        view.textMedicineType.text = item.medicineType
        val stock = item.stockQuantity
        view.textMedicineStock.text = "Stok : $stock"
        view.layoutWrapper.setOnClickListener {
            callback?.onClick(view, item)
        }
    }
}