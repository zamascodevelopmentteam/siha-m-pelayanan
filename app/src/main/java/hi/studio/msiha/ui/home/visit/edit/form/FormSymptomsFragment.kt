package hi.studio.msiha.ui.home.visit.edit.form

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.utils.extention.fromForm
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_symptomps.*

class FormSymptomsFragment : BaseFragment(), WizardStep {
    private var bundle = Bundle()

    companion object {
        const val KEY_PREGNANT_STATUS = "pregnant_status"
        const val KEY_KB_METHOD = "kb_method"
        const val KEY_INFECTION_OPPORTUNITIES = "infection_opportunities"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            FormSymptomsFragment().apply {
                arguments = bundle
            }
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_symptomps
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        displayData(arguments)
    }

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(R.id.kbMethodTxt, KEY_KB_METHOD, true) {
            }
            input(R.id.infectionOpportunitiesTxt, KEY_INFECTION_OPPORTUNITIES, true) {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(R.id.pregnantStatusSpn, KEY_PREGNANT_STATUS) {

            }
        }
        val result = form.validate()
        result.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.pregnantStatusSpn -> showMessage(
                            description
                        )
                    }
                }
            } else {
                bundle.apply {
                    putString(KEY_PREGNANT_STATUS, pregnantStatusSpn.selectedItem.toString())
                    putString(KEY_KB_METHOD, result[KEY_KB_METHOD]?.asString())
                    putString(
                        KEY_INFECTION_OPPORTUNITIES,
                        result[KEY_INFECTION_OPPORTUNITIES]?.asString()
                    )
                }
            }
        }
        return result.success()
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            infectionOpportunitiesTxt.setText(getString(KEY_INFECTION_OPPORTUNITIES, "").fromForm())
            kbMethodTxt.setText(getString(KEY_KB_METHOD, "").fromForm())
            pregnantStatusSpn.selectItem(getString(KEY_PREGNANT_STATUS))
        }
    }
}