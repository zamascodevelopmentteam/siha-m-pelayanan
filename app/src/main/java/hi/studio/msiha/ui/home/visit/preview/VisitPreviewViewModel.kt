package hi.studio.msiha.ui.home.visit.preview

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class VisitPreviewViewModel(private val getVisit: GetVisit) : BaseViewModel() {
    val visitsResult = MutableLiveData<Visit>().toSingleEvent()

    fun getVisits(idVisit: Int) {
        getVisit.getVisitDetail(idVisit) {
            when (it) {
                is Either.Left -> visitsResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}