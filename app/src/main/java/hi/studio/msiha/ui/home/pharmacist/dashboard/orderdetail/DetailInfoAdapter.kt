package hi.studio.msiha.ui.home.pharmacist.dashboard.orderdetail

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import hi.studio.msiha.R
import hi.studio.msiha.domain.model.Medicine

class DetailInfoAdapter(
    private val context: Context?,
    private val dataSource:ArrayList<Medicine>?
):BaseAdapter(){

    private val inflater: LayoutInflater
            = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val view = inflater.inflate(R.layout.item_detail_informasi, p2, false)
        val textMedicineName = view.findViewById<TextView>(R.id.textMedicineName)
        val textStockAmount = view.findViewById<TextView>(R.id.textStockAmount)

        val med = getItem(p0)
        if (med != null) {
            textMedicineName.text=med.name
            textStockAmount.text="Jumlah : 0"
        }
        return view
    }

    override fun getItem(p0: Int): Medicine? {
        if (dataSource != null) {
            return dataSource[p0]
        }
        return null
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong() //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCount(): Int {
        if (dataSource != null) {
            return dataSource.size
        }
        return 0
    }

}