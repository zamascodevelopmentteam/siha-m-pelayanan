package hi.studio.msiha.ui.home.patient.detail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.data.datasource.remote.response.VisitAllDetailResponse
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import hi.studio.msiha.domain.interactor.GetExam
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Exam
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ExamViewModel(
    private val getExam: GetExam,
    private val getVisit: GetVisit
) : BaseViewModel() {
    val examResult = MutableLiveData<String>().toSingleEvent()
    val getExamResult = MutableLiveData<Exam>().toSingleEvent()
    val getVisitAllDetailResult = MutableLiveData<VisitAllDetailResponse>().toSingleEvent()
    val getHivHistoryListResult = MutableLiveData<List<HivTestHistory>>().toSingleEvent()
    val getVlHistoryListResult = MutableLiveData<List<VlTestHistory>>().toSingleEvent()
    val getImsHistoryListResult = MutableLiveData<List<ImsTestHistory>>().toSingleEvent()
    val endVisitResult = MutableLiveData<String>().toSingleEvent()

    fun updateHIVExam(request: HIVExamRequest) {
        isLoading.postValue(true)
        getExam.updateHIVExam(request.visitId?:0,request) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> examResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getHivHistoryList(visitId: Int){
        isLoading.postValue(true)
        getExam.getHivHistoryByVisit(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getHivHistoryListResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getImsHistoryList(visitId: Int){
        isLoading.postValue(true)
        getExam.getImsHistoryByVisit(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getImsHistoryListResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getVlHistoryList(visitId: Int){
        isLoading.postValue(true)
        getExam.getVlHistoryByVisit(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getVlHistoryListResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun updateIMSExam(request: IMSExamRequest) {
        isLoading.postValue(true)
        getExam.updateIMSExam(request.visitId?:0,request) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> examResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun updateVLCExam(request: VLCExamRequest) {
        isLoading.postValue(true)
        getExam.updateVLCExam(request.visitId?:0,request) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> examResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getExam(id: Int) {
        isLoading.postValue(true)
        getExam.getExamByVisit(id) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getExamResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getVisitAllDetail(visitId:Int){
        isLoading.postValue(true)
        getVisit.getVisitAllDetail(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left->getVisitAllDetailResult.postValue(it.left)
                is Either.Right->isError.postValue(it.right)
            }
        }
    }

    fun endVisit(visitId: Int) {
        isLoading.postValue(true)
        getVisit.endVisit(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> endVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }
}