package hi.studio.msiha.ui.home.pharmacist.recipe.medicine

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import kotlin.reflect.KProperty

class ChooseArvAdapter : BaseAdapter<ArvStock>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_arv
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return ChooseArvViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: ArvStock,
        callback: OnItemClick<ArvStock>?
    ) {
        (holder as ChooseArvViewHolder).onBind(index, count, item, callback)
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<ArvStock>,
        new: MutableList<ArvStock>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}