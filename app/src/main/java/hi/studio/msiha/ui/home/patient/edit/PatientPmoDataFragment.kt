package hi.studio.msiha.ui.home.patient.edit

import android.os.Bundle
import android.util.Log
import android.view.View
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.KEY_PATIENT
import kotlinx.android.synthetic.main.fragment_patient_pmo_data.*

class PatientPmoDataFragment : BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            PatientPmoDataFragment().apply {
                arguments = bundle
            }
    }

    private var patient: Patient? = null

    override fun setView(): Int {
        return R.layout.fragment_patient_pmo_data
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        patient = arguments?.getParcelable(KEY_PATIENT)
        if(Hawk.contains(KEY_PATIENT)){
            Log.d("test","patient exist")
            patient= Hawk.get(KEY_PATIENT)
        }else{
            Log.d("test","patient not exist")
        }
        fillPatientPmoInfo()
    }

    private fun fillPatientPmoInfo() {
        patient?.run {
            textViewPmoName.text = namaPmo
            textViewPmoRelation.text = hubunganPmo
            textViewPmoPhone.text = noHpPmo
        }
    }

}