package hi.studio.msiha.ui.home.patient.detail.exam.reagen

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ReagenChooseViewModel(
    private val getRecipe: GetRecipe
) : BaseViewModel() {
    val reagenResult = MutableLiveData<List<Reagen>>().toSingleEvent()

    fun getReagens() {
        getRecipe.getNonArvReagen("NULL") {
            when (it) {
                is Either.Left -> reagenResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}