package hi.studio.msiha.ui.home.patient.overview

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class OverviewViewModel(private val getVisit: GetVisit) :BaseViewModel(){
    val result = MutableLiveData<String>().toSingleEvent()

    fun updateVisit(id: Int, bundle: Bundle) {
        getVisit.updateVisit(id, bundle) {
            when (it) {
                is Either.Left -> result.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}