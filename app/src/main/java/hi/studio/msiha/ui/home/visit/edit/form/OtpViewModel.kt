package hi.studio.msiha.ui.home.visit.edit.form

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.model.Otp
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class OtpViewModel(
    private val getUser: GetUser,
    private val getPatient: GetPatient
) : BaseViewModel() {
    val getOtpResult = MutableLiveData<Otp>().toSingleEvent()
    val submitOtpResult = MutableLiveData<Otp>().toSingleEvent()

    fun getOtp(idPatient: Int) {
        isLoading.postValue(true)
        getUser.getOtp(idPatient) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getOtpResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun submitOtp(idOtp: Int, code: String) {
        isLoading.postValue(true)
        getUser.submitOtp(idOtp, code) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> submitOtpResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

}