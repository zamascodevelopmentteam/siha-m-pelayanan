package hi.studio.msiha.ui.home.patient.detail.exam.history

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_HIV_RESULT
import hi.studio.msiha.data.constant.KEY_IMS_RESULT
import hi.studio.msiha.data.constant.KEY_VL_RESULT
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.detail.ExamViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.KEY_PATIENT
import hi.studio.msiha.ui.home.patient.detail.exam.HivExamAdapter
import kotlinx.android.synthetic.main.fragment_exam_history.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HivExamHistoryFragment : BaseFragment() {

    private val viewModel:ExamViewModel by viewModel()
    private lateinit var hivExamAdapter:HivExamAdapter
    private lateinit var imsExamAdapter:ImsExamHistoryAdapter
    private lateinit var vlExamAdapter:VlcExamHistoryAdapter
    private lateinit var patient:Patient

    override fun setView(): Int {
        return R.layout.fragment_exam_history
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            val examType = getString("exam")
            patient = getParcelable(KEY_PATIENT)
            examTitleLbl.text = "Riwayat Pemeriksaan $examType"
            when(examType){
                "HIV" -> patient.id?.let {
                    setupHivAdapter()
                    viewModel.getHivHistoryList(it) }
                "IMS" -> patient.id?.let {
                    setupImsAdapter()
                    viewModel.getImsHistoryList(it) }
                "VLC" -> patient.id?.let {
                    setupVlAdapter()
                    viewModel.getVlHistoryList(it) }
            }
        }
        observeViewModel()

    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(viewLifecycleOwner, Observer {
                refreshLayout.post {
                    refreshLayout.isRefreshing = it
                }
            })

            isError.observe(viewLifecycleOwner, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getHivHistoryListResult.observe(viewLifecycleOwner, Observer {
               // Toast.makeText(context,"exam history count ${it.size}",Toast.LENGTH_LONG).show()
                if(it.isNotEmpty()) {
                    contentLayout.visibility=View.VISIBLE
                    emptyLayout.visibility=View.GONE
                    hivExamAdapter.addAll(it)
                }else{
                    contentLayout.visibility=View.INVISIBLE
                    emptyLayout.visibility=View.VISIBLE
                }
            })


            getImsHistoryListResult.observe(viewLifecycleOwner, Observer {
                // Toast.makeText(context,"exam history count ${it.size}",Toast.LENGTH_LONG).show()
                if(it.isNotEmpty()) {
                    contentLayout.visibility=View.VISIBLE
                    emptyLayout.visibility=View.GONE
                    imsExamAdapter.addAll(it)
                }else{
                    contentLayout.visibility=View.INVISIBLE
                    emptyLayout.visibility=View.VISIBLE
                }
            })

            getVlHistoryListResult.observe(viewLifecycleOwner, Observer {
                // Toast.makeText(context,"exam history count ${it.size}",Toast.LENGTH_LONG).show()
                if(it.isNotEmpty()) {
                    contentLayout.visibility=View.VISIBLE
                    emptyLayout.visibility=View.GONE
                    vlExamAdapter.addAll(it)
                }else{
                    contentLayout.visibility=View.INVISIBLE
                    emptyLayout.visibility=View.VISIBLE
                }
            })
        }
    }

    private fun setupHivAdapter() {
        hivExamAdapter = HivExamAdapter()
        examList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = hivExamAdapter
        }
        hivExamAdapter.itemCallback { _: View, testHistory: HivTestHistory, _: Int ->
            if(findNavController().currentDestination?.id == R.id.HIVExamPreviewFragment2) {
                findNavController().navigate(R.id.hivHistoryDetailFragment, arguments?.apply {
                    putString("exam", "HIV")
                    putParcelable(KEY_HIV_RESULT, testHistory)
                })
            }else{
                findNavController().navigate(R.id.hivHistoryDetailFragment2, arguments?.apply {
                    putString("exam", "HIV")
                    putParcelable(KEY_HIV_RESULT, testHistory)
                })
            }
        }

    }

    private fun setupImsAdapter(){
        imsExamAdapter = ImsExamHistoryAdapter()
        examList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = imsExamAdapter
        }
        imsExamAdapter.itemCallback { _: View, testHistory: ImsTestHistory, _: Int ->
            Hawk.put(KEY_IMS_RESULT,testHistory.testIms)
            if(findNavController().currentDestination?.id == R.id.HIVExamPreviewFragment2) {
                findNavController().navigate(R.id.IMSExamPreviewFragment2, arguments?.apply {
                    putString("exam", "IMS")
                    putParcelable(KEY_IMS_RESULT, testHistory)
                })
            }else{
                findNavController().navigate(R.id.IMSExamPreviewFragment, arguments?.apply {
                    putString("exam", "IMS")
                    putParcelable(KEY_IMS_RESULT, testHistory)
                })
            }
        }
    }

    private fun setupVlAdapter(){
        vlExamAdapter = VlcExamHistoryAdapter()
        examList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = vlExamAdapter
        }
        vlExamAdapter.itemCallback { _: View, testHistory: VlTestHistory, _: Int ->
            Hawk.put(KEY_VL_RESULT,testHistory.testVL)
            if(findNavController().currentDestination?.id == R.id.HIVExamPreviewFragment2) {
                findNavController().navigate(R.id.VLExamPreviewFragment2, arguments?.apply {
                    putString("exam", "VLC")
                    putParcelable(KEY_VL_RESULT, testHistory.testVL)
                })
            }else{
                findNavController().navigate(R.id.VLExamPreviewFragment, arguments?.apply {
                    putString("exam", "VLC")
                    putParcelable(KEY_VL_RESULT, testHistory.testVL)
                })
            }
        }
    }
}