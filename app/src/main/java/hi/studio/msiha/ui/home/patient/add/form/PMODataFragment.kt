package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.extention.fromForm
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_form_pmo.*
import timber.log.Timber

class PMODataFragment : BaseFragment(), WizardStep {
    private val bundle = Bundle()
    private var isODHA = false

    companion object {
        const val KEY_PMO_NAME = "pmo_name"
        const val KEY_PMO_RELATION = "pmo_relation"
        const val KEY_PMO_PHONE = "pmo_phone"
        const val KEY_PMO_EMAIL = "pmo_email"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            PMODataFragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(R.id.pmoNameTxt, KEY_PMO_NAME, optional = isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.pmoEmailTxt, KEY_PMO_EMAIL, optional = isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.pmoPhoneTxt, KEY_PMO_PHONE, optional = isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.pmoRelationShipTXt, KEY_PMO_RELATION, optional = isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
        }
        val result = form.validate()
        result.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {

                }
            } else {
                bundle.apply {
                    putString(KEY_PMO_NAME, result[KEY_PMO_NAME]?.asString())
                    putString(KEY_PMO_EMAIL, result[KEY_PMO_EMAIL]?.asString())
                    putString(KEY_PMO_PHONE, result[KEY_PMO_PHONE]?.asString())
                    putString(KEY_PMO_RELATION, result[KEY_PMO_RELATION]?.asString())
                }
            }
        }
        return result.success()
    }

    override fun setView(): Int {
        return R.layout.fragment_form_pmo
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if (arguments == null) {
            if (requireParentFragment() is AddPatientFragment) {
                val bundle = (requireParentFragment() as AddPatientFragment).getUserBundle()
                isODHA = (requireParentFragment() as AddPatientFragment).isODHA
                displayData(bundle)
            }
        } else {
            displayData(arguments)
        }
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            Timber.d(bundle.toString())
            val name = getString(KEY_PMO_NAME)
            val phone = getString(KEY_PMO_PHONE)
            //val email = getString(KEY_PMO_EMAIL)
            val relation = getString(KEY_PMO_RELATION)
            pmoNameTxt.setText(name?.fromForm())
            pmoPhoneTxt.setText(phone?.fromForm())
            //pmoEmailTxt.setText(email)
            pmoRelationShipTXt.setText(relation)
        }
    }
}