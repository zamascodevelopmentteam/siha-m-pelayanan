package hi.studio.msiha.ui.home.pharmacist.recipe.medicine

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ChooseArvViewModel(
    private val getRecipe: GetRecipe
) : BaseViewModel() {
    val arvReagensResult = MutableLiveData<List<Reagen>>().toSingleEvent()
    val arvStockResult = MutableLiveData<List<ArvStock>>().toSingleEvent()

    fun getReagens() {
        getRecipe.getArvReagen{
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> arvReagensResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getArvMedicine(){
        getRecipe.getArvStock {
            isLoading.postValue(false)
            when(it){
                is Either.Left->arvStockResult.postValue(it.left)
                is Either.Right->isError.postValue(it.right)
            }
        }
    }
}