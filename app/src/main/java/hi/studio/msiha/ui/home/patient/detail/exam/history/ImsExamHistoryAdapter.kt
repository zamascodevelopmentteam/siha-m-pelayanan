package hi.studio.msiha.ui.home.patient.detail.exam.history

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.ui.home.patient.detail.exam.HivExamListViewHolder
import hi.studio.msiha.ui.home.patient.detail.exam.ImsExamListViewHolder
import kotlin.reflect.KProperty


class ImsExamHistoryAdapter:
    BaseAdapter<ImsTestHistory>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_exam_history
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return ImsExamListViewHolder(context, view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: ImsTestHistory,
        callback: OnItemClick<ImsTestHistory>?
    ) {
        if (holder is ImsExamListViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<ImsTestHistory>,
        new: MutableList<ImsTestHistory>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}