package hi.studio.msiha.ui.home.pharmacist.recipe.arv

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.Arv
import hi.studio.msiha.domain.model.MedicineInstruction
import kotlin.reflect.KProperty

class ArvAdapter(private val clickable:Boolean) : BaseAdapter<MedicineInstruction>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_arv
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return ArvViewHolder(view,clickable)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: MedicineInstruction,
        callback: OnItemClick<MedicineInstruction>?
    ) {
        (holder as ArvViewHolder).onBind(index, count, item, callback)
    }

    override fun compareDiffUtil(prop: KProperty<*>, old: MutableList<MedicineInstruction>, new: MutableList<MedicineInstruction>) {
        autoNotify(old, new) { o, n ->
            o.medicineId == n.medicineId
        }
    }
}