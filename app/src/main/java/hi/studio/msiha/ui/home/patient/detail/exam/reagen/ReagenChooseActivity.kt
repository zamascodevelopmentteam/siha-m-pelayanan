package hi.studio.msiha.ui.home.patient.detail.exam.reagen

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import kotlinx.android.synthetic.main.default_toolbar.*

class ReagenChooseActivity : BaseActivity() {
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun setView(): Int {
        return R.layout.activity_choose_reagen
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        setupNavController()
    }

    private fun setupNavController() {
        navController = Navigation.findNavController(this, R.id.chooseReagenNavHost)
        navController.setGraph(R.navigation.rr_reagen_nav_graph, intent.extras)
        appBarConfiguration = AppBarConfiguration.Builder()
            .setFallbackOnNavigateUpListener {
                onBackPressed()
                return@setFallbackOnNavigateUpListener false
            }
            .build()
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }
}