package hi.studio.msiha.ui.home.pharmacist.recipe.medicine

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_MEDICINE_INS
import hi.studio.msiha.domain.model.MedicineInstruction
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.utils.AddMedInstruction
import hi.studio.msiha.utils.AddMedInstructionBottomSheet
import kotlinx.android.synthetic.main.fragment_choose_arv.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChooseArvFragment : BaseFragment(), AddMedInstruction{

    private val viewModel by viewModel<ChooseArvViewModel>()
    private lateinit var adapter: ChooseArvAdapter
    private lateinit var medInstructionBottomSheet:AddMedInstructionBottomSheet
    private val args:ChooseArvFragmentArgs by navArgs()
    override fun setView(): Int {
        return R.layout.fragment_choose_arv
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        viewModel.getArvMedicine()
        refreshLayout.setOnRefreshListener {
            viewModel.getArvMedicine()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun addPrescription(addMedInstruction: MedicineInstruction) {
        medInstructionBottomSheet.dismiss()
        if(!Hawk.contains(KEY_MEDICINE_INS)){
            val arrayList= ArrayList<MedicineInstruction>()
            arrayList.add(addMedInstruction)
            Hawk.put(KEY_MEDICINE_INS,arrayList)
        }else{
            val arrayList:ArrayList<MedicineInstruction> = ArrayList()
            arrayList.addAll(Hawk.get(KEY_MEDICINE_INS))
            val idx = arrayList.indexOfFirst {
                it.medicineId==addMedInstruction.medicineId
            }
            if(idx==-1 && args.pos==-1){
                arrayList.add(addMedInstruction)
            }else{
                if(idx!=-1 && args.pos==-1){
                    arrayList[idx] = addMedInstruction
                }else if(idx!=-1 && args.pos!=-1 && idx!=args.pos){
                    arrayList.removeAt(idx)
                    arrayList[args.pos]=addMedInstruction
                }else if(idx==-1 && args.pos!=-1){
                    arrayList[args.pos]=addMedInstruction
                }
            }
            Log.d("key_medicine","delete coys")
            Hawk.delete(KEY_MEDICINE_INS)
            Hawk.put(KEY_MEDICINE_INS,arrayList)
        }
        findNavController().navigateUp()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@ChooseArvFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@ChooseArvFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            isMessage.observe(this@ChooseArvFragment, Observer {
                (requireActivity() as PatientDetailActivity).showMessage(it)
            })

            arvStockResult.observe(this@ChooseArvFragment,Observer{
                adapter.addAll(it)
            })
        }
    }

    private fun setupAdapter() {
        adapter = ChooseArvAdapter()
        arvList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@ChooseArvFragment.adapter
        }

        adapter.itemCallback { view, item, position ->
            when (view.id) {
                R.id.editBtn -> {
                    medInstructionBottomSheet = AddMedInstructionBottomSheet(item,this)
                    this@ChooseArvFragment.fragmentManager?.let {
                        medInstructionBottomSheet.show(
                            it,
                            "add_instruction"
                        )
                    }
                }
            }
        }
    }
}