package hi.studio.msiha.ui.home.patient.detail

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.datasource.remote.request.MedicineLeftRequest
import hi.studio.msiha.domain.model.VisitHistory
import kotlinx.android.synthetic.main.fragment_medicine_left.*
import kotlinx.android.synthetic.main.layout_visit_history.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MedicineLeftFragment : BaseFragment() {

    private val viewModel:MedicineLeftViewModel by viewModel()
    private var visitHistory:VisitHistory?=null

    override fun setView(): Int {
        return R.layout.fragment_medicine_left
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        observeViewModel()
        arguments?.apply {
            visitHistory = getParcelable(PatientDetailFragment.KEY_VISIT_HISTORY)
        }

        visitHistory?.id?.let { viewModel.getPreviousTreatment(it) }
        //visitHistory?.let { it.patient?.id?.let { it1 -> viewModel.getLastGivenPrescription(it1) } }

        saveBtn.setOnClickListener {
            val medicineLeftRequest = MedicineLeftRequest(
                etSisaArv1.text.toString().toInt(),
                etSisaArv2.text.toString().toInt(),
                etSisaArv3.text.toString().toInt()
            )

            visitHistory?.id?.let { it1 -> viewModel.updateSisaObat(it1,medicineLeftRequest) }
        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@MedicineLeftFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@MedicineLeftFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            getPreviousTreatmentResult.observe(this@MedicineLeftFragment, Observer {
                if(it.data==null || it.data.treatment==null){
                    layoutNotEmpty.visibility=View.INVISIBLE
                    layoutEmptyState.visibility=View.VISIBLE
                }else{
                    layoutNotEmpty.visibility=View.VISIBLE
                    layoutEmptyState.visibility=View.GONE
                    if(it.data.treatment.obatArv1Data!=null){
                        layoutArv1.visibility=View.VISIBLE
                        tvNamaArv1.text=it.data.treatment.obatArv1Data.name
                        etSisaArv1.setText("${it.data.treatment.sisaObatArv1?:0}")
                    }
                    if(it.data.treatment.obatArv2Data!=null){
                        layoutArv2.visibility=View.VISIBLE
                        tvNamaArv2.text=it.data.treatment.obatArv2Data.name
                        etSisaArv2.setText("${it.data.treatment.sisaObatArv2?:0}")
                    }
                    if(it.data.treatment.obatArv3Data!=null){
                        layoutArv3.visibility=View.VISIBLE
                        tvNamaArv3.text=it.data.treatment.obatArv3Data.name
                        etSisaArv3.setText("${it.data.treatment.sisaObatArv3?:0}")
                    }
                }
            })

//            getLastGivenPresc.observe(this@MedicineLeftFragment,Observer{
//                if(it!=null){
//                    if(it.prescriptionMedicines.isNotEmpty())
//                }else{
//
//                }
//                if(it==null || it.prescriptionNumber?.isEmpty()){
//                    layoutNotEmpty.visibility=View.INVISIBLE
//                    layoutEmptyState.visibility=View.VISIBLE
//                }else{
//                    layoutEmptyState.visibility=View.GONE
//                    if(it.data.treatment.obatArv1Data!=null){
//                        layoutArv1.visibility=View.VISIBLE
//                        tvNamaArv1.text=it.data.treatment.obatArv1Data.name
//                        etSisaArv1.setText("${it.data.treatment.sisaObatArv1?:0}")
//                    }
//                    if(it.data.treatment.obatArv2Data!=null){
//                        layoutArv2.visibility=View.VISIBLE
//                        tvNamaArv2.text=it.data.treatment.obatArv2Data.name
//                        etSisaArv2.setText("${it.data.treatment.sisaObatArv2?:0}")
//                    }
//                    if(it.data.treatment.obatArv3Data!=null){
//                        layoutArv3.visibility=View.VISIBLE
//                        tvNamaArv3.text=it.data.treatment.obatArv3Data.name
//                        etSisaArv3.setText("${it.data.treatment.sisaObatArv3?:0}")
//                    }
//                }
//            })

            updateSisaObatResult.observe(viewLifecycleOwner,Observer{
                findNavController().navigateUp()
            })
        }
    }
}