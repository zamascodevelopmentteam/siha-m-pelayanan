package hi.studio.msiha.ui.home.patient.edit

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.KEY_PATIENT
import kotlinx.android.synthetic.main.fragment_patient_data.*

class PatientDataFragment : BaseFragment() {

    private var patient: Patient? = null
    private var adapter: PatientDataPagerAdapter? = null

    override fun setView(): Int {
        return R.layout.fragment_patient_data
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        patient = arguments?.getParcelable(KEY_PATIENT)
        setupPatientPagerAdapter()
        setHasOptionsMenu(true)
    }

    private fun setupPatientPagerAdapter() {
        adapter = PatientDataPagerAdapter(childFragmentManager, arguments)
        pager.adapter = adapter
        tabLayout.setupWithViewPager(pager)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        if (AppSIHA.instance.getUser()?.role == KEY_ROLE_RR) {
            inflater.inflate(R.menu.menu_patient_data, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_edit -> findNavController().navigate(R.id.editPatientDataFragment, arguments)
        }
        return super.onOptionsItemSelected(item)
    }

    inner class PatientDataPagerAdapter(fm: FragmentManager, val bundle: Bundle?) :
        FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val fragments = mutableListOf<Fragment>()
        private val pageTitles = mutableListOf<String>()

        init {
            fragments.add(PatientPersonalDataFragment.newInstance(bundle))
            pageTitles.add(getString(R.string.title_personal_data))

            if (patient?.statusPatient == TYPE_ODHA) {
                fragments.add(PatientPmoDataFragment.newInstance(bundle))
                pageTitles.add(getString(R.string.title_pmo_data))
            }
        }

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size

        override fun getPageTitle(position: Int): CharSequence? = pageTitles[position]
    }
}