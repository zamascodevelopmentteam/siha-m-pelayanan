package hi.studio.msiha.ui.home.profile

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetAuth
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ProfileViewModel(private val getUser: GetUser, private val getAuth: GetAuth) :
    BaseViewModel() {
    val userResult = MutableLiveData<User>().toSingleEvent()
    val logoutResult = MutableLiveData<String>().toSingleEvent()
    val logoutErrorResult = MutableLiveData<Throwable>().toSingleEvent()

    fun getProfile(id: Int) {
        getUser.getProfile(id) {
            when (it) {
                is Either.Left -> userResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun logout() {
        isLoading.postValue(true)
        getAuth.logout {
            when (it) {
                is Either.Left -> logoutResult.postValue(it.left)
                is Either.Right -> logoutErrorResult.postValue(it.right)
            }
        }
    }
}