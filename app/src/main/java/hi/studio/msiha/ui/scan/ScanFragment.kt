package hi.studio.msiha.ui.scan

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_QR_RESULT
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.RecipeDetail
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.pharmacist.dashboard.PharmacistDashboardFragment
import hi.studio.msiha.ui.home.recipe.detail.RecipeDetailFragment
import hi.studio.msiha.ui.scan.ScanActivity.Companion.TYPE_PATIENT
import hi.studio.msiha.ui.scan.ScanActivity.Companion.TYPE_PATIENT_DETAIL
import hi.studio.msiha.ui.scan.ScanActivity.Companion.TYPE_RECIPE
import hi.studio.msiha.ui.scan.ScanActivity.Companion.TYPE_VISIT
import kotlinx.android.synthetic.main.fragment_scan.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class ScanFragment : BaseFragment(), ZXingScannerView.ResultHandler {
    private val viewModel by viewModel<ScanViewModel>()
    private var type: Int = 0
    private var visitSheet: PatientVisitBottomSheetFragment? = null
    private var recipeDetail: RecipeDetail? = null
    private var visit: Visit? = null

    companion object {
        const val MODE_USER_PROFILE = 0
        const val MODE_PATIENT_DETAIL = 1
    }

    override fun setView(): Int {
        return R.layout.fragment_scan
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            recipeDetail = this[RecipeDetailFragment.KEY_RECIPE_DETAIL] as RecipeDetail?
            val type = this["scan_type"] as Int
            this@ScanFragment.type = type
            initUI(type)
        }
        initScanner()
        registerPatientBtn.setOnClickListener {
            when (type) {
                TYPE_PATIENT -> {
                    NavHostFragment.findNavController(this@ScanFragment)
                        .navigate(R.id.addPatientFragment, Bundle())
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@ScanFragment, Observer {
                (requireActivity() as ScanActivity).showLoading(it)
            })

            isError.observe(this@ScanFragment, Observer {
                (requireActivity() as ScanActivity).showError(it)
            })

            userResult.observe(this@ScanFragment, Observer {
                observeUser(it)
            })

            patientResult.observe(this@ScanFragment, Observer {
                if(it!=null) {
                    observePatient(it)
                }else{
                    Toast.makeText(context,"Pasien tidak ditemukan",Toast.LENGTH_SHORT).show()
                    startCameraWithPermissionCheck()
                }
            })

            patientDetailResult.observe(this@ScanFragment, Observer {
                visitSheet?.dismiss()
                requireActivity().startActivity<PatientDetailActivity>(
                    Pair(
                        PatientDetailActivity.KEY_PATIENT,
                        it
                    ),
                    Pair(
                        PatientDetailActivity.KEY_VISIT,
                        visit
                    ),
                    Pair(
                        PatientDetailActivity.KEY_MODE,
                        PatientDetailActivity.MODE_VISIT
                    )
                )
                requireActivity().finish()
            })

            visitResult.observe(this@ScanFragment, Observer {
                visitSheet = PatientVisitBottomSheetFragment()
                visitSheet?.apply {
                    isCancelable = false
                    setCallback {
                        visit = it
                        viewModel.getProfile(it.userId ?: 0, MODE_PATIENT_DETAIL)
                    }
                }
                visitSheet?.show(childFragmentManager, "visit")
            })

            visitErrorResult.observe(this@ScanFragment, Observer {
                startCameraWithPermissionCheck()
            })

            giveRecipeResult.observe(this@ScanFragment, Observer {
                requireActivity().toast(it)
                requireActivity().finish()
            })

            recipeDetailResult.observe(this@ScanFragment, Observer {
                if (it == null) {
                    showMessage("Resep Tidak Ditemukan")
                    startCameraWithPermissionCheck()
                } else {
                    requireActivity().setResult(
                        Activity.RESULT_OK,
                        Intent().putExtra(PharmacistDashboardFragment.KEY_RECIPE_DETAIL, it)
                    )
                    requireActivity().finish()
                }
            })

            createVisitResult.observe(viewLifecycleOwner,Observer{
                it?.let {
                    Log.d("testing", "test ${it.message}")
                    Log.d("test","testing ${it.data.ordinal}")
                    val bundle = bundleOf(PatientDetailFragment.KEY_VISIT_HISTORY to it.data)
                    //Bundle().putParcelable(PatientDetailFragment.KEY_VISIT_HISTORY, it.data)
                    if (it.data.visitType == "TEST") {
                        findNavController().navigate(R.id.visitDetailFragment2,bundle)
                    } else {
                        findNavController().navigate(R.id.visitDetailRRFragment2,bundle)
                    }
                }
            })
        }
    }

    private fun observePatient(patient: Patient) {
        (requireActivity() as ScanActivity).showLoading(false)
        requireActivity().setResult(
            Activity.RESULT_OK, Intent().putExtra(
                PatientDetailActivity.KEY_PATIENT, patient
            )
        )
        Hawk.put(KEY_QR_RESULT,patient)
        requireActivity().finish()
    }

    private fun observeUser(user: User) {
        (requireActivity() as ScanActivity).showLoading(false)
        when (type) {
            TYPE_PATIENT -> {
                NavHostFragment.findNavController(this@ScanFragment)
                    .navigate(R.id.patientPreviewFragment, Bundle().apply {
                        putParcelable("user", user)
                    })
            }
        }
    }

    private fun initScanner() {
        scanner.apply {
            setFormats(listOf(BarcodeFormat.QR_CODE))
            setAutoFocus(true)
            if (Build.MANUFACTURER.equals("HUAWEI", ignoreCase = true)) {
                setAspectTolerance(0.5f)
            }
            setResultHandler(this@ScanFragment)
        }
    }

    private fun initUI(type: Int) {
        registerPatientLbl.visibility = View.GONE
        registerPatientBtn.text = "Lanjut"
        registerPatientBtn.isEnabled = false
        when (type) {
            TYPE_PATIENT -> {
                registerPatientLbl.visibility = View.VISIBLE
                registerPatientBtn.text = "Buatkan Akun"
                registerPatientBtn.isEnabled = true
                (requireActivity() as ScanActivity).supportActionBar?.title = "Tambah Pasien"
            }
            TYPE_VISIT, TYPE_RECIPE, TYPE_PATIENT_DETAIL -> {
                (requireActivity() as ScanActivity).supportActionBar?.title = "Pindai QR Code"
            }
        }
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    fun startCamera() {
        scanner.apply {
            startCamera()
            resumeCameraPreview(this@ScanFragment)
        }
    }

    @SuppressLint("NoDelegateOnResumeDetector")
    override fun onResume() {
        super.onResume()
        startCameraWithPermissionCheck()
    }

    override fun onPause() {
        scanner.stopCamera()
        super.onPause()
    }

    override fun handleResult(p0: Result?) {
        when (type) {
            TYPE_PATIENT -> {
                p0?.text?.let {
                    viewModel.getProfile(it.toIntOrNull() ?: 0, MODE_USER_PROFILE)
                }
            }
            TYPE_VISIT -> {
                p0?.text?.let {
                    //Log.d("testing","patientId : $it")
                    viewModel.getPatientByNIK(it)
                }
            }

            TYPE_RECIPE -> {
                when (AppSIHA.instance.getUser()?.role) {
                    KEY_ROLE_RR -> {
                        recipeDetail?.let {
                            viewModel.giveRecipe(1, it.userId, it.id)
                        }
                    }
                    KEY_ROLE_PHARMACIST -> {
                        p0?.text?.let {
                            viewModel.getRecipeDetail(it.toIntOrNull() ?: 0)
                        }
                    }
                }
            }
            TYPE_PATIENT_DETAIL -> {
                p0?.text?.let {
                    viewModel.getPatientByNIK(it)
                }
            }
        }
    }
}