package hi.studio.msiha.ui.home.patient.overview

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.findNavController
import com.afollestad.vvalidator.form
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitOverview
import hi.studio.msiha.ui.LocalSharedViewModel
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.toFormat
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_overview.*
import kotlinx.android.synthetic.main.fragment_visit_history_info.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*
import java.text.SimpleDateFormat


class OverviewFragment : BaseFragment(), WizardStep{
    private var bundle = Bundle()

    companion object {
        const val KEY_TREATMENT_DATE = "tanggal_pemeriksaan"
        const val KEY_REGNAS = "REGNAS"
        const val KEY_HEIGHT="height"
        const val KEY_WEIGHT="width"
        const val KEY_FUNCTIONAL_STATUS = "functional_status"
        const val KEY_CLINICAL_STADIUM = "clinical_status"

        @JvmStatic
        fun newInstance() =
            OverviewFragment().apply {
            }
    }
    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(dateTreatmentTxt, KEY_TREATMENT_DATE){
                isNotEmpty().description("Kolom harus diisi")
            }
//            input(R.id.lsmTxt, Overview.KEY_LSM, true) {
//                isNotEmpty().description(R.string.empty_field)
//            }
//            input(R.id.fasyankesTxt, KEY_FASYANKES) {
//                isNotEmpty().description(R.string.empty_field)
//            }
        }
        val result = form.validate()
        result.let {
            if (it.hasErrors()) {
            } else {
                val weight = if(weightTxt.text.toString().isNullOrEmpty()){
                    0
                }else{
                    weightTxt.text.toString().toInt()
                }

                val height=if(heightTxt.text.toString().isNullOrEmpty()){
                    0
                }else{
                    heightTxt.text.toString().toInt()
                }
                bundle.apply {
                    putString(KEY_TREATMENT_DATE, dateTreatmentTxt.tag.toString())
                    putString(KEY_REGNAS, regnasTxt.text.toString())
                    putString(KEY_FUNCTIONAL_STATUS, statusFunctionSpn.selectedItem.toString())
                    putString(KEY_CLINICAL_STADIUM, clinicalStadiumSpn.selectedItem.toString())
                    putInt(KEY_HEIGHT,height)
                    putInt(KEY_WEIGHT,weight)
                }
            }
        }
        return result.success()
    }

    private val viewModel by sharedViewModel<LocalSharedViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_overview
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if(Hawk.get<VisitDetail>(VisitDetailRRFragment.KEY_VISIT_DETAIL)!=null){
            val visitDetail:VisitDetail?=Hawk.get(VisitDetailRRFragment.KEY_VISIT_DETAIL)
            if(visitDetail?.checkOutDate!=null){
                dateTreatmentTxt.isEnabled=false
                regnasTxt.isEnabled=false
                heightTxt.isEnabled=false
                weightTxt.isEnabled=false
                statusFunctionSpn.isEnabled=false
                clinicalStadiumSpn.isEnabled=false
            }
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            var date =""
            if(visitDetail?.treatment?.treatementStartDate!=null) {
                date = dateFormat.parse(visitDetail?.treatment?.treatementStartDate).toFormat()
            }
            dateTreatmentTxt.tag=visitDetail?.treatment?.treatementStartDate
            dateTreatmentTxt.setText(date)
            regnasTxt.setText(visitDetail?.treatment?.noRegNas?:"")
            heightTxt.setText(visitDetail?.treatment?.height?:"")
            weightTxt.setText(visitDetail?.treatment?.weight?:"")
            Log.d("test","testing")
        }else{
            Log.d("test","hawk empty")
        }

        dateTreatmentTxt.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, s: String, s1: String ->
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                dateTreatmentTxt.tag=sdf.format(date)
                dateTreatmentTxt.setText(s1)
            }
        }

        buttonSave.setOnClickListener {
            viewModel.setOverview(
                VisitOverview(
                    noRegNas = regnasTxt.text.toString(),
                    treatementStartDate = dateTreatmentTxt.tag as String?
                )
            )
            findNavController().navigateUp()
        }
    }
}