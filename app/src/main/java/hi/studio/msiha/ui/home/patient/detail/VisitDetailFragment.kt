package hi.studio.msiha.ui.home.patient.detail

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_HIV_RESULT
import hi.studio.msiha.data.constant.KEY_IMS_RESULT
import hi.studio.msiha.data.constant.KEY_QR_RESULT
import hi.studio.msiha.data.constant.KEY_VL_RESULT
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.odhaData
import kotlinx.android.synthetic.main.fragment_visit_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class VisitDetailFragment : BaseFragment() {
    private var visitHistory: VisitHistory? = null
    private var patient: Patient? = null

    private val viewModel:VisitDetailViewModel by viewModel()

    override fun setView(): Int {
        return R.layout.fragment_visit_detail
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            visitHistory = getParcelable(PatientDetailFragment.KEY_VISIT_HISTORY)
            patient = getParcelable(PatientDetailActivity.KEY_PATIENT)
            visitHistory?.apply {
                ordinalLbl.text = "Data Kunjungan ke-$ordinal"
                Hawk.put(KEY_VL_RESULT,this.testVl)
                viewModel.getVisitDetailByVisitId(this.id?:0)
                patient?.let {
                    odhaData.ordinal = ordinal
                    odhaData.patientId = it.id
                    odhaData.date = visitDate
                }
              }
        }

        hivExamBtn.setOnClickListener {
            if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
                findNavController().navigate(R.id.HIVExamPreviewFragment2, arguments)
            else
                findNavController().navigate(R.id.HIVExamFragmentRR, updateArgument(ExamFragment.TYPE_HIV))
        }

        hivButton.setOnClickListener {
            if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
                findNavController().navigate(R.id.HIVExamPreviewFragment2, arguments)
            else
                findNavController().navigate(R.id.HIVExamFragmentRR, updateArgument(ExamFragment.TYPE_HIV))
        }

        imsButton.setOnClickListener {
            if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
                findNavController().navigate(R.id.IMSExamPreviewFragment2, arguments)
            else
                findNavController().navigate(R.id.IMSExamFragmentRR, updateArgument(ExamFragment.TYPE_IMS))
        }

        imsExamBtn.setOnClickListener {
            if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
            findNavController().navigate(R.id.IMSExamPreviewFragment2, arguments)
            else
                findNavController().navigate(R.id.IMSExamFragmentRR, updateArgument(ExamFragment.TYPE_IMS))
        }
        if(visitHistory?.checkOutDate!=null){
            endVisitButton.visibility=View.GONE
        }
        endVisitButton.setOnClickListener {
            viewModel.endVisit(visitHistory?.id?:0)
        }
        if(Hawk.contains(KEY_QR_RESULT)) Hawk.delete(KEY_QR_RESULT)
        observeLiveData()
    }

    private fun updateArgument(type: String?): Bundle? {
        val bundle = Bundle().apply {
            patient?.apply {
                putInt(ExamFragment.KEY_ID, id ?: 0)
                putInt(ExamFragment.KEY_HOSPITAL_ID, upkId ?: 0)
            }
            visitHistory?.apply {
                id?.let {
                    putInt(ExamFragment.KEY_ID_VISIT, id)
                }
            }
            putString(ExamFragment.KEY_EXAM_TYPE, type)
        }
        return arguments?.apply {
            putBundle(ExamFragment.KEY_EXAM_DATA, bundle)
        }
    }

    private fun observeLiveData(){
        viewModel.apply {
            isLoading.observe(this@VisitDetailFragment, Observer {
                if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
                    (requireActivity() as MainActivity).showLoading(it)
                else
                    (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@VisitDetailFragment, Observer {
                if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
                    (requireActivity() as MainActivity).showError(it)
                else
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            isMessage.observe(this@VisitDetailFragment, Observer {
                if(findNavController().currentDestination?.id == R.id.visitDetailFragment2)
                    (requireActivity() as MainActivity).showMessage(it)
                else
                (requireActivity() as PatientDetailActivity).showMessage(it)
            })

            endVisitResult.observe(viewLifecycleOwner,Observer{
                findNavController().navigateUp()
            })

            getVisitDetailResult.observe(viewLifecycleOwner,Observer{
                Hawk.put(KEY_IMS_RESULT,it.testIms)
                Hawk.put(KEY_HIV_RESULT,it.testHiv)
            })
        }
    }
}