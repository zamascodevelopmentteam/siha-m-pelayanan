package hi.studio.msiha.ui.home.pharmacist.recipe.arv

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.request.ARVRequest
import hi.studio.msiha.data.datasource.remote.request.MedicinePrescriptionRequest
import hi.studio.msiha.data.datasource.remote.response.VisitAllDetailResponse
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Prescription
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ArvViewModel(
    private val getRecipe: GetRecipe,
    private val getVisit: GetVisit
) : BaseViewModel() {
    val arvResult = MutableLiveData<String>().toSingleEvent()
    val createPrescriptionResult = MutableLiveData<String>().toSingleEvent()
    val giveMedicineResult = MutableLiveData<String>().toSingleEvent()
    val getPreviousVisitResult = MutableLiveData<VisitAllDetailResponse>().toSingleEvent()
    val getVisitDetailResult = MutableLiveData<VisitDetail>().toSingleEvent()
    val getLastGivenMedicine = MutableLiveData<Prescription>().toSingleEvent()

    fun createArv(request: ARVRequest) {
        isLoading.postValue(true)
        getRecipe.createArvRecipe(request) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> arvResult.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }

    fun getLastGivenPrescription(patientId:Int){
        isLoading.postValue(true)
        getRecipe.getLastGivenMedicine(patientId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getLastGivenMedicine.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }

    fun getVisitDetailByVisitId(visitId:Int){
        isLoading.postValue(true)
        getVisit.getVisitAllDetail(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getVisitDetailResult.postValue(it.left.data)
                is Either.Right ->isError.postValue(it.right)
            }
        }

    }

    fun getPreviousVisit(visitId: Int){
        isLoading.postValue(true)
        getVisit.getPreviousTreatment(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getPreviousVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createPrescription(request:MedicinePrescriptionRequest,visitId:Int){
        isLoading.postValue(true)
        getRecipe.createPrescription(request,visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createPrescriptionResult.postValue(it.left)
                is Either.Right->{
                    isError.postValue(it.right)
                }
            }
        }
    }

    fun giveMedicine(visitId:Int){
        isLoading.postValue(true)
        getRecipe.giveMedicine(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> giveMedicineResult.postValue(it.left)
                is Either.Right->{
                    isError.postValue(it.right)
                }
            }
        }
    }

}