package hi.studio.msiha.ui.home.patient.add

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseBottomSheetFragment
import hi.studio.msiha.domain.model.Patient
import kotlinx.android.synthetic.main.pop_up_pasient_exist.*

class PatientExistBottomSheetFragment(private val patient: Patient) : BaseBottomSheetFragment() {
    private var callback: (patient: Patient?) -> Unit = {}

    override fun setView(): Int {
        return R.layout.pop_up_pasient_exist
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        patient.apply {
            patientInfoLbl.text = "Nama: $name" +
                    "\nRegnas: " +
                    "\nNIK: $nik"
        }
        noBtn.setOnClickListener {
            callback(null)
        }
        yesBtn.setOnClickListener {
            callback(patient)
        }
    }

    fun setCallback(callback: (patient: Patient?) -> Unit) {
        this.callback = callback
    }
}