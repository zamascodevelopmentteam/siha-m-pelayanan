package hi.studio.msiha.ui.home.visit.visitplan

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.extention.toDateFormat
import kotlinx.android.synthetic.main.item_today_visit.view.*


class VisitPlanViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<VisitHistory>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: VisitHistory,
        callback: BaseAdapter.OnItemClick<VisitHistory>?
    ) {
        item.apply {
            //TODO handle kasus data lengkap dan kurang lengkap
            view.tvPatientName.text = this.patient?.name
            view.tvVisitOrdinal.text="Kunjungan ke-${this.ordinal}"
            view.tvVisitDate.text=this.visitDate.toDateFormat()
            view.tvVisitStatus.visibility=View.GONE
            showDataCompletionStatus(this.needConfirmation?:false,view)

        }

        view.setOnClickListener {
//            callback?.onClick(it, item)
        }
    }

    private fun showDataCompletionStatus(needConfirmation:Boolean,view: View){
        view.tvDataReqComplete.visibility=View.GONE
        if(needConfirmation){
            view.tvDataReqNotComplete.text="Menunggu Konfirmasi"
            view.tvDataReqNotComplete.visibility= View.VISIBLE
        }else{
            view.tvDataReqNotComplete.visibility= View.INVISIBLE
        }
    }
}