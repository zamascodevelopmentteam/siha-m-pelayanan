package hi.studio.msiha.ui.home.pharmacist.recipe.arv

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Arv
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.ui.LocalSharedViewModel
import kotlinx.android.synthetic.main.fragment_add_arv_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AddArvDetailFragment : BaseFragment() {
    private val viewModel by sharedViewModel<LocalSharedViewModel>()
    private var reagen: Reagen? = null

    override fun setView(): Int {
        return R.layout.fragment_add_arv_detail
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            reagen = getParcelable("reagen")
            val arv = getParcelable<Arv>("arv")
            reagen?.let {
                arvNameLbl.text = it.name
                arvTypeTxt.setText(it.codeName)
                arvTypeTxt.isEnabled = false
                arvStockLbl.text = it.sum
                arvUnitLbl.text = it.stockUnitType
                arvUnit2Lbl.text = it.stockUnitType
            }
            arv?.apply {
                reagen = Reagen(
                    id = id,
                    name = name,
                    stockUnitType = medicineUnit,
                    sum = stock.toString(),
                    codeName = type
                )
                arvDaysTxt.setText(days.toString())
                arvQtyTxt.setText(medicineAmount.toString())
                arvTypeTxt.setText(type)
            }
        }

        saveBtn.setOnClickListener {
            reagen?.let {
                viewModel.setArv(
                    Arv(
                        it.id,
                        it.name,
                        arvDaysTxt.text.toString().toIntOrNull(),
                        arvQtyTxt.text.toString().toIntOrNull(),
                        it.stockUnitType,
                        it.codeName,
                        null,
                        it.sum?.toIntOrNull()
                    )
                )
                findNavController().navigateUp()
            }
        }
    }
}