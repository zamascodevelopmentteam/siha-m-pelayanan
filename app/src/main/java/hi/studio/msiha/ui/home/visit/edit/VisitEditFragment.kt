package hi.studio.msiha.ui.home.visit.edit

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.LocalSharedViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.KEY_VISIT_DETAIL
import hi.studio.msiha.ui.home.patient.overview.OverviewFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormCoupleFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormCoupleFragment.Companion.KEY_COUPLES
import hi.studio.msiha.ui.home.visit.edit.form.FormTBCFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormVisitInfoFragment
import hi.studio.msiha.utils.wizard.WizardManager
import kotlinx.android.synthetic.main.fragment_edit_visit.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class VisitEditFragment : BaseFragment() {
    private val viewModel by sharedViewModel<LocalSharedViewModel>()
    private val visitEditViewModel:VisitEditViewModel by viewModel()
    private lateinit var wm: WizardManager
    private lateinit var adapter: VisitEditPagerAdapter
    var visitHistory: VisitHistory? = null
    var visitDetail: VisitDetail? = null

    override fun setView(): Int {
        return R.layout.fragment_edit_visit
    }

    override fun onDestroy() {
        super.onDestroy()
        if(Hawk.contains(KEY_COUPLES)) Hawk.delete(KEY_COUPLES)
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            visitHistory = getParcelable(PatientDetailFragment.KEY_VISIT_HISTORY)
            visitEditViewModel.getVisitDetailByVisitId(visitHistory?.id?:0)
        }

        observeLiveData()
        saveBtn.setOnClickListener {
            val result = wm.getResult()
            visitDetail = Hawk.get<VisitDetail>(KEY_VISIT_DETAIL)
            Log.d("couples","is null? ${result[KEY_COUPLES]}")
            var tempCouple:ArrayList<ODHAVisitRequest.Couple> = Hawk.get(KEY_COUPLES)?:ArrayList()
            var listCouple=ArrayList<ODHAVisitRequest.Couple>()


            var akhirFollowUp:String = Hawk.get("follow_up")
            if(akhirFollowUp=="Tidak Ada"){
                akhirFollowUp=""
            }
            var dateRujukKeluar:String? = null
            var dateMeninggal:String? = null
            var dateBerhentiArv:String? = null
            when(akhirFollowUp){
                "RUJUK KELUAR"->{
                    dateRujukKeluar=Hawk.get("follow_up_date")
                }
                "MENINGGAL"->{
                    dateMeninggal=Hawk.get("follow_up_date")
                }
                "BERHENTI ARV"->{
                    dateBerhentiArv=Hawk.get("follow_up_date")
                }
            }
            var notifCouple = result[FormCoupleFragment.KEY_NOTIFICATION_STATUS] as String?
            Log.d("notif kopel","notif : ${notifCouple}")
            Log.d("notif kopel","temp couple : ${tempCouple.size}")
            Log.d("notif kopel","visit couple : ${ visitDetail?.treatment?.couples?.size}")
            if(notifCouple==null){
               notifCouple =visitDetail?.treatment?.notifikasiPasangan
            }
            if(notifCouple=="Menerima"){
                listCouple = if(tempCouple==null || tempCouple.size==0){
                    visitDetail?.treatment?.couples as ArrayList<ODHAVisitRequest.Couple>
                }else{
                    tempCouple
                }
            }
            var notifikasiPasangan:String? =
                when(notifCouple){
                    "Menerima" -> "menerima"
                    "Menolak" -> "menolak"
                    "Tidak memenuhi syarat" -> "tidak memenuhi syarat"
                    else -> ""
                }
            Log.d("notif kopel","temp couple : ${listCouple.size}")
            val visitData = VisitData(
                0,
                0,
                treatmentStartDate = result[OverviewFragment.KEY_TREATMENT_DATE] as String?,
                height = result[OverviewFragment.KEY_HEIGHT] as Int?,
                weight = result[OverviewFragment.KEY_WEIGHT] as Int?,
                //lsmPenjangkau = result[FormVisitInfoFragment.KEY_LSM] as String?,
                lsmPenjangkau = visitDetail?.treatment?.lsmPenjangkau,
                noRegNas = result[OverviewFragment.KEY_REGNAS] as String?,
                //tglKonfirmasiHivPos = result[FormVisitInfoFragment.KEY_CONFIRM_DATE] as String?,
                tglKonfirmasiHivPos = visitDetail?.treatment?.tglKonfirmasiHivPos,
                //tglKunjungan = result[FormVisitInfoFragment.KEY_VISIT_DATE] as String?,
                tglKunjungan = visitDetail?.treatment?.tglKunjungan,
                tglRujukMasuk = result[FormVisitInfoFragment.KEY_REFER_IN_DATE] as String?,
                kelompokPopulasi = result[FormVisitInfoFragment.KEY_POPULATION] as String?,
                ppk = result[FormTBCFragment.KEY_PPK] as Boolean?,
                tglPengobatanTb = result[FormTBCFragment.KEY_TREATMENT_DATE] as String?,
                statusFungsional = result[OverviewFragment.KEY_FUNCTIONAL_STATUS] as String?,
                stadiumKlinis = result[OverviewFragment.KEY_CLINICAL_STADIUM] as String?,
                statusTb = result[FormTBCFragment.KEY_STATUS_TB] as String?,
                tglPemberianPpk = result[FormTBCFragment.KEY_PPK_DATE] as String?,
                statusTbTpt = result[FormTBCFragment.KEY_THERAPY_STATUS] as String?,
                tglPemberianTbTpt = result[FormTBCFragment.KEY_THERAPY_DATE] as String?,
                notifikasiPasangan = notifikasiPasangan,
                couples = listCouple,
                akhirFollowUp = akhirFollowUp,
                tglBerhentiArv = dateBerhentiArv,
                tglMeninggal = dateMeninggal,
                tglRujukKeluar = dateRujukKeluar
            )
            viewModel.setVisitData(visitData)
            visitEditViewModel.saveTreatmentData(visitHistory?.id?:0,visitData)

        }
    }

    private fun observeLiveData(){
        visitEditViewModel.apply {
            isLoading.observe(this@VisitEditFragment, Observer {

            })

            isError.observe(this@VisitEditFragment, Observer {
               Toast.makeText(context,"Gagal menyimpan data e",Toast.LENGTH_SHORT).show()
            })

            saveTreatmentResult.observe(this@VisitEditFragment, Observer {
                it?.let {
                    findNavController().navigateUp()
                }
            })

            getVisitDetailResult.observe(viewLifecycleOwner,Observer{
                    visitDetail=it
                Log.d("put","put visit detail")
                    Hawk.put(KEY_VISIT_DETAIL,it)
                    setupWizardForm()
            })
        }
    }

    private fun setupWizardForm() {
        Log.d("visit-detail","visit detail ${visitDetail}")
        adapter = VisitEditPagerAdapter(childFragmentManager,arguments)
        pager.adapter = adapter
        pagerIndicator.setViewPager(pager)
        tabLayout.setupWithViewPager(pager)
        wm = WizardManager(pager, prevBtn, nextBtn)
            .setOnWizardChangeListener { position, _ ->
                when (position) {
                    0 -> {
                        prevBtn.visibility = View.GONE
                    }
                    4 -> {
                        nextBtn.visibility = View.GONE
                        if(visitDetail?.checkOutDate==null) {
                            saveBtn.visibility = View.VISIBLE
                        }else{
                            saveBtn.visibility=View.INVISIBLE
                        }
                    }
                    else -> {
                        prevBtn.visibility = View.VISIBLE
                        nextBtn.visibility = View.VISIBLE
                        saveBtn.visibility = View.GONE
                    }
                }
            }
            .build()
    }
}