package hi.studio.msiha.ui.home.patient.activation

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseBottomSheetFragment
import hi.studio.msiha.domain.model.User
import kotlinx.android.synthetic.main.pop_up_pasient_activation.*
import org.jetbrains.anko.toast

class PatientActivationBottomSheetFragment(private val user: User) : BaseBottomSheetFragment() {
    private var callback: (password: String?) -> Unit = {}

    override fun setView(): Int {
        return R.layout.pop_up_pasient_activation
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        user.apply {
            patientInfoLbl.text = "Nama: $name" +
                    "\nRegnas: " +
                    "\nNIK: $nik"
        }

        noBtn.setOnClickListener {
            callback(null)
        }

        yesBtn.setOnClickListener {
            if (patientPasswordTxt.text.toString().equals(
                    patientPasswordConfirmTxt.text.toString(),
                    true
                )
            ) {
                callback(patientPasswordTxt.text.toString())
            } else {
                requireActivity().toast("Password tidak sama")
            }
        }
    }

    fun setCallback(callback: (password: String?) -> Unit) {
        this.callback = callback
    }
}