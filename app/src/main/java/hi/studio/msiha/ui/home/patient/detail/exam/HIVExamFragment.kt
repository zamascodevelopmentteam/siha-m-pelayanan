package hi.studio.msiha.ui.home.patient.detail.exam

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_HIV_RESULT
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.detail.ExamViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.patient.detail.exam.reagen.ReagenChooseActivity
import hi.studio.msiha.utils.extention.*
import kotlinx.android.synthetic.main.fragment_exam_hiv.*
import kotlinx.android.synthetic.main.layout_hiv_dna.*
import kotlinx.android.synthetic.main.layout_hiv_elisa.*
import kotlinx.android.synthetic.main.layout_hiv_rdt.*
import kotlinx.android.synthetic.main.layout_hiv_rna.*
import kotlinx.android.synthetic.main.layout_hiv_western.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import android.widget.ArrayAdapter
import android.widget.Toast


class HIVExamFragment : BaseFragment() {
    private val viewModel by viewModel<ExamViewModel>()
    private var qtyR1:Int?=null
    private var qtyR2:Int?=null
    private var qtyR3:Int?=null
    private var qtyRNA:Int?=null
    private var qtyDNA:Int?=null
    private var qtyELISA:Int?=null
    private var qtyNAT:Int?=null
    private var qtyWESTERN:Int?=null
    private var historyHiv:HivTestHistory?=null
    private var isRDTParalel = false
    companion object {
        const val RC_R1 = 100
        const val RC_R2 = 200
        const val RC_R3 = 300
        const val RC_NAT = 400
        const val RC_RNA = 500
        const val RC_DNA = 600
        const val RC_ELISA = 700
        const val RC_WESTERN = 800

        const val KEY_POPULATION = "population"
        const val KEY_LSM = "lsm"
        const val KEY_REASON = "reason_test"
        const val KEY_TEST_DATE = "test_date"
        const val KEY_REAGEN_TYPE = "reagen_type_1"
        const val KEY_TEST_METHOD = "hiv_test_method"
        const val KEY_REAGEN_R1 = "r1_reagen_hiv"
        const val KEY_REAGEN_R1_RESULT = "r1_reagen_hiv_result"
        const val KEY_REAGEN_R2 = "r2_reagen"
        const val KEY_REAGEN_R2_RESULT = "r2_reagen_hiv_result"
        const val KEY_REAGEN_R3 = "r3_reagen"
        const val KEY_REAGEN_R3_RESULT = "r3_reagen_hiv_result"
        const val KEY_REAGEN_NAT = "nat_reagen"
        const val KEY_REAGEN_NAT_RESULT = "nat_reagen_hiv_result"
        const val KEY_REAGEN_PCR = "pcr_reagen"
        const val KEY_REAGEN_PCR_RESULT = "pcr_reagen_result"
        const val KEY_REAGEN_ELISA = "elisa_reagen"
        const val KEY_REAGEN_ELISA_RESULT = "elisa_reagen_result"
        const val KEY_REAGEN_WESTERN = "western_reagen"
        const val KEY_REAGEN_WESTERN_RESULT = "western_reagen_result"
        const val KEY_TEST_RESULT = "hiv_result"
        const val KEY_TEST_RESULT_CONCLUSION = "hiv_result_conclusion"
    }

    override fun setView(): Int {
        return R.layout.fragment_exam_hiv
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            historyHiv = getParcelable(KEY_HIV_RESULT)
        }

        if(AppSIHA.instance.getUser()?.role== KEY_ROLE_LAB){
            populationSpn.isEnabled=false
            lsmTxt.isEnabled=false
            testReasonSpn.isEnabled=false
        }else{
            populationSpn.isEnabled=true
            lsmTxt.isEnabled=true
            testReasonSpn.isEnabled=true
        }
        //val calendar = Calendar.getInstance()
        try {
            if(historyHiv?.testHiv?.tanggalTest!=null) {
                examDateLbl.text = historyHiv?.testHiv?.tanggalTest.toDateFormat()
                examDateLbl.tag = historyHiv?.testHiv?.tanggalTest.toDateFormat("yyyy-MM-dd")
            }
        }catch(e:Exception){

        }
        val role =AppSIHA.instance.getUser()?.role
        if(role == KEY_ROLE_RR || role == KEY_ROLE_PHARMACIST){
            labInputWrapper.visibility=View.GONE
        }else{
            labInputWrapper.visibility=View.VISIBLE
        }
//        reagenR1ResultSpn.setSelection(0)
//        reagenR2ResultSpn.setSelection(0)
//        reagenR3ResultSpn.setSelection(0)
//        reagenDNAResultSpn.setSelection(1)
//        reagenRNAResultSpn.setSelection(1)
//        reagenElisaResultSpn.setSelection(1)
//        reagenWesternResultSpn.setSelection(1)
//        advanceTestSpn.setSelection(0)
        reagenR1Lbl.setOnClickListener {
            showReagen(RC_R1)
        }
        reagenR2Lbl.setOnClickListener {
            showReagen(RC_R2)
        }
        reagenR3Lbl.setOnClickListener {
            showReagen(RC_R3)
        }
        reagenDNALbl.setOnClickListener {
            showReagen(RC_DNA)
        }
        reagenRNALbl.setOnClickListener {
            showReagen(RC_RNA)
        }
        reagenNatLbl.setOnClickListener {
            showReagen(RC_NAT)
        }
        reagenElisaLbl.setOnClickListener {
            showReagen(RC_ELISA)
        }
        reagenWesternLbl.setOnClickListener {
            showReagen(RC_WESTERN)
        }
        testMethodSpn.onItemSelectedListener = spinnerListener
        advanceTestSpn.onItemSelectedListener = spinnerListener
        reagenR1ResultSpn.onItemSelectedListener = spinnerListener
        reagenR2ResultSpn.onItemSelectedListener = spinnerListener
        reagenR3ResultSpn.onItemSelectedListener = spinnerListener
        reagenDNAResultSpn.onItemSelectedListener = spinnerListener
        reagenRNAResultSpn.onItemSelectedListener = spinnerListener
        reagenElisaResultSpn.onItemSelectedListener = spinnerListener
        reagenWesternResultSpn.onItemSelectedListener = spinnerListener
        paralelCheckSpinner.onItemSelectedListener = spinnerListener
        examDateLbl.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, _: String, s1: String ->
                examDateLbl.text = s1
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                examDateLbl.tag=sdf.format(date)
            }
        }

        populateView()
        saveBtn.setOnClickListener {
            val visitHistory: VisitHistory =
                arguments?.get(PatientDetailFragment.KEY_VISIT_HISTORY) as VisitHistory
            val jenisTest = testMethodSpn.selectedItem.toString()
            //temporary variable for RDT test
            var hasilTest1:String?= reagenR1ResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault())
            var namaReagen1 = reagenR1Lbl.tag as Int?

            var hasilTest2:String?= reagenR2ResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault())
            var namaReagen2 = reagenR2Lbl.tag as Int?
            var hasilTest3:String?= reagenR3ResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault())
            var namaReagen3 = reagenR3Lbl.tag as Int?
            //temporary variable for PCR(DNA)
            var namaReagenDna =  reagenDNALbl.tag as Int?
            var hasilTestDna:String? =  reagenDNAResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault())
            if(hasilTestDna=="-")hasilTestDna=""
            //temporary variable for PCR(RNA)
            var namaReagenRna = reagenRNALbl.tag as Int?
            var hasilTestRna:String? = reagenRNAResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault())

            var namaReagenNat = reagenNatLbl.tag as Int?
            var hasilTestNat:String? = reagenNatResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault())
            when(jenisTest){
                "RDT"->{
                    namaReagenDna=null
                    hasilTestDna=null
                    namaReagenRna=null
                    hasilTestRna=null
                    namaReagenNat=null
                    hasilTestNat=null
                }
                "PCR (DNA)"->{
                    hasilTest1=null
                    namaReagen1=null
                    hasilTest2=null
                    namaReagen2=null
                    hasilTest3=null
                    namaReagen3=null
                    namaReagenRna=null
                    hasilTestRna=null
                    namaReagenNat=null
                    hasilTestNat=null
                }
                "PCR (RNA)"->{
                    hasilTest1=null
                    namaReagen1=null
                    hasilTest2=null
                    namaReagen2=null
                    hasilTest3=null
                    namaReagen3=null
                    namaReagenDna=null
                    hasilTestDna=null
                    namaReagenNat=null
                    hasilTestNat=null
                }
                "NAT"->{
                    hasilTest1=null
                    namaReagen1=null
                    hasilTest2=null
                    namaReagen2=null
                    hasilTest3=null
                    namaReagen3=null
                    namaReagenDna=null
                    hasilTestDna=null
                    namaReagenRna=null
                    hasilTestRna=null
                }
            }
//
//            val summarize:String = when (testResultLbl.text.toString().toLowerCase()) {
//                "non reaktif" -> "NON_REAKTIF"
//                "reaktif" -> "REAKTIF"
//                else -> "INKONKLUSIF"
//            }
            var summarize:String = testResultSpinner.selectedItem.toString()
            if(summarize=="-")summarize=""
            val hivExam = HIVExamRequest(
                ordinal = visitHistory.ordinal,
                visitId = visitHistory.id,
                kelompokPopulasi = populationSpn.selectedItem.toString(),
                lsmPenjangkau = lsmTxt.text.toString(),
                aasanTest = testReasonSpn.selectedItem.toString(),
                tanggalTest = examDateLbl.tag as String?,
                jenisTest = jenisTest,
                namaReagen1 = namaReagen1,
                qtyReagenR1 = qtyR1,
                hasilTest1 = hasilTest1,
                namaReagen2 = namaReagen2,
                qtyReagenR2 = qtyR2,
                hasilTest2 = hasilTest2,
                namaReagen3 = namaReagen3,
                qtyReagenR3 = qtyR3,
                hasilTest3 = hasilTest3,
                namaReagenDna = namaReagenDna,
                qtyReagenDna = qtyDNA,
                hasilTestDna = hasilTestDna,
                namaReagenRna = namaReagenRna,
                qtyReagenRna = qtyRNA,
                hasilTestRna = hasilTestRna,
                namaReagenNat = namaReagenNat,
                qtyReagenElisa = qtyELISA,
                hasilTestNat = hasilTestNat,
                qtyReagenNat = qtyNAT,
                qtyReagenWb = qtyWESTERN,
                hasilTestElisa = reagenElisaResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault()),
                namaReagenWb = reagenWesternLbl.tag as Int?,
                hasilTestWb = reagenWesternResultSpn.selectedItem.toString().toUpperCase(Locale.getDefault()),
                rujukanLabLanjut = when (advanceTestSpn.selectedItemPosition) {
                    0 -> ""
                    2 -> "WESTERN BLOT"
                    else -> "ELISA"
                },
                kesimpulanHiv = summarize
            )

            Hawk.put(KEY_HIV_RESULT,hivExam)
            viewModel.updateHIVExam(hivExam)
        }
    }

    private fun setupLsmCase(){
        populationSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2 < 5) {
                    lsmWrapper.visibility = View.VISIBLE
                } else {
                    lsmWrapper.visibility = View.GONE
                }
            }
        }
    }

    private fun populateView(){

        if(Hawk.contains(KEY_HIV_RESULT)){
            val hivExam:HIVExamRequest = Hawk.get(KEY_HIV_RESULT)
            Log.d("hiv test","EXIST ${hivExam.hasilTest1}")
            if(hivExam.kelompokPopulasi!=null)populationSpn.selectItem(hivExam.kelompokPopulasi)
            if(hivExam.aasanTest!=null)testReasonSpn.selectItem(hivExam.aasanTest)
            if(hivExam.jenisTest!=null)testMethodSpn.selectItem(hivExam.jenisTest)
            if(hivExam.namaReagen1!=null)reagenR1Lbl.tag=hivExam.namaReagen1
            if(hivExam.namaReagen2!=null)reagenR2Lbl.tag=hivExam.namaReagen2
            if(hivExam.namaReagen3!=null)reagenR3Lbl.tag=hivExam.namaReagen3
            if(hivExam.namaReagenDna!=null)reagenDNALbl.tag=hivExam.namaReagenDna
            if(hivExam.namaReagenRna!=null)reagenRNALbl.tag=hivExam.namaReagenRna
            if(hivExam.namaReagenElisa!=null)reagenElisaLbl.tag=hivExam.namaReagenElisa
            if(hivExam.namaReagenWb!=null)reagenWesternLbl.tag=hivExam.namaReagenWb
            if(hivExam.namaReagenNat!=null)reagenNatLbl.tag=hivExam.namaReagenNat
            if(hivExam.hasilTest1!=null)reagenR1ResultSpn.selectItem(hivExam.hasilTest1)
            if(hivExam.hasilTest2!=null)reagenR2ResultSpn.selectItem(hivExam.hasilTest2)
            if(hivExam.hasilTest3!=null)reagenR3ResultSpn.selectItem(hivExam.hasilTest3)
            if(hivExam.hasilTestDna!=null)reagenDNAResultSpn.selectItem(hivExam.hasilTestDna)
            if(hivExam.hasilTestRna!=null)reagenRNAResultSpn.selectItem(hivExam.hasilTestRna)
            if(hivExam.hasilTestElisa!=null)reagenElisaResultSpn.selectItem(hivExam.hasilTestElisa)
            if(hivExam.hasilTestWb!=null)reagenWesternResultSpn.selectItem(hivExam.hasilTestWb)
            if(hivExam.hasilTestNat!=null)reagenNatResultSpn.selectItem(hivExam.hasilTestNat)
            if(hivExam.rujukanLabLanjut!=null)testMethodSpn.selectItem(hivExam.jenisTest)
            if(hivExam.kesimpulanHiv!=null)testResultLbl.text = hivExam.kesimpulanHiv
            if(hivExam.lsmPenjangkau!=null)lsmTxt.setText(hivExam.lsmPenjangkau)
            if(hivExam.namaReagenR1Data!=null) reagenR1Lbl.text = hivExam.namaReagenR1Data.name
            if(hivExam.namaReagenR2Data!=null) reagenR2Lbl.text = hivExam.namaReagenR2Data.name
            if(hivExam.namaReagenR3Data!=null) reagenR3Lbl.text = hivExam.namaReagenR3Data.name
            if(hivExam.namaReagenElisaData!=null) reagenElisaLbl.text = hivExam.namaReagenElisaData.name
            if(hivExam.namaReagenWbData!=null) reagenWesternLbl.text = hivExam.namaReagenWbData.name
            if(hivExam.namaReagenDnaData!=null) reagenDNALbl.text = hivExam.namaReagenDnaData.name
            if(hivExam.namaReagenRnaData!=null) reagenRNALbl.text = hivExam.namaReagenRnaData.name
            if(hivExam.namaReagenNatData!=null) reagenNatLbl.text = hivExam.namaReagenNatData.name
            if(hivExam.tanggalTest!=null) {
                examDateLbl.text = hivExam.tanggalTest.toDate().toFormat()
                examDateLbl.tag = hivExam.tanggalTest
            }

        }else{
            
        }

        setupLsmCase()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@HIVExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@HIVExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            examResult.observe(this@HIVExamFragment, Observer {
                findNavController().navigateUp()
            })
        }
    }

    private fun setResultSpinner(code:Int){
        val adapter = when(code) {
            1 ->{
               ArrayAdapter.createFromResource(
                    context,
                    R.array.hiv_dna_summary,
                    android.R.layout.simple_spinner_item
                )
            }
            2->{
                ArrayAdapter.createFromResource(
                    context,
                    R.array.hiv_rna_summary,
                    android.R.layout.simple_spinner_item
                )
            }
            else -> {
                ArrayAdapter.createFromResource(
                    context,
                    R.array.hiv_summary_result,
                    android.R.layout.simple_spinner_item
                )
            }
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        testResultSpinner.adapter = adapter
    }
    private val spinnerListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            when (parent?.id) {
                R.id.testMethodSpn -> {
                    setResultSpinner(position)
                    when (position) {
                        0 -> {
                            layoutRDT.visibility = View.VISIBLE
                            layoutRNA.visibility = View.GONE
                            layoutDNA.visibility = View.GONE
                            layoutNAT.visibility = View.GONE
                        }
                        1 -> {
                            layoutRDT.visibility = View.GONE
                            layoutRNA.visibility = View.GONE
                            layoutDNA.visibility = View.VISIBLE
                            layoutNAT.visibility = View.GONE
                        }
                        2 -> {
                            layoutRDT.visibility = View.GONE
                            layoutRNA.visibility = View.VISIBLE
                            layoutDNA.visibility = View.GONE
                            layoutNAT.visibility = View.GONE
                        }
                    }
                }

                R.id.reagenDNAResultSpn->{

                }

                R.id.paralelCheckSpinner->{
                    isRDTParalel = when(position){
                        0 -> false
                        else -> true
                    }
                    if(!isRDTParalel){
                        //reagenR1ResultSpn.setSelection(0)
                        layoutRDTReagen2.visibility = View.GONE
                        layoutRDTReagen2Result.visibility = View.GONE

                        layoutRDTReagen3.visibility = View.GONE
                        layoutRDTReagen3Result.visibility = View.GONE
                    }
                    if(isRDTParalel && position==1){
                        layoutRDTReagen2.visibility = View.VISIBLE
                        layoutRDTReagen2Result.visibility = View.VISIBLE

                        layoutRDTReagen3.visibility = View.GONE
                        layoutRDTReagen3Result.visibility = View.GONE
                    }
                }

                R.id.advanceTestSpn -> {
                    when (position) {
                        0 ->{
                            layoutElisa.visibility = View.GONE
                            layoutWestern.visibility = View.GONE
                        }
                        1 -> {
                            layoutElisa.visibility = View.VISIBLE
                            layoutWestern.visibility = View.GONE
                        }
                        2 -> {
                            layoutElisa.visibility = View.GONE
                            layoutWestern.visibility = View.VISIBLE
                        }
                    }
                }

                R.id.reagenR1ResultSpn->{
                    if(!isRDTParalel) {
                        if (position == 1) {
                            layoutRDTReagen2.visibility = View.VISIBLE
                            layoutRDTReagen2Result.visibility = View.VISIBLE
                            //reagenR2ResultSpn.setSelection(0)
                        } else {
                            layoutRDTReagen2.visibility = View.GONE
                            layoutRDTReagen2Result.visibility = View.GONE
                        }
                    }else{
                        showReagenR3()
                    }
                }

                R.id.reagenR2ResultSpn->{
                    if(!isRDTParalel) {
                        if (position == 1) {
                            layoutRDTReagen3.visibility = View.VISIBLE
                            layoutRDTReagen3Result.visibility = View.VISIBLE
                        } else {
                            layoutRDTReagen3.visibility = View.GONE
                            layoutRDTReagen3Result.visibility = View.GONE
                        }
                    }else{
                       showReagenR3()
                    }
                }
            }
            //updateTestResult()
        }
    }

    private fun showReagenR3(){
        if(reagenR1ResultSpn.selectedItemPosition==1 && reagenR2ResultSpn.selectedItemPosition==1){
            layoutRDTReagen3.visibility = View.VISIBLE
            layoutRDTReagen3Result.visibility = View.VISIBLE
        }else{
            layoutRDTReagen3.visibility = View.GONE
            layoutRDTReagen3Result.visibility = View.GONE
        }
    }

    private fun showReagen(requestCode: Int) {
        if(!isReagenExist(requestCode)) {
            when (requestCode) {
                RC_R1 -> Hawk.put(KEY_REAGEN_TYPE, "is_r1")
                RC_R2 -> Hawk.put(KEY_REAGEN_TYPE, "is_r2")
                RC_R3 -> Hawk.put(KEY_REAGEN_TYPE, "is_r3")
                RC_DNA -> Hawk.put(KEY_REAGEN_TYPE, "is_reagen")
                RC_RNA -> Hawk.put(KEY_REAGEN_TYPE, "is_reagen")
                RC_ELISA -> Hawk.put(KEY_REAGEN_TYPE, "is_reagen")
                RC_WESTERN -> Hawk.put(KEY_REAGEN_TYPE, "is_reagen")
            }

            startActivityForResult(
                Intent(requireContext(), ReagenChooseActivity::class.java),
                requestCode
            )
        }else{
            Toast.makeText(context,"Tidak diperbolehkan mengubah Reagen", Toast.LENGTH_LONG).show()
        }
    }

    private fun isReagenExist(requestCode: Int):Boolean{
        var check=false
        if(Hawk.contains(KEY_HIV_RESULT)){
            val hivExam:HIVExamRequest = Hawk.get(KEY_HIV_RESULT)
            when(requestCode){
                RC_R1 ->if(hivExam.namaReagenR1Data!=null)check=true
                RC_R2 ->if(hivExam.namaReagenR2Data!=null)check=true
                RC_R3 ->if(hivExam.namaReagenR3Data!=null)check=true
                RC_DNA ->if(hivExam.namaReagenDnaData!=null)check=true
                RC_RNA ->if(hivExam.namaReagenRnaData!=null)check=true
                RC_ELISA ->if(hivExam.namaReagenElisaData!=null)check=true
                RC_WESTERN ->if(hivExam.namaReagenWbData!=null)check=true
            }
        }

        return check
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            data?.apply {
                val medicineName = getStringExtra("name")
                val medicineId = getIntExtra("id", 0)
                val medicineQty = getIntExtra("qty", 0)
                when(requestCode){
                    RC_R1 -> qtyR1=medicineQty
                    RC_R2 -> qtyR2=medicineQty
                    RC_R3 -> qtyR3=medicineQty
                    RC_NAT -> qtyNAT=medicineQty
                    RC_RNA -> qtyRNA=medicineQty
                    RC_DNA -> qtyDNA=medicineQty
                    RC_ELISA -> qtyELISA=medicineQty
                    RC_WESTERN -> qtyWESTERN=medicineQty
                }
                updateButton(
                    when (requestCode) {
                        RC_R1 -> reagenR1Lbl
                        RC_R2 -> reagenR2Lbl
                        RC_R3 -> reagenR3Lbl
                        RC_NAT -> reagenNatLbl
                        RC_RNA -> reagenRNALbl
                        RC_DNA -> reagenDNALbl
                        RC_ELISA -> reagenElisaLbl
                        RC_WESTERN -> reagenWesternLbl
                        else -> null
                    }, medicineId, "$medicineName($medicineQty)"
                )
            }
        }
    }

    private fun updateButton(view: AppCompatTextView?, id: Int, name: String) {
        view?.apply {
            text = name
            tag = id
        }
    }

    private fun updateTestResult() {
//        testResultLbl.text =
//            when {
//                (reagenR1ResultSpn.selectedItemPosition == 0
//                        && reagenR2ResultSpn.selectedItemPosition == 0
//                        && reagenR3ResultSpn.selectedItemPosition == 0)
//                        || reagenElisaResultSpn.selectedItemPosition == 0
//                        || reagenWesternResultSpn.selectedItemPosition == 0
//                        || reagenDNAResultSpn.selectedItemPosition == 0
//                        || reagenRNAResultSpn.selectedItemPosition == 0
//                -> "Reaktif"
//                reagenR1ResultSpn.selectedItemPosition == 0
//                        && (reagenR2ResultSpn.selectedItemPosition == 1
//                        || reagenR3ResultSpn.selectedItemPosition == 1)
//                -> "Inkonklusif"
//                reagenR1ResultSpn.selectedItemPosition == 1
//                        && reagenElisaResultSpn.selectedItemPosition == 1
//                        && reagenWesternResultSpn.selectedItemPosition == 1
//                        && reagenDNAResultSpn.selectedItemPosition == 1
//                        && reagenRNAResultSpn.selectedItemPosition == 1
//                -> "Non Reaktif"
//                reagenR1ResultSpn.selectedItemPosition ==3
//                        ||  reagenR2ResultSpn.selectedItemPosition == 3
//                        || reagenR3ResultSpn.selectedItemPosition == 3
//                        || reagenElisaResultSpn.selectedItemPosition == 3
//                        || reagenDNAResultSpn.selectedItemPosition == 3
//                        || reagenRNAResultSpn.selectedItemPosition == 3
//                -> "INVALID"
//                else->{
//                    "Non Reaktif"
//                }
//            }
    }
}