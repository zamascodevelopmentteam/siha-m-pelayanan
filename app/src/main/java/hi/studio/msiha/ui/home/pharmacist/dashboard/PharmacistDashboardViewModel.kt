package hi.studio.msiha.ui.home.pharmacist.dashboard

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetHospital
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class PharmacistDashboardViewModel(
    private val getPharmacist: GetPharmacist,
    private val getHospital: GetHospital): BaseViewModel(){

    val getListReagenResult = MutableLiveData<List<Order>>().toSingleEvent()
    val getHospitalResult = MutableLiveData<Hospital>().toSingleEvent()

    fun getListReagen(){
        getPharmacist.getListReagen(0,100){
            when (it) {
                is Either.Left -> getListReagenResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getHospital(id:Int) {
        getHospital.getHospitalById(id) {
            when (it) {
                is Either.Left -> getHospitalResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }

        }
    }
}