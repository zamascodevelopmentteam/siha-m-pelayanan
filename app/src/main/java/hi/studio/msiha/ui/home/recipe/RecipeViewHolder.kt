package hi.studio.msiha.ui.home.recipe

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.Recipe
import kotlinx.android.synthetic.main.item_recipe.view.*

class RecipeViewHolder(private val view: View) : BaseViewHolder<Recipe>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: Recipe,
        callback: BaseAdapter.OnItemClick<Recipe>?
    ) {
        item.apply {
            view.recipeNameLbl.text = "Resep Obat"
            view.medicineCountLbl.text = "$totalMedicine Obat"
            view.recipeNumberLbl.text = "$recipeNo"
        }
        view.setOnClickListener {
            callback?.onClick(it, item, index)
        }
    }
}