package hi.studio.msiha.ui.home.visit.add

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class AddVisitViewModel(private val getVisit: GetVisit) : BaseViewModel() {
    val addVisitResult = MutableLiveData<Visit>().toSingleEvent()

    fun addVisit(ordinal: Int, patientId: Int, date: String) {
        isLoading.postValue(true)
        getVisit.createVisitHIVNegative(ordinal, patientId, date) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> addVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}