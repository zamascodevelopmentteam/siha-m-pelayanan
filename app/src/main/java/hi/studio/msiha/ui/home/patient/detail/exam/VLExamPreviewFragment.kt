package hi.studio.msiha.ui.home.patient.detail.exam

import android.os.Bundle
import android.view.View
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_IMS_RESULT
import hi.studio.msiha.data.constant.KEY_VL_RESULT
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import kotlinx.android.synthetic.main.fragment_preview_vl.*

class VLExamPreviewFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_preview_vl
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if(Hawk.contains(KEY_VL_RESULT)) {
            val vl: VLCExamRequest = Hawk.get(KEY_VL_RESULT)
            val qty = if(vl.qty==null || vl.qty==0 && vl.resultTest==null){
                "-"
            }else{
                "${vl.qty}"
            }

            val result = if(vl.resultTest==null){
                "-"
            }else{
                "${vl.resultTest}"
            }
            testLabLbl.text=vl.testType?:"-"
            reagenLbl.text=vl.namaReagenData?.name?:"-"
            reagenAmountLbl.text=qty
            testResultLbl.text=result
        }
    }
}