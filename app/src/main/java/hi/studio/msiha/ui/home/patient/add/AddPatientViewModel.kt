package hi.studio.msiha.ui.home.patient.add

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class AddPatientViewModel(
    private val getPatient: GetPatient
) : BaseViewModel() {
    val patientResult = MutableLiveData<Patient?>().toSingleEvent()
    val createPatientResult = MutableLiveData<String>().toSingleEvent()
    val updatePatientResult = MutableLiveData<String>().toSingleEvent()

    fun getUserByNIK(nik: String) {
        getPatient.getPatientByNIK(nik) {
            when (it) {
                is Either.Left -> patientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createPatient(bundle: Bundle) {
        isLoading.postValue(true)
        getPatient.createPatient(bundle) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> createPatientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }

    }

    fun updatePatient(id: Int, bundle: Bundle) {
        isLoading.postValue(true)
        getPatient.updatePatient(id, bundle) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> updatePatientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }

    }
}