package hi.studio.msiha.ui.home.patient.detail.exam.reagen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_PAGE_LIMIT
import hi.studio.msiha.domain.model.Reagen
import kotlinx.android.synthetic.main.fragment_choose_reagen.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReagenChooseFragment : BaseFragment() {
    private val viewModel by viewModel<ReagenChooseViewModel>()
    private lateinit var adapter: ReagenAdapter
    private var limit = KEY_PAGE_LIMIT
    private var reagenQty =0

    override fun setView(): Int {
        return R.layout.fragment_choose_reagen
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        viewModel.getReagens()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupAdapter()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@ReagenChooseFragment, Observer {
                (requireActivity() as ReagenChooseActivity).showLoading(it)
            })

            isError.observe(this@ReagenChooseFragment, Observer {
                (requireActivity() as ReagenChooseActivity).showError(it)
            })

            reagenResult.observe(this@ReagenChooseFragment, Observer {
                adapter.addAll(it)
            })
        }
    }

    private fun setupAdapter() {
        adapter = ReagenAdapter()
        reagenList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@ReagenChooseFragment.adapter
        }
        adapter.itemCallback{ _: View, item: Reagen, _: Int ->
            MaterialDialog(activity!!).show {
                title(text="Jumlah Reagen")
                input(
                    inputType= InputType.TYPE_CLASS_NUMBER,
                    prefill = "1"){ _, text ->
                    reagenQty = try {
                        text.toString().toInt()
                    }catch (e:Exception){
                        1
                    }
                }
                positiveButton(text="Simpan"){
                    if(reagenQty>9){
                        Toast.makeText(context,"Maksimum 1 digit angka",Toast.LENGTH_SHORT).show()
                    }else {
                        val bundle = Bundle()
                        bundle.apply {
                            putInt("id", item.id ?: 0)
                            putString("name", item.name)
                            putInt("qty", reagenQty)
                        }
                        bundle.let {
                            requireActivity().setResult(
                                Activity.RESULT_OK,
                                Intent().putExtras(it)
                            )
                            requireActivity().finish()
                        }
                    }
                }
            }

        }
    }
}