package hi.studio.msiha.ui.home.recipe.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Recipe
import hi.studio.msiha.domain.model.RecipeDetail
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.scan.ScanActivity
import hi.studio.msiha.utils.extention.showDate
import kotlinx.android.synthetic.main.fragment_recipe_detail.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class RecipeDetailFragment : BaseFragment() {
    private val viewModel by viewModel<RecipeDetailViewModel>()
    private lateinit var recipeDetailAdapter: RecipeDetailAdapter
    private var recipe: Recipe? = null
    private var recipeDetail: RecipeDetail? = null

    companion object {
        const val KEY_RECIPE_DETAIL = "recipe_detail"
    }

    override fun setView(): Int {
        return R.layout.fragment_recipe_detail
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            recipe = getParcelable(KEY_RECIPE_DETAIL) as Recipe
            when (getInt(PatientDetailActivity.KEY_MODE)) {
                PatientDetailActivity.MODE_PATIENT -> {
                    giveReceiptBtn.isEnabled = false
                    giveReceiptBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                }
                PatientDetailActivity.MODE_VISIT -> {
                    giveReceiptBtn.isEnabled = true
                }
            }
        }
        setupAdapter()
        giveReceiptBtn.setOnClickListener {
            requireActivity().startActivity<ScanActivity>(
                Pair(ScanActivity.KEY_SCAN_TYPE, ScanActivity.TYPE_RECIPE),
                Pair(KEY_RECIPE_DETAIL, recipeDetail)
            )
        }
    }

    override fun onResume() {
        super.onResume()
        recipe?.let {
            viewModel.getRecipeDetail(it.id)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@RecipeDetailFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@RecipeDetailFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            recipeDetailResult.observe(this@RecipeDetailFragment, Observer {
                recipeDetail = it
                it?.let {
                    observeRecipeDetail(it)
                }
            })
        }
    }

    private fun setupAdapter() {
        recipeDetailAdapter = RecipeDetailAdapter()
        medicineList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = recipeDetailAdapter
        }
    }

    private fun observeRecipeDetail(recipeDetail: RecipeDetail) {
        recipeDetail.apply {
            recipeNumberLbl.text = recipeNo
            dateLbl.showDate(givenAt.toLong())
            notesLbl.text = notes
            recipeDetailAdapter.addAll(recipeMedicine)
        }
    }
}