package hi.studio.msiha.ui.home.patient.detail.exam

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import kotlinx.android.synthetic.main.item_exam_history.view.*

class ImsExamListViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<ImsTestHistory>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: ImsTestHistory,
        callback: BaseAdapter.OnItemClick<ImsTestHistory>?
    ) {
        item.apply {
            view.examTitleLbl.text = "Kunjungan ke -${item.ordinal}"
        }

        view.examPreviewBtn.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}