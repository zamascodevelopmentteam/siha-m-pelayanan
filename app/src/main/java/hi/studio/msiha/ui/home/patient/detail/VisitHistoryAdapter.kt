package hi.studio.msiha.ui.home.patient.detail

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.VisitHistory
import kotlin.reflect.KProperty

class VisitHistoryAdapter(private val isOdha:Boolean) :
    BaseAdapter<VisitHistory>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_visit_history
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return VisitHistoryViewHolder(context, view,isOdha)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: VisitHistory,
        callback: OnItemClick<VisitHistory>?
    ) {
        if (holder is VisitHistoryViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<VisitHistory>,
        new: MutableList<VisitHistory>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}