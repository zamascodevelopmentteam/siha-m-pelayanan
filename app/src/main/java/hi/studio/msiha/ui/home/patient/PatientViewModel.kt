package hi.studio.msiha.ui.home.patient

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class PatientViewModel(
    private val getPatient: GetPatient
) : BaseViewModel() {
    val patientResult = MutableLiveData<Patient>().toSingleEvent()
    val patientsResult = MutableLiveData<List<Patient>>().toSingleEvent()

    fun getPatients(page: Int, limit: Int) {
        isLoading.postValue(true)
        getPatient.getPatient(page, limit) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> patientsResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getPatientByNIK(nik: String) {
        isLoading.postValue(true)
        getPatient.getPatientByNIK(nik) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> patientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}