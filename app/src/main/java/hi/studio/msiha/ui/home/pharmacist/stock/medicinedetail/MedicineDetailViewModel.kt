package hi.studio.msiha.ui.home.pharmacist.stock.medicinedetail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class MedicineDetailViewModel (private val getPharmacist: GetPharmacist):BaseViewModel(){
    val updateMedicineResult = MutableLiveData<MedicineRsp>().toSingleEvent()

    fun updateMedicine(medicine: Medicine) {
        isLoading.postValue(true)
        getPharmacist.updateMedicine(medicine) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> updateMedicineResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}