package hi.studio.msiha.ui.home.patient.detail

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_odha_success.*

class ODHASuccessFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_odha_success
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        backBtn.setOnClickListener {
            findNavController().popBackStack(R.id.patientDetailFragment, false)
        }
    }
}