package hi.studio.msiha.ui.home.recipe

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.model.Recipe
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class RecipeViewModel(
    private val getPatient: GetPatient
) : BaseViewModel() {
    val recipesResult = MutableLiveData<List<Recipe>>().toSingleEvent()

    fun getRecipes(idPatient: Int, page: Int, limit: Int) {
        isLoading.postValue(true)
        getPatient.getRecipes(idPatient, page, limit) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> recipesResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}