package hi.studio.msiha.ui.home.patient.detail.exam

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_HIV_RESULT
import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.detail.ExamViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.utils.extention.showDate
import hi.studio.msiha.utils.extention.toDateFormat
import kotlinx.android.synthetic.main.fragment_preview_hiv.*
import kotlinx.android.synthetic.main.layout_preview_dna.*
import kotlinx.android.synthetic.main.layout_preview_nat.*
import kotlinx.android.synthetic.main.layout_preview_rdt.*
import kotlinx.android.synthetic.main.layout_preview_rna.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HIVExamPreviewFragment : BaseFragment() {
    private var visitHistory: VisitHistory? = null
    private val viewModel by viewModel<ExamViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_preview_hiv
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        populateView()
    }

    private fun populateView(){
        if(Hawk.contains(KEY_HIV_RESULT)) {

            val testHiv:HIVExamRequest? = Hawk.get(KEY_HIV_RESULT)
            populationLbl.text = testHiv?.kelompokPopulasi
            lsmLbl.text = testHiv?.lsmPenjangkau
            testReasonLbl.text = testHiv?.aasanTest
            examDateLbl.text = testHiv?.tanggalTest.toDateFormat()
            examTypeLbl.text = testHiv?.jenisTest
            when (testHiv?.jenisTest) {
                "RDT" -> {
                    layoutRDT.visibility = View.VISIBLE
                    reagenR1Lbl.text = testHiv.namaReagenR1Data?.name
                    reagenR1ResultLbl.text = testHiv.hasilTest1

                    reagenR2Lbl.text = testHiv.namaReagenR2Data?.name
                    reagenR2ResultLbl.text = testHiv.hasilTest2

                    reagenR3Lbl.text = testHiv.namaReagenR1Data?.name
                    reagenR3ResultLbl.text = testHiv.hasilTest3

                }
                "DNA" -> {
                    layoutDNA.visibility = View.VISIBLE
                    reagenDNALbl.text = testHiv.namaReagenDnaData?.name
                    reagenDNAResultLbl.text = testHiv.hasilTestDna
                }
                "RNA" -> {
                    layoutRNA.visibility = View.VISIBLE
                    reagenRNALbl.text = testHiv.namaReagenRnaData?.name
                    reagenRNAResultLbl.text = testHiv.hasilTestRna
                }
            }
            reagenNatLbl.text=testHiv?.namaReagenNatData?.name
            reagenNatResultLbl.text=testHiv?.hasilTestNat
            advanceTestLbl.text = testHiv?.rujukanLabLanjut
            testResultLbl.text = testHiv?.kesimpulanHiv
        }
    }


}