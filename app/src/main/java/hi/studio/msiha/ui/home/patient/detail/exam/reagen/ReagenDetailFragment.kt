package hi.studio.msiha.ui.home.patient.detail.exam.reagen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.fragment_choose_reagen_preview.*

class ReagenDetailFragment : BaseFragment() {
    private var idMedicine: Int? = 0
    private var medicineName: String? = null
    private var medicineQty =0

    companion object {
        const val KEY_MEDICINE = "medicine_detail"
    }

    override fun setView(): Int {
        return R.layout.fragment_choose_reagen_preview
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            val medicine = getParcelable<Reagen>(KEY_MEDICINE)
            medicine?.apply {
                idMedicine = id
                medicineName = name
                medicineImg.showFromUrl(null)
                nameLbl.text = name
                codeNameLbl.text = codeName
                typeLbl.text = medicineType
            }
        }

        saveBtn.setOnClickListener {
            val bundle = arguments?.apply {
                putInt("id", idMedicine ?: 0)
                putString("name", medicineName)
                putInt("qty",medicineQty)
            }
            bundle?.let {
                requireActivity().setResult(
                    Activity.RESULT_OK,
                    Intent().putExtras(it)
                )
                requireActivity().finish()
            }
        }
    }


}