package hi.studio.msiha.ui.home.pharmacist.recipe

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseBottomSheetFragment
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.pop_up_medicine_stock_confirm.*

class CheckStockBottomSheet(private val medicine: MedicineRsp) : BaseBottomSheetFragment() {
    private var callback: (medicineId: Int) -> Unit = {}

    override fun setView(): Int {
        return R.layout.pop_up_medicine_stock_confirm
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        medicine.apply {
            medicineImg.showFromUrl(picture ?: "")
            medicineNameTxt.setText(name)
            medicineTypeTxt.setText(medicineType.capitalize())
            medicineCodeTxt.setText(codeName)
            medicineStockTxt.setText("${medicineStock.stock}")
        }
        confirmBtn.setOnClickListener {
            callback(medicine.id)
        }

        closeBtn.setOnClickListener {
            dismiss()
        }
    }

    fun setCallback(callback: (medicineId: Int) -> Unit) {
        this.callback = callback
    }
}