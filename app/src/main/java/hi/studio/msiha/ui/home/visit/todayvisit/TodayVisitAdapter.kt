package hi.studio.msiha.ui.home.visit.todayvisit

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.VisitHistory
import kotlin.reflect.KProperty


class TodayVisitAdapter: BaseAdapter<VisitHistory>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_today_visit
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return TodayVisitViewHolder(context, view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: VisitHistory,
        callback: OnItemClick<VisitHistory>?
    ) {
        if (holder is TodayVisitViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<VisitHistory>,
        new: MutableList<VisitHistory>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}