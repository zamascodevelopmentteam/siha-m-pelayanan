package hi.studio.msiha.ui.home.patient.detail.exam

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_HIV_RESULT
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.utils.extention.toDateFormat
import kotlinx.android.synthetic.main.fragment_preview_hiv.*
import kotlinx.android.synthetic.main.fragment_preview_hiv.view.*
import kotlinx.android.synthetic.main.layout_preview_dna.*
import kotlinx.android.synthetic.main.layout_preview_rdt.*
import kotlinx.android.synthetic.main.layout_preview_rna.*

class HivHistoryDetailFragment:BaseFragment(){

    private var testHistory:HivTestHistory?=null
    override fun setView(): Int {
        return R.layout.fragment_preview_hiv
    }


    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            testHistory = getParcelable(KEY_HIV_RESULT)
        }
        populateView()
    }

    private fun populateView(){
        populationLbl.text=testHistory?.testHiv?.kelompokPopulasi
        lsmLbl.text=testHistory?.testHiv?.lsmPenjangkau
        testReasonLbl.text=testHistory?.testHiv?.aasanTest
        examDateLbl.text=testHistory?.testHiv?.tanggalTest.toDateFormat()
        examTypeLbl.text=testHistory?.testHiv?.jenisTest
        when(testHistory?.testHiv?.jenisTest){
            "RDT"->{
                layoutRDT.visibility=View.VISIBLE
                if(testHistory?.testHiv?.namaReagenR1Data!=null) {
                    reagenR1Lbl.text = testHistory?.testHiv?.namaReagenR1Data?.name
                    reagenR1ResultLbl.text = testHistory?.testHiv?.hasilTest1
                }else{
                    reagenR1Lbl.text = "-"
                    reagenR1ResultLbl.text = "-"
                }

                if(testHistory?.testHiv?.namaReagenR2Data!=null) {
                    reagenR2Lbl.text =testHistory?.testHiv?.namaReagenR2Data?.name
                    reagenR2ResultLbl.text = testHistory?.testHiv?.hasilTest2
                }else{
                    reagenR2Lbl.text = "-"
                    reagenR2ResultLbl.text = "-"
                }

                if(testHistory?.testHiv?.namaReagenR3Data!=null) {
                    reagenR3Lbl.text = testHistory?.testHiv?.namaReagenR3Data?.name
                    reagenR3ResultLbl.text = testHistory?.testHiv?.hasilTest3
                }else{
                    reagenR3Lbl.text = "-"
                    reagenR3ResultLbl.text = "-"
                }

            }
            "PCR-DNA"->{
                layoutDNA.visibility=View.VISIBLE
                reagenDNALbl.text=testHistory?.testHiv?.namaReagenDnaData?.name
                reagenDNAResultLbl.text=testHistory?.testHiv?.hasilTestDna
            }
            "PCR-RNA"->{
                layoutRNA.visibility=View.VISIBLE
                reagenRNALbl.text=testHistory?.testHiv?.namaReagenRnaData?.name
                reagenRNAResultLbl.text=testHistory?.testHiv?.hasilTestRna
            }
            "NAT"->{
                layoutNAT.visibility=View.VISIBLE
                reagenRNALbl.text=testHistory?.testHiv?.namaReagenNatData?.name
                reagenRNAResultLbl.text=testHistory?.testHiv?.hasilTestNat
            }
        }

        advanceTestLbl.text=testHistory?.testHiv?.rujukanLabLanjut
        testResultLbl.text=testHistory?.testHiv?.kesimpulanHiv
    }

}