package hi.studio.msiha.ui.home.visit

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetHospital
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class VisitViewModel(private val getHospital: GetHospital) : BaseViewModel() {
    val visitsResult = MutableLiveData<List<Patient>>().toSingleEvent()

    fun getVisits(page: Int, limit: Int) {
        isLoading.postValue(true)
        getHospital.getVisits(page, limit) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> visitsResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}