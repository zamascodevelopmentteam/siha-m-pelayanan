package hi.studio.msiha.ui.home.pharmacist.order.createorderdetail


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.request.RequestOptions
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.datasource.remote.request.FarmasiOrderRequest
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.utils.GlideApp
import hi.studio.msiha.utils.extention.getSelectedContent
import kotlinx.android.synthetic.main.fragment_create_order_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class CreateOrderDetailFragment : BaseFragment() {

    private lateinit var medicine: MedicineRsp
    private val viewModel by viewModel<CreateOrderDetailViewModel>()
    private var hospitalId = 0
    override fun setView(): Int {
        return R.layout.fragment_create_order_detail
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        handleArgument()
        handleCreateOrderButton()
        val user = AppSIHA.instance.getUser()
        user?.let {
            hospitalId = user.upkId ?: 0
        }
        imgBackButton.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onResume() {
        super.onResume()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@CreateOrderDetailFragment, Observer {

            })

            getCreateOrderResult.observe(this@CreateOrderDetailFragment, Observer {
                //Toast.makeText(context,"Add ${it.amount}",Toast.LENGTH_SHORT).show()
//                findNavController().navigate(R.id.action_createOrderDetailFragment_to_pharmacistOrderFragment)
            })
        }
    }

    private fun handleArgument() {
        val bundle = arguments
        if (bundle != null) {
            medicine = bundle.getParcelable("medicine")
            populateMedicineCard(medicine)
        }
    }

    private fun populateMedicineCard(medicine: MedicineRsp) {
        val reg = RequestOptions()
        reg.circleCrop()
        GlideApp.with(this)
            .load(R.drawable.ic_pill)
            .apply(reg)
            .into(imgMedicine)
        textMedicineName.text = medicine.name
        textMedicineType.text = medicine.type
        textMedicineStock.text = "${medicine.medicineStock.stock}"
    }

    private fun handleCreateOrderButton() {
        try {
            buttonCreateOrder.setOnClickListener {
                viewModel.createOrder(
                    FarmasiOrderRequest(
                        spinnerOrderTo.getSelectedContent(),
                        hospitalId,
                        medicine.id,
                        editTextOrderAmount.text.toString().toInt()

                    )
                )
            }
        } catch (e: NumberFormatException) {
            Toast.makeText(context, "Jumlah harus angka", Toast.LENGTH_SHORT).show()
        }

    }

}
