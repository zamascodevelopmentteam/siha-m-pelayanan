package hi.studio.msiha.ui.home.visit.visitplan

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.response.CreateVisitResponse
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent


class VisitPlanViewModel(private val getVisit: GetVisit): BaseViewModel(){
    val getPlanVisitResult = MutableLiveData<List<VisitHistory>>().toSingleEvent()
    val createVisitResult = MutableLiveData<CreateVisitResponse>().toSingleEvent()

    fun getPlanVisit(page: Int, limit: Int) {
        isLoading.postValue(true)
        getVisit.getPlanVisit{
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getPlanVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createVisit(patientId:Int){
        isLoading.postValue(true)
        getVisit.createGeneralVisit(patientId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createVisitResult.postValue(it.left)
                is Either.Right->isError.postValue(it.right)
            }
        }
    }
}