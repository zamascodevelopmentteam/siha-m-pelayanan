package hi.studio.msiha.ui.home.patient.detail.exam

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import kotlinx.android.synthetic.main.item_exam_history.view.*


class VlExamListViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<VlTestHistory>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: VlTestHistory,
        callback: BaseAdapter.OnItemClick<VlTestHistory>?
    ) {
        item.apply {
            view.examTitleLbl.text = "Kunjungan ke -${item.ordinal}"
        }

        view.examPreviewBtn.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}