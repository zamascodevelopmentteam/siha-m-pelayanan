package hi.studio.msiha.ui.home.visit.edit

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.request.IkhtisarRequest
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class VisitEditViewModel(private val getVisit: GetVisit) : BaseViewModel() {
    val result = MutableLiveData<String>().toSingleEvent()
    val iktisarResult = MutableLiveData<String>().toSingleEvent()
    val getIktisarResult = MutableLiveData<IkhtisarRequest>().toSingleEvent()
    val saveTreatmentResult = MutableLiveData<String>().toSingleEvent()
    val getVisitDetailResult = MutableLiveData<VisitDetail>().toSingleEvent()

    fun getVisitDetailByVisitId(visitId:Int){
        isLoading.postValue(true)
        getVisit.getVisitAllDetail(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left ->getVisitDetailResult.postValue(it.left.data)
                is Either.Right ->isError.postValue(it.right)
            }
        }

    }

    fun updateVisit(id: Int, bundle: Bundle) {
        getVisit.updateVisit(id, bundle) {
            when (it) {
                is Either.Left -> result.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun updateIkhtisar(id: Int, visit: IkhtisarRequest) {
        getVisit.updateIkhtisar(id, visit) {
            when (it) {
                is Either.Left -> iktisarResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getIkhtisar(id:Int){
        getVisit.getIkhtisar(id){
            when(it){
                is Either.Left -> getIktisarResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun saveTreatmentData(visitId:Int,visitData: VisitData){
        isLoading.postValue(true)
        getVisit.saveTreatmentData(visitId,visitData){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> saveTreatmentResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}