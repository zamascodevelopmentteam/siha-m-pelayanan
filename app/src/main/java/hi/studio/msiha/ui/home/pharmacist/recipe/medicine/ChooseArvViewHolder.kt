package hi.studio.msiha.ui.home.pharmacist.recipe.medicine

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_arv.*

class ChooseArvViewHolder(view: View) : BaseViewHolder<ArvStock>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: ArvStock,
        callback: BaseAdapter.OnItemClick<ArvStock>?
    ) {
        item.apply {
            arvImg.showFromUrl(null)
            arvNameLbl.text = name
            arvStockLbl.text= "$stockQuantity $stockUnitType"
            arvNotesLbl.visibility=View.GONE
            arvDaysLbl.visibility = View.GONE
            if(stockQuantity?:0<1){
                editBtn.visibility=View.INVISIBLE
            }else {
                editBtn.visibility=View.VISIBLE
                editBtn.text = "Tambah"
            }
        }

        editBtn.setOnClickListener {
            callback?.onClick(it, item, index)
        }
    }
}