package hi.studio.msiha.ui.home.visit.edit.form

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.KEY_VISIT_DETAIL
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.toDate
import hi.studio.msiha.utils.extention.toFormat
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_tbc.*
import java.text.SimpleDateFormat
import java.util.*

class FormTBCFragment : BaseFragment(), WizardStep {
    private val calendar = Calendar.getInstance()
    private val bundle = Bundle()
    private var visitDetail:VisitDetail? = null

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        bundle.apply {
            putString(KEY_STATUS_TB, statusTbSpn.selectedItem.toString())
            putString(KEY_TREATMENT_DATE, startTbExamLbl.tag as String?)
            putBoolean(KEY_PPK, ppkSpn.selectedItemPosition == 1)
            putString(KEY_PPK_DATE, startDatePPKGivenLbl.tag as String?)
            putString(KEY_THERAPY_STATUS, therapyStatusSpn.selectedItem.toString())
            putString(KEY_THERAPY_DATE, startDateTherapyTBLbl.tag as String?)
        }
        return true
    }

    companion object {
        const val KEY_STATUS_TB = "status_tb"
        const val KEY_TREATMENT_DATE = "treatment_date"
        const val KEY_PPK = "ppk"
        const val KEY_PPK_DATE = "ppk_date"
        const val KEY_THERAPY_STATUS = "therapy_status"
        const val KEY_THERAPY_DATE = "therapy_date"

        @JvmStatic
        fun newInstance() =
            FormTBCFragment().apply {
            }
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_tbc
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if(Hawk.get<VisitDetail>(KEY_VISIT_DETAIL)!=null){
            visitDetail = Hawk.get(KEY_VISIT_DETAIL)
            if(visitDetail?.checkOutDate!=null){
                statusTbSpn.isEnabled=false
                ppkSpn.isEnabled=false
                therapyStatusSpn.isEnabled=false
                startTbExamLbl.isEnabled=false
            }
            if(!visitDetail?.treatment?.statusTb.isNullOrEmpty()){
                val arrays = context?.resources?.getStringArray(R.array.tb_status)
                val idx = arrays?.indexOf(visitDetail?.treatment?.statusTb)?:-1
                if(idx!=-1) statusTbSpn.setSelection(idx)
            }
            if(!visitDetail?.treatment?.tglPengobatanTb.isNullOrEmpty()){
                try {
                    val startTbExamDate = visitDetail?.treatment?.tglRujukMasuk.toDate().toFormat()
                    startTbExamLbl.text = startTbExamDate
                    startTbExamLbl.tag = visitDetail?.treatment?.tglRujukMasuk
                }catch(e:Exception){

                }
            }

            if(!visitDetail?.treatment?.statusFungsional.isNullOrEmpty()){
                val arrays = context?.resources?.getStringArray(R.array.functional_status)
                val idx = arrays?.indexOf(visitDetail?.treatment?.statusFungsional)?:-1
                if(idx!=-1) statusTbSpn.setSelection(idx)
                if(idx==2) {
                    startTbExamLayout.visibility = View.VISIBLE
                    startTbExamLine.visibility = View.VISIBLE
                }else{
                    startTbExamLayout.visibility = View.GONE
                    startTbExamLine.visibility = View.GONE
                }
            }
            if(!visitDetail?.treatment?.stadiumKlinis.isNullOrEmpty()){
                val arrays = context?.resources?.getStringArray(R.array.clinical_stadium)
                val idx = arrays?.indexOf(visitDetail?.treatment?.stadiumKlinis)?:-1
                if(idx!=-1) statusTbSpn.setSelection(idx)
            }

            if(visitDetail?.treatment?.ppk == true){
                ppkSpn.setSelection(1)
                startDatePPKGivenLayout.visibility = View.VISIBLE
                startDatePPKGivenLine.visibility = View.VISIBLE
            }else{
                ppkSpn.setSelection(0)
            }

            if(!visitDetail?.treatment?.tglPemberianPpk.isNullOrEmpty()){
                val ppkStartDate=visitDetail?.treatment?.tglPemberianPpk.toDate().toFormat()
                Log.d("ppk","ppk :$ppkStartDate")
                startDatePPKGivenLbl.text=ppkStartDate
                startDatePPKGivenLbl.tag=visitDetail?.treatment?.tglPemberianPpk
            }

            if(!visitDetail?.treatment?.statusTbTpt.isNullOrEmpty()){
                val arrays = context?.resources?.getStringArray(R.array.therapy_status_tb)
                val idx = arrays?.indexOf(visitDetail?.treatment?.statusTbTpt)?:-1
                if(idx!=-1) therapyStatusSpn.setSelection(idx)
                if(idx<1) {
                    startDateTherapyTBLayout.visibility = View.VISIBLE
                    startDateTherapyTBLine.visibility = View.VISIBLE
                }else{
                    startDateTherapyTBLayout.visibility = View.GONE
                    startDateTherapyTBLine.visibility = View.GONE
                }
            }

            if(!visitDetail?.treatment?.tglPemberianTbTpt.isNullOrEmpty()){
                val ppkStartDate=visitDetail?.treatment?.tglPemberianTbTpt.toDate().toFormat()
                startDateTherapyTBLbl.text=ppkStartDate
                startDateTherapyTBLbl.tag=visitDetail?.treatment?.tglPemberianTbTpt
            }

        }
        statusTbSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 2) {
                    startTbExamLayout.visibility = View.VISIBLE
                    startTbExamLine.visibility = View.VISIBLE
                    startTbExamLbl.text = calendar.time.toFormat()
                    startTbExamLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
                } else {
                    startTbExamLayout.visibility = View.GONE
                    startTbExamLine.visibility = View.GONE
                }
            }
        }

        ppkSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 1) {
                    startDatePPKGivenLayout.visibility = View.VISIBLE
                    startDatePPKGivenLine.visibility = View.VISIBLE

                    if(visitDetail?.treatment?.tglPemberianPpk==null) {
                        startDatePPKGivenLbl.text = calendar.time.toFormat()
                        startDatePPKGivenLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
                    }
                } else {
                    startDatePPKGivenLayout.visibility = View.GONE
                    startDatePPKGivenLine.visibility = View.GONE
                }
            }
        }

        therapyStatusSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    startDateTherapyTBLayout.visibility = View.VISIBLE
                    startDateTherapyTBLine.visibility = View.VISIBLE
                    if(visitDetail?.treatment?.tglPemberianTbTpt==null) {
                        startDateTherapyTBLbl.text = calendar.time.toFormat()
                        startDateTherapyTBLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
                    }
                } else {
                    startDateTherapyTBLayout.visibility = View.GONE
                    startDateTherapyTBLine.visibility = View.GONE
                }
            }
        }

        startTbExamLbl.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, s: String, s1: String ->
                startTbExamLbl.text = s1
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                startTbExamLbl.tag=sdf.format(date)

            }
        }

        startDatePPKGivenLbl.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, s: String, s1: String ->
                startDatePPKGivenLbl.text = s1
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                startDatePPKGivenLbl.tag=sdf.format(date)
            }
        }

        startDateTherapyTBLbl.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, s: String, s1: String ->
                startDateTherapyTBLbl.text = s1
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                startDateTherapyTBLbl.tag=sdf.format(date)
            }
        }
    }
}