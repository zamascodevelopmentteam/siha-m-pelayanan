package hi.studio.msiha.ui.home.pharmacist.order.createorder

import android.view.View
import com.bumptech.glide.request.RequestOptions
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.utils.GlideApp
import kotlinx.android.synthetic.main.item_farmasi_stok_obat.view.*


class OrderMedicineViewHolder(private val view: View) : BaseViewHolder<MedicineRsp>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: MedicineRsp,
        callback: BaseAdapter.OnItemClick<MedicineRsp>?
    ) {
        val requestOptions = RequestOptions()
        requestOptions.circleCrop()
        GlideApp.with(view)
            .load(R.drawable.ic_pill)
            .apply(requestOptions)
            .into(view.imgMedicine)
        view.textMedicineName.text=item.name
        view.textMedicineType.text=item.type
        view.textMedicineStock.text = "Stok : ${item.medicineStock.stock}"
        view.layoutWrapper.setOnClickListener {
            callback?.onClick(view,item)
        }
    }
}