package hi.studio.msiha.ui.home.pharmacist.order

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_create_order.*


class PharmacistOrderFragment : BaseFragment() {

    override fun setView(): Int {
        return R.layout.fragment_pharmacist_order
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupToolbarMenu()
    }

    private fun setupToolbarMenu(){
        toolbar.inflateMenu(R.menu.menu_farmasi_order)
        toolbar.setOnMenuItemClickListener { item ->
            if (item != null) {
                when(item.itemId){
                    R.id.createOrder->{
//                        findNavController().navigate(R.id.createOrderFragment)
                    }
                    R.id.showOrderHistory->{

                    }
                }
            }
            false
        }

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_farmasi_order,menu)
    }


}