package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.extention.fromForm
import hi.studio.msiha.utils.extention.getSelectedContent
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_form_personal2.*

class PersonalData2Fragment : BaseFragment(), WizardStep,
    AddPatientFragment.OnAddPatientParentListener {

    private var result: FormResult? = null
    private var religion = ""
    private var profession = ""
    private var education = ""
    private var citizen = ""
    private var blood = ""
    private var marital = ""

    companion object {
        const val KEY_PERSONAL_RELIGION = "personal_religion"
        const val KEY_PERSONAL_PROFESSION = "personal_profession"
        const val KEY_PERSONAL_EDUCATION = "personal_education"
        const val KEY_PERSONAL_CITIZEN = "personal_citizen"
        const val KEY_PERSONAL_ETHNIC = "personal_ethnic"
        const val KEY_PERSONAL_BLOOD = "personal_blood"
        const val KEY_PERSONAL_MARITAL = "personal_marital"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            PersonalData2Fragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = Bundle().apply {
            result?.apply {
                if (success()) {
                    putString(KEY_PERSONAL_RELIGION, religion)
                    putString(KEY_PERSONAL_PROFESSION, profession)
                    putString(KEY_PERSONAL_EDUCATION, education)
                    putString(KEY_PERSONAL_CITIZEN, citizen)
                    putString(KEY_PERSONAL_ETHNIC, this[KEY_PERSONAL_ETHNIC]?.asString())
                    putString(KEY_PERSONAL_MARITAL, marital)
                    putString(KEY_PERSONAL_BLOOD, blood)
                }
            }
        }
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            spinner(R.id.religion_spn, KEY_PERSONAL_RELIGION) {
                //                selection().greaterThan(0).description("Harap pilih jenis agama")
            }
            spinner(R.id.profession_spn, KEY_PERSONAL_PROFESSION) {
                //                selection().greaterThan(0).description("Harap pilih jenis pekerjaan")
            }
            spinner(R.id.education_spn, KEY_PERSONAL_EDUCATION) {
                //                selection().greaterThan(0).description("Harap pilih pendidikan terakhir")
            }
            spinner(R.id.citizen_spn, KEY_PERSONAL_CITIZEN) {
                //                selection().greaterThan(0).description("Harap pilih kewarganegaraan")
            }
            input(R.id.ethnicTxt, KEY_PERSONAL_ETHNIC, optional = true) {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(R.id.blood_spn, KEY_PERSONAL_BLOOD) {
                //                selection().greaterThan(0).description("Harap pilih golongan darah")
            }
            spinner(R.id.marital_status_spn, KEY_PERSONAL_MARITAL) {
                //                selection().greaterThan(0).description("Harap pilih status pernikahan")
            }
        }
        result = form.validate()
        result?.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.religion_spn,
                        R.id.profession_spn,
                        R.id.education_spn,
                        R.id.citizen_spn,
                        R.id.blood_spn,
                        R.id.marital_status_spn -> showMessage(
                            description
                        )
                    }
                }
            } else {
                religion = religion_spn.getSelectedContent()
                profession = profession_spn.getSelectedContent()
                education = education_spn.getSelectedContent()
                citizen = citizen_spn.getSelectedContent()
                blood = blood_spn.getSelectedContent()
                marital = marital_status_spn.getSelectedContent()
            }
        }
        return result?.success() ?: false
    }

    override fun setView(): Int {
        return R.layout.fragment_form_personal2
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if (arguments == null) {
            if (requireParentFragment() is AddPatientFragment) {
                val bundle = (requireParentFragment() as AddPatientFragment).getUserBundle()
                displayData(bundle)
            }
        } else {
            displayData(arguments)
        }
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            val religion = getString(KEY_PERSONAL_RELIGION)
            val profession = getString(KEY_PERSONAL_PROFESSION)
            val education = getString(KEY_PERSONAL_EDUCATION)
            val citizen = getString(KEY_PERSONAL_CITIZEN)
            val ethnic = getString(KEY_PERSONAL_ETHNIC)
            val blood = getString(KEY_PERSONAL_BLOOD)
            val marital = getString(KEY_PERSONAL_MARITAL)
            religion_spn.selectItem(religion)
            profession_spn.selectItem(profession)
            education_spn.selectItem(education)
            citizen_spn.selectItem(citizen)
            ethnicTxt.setText(ethnic?.fromForm())
            blood_spn.selectItem(blood)
            marital_status_spn.selectItem(marital)
        }
    }

    override fun onGetUserByNIK(bundle: Bundle?) {
        displayData(bundle)
    }
}