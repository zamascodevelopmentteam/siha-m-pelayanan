package hi.studio.msiha.ui.home.patient.detail.exam

import android.os.Bundle
import android.view.View
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_IMS_RESULT
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.response.TestIm
import kotlinx.android.synthetic.main.fragment_preview_ims.*

class IMSExamPreviewFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_preview_ims
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        populateView()
    }

    private fun populateView(){
        if(Hawk.contains(KEY_IMS_RESULT)){
            val ims:IMSExamRequest = Hawk.get(KEY_IMS_RESULT)
            if(ims!=null){
                val diTestIms = if(ims.testIms!=null){
                    if(ims.testIms){
                        "YA"
                    }else{
                        "TIDAK"
                    }
                }else{
                    "-"
                }

                val hasilTestIms = if(ims.resultTestIms!=null && ims.testIms == true){
                    if(ims.resultTestIms!=null && ims.resultTestIms){
                        "POSITIF"
                    }else{
                        "NEGATIF"
                    }
                }else{
                    "-"
                }

                val ditesSifilis = if(ims.testSifilis!=null && ims.testIms == true){
                    if(ims.testSifilis!=null && ims.testSifilis){
                        "IYA"
                    }else{
                        "TIDAK"
                    }
                }else{
                    "-"
                }

                val hasilTestSifilis = if(ims.resultTestSifilis!=null && ims.testIms == true){
                    if(ims.resultTestSifilis!=null && ims.resultTestSifilis){
                        "POSITIF"
                    }else{
                        "NEGATIF"
                    }
                }else{
                    "-"
                }




                testImsLbl.text=diTestIms
                testImsResultLbl.text=hasilTestIms
                testSyphilisLbl.text=ditesSifilis
                testSyphilisResultLbl.text=hasilTestSifilis
                reagenLbl.text= ims.namaReagenData?.name?:"-"
            }
        }
    }
}