package hi.studio.msiha.ui.home.visit.edit

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.ui.home.patient.overview.OverviewFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormCoupleFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormFinalFollowUpFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormTBCFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormVisitInfoFragment
import hi.studio.msiha.utils.wizard.WizardAdapter
import hi.studio.msiha.utils.wizard.WizardStep

class VisitEditPagerAdapter(fm: FragmentManager,val bundle: Bundle? = null) : WizardAdapter(fm) {
    private val titles = listOf(
        "Detail Pasien",
        "Ikhtisar",
        "TBC",
        "Pasangan",
        "Akhir Follow Up"
    )

    override fun setData(): MutableList<WizardStep> {
        return mutableListOf(
            FormVisitInfoFragment.newInstance(bundle),
            OverviewFragment.newInstance(),
            FormTBCFragment.newInstance(),
            FormCoupleFragment.newInstance(),
            FormFinalFollowUpFragment.newInstance()
        )
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }
}