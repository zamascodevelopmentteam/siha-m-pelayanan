package hi.studio.msiha.ui.home.profile

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PATIENT
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.profile.edit.EditProfileActivity
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment() {
    private val viewModel by viewModel<ProfileViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_profile
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        logoutBtn.setOnClickListener {
            requireActivity().alert("Anda yakin akan keluar?") {
                positiveButton("Logout") { viewModel.logout() }
                negativeButton("Batal") { it.dismiss() }
            }.show()
        }
        editProfileLbl.setOnClickListener {
            requireActivity().startActivity<EditProfileActivity>()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getProfile(AppSIHA.instance.getUser()?.id ?: 0)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@ProfileFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
            })

            isError.observe(this@ProfileFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            userResult.observe(this@ProfileFragment, Observer {
                observeUser(it)
            })

            logoutResult.observe(this@ProfileFragment, Observer {
                observeLogout(it)
            })

            logoutErrorResult.observe(this@ProfileFragment, Observer {
                observeLogout("")
            })
        }
    }

    private fun observeUser(user: User) {
        (requireActivity() as MainActivity).showLoading(false)
        user.apply {
            userAvatarImg.showFromUrl(avatar)
            userNameLbl.text = name
            userRoleLbl.text = when (role) {
                KEY_ROLE_PATIENT -> "Pasien"
                KEY_ROLE_PHARMACIST -> "Petugas Farmasi"
                KEY_ROLE_RR -> "Petugas RR"
                KEY_ROLE_LAB -> "Petugas Lab"
                else -> ""
            }
        }
    }

    private fun observeLogout(msg: String) {
        (requireActivity() as MainActivity).showLoading(false)
        requireActivity().finish()
        AppSIHA.instance.logout()
    }
}