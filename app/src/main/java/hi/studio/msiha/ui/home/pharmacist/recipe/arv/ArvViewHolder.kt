package hi.studio.msiha.ui.home.pharmacist.recipe.arv

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.Arv
import hi.studio.msiha.domain.model.MedicineInstruction
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_arv.*

class ArvViewHolder(view: View,val clickable:Boolean) : BaseViewHolder<MedicineInstruction>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: MedicineInstruction,
        callback: BaseAdapter.OnItemClick<MedicineInstruction>?
    ) {
        item.apply {
            arvImg.showFromUrl(null)
            arvNameLbl.text = this.name
            arvDaysLbl.text = "${this.jumlahHari} Hari"
            arvStockLbl.text = "${this.stockQty} ${this.unit}"
            arvNotesLbl.text= this.notes
        }
        if(clickable){
            editBtn.visibility=View.VISIBLE
            editBtn.setOnClickListener {
                callback?.onClick(it, item, index)
            }
        }else{
            editBtn.visibility=View.INVISIBLE
        }

    }
}