package hi.studio.msiha.ui.home.recipe.detail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.model.RecipeDetail
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class RecipeDetailViewModel(
    private val getRecipe: GetRecipe
) : BaseViewModel() {
    val recipeDetailResult = MutableLiveData<RecipeDetail?>().toSingleEvent()

    fun getRecipeDetail(recipeId: Int) {
        isLoading.postValue(true)
        getRecipe.getRecipeDetail(recipeId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> recipeDetailResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}