package hi.studio.msiha.ui.auth

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.utils.UpdateChecker
import org.jetbrains.anko.startActivity

class AuthActivity : BaseActivity(), UpdateChecker.Callback {
    private lateinit var navController: NavController
    private var updateChecker: UpdateChecker? = null

    override fun setView(): Int {
        return R.layout.activity_auth
    }

    override fun initView(savedInstanceState: Bundle?) {
        updateChecker = UpdateChecker.init(this).setCallback(this)
        setupNavController()
    }

    override fun onResume() {
        super.onResume()
        updateChecker?.check()
    }

    private fun setupNavController() {
        navController = Navigation.findNavController(this, R.id.authNavHost)
    }

    override fun onStartUpdateCheck() {
        showLoading(true)
    }

    override fun onStopUpdateCheck() {
        showLoading(false)
    }

    override fun onUpdatePressed(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onSkipUpdatePressed() {
        goToHome()
    }

    override fun onNoUpdate() {
        goToHome()
    }

    override fun onCheckUpdateError(e: Throwable) {
        showError(e)
    }

    private fun goToHome() {
        if (AppSIHA.instance.getUser() != null) {
            startActivity<MainActivity>()
            finish()
        }
    }
}