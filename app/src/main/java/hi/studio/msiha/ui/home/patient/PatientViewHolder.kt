package hi.studio.msiha.ui.home.patient

import android.content.Context
import android.util.Log
import android.view.View
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.constant.*
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_patient.*
import kotlinx.android.synthetic.main.item_patient.view.*

class PatientViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<Patient>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: Patient,
        callback: BaseAdapter.OnItemClick<Patient>?
    ) {
        Log.d("testing","patient id : ${item.id} - ${item.name}")
        item.apply {
            view.nameLbl.text = name
            view.nikLbl.text = nik
            view.userAvatarImg.showFromUrl(null)
            val status = when (statusPatient) {
                TYPE_HIV_POSITIF -> "ODHA"
                TYPE_ODHA -> "ODHA dalam Pengobatan "
                TYPE_UNKNOWN -> "Belum Tahu"
                else -> "HIV Negatif"
            }
            hivStatusLbl.text = "Status: $status"
            if (AppSIHA.instance.getUser()?.role == KEY_ROLE_LAB){
                patientGenderLbl.visibility=View.VISIBLE
                patientGenderLbl.text = "Jenis kelamin : ${item.gender}"
            }else {
                patientGenderLbl.visibility=View.GONE
            }
        }

        view.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}