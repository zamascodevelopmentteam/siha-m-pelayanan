package hi.studio.msiha.ui.home.visit.edit.form

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.afollestad.vvalidator.form
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.KEY_VISIT_DETAIL
import hi.studio.msiha.ui.home.visit.edit.VisitEditFragment
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.toDate
import hi.studio.msiha.utils.extention.toDateFormat
import hi.studio.msiha.utils.extention.toFormat
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_info.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class FormVisitInfoFragment : BaseFragment(), WizardStep {
    private var bundle = Bundle()
    private var visitDetail:VisitDetail? =null

    companion object {
        const val KEY_VISIT_DATE = "visitDate"
        const val KEY_VISIT_DATE_TEXT = "visitDate_text"
        const val KEY_CONFIRM_DATE = "confirm_date"
        const val KEY_CONFIRM_DATE_TEXT = "confirm_date_text"
        const val KEY_REFER_IN_DATE = "refer_in_date"
        const val KEY_POPULATION = "population"
        const val KEY_LSM = "lsm"
        const val KEY_FASYANKES = "fasyankes"
        const val KEY_VISIT = "visit_detail"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            FormVisitInfoFragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(R.id.lsmTxt, KEY_LSM, true) {
                isNotEmpty().description(R.string.empty_field)
            }
        }
        val result = form.validate()
        result.let {
            if (it.hasErrors()) {
            } else {
                Log.d("tag","called")
                visitDetail?.treatment?.lsmPenjangkau=lsmTxt.text.toString()
                Hawk.put(KEY_VISIT_DETAIL,visitDetail)
                bundle.apply {
                    putString(KEY_POPULATION, populationSpn.selectedItem.toString())
                    putString(KEY_LSM, lsmTxt.text.toString())
                    putString(KEY_FASYANKES, fasyankesTxt.text.toString())
                    putString(KEY_VISIT_DATE, visitDateLbl.tag as String?)
                    putString(KEY_VISIT_DATE_TEXT, visitDateLbl.text as String?)
                    putString(KEY_CONFIRM_DATE, confirmDateLbl.tag as String?)
                    putString(KEY_CONFIRM_DATE_TEXT, confirmDateLbl.text as String?)
                    putString(KEY_REFER_IN_DATE, referInDateLbl.tag as String?)
                }
            }
        }
        return result.success()
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_info
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        var tglTesHivPositifVal = ""
        var tglKunjunganVal =""
        var tglRujukMasukVal =""
        var namaFasyankesSebelumnya = ""
        var kelompokPopulasiVal=0
        if(Hawk.get<VisitDetail>(KEY_VISIT_DETAIL)!=null){
            visitDetail=Hawk.get(KEY_VISIT_DETAIL)
            if(visitDetail?.checkOutDate!=null){
                referInDateLbl.isEnabled=false
                confirmDateLbl.isEnabled=false
                visitDateLbl.isEnabled=false
                populationSpn.isEnabled=false
                lsmTxt.isEnabled=false
            }

            Timber.d(visitDetail.toString())
            if(!visitDetail?.treatment?.tglRujukMasuk.isNullOrEmpty()){
                tglRujukMasukVal=visitDetail?.treatment?.tglRujukMasuk.toDate().toFormat()
                referInDateLbl.text=tglRujukMasukVal
                referInDateLbl.tag=visitDetail?.treatment?.tglRujukMasuk
            }
            if(!visitDetail?.treatment?.tglKonfirmasiHivPos.isNullOrEmpty()){
                tglTesHivPositifVal= visitDetail?.treatment?.tglKonfirmasiHivPos.toDateFormat().toString()
                confirmDateLbl.text=tglTesHivPositifVal
                confirmDateLbl.tag= visitDetail?.treatment?.tglKonfirmasiHivPos
            }
            if(!visitDetail?.treatment?.tglKunjungan.isNullOrEmpty()){
                tglKunjunganVal=visitDetail?.treatment?.tglKunjungan.toDateFormat().toString()
                visitDateLbl.text=tglKunjunganVal
                visitDateLbl.tag=visitDetail?.treatment?.tglKunjungan
            }
            if(!visitDetail?.treatment?.kelompokPopulasi.isNullOrEmpty()){
                val arrayPopulasi = context?.resources?.getStringArray(R.array.population_group)
                if(arrayPopulasi?.contains(visitDetail?.treatment?.kelompokPopulasi) == true){
                    val idx=arrayPopulasi.indexOf(visitDetail?.treatment?.kelompokPopulasi)
                    if(idx!=-1)populationSpn.setSelection(idx)
                }
            }

            lsmTxt.setText(visitDetail?.treatment?.lsmPenjangkau?:"")
            Log.d("test","testing")
        }else{
            Log.d("test","hawk empty")
        }
        val visitHistory = (parentFragment as VisitEditFragment).visitHistory
        visitHistory?.let {
            Timber.d(visitHistory.toString())
            ordinalLbl.text = "${it.ordinal}"
        }
//        val calendar = Calendar.getInstance()
//        confirmDateLbl.text = calendar.time.toFormat()
//        visitDateLbl.text = calendar.time.toFormat()
//        referInDateLbl.text = calendar.time.toFormat()
//        confirmDateLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
//        visitDateLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
//        referInDateLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
        confirmDateLbl.setOnClickListener(dateClickListener)
        visitDateLbl.setOnClickListener(dateClickListener)
       // referInDateLbl.setOnClickListener(dateClickListener)
    }

    private val dateClickListener = View.OnClickListener {
        showDatePicker(requireContext()) { calendar: Calendar, displayDate: String, dbDate: String ->
            if (it is TextView) {
                it.text = displayDate
                //it.tag = dbDate
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                if(it == confirmDateLbl){
                    visitDetail?.treatment?.tglKonfirmasiHivPos=sdf.format(date)
                }else{
                    visitDetail?.treatment?.tglKunjungan=sdf.format(date)
                }
                if(Hawk.contains(KEY_VISIT_DETAIL)) {
                    Hawk.put(KEY_VISIT_DETAIL,visitDetail)
                }
                it.tag=sdf.format(date)
            }
        }
    }
}