package hi.studio.msiha.ui.home.pharmacist.stock.medicinelist

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class MedicineListViewModel(private val getRecipe: GetRecipe) : BaseViewModel() {
    val getMedicineListResult = MutableLiveData<List<ArvStock>>().toSingleEvent()
    val getReagenListResult = MutableLiveData<List<ArvStock>>().toSingleEvent()

    fun getListMedicine() {
        isLoading.postValue(true)
        getRecipe.getArvStock {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getMedicineListResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getReagenList() {
        isLoading.postValue(true)
        getRecipe.getReagenStock {
            isLoading.postValue(false)
            when (it) {

                is Either.Left -> getReagenListResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}