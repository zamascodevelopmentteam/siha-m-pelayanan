package hi.studio.msiha.ui.home.patient.preview

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.fragment_preview_patient_profile.*

class PatientProfilePreviewFragment : BaseFragment() {
    override fun setView(): Int {
        return R.layout.fragment_preview_patient_profile
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.let { it ->
            val user: User? = it.getParcelable("user")
            user?.apply {
                userAvatarImg.showFromUrl(avatar)
                userInfoLbl.text = "Nama: $name" +
                        "\nNo.Regnas: " +
                        "\nNIK: $nik" +
                        "\n\nKelamin: Laki-Laki" +
                        "\nTanggal Lahir: 01/11/90" +
                        "\nTempat Lahir: Bandung"
                userInfoConfirmLbl.text = "Tambahkan $name kedalam\ndata Pasien?"
            }
        }
    }
}