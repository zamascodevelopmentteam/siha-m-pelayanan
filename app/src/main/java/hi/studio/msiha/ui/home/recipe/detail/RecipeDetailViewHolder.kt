package hi.studio.msiha.ui.home.recipe.detail

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.RecipeMedicine
import kotlinx.android.synthetic.main.item_recipe_medicine_label_only.view.*

class RecipeDetailViewHolder(private val view: View) : BaseViewHolder<RecipeMedicine>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: RecipeMedicine,
        callback: BaseAdapter.OnItemClick<RecipeMedicine>?
    ) {
        view.nameLbl.text = item.detail?.name
        view.ruleLbl.text = item.rule
        view.amountLbl.text = "${item.amount} Tablet"
    }
}