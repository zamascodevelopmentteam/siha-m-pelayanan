package hi.studio.msiha.ui.home.pharmacist.recipe

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.RecipeMedicine
import kotlin.reflect.KProperty

class MedicineAdapter : BaseAdapter<RecipeMedicine>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_pharmacy_recipe_medicine
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return MedicineViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: RecipeMedicine,
        callback: OnItemClick<RecipeMedicine>?
    ) {
        if (holder is MedicineViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<RecipeMedicine>,
        new: MutableList<RecipeMedicine>
    ) {
        autoNotify(old, new) { o, n ->
            o.medicineId == n.medicineId
                    && o.confirmed == n.confirmed
        }
    }
}