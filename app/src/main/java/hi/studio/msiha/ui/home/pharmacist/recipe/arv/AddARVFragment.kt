package hi.studio.msiha.ui.home.pharmacist.recipe.arv

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_MEDICINE_INS
import hi.studio.msiha.data.datasource.remote.request.MedicinePrescriptionRequest
import hi.studio.msiha.domain.model.Arv
import hi.studio.msiha.domain.model.MedicineInstruction
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.LocalSharedViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.ui.home.pharmacist.recipe.ConfirmRecipeBottomSheet
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.toFormat
import kotlinx.android.synthetic.main.fragment_create_recipe.*
import kotlinx.android.synthetic.main.fragment_visit_detail_rr.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.lang.IndexOutOfBoundsException
import java.util.*

class AddARVFragment : BaseFragment() {
    private val sharedViewModel by sharedViewModel<LocalSharedViewModel>()
    private val viewModel by viewModel<ArvViewModel>()
    private lateinit var adapter: ArvAdapter
    private var visitHistory: VisitHistory? = null
    private var visitDetail: VisitDetail? = null
    private lateinit var confirmBottomSheet:ConfirmRecipeBottomSheet

    companion object {
        private val arvs = mutableListOf<Arv>()
    }

    override fun setView(): Int {
        return R.layout.fragment_create_recipe
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            visitHistory = getParcelable(PatientDetailFragment.KEY_VISIT_HISTORY)
            visitDetail = getParcelable(VisitDetailRRFragment.KEY_VISIT_DETAIL)
            visitHistory?.id?.let {
                viewModel.getPreviousVisit(it)
                viewModel.getVisitDetailByVisitId(it)
            }
            visitHistory?.patientId?.let { viewModel.getLastGivenPrescription(it)}
            setupAdapter(visitHistory?.prescription?.isDraft?:true)
        }


        val calendar = Calendar.getInstance()
        arvDateLbl.text = calendar.time.toFormat()
        arvDateLbl.tag = calendar.time.toFormat("yyyy-MM-dd")
        arvDateLbl.setOnClickListener {
            showDatePicker(requireContext()) { _: Calendar, s: String, s1: String ->
                arvDateLbl.text = s1
                arvDateLbl.tag = s
            }
        }

        addArvBtn.setOnClickListener {
            if(Hawk.contains(KEY_MEDICINE_INS)){
                val arrayList:ArrayList<MedicineInstruction> = Hawk.get(KEY_MEDICINE_INS)
                if(arrayList.size>=3) {
                    Toast.makeText(context,"Tidak dapat menambahkan(max 3)",Toast.LENGTH_SHORT).show()
                }else{
                    findNavController().navigate(R.id.action_addARVFragment_to_chooseArvFragment)
                }
            }else{
                findNavController().navigate(R.id.action_addARVFragment_to_chooseArvFragment)
            }

        }

        confirmBtn.setOnClickListener {
            if(Hawk.contains(KEY_MEDICINE_INS)) {
                val medicinePrescription = MedicinePrescriptionRequest(
                    arvDateLbl.tag as String?,
                    arvStatusSpn.selectedItem.toString().toLowerCase(Locale.getDefault()),
                    Hawk.get(KEY_MEDICINE_INS)
                )

                visitHistory?.id?.let { it1 ->
                    observeCreatePrescription()
                    viewModel.createPrescription(medicinePrescription, it1)
                }
            }else{
                Toast.makeText(context,"Anda belum menambahkan obat",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setupPaduanObat()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun setupPaduanObat(){
        if(Hawk.contains(KEY_MEDICINE_INS)){
            val arrayList:ArrayList<MedicineInstruction> = Hawk.get(KEY_MEDICINE_INS)
            var combination = ""
            for(med in arrayList){
                combination+="${med.code} +"
            }
            combination = combination.substring(0,combination.length-1)
            arvCombinationTxt.setText(combination)
        }else{
            arvCombinationTxt.setText(visitDetail?.prescription?.paduanObatStr)

            var selection=0
            when(visitDetail?.prescription?.statusPaduan?.toLowerCase()){
                "orisinal" -> selection=0
                "subtitusi"-> selection=1
                else -> selection=2
            }
            arvStatusSpn.setSelection(selection)
        }
    }

    private fun setupAdapter(isDraft:Boolean) {
        adapter = ArvAdapter(isDraft)
        arvList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@AddARVFragment.adapter
        }

        adapter.itemCallback { view, _, pos ->
            when (view.id) {
                R.id.editBtn -> {
                    Hawk.put("KEY_MEDICINE_EDIT", adapter[pos])
                    val action = AddARVFragmentDirections.actionAddARVFragmentToChooseArvFragment(pos)
                    findNavController().navigate(action)
                }
            }
        }

        Log.d("medicineIns","ins ${Hawk.contains(KEY_MEDICINE_INS)}")
        if(Hawk.contains(KEY_MEDICINE_INS)){
            adapter.addAll(Hawk.get(KEY_MEDICINE_INS))
            textEmpytMedicine.visibility=View.GONE
        }else{
            textEmpytMedicine.visibility=View.VISIBLE
        }
    }

    private fun observeCreatePrescription(){
        viewModel.apply{
            createPrescriptionResult.observe(this@AddARVFragment,Observer{
                confirmBottomSheet = ConfirmRecipeBottomSheet()
                confirmBottomSheet.setCallback {
                    Hawk.delete(KEY_MEDICINE_INS)
                    visitHistory?.id?.let { it1 -> viewModel.giveMedicine(it1) }
                }
                fragmentManager?.let { it1 -> confirmBottomSheet.show(it1,"confirmed") }
                this.createPrescriptionResult.removeObservers(this@AddARVFragment)
            })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewModel() {
        sharedViewModel.apply {
            getArv().observe(this@AddARVFragment, Observer {
                arvs.add(it)
                Timber.d(arvs.toString())
                var combination = ""
                adapter.all.forEachIndexed { index, _ ->
                    if (index < adapter.itemCount) {
                        combination += it.type + "+"
                    }
                }
                arvCombinationTxt.setText(combination)
            })
        }

        viewModel.apply {
            isLoading.observe(this@AddARVFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@AddARVFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            isMessage.observe(this@AddARVFragment, Observer {
                (requireActivity() as PatientDetailActivity).showMessage(it)
            })

            arvResult.observe(this@AddARVFragment, Observer {
                findNavController().navigateUp()
            })

            giveMedicineResult.observe(this@AddARVFragment, Observer {
                confirmBottomSheet.dismiss()
                findNavController().navigateUp()
            })

            getVisitDetailResult.observe(viewLifecycleOwner,Observer{
                Log.d("get visit detail","test")
                Log.d("get visit detail","${Hawk.contains(KEY_MEDICINE_INS)}")
                var isnewData = Hawk.contains(KEY_MEDICINE_INS)
                visitDetail=it
                if(it?.prescription!=null){
                    val medicines = it.prescription.prescriptionMedicines
                    val medicineIns = ArrayList<MedicineInstruction>()

                    if (medicines != null) {
                        for(medicine in medicines){
                            medicineIns.add(medicine.toMedicineInstruction())
                        }
                        if(medicineIns.size==1
                            && medicineIns[0].jumlahHari==0
                            && medicineIns[0].stockQty==0
                            && isnewData){
                            adapter.addAll(Hawk.get(KEY_MEDICINE_INS))
                        }else {
                            adapter.addAll(medicineIns)
                        }
                        textEmpytMedicine.visibility=View.GONE
                    }else{
                        textEmpytMedicine.visibility=View.VISIBLE
                    }
                }

            })

//            getLastGivenMedicine.observe(viewLifecycleOwner,Observer{
//                if(it!=null){
//                    Timber.d("cuk ${it.toString()}")
//                    Timber.d("asu ${it.prescriptionMedicines.toString()}")
//                    val prescriptionList = it.prescriptionMedicines
//                    if(prescriptionList!=null){
//                        medicineLeftWrapper.visibility = View.VISIBLE
//                        Timber.d("presctiption ${prescriptionList.size}")
//                        try{
//                            if(prescriptionList[0]!=null){
//                                arv1Wrapper.visibility=View.VISIBLE
//                                arv1Separator.visibility=View.VISIBLE
//                                Log.d("siha asu","tvObatarv ${prescriptionList[0].medicine?.name?:""}")
//                                tvObatArv1Prev.text = prescriptionList[0].medicine?.name?:""
//                                arv1Days.text="Hari : ${prescriptionList[0].jumlahHari}"
//                                arv1Qty.text="Jumlah : ${prescriptionList[0].amount}"
//                                arv1Notes.text="${prescriptionList[0].notes}"
//                            }else{
//                                arv1Wrapper.visibility=View.GONE
//                                arv1Separator.visibility=View.GONE
//                            }
//                        }catch(e:Exception){
//                            arv1Wrapper.visibility=View.GONE
//                            arv1Separator.visibility=View.GONE
//                        }
//                        try{
//                            if(prescriptionList[1]!=null){
//                                arv2Wrapper.visibility=View.VISIBLE
//                                arv2Separator.visibility=View.VISIBLE
//                                tvObatArv2Prev.text = prescriptionList[1].medicine?.name?:""
//                                arv2Days.text="${prescriptionList[1].jumlahHari}"
//                                arv2Qty.text="${prescriptionList[1].amount}"
//                                arv2Notes.text="${prescriptionList[1].notes}"
//                            }else{
//                                arv2Wrapper.visibility=View.GONE
//                                arv2Separator.visibility=View.GONE
//                            }
//                        }catch(e:Exception){
//                            arv2Wrapper.visibility=View.GONE
//                            arv2Separator.visibility=View.GONE
//                        }
//                        try{
//                            if(prescriptionList[2]!=null){
//                                arv3Wrapper.visibility=View.VISIBLE
//                                arv3Separator.visibility=View.VISIBLE
//                                tvObatArv3Prev.text = prescriptionList[2].medicine?.name?:""
//                                arv3Days.text="${prescriptionList[2].jumlahHari}"
//                                arv3Qty.text="${prescriptionList[2].amount}"
//                                arv3Notes.text="${prescriptionList[2].notes}"
//                            }else{
//                                arv3Wrapper.visibility=View.GONE
//                                arv3Separator.visibility=View.GONE
//                            }
//                        }catch(e:Exception){
//                            arv3Wrapper.visibility=View.GONE
//                            arv3Separator.visibility=View.GONE
//                        }
//                    }else{
//                        Timber.d("null cuk")
//                        arv1Wrapper.visibility=View.GONE
//                        arv1Separator.visibility=View.GONE
//                        arv2Wrapper.visibility=View.GONE
//                        arv2Separator.visibility=View.GONE
//                        arv3Wrapper.visibility=View.GONE
//                        arv3Separator.visibility=View.GONE
//                    }
//                }else{
//                    arv1Wrapper.visibility=View.GONE
//                    arv1Separator.visibility=View.GONE
//                    arv2Wrapper.visibility=View.GONE
//                    arv2Separator.visibility=View.GONE
//                    arv3Wrapper.visibility=View.GONE
//                    arv3Separator.visibility=View.GONE
//                }
//            })

            getPreviousVisitResult.observe(viewLifecycleOwner,Observer{
              if(it!=null){
                  if(it.data!=null) {
                      if (it.data.treatment?.prescription != null) {
                          medicineLeftWrapper.visibility = View.VISIBLE
                          val prescMedicine =  it.data.treatment.prescription.prescriptionMedicines
                          if(prescMedicine==null ||
                             prescMedicine.isEmpty()
                          ){
                              medicineLeftWrapper.visibility=View.GONE
                          }else{
                              try {
                                  if (prescMedicine?.get(0) != null) {
                                      arv1Wrapper.visibility = View.VISIBLE
                                      arv1Separator.visibility = View.VISIBLE
                                      tvObatArv1Prev.text = it.data.treatment.obatArv1Data?.name
                                      arv1Days.text = "Hari : ${prescMedicine[0].jumlahHari}"
                                      arv1Qty.text = "Jumlah : ${prescMedicine[0].amount}"
                                      arv1Notes.text = "${prescMedicine[0].notes}"
                                  } else {
                                      arv1Wrapper.visibility = View.GONE
                                      arv1Separator.visibility = View.GONE
                                  }
                              }catch(e:Exception){
                                  arv1Wrapper.visibility = View.GONE
                                  arv1Separator.visibility = View.GONE
                              }
                          }

                              try {
                                  if (prescMedicine?.get(1) != null) {
                                      arv2Wrapper.visibility = View.VISIBLE
                                      arv2Separator.visibility = View.VISIBLE
                                      tvObatArv2Prev.text = prescMedicine[1].medicine?.name ?: ""
                                      arv2Days.text = "Hari : ${prescMedicine[1].jumlahHari}"
                                      arv2Qty.text = "Jumlah : ${prescMedicine[1].amount}"
                                      arv2Notes.text = "${prescMedicine[1].notes}"
                                  } else {
                                      arv2Wrapper.visibility = View.GONE
                                      arv2Separator.visibility = View.GONE
                                  }
                              }catch(e:Exception){
                                  arv2Wrapper.visibility = View.GONE
                                  arv2Separator.visibility = View.GONE
                              }

                          try {
                              if (prescMedicine?.get(2) != null) {
                                  arv3Wrapper.visibility = View.VISIBLE
                                  arv3Separator.visibility = View.VISIBLE
                                  tvObatArv3Prev.text = prescMedicine[2].medicine?.name ?: ""
                                  arv3Days.text = "Hari : ${prescMedicine[2].jumlahHari}"
                                  arv3Qty.text = "Jumlah : ${prescMedicine[2].amount}"
                                  arv3Notes.text = "${prescMedicine[2].notes}"
                              } else {
                                  arv3Wrapper.visibility = View.GONE
                                  arv3Separator.visibility = View.GONE
                              }
                          }catch (e:Exception){
                              arv3Wrapper.visibility = View.GONE
                              arv3Separator.visibility = View.GONE
                          }

                      } else {
                          medicineLeftWrapper.visibility = View.GONE
                      }
                  }else{
                      medicineLeftWrapper.visibility = View.GONE
                  }
              }else{
                  medicineLeftWrapper.visibility=View.GONE
              }
            })
        }
    }
}