package hi.studio.msiha.ui.home.pharmacist.stock

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.pharmacist.stock.medicinelist.MedicineListViewModel
import kotlinx.android.synthetic.main.fragment_reagen_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReagenListFragment : BaseFragment() {

    private lateinit var medicineListAdapter:AdapterMedicine
    private val viewModel by viewModel<MedicineListViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_reagen_list
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        viewModel.getReagenList()
        observeLiveData()
    }

    private fun observeLiveData(){
        viewModel.apply {
            isLoading.observe(this@ReagenListFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
                layoutEmptyState.visibility=View.INVISIBLE
            })

            isError.observe(this@ReagenListFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getReagenListResult.observe(this@ReagenListFragment, Observer {
                if(it.isNotEmpty()) {
                    recyclerReagen.visibility=View.VISIBLE
                    layoutEmptyState.visibility=View.INVISIBLE
                    medicineListAdapter.addAll(it)
                }else{
                    recyclerReagen.visibility=View.INVISIBLE
                    layoutEmptyState.visibility=View.VISIBLE
                }
            })
        }
    }

    private fun setupAdapter() {
        medicineListAdapter = AdapterMedicine()
        recyclerReagen.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = medicineListAdapter
            addItemDecoration(
                DividerItemDecoration(
                    recyclerReagen.context,
                    RecyclerView.VERTICAL
                )
            )
        }
        medicineListAdapter.itemCallback { _: View, item: ArvStock, _: Int ->
//            val bundle = Bundle()
//            bundle.putParcelable("medicine",item)
//            findNavController().navigate(R.id.medicineDetailFragment,bundle)
        }
        recyclerReagen.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
            }
        })
    }



}