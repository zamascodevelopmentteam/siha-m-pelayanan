package hi.studio.msiha.ui.home.pharmacist.dashboard

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.Order
import kotlinx.android.synthetic.main.item_farmasi_dashboard_reagen.view.*
import java.text.SimpleDateFormat
import java.util.*

class ReagenViewHolder(private val view: View) : BaseViewHolder<Order>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: Order,
        callback: BaseAdapter.OnItemClick<Order>?
    ) {
        view.textInvoiveNo.text= item.invoiceNumber ?: ""
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        view.textDate.text=formatter.format(Date(item.transactionDate))
        view.layoutWrapper.setOnClickListener {
            callback?.onClick(view,item)
        }
    }
}