package hi.studio.msiha.ui.home.pharmacist.dashboard


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.scan.ScanActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_pharmacist_dashboard.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class PharmacistDashboardFragment : BaseFragment() {
    private val viewModel by viewModel<PharmacistDashboardViewModel>()

    companion object {
        const val RC_SCAN_RECIPE = 123
        const val KEY_RECIPE_DETAIL = "recipe_detail"
    }

    override fun setView(): Int {
        return R.layout.fragment_pharmacist_dashboard
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        val user = AppSIHA.instance.getUser()
        user?.let { it ->
            textPharmacistName.text = it.name
            it.upkId?.let {
                viewModel.getHospital(it)
            }
        }
        requireActivity().actionBtn.setOnClickListener {
            startActivityForResult(
                Intent(requireContext(), ScanActivity::class.java)
                    .putExtra(
                        ScanActivity.KEY_SCAN_TYPE,
                        ScanActivity.TYPE_RECIPE
                    ), RC_SCAN_RECIPE
            )
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@PharmacistDashboardFragment, Observer {
                (requireActivity() as MainActivity).showLoading(it)
            })

            isError.observe(this@PharmacistDashboardFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            getHospitalResult.observe(this@PharmacistDashboardFragment, Observer {
                textHospitalName.text = it.name
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SCAN_RECIPE && resultCode == Activity.RESULT_OK) {
            data?.let {
                findNavController().navigate(R.id.patientRecipeFragment, it.extras)
            }
        }
    }
}
