package hi.studio.msiha.ui.home.profile.edit

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.Observer
import com.afollestad.vvalidator.form
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.extention.getAge
import hi.studio.msiha.utils.extention.showCalendar
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditProfileActivity : BaseActivity() {
    private val viewModel by viewModel<EditProfileViewModel>()
    private var user: User? = null

    override fun setView(): Int {
        return R.layout.activity_edit_profile
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        dateOfBornTxt.setOnClickListener {
            dateOfBornTxt.showCalendar(displayFormat = "dd-MM-yyyy") {
                ageTxt.setText(it.getAge().toString())
            }
        }
        validateForm()
    }

    override fun onStart() {
        super.onStart()
        observeViewModel()
        viewModel.getHospitals()
        user = AppSIHA.instance.getUser()
        user?.let { it ->
            it.id?.let {
                viewModel.getProfile(it)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@EditProfileActivity, Observer {
                showLoading(it)
            })

            isError.observe(this@EditProfileActivity, Observer {
                showError(it)
            })

            hospitalsResult.observe(this@EditProfileActivity, Observer {
                setupAdapter(it as MutableList<Hospital>)
            })

            updatedProfileResult.observe(this@EditProfileActivity, Observer {
                it?.let {
                    AppSIHA.instance.setUser(it)
                    finish()
                }
            })

            profileResult.observe(this@EditProfileActivity, Observer {
                displayData(it)
            })
        }
    }

    private fun displayData(user: User?) {
        user?.apply {
            this@EditProfileActivity.user = user
            nikTxt.setText(nik)
            nikTxt.isEnabled = false
            nameTxt.setText(name)
            when (role) {
                KEY_ROLE_RR -> roleSpn.setSelection(1)
                KEY_ROLE_LAB -> roleSpn.setSelection(2)
                KEY_ROLE_PHARMACIST -> roleSpn.setSelection(3)
            }
            phoneTxt.setText(phone)
            emailTxt.setText(email)
            AppSIHA.instance.setUser(this)
        }
    }

    private fun setupAdapter(data: MutableList<Hospital>) {
        data.add(0, Hospital(0, "Pilih Rumah Sakit", "", ""))
        val adapter = object :
            ArrayAdapter<Hospital>(
                this,
                android.R.layout.simple_list_item_1,
                data.toTypedArray()
            ) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                val text1 = view.findViewById(android.R.id.text1) as TextView
                text1.setPadding(16, 0, 0, 0)
                data[position].apply {
                    text1.text = name
                }
                return view
            }

            override fun getItem(position: Int): Hospital? {
                return data[position]
            }

            override fun getCount(): Int {
                return data.size
            }

            override fun getItemId(position: Int): Long {
                return position.toLong()
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val text1 = view.findViewById(android.R.id.text1) as TextView
                data[position].apply {
                    text1.text = name
                }
                return view
            }
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        hospitalSpn.adapter = adapter

        var idx = 0
        data.forEachIndexed { index, hospital ->
            if (hospital.id == AppSIHA.instance.getUser()?.upkId ?: 0) {
                idx = index
                return@forEachIndexed
            }
        }
        hospitalSpn.setSelection(idx)
    }

    private fun validateForm() {
        form {
            input(nikTxt, "nik") {
                isNotEmpty().description(R.string.empty_field)
            }
            input(nameTxt, "name") {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(roleSpn, "role") {
                selection().greaterThan(0).description("Anda belum memilih jenis petugas")
                onErrors { _, errors ->
                    errors.firstOrNull()?.description?.let {
                        showMessage(it)
                    }
                }
            }
            spinner(hospitalSpn, "hospital") {
                selection().greaterThan(0).description("Anda belum memilih rumah sakit")
                onErrors { _, errors ->
                    errors.firstOrNull()?.description?.let {
                        showMessage(it)
                    }
                }
            }
            input(placeOfBornTxt, "place_of_born") {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(genderSpn, "gender") {
                selection().greaterThan(0).description("Anda belum memilih jenis kelamin")
                onErrors { _, errors ->
                    errors.firstOrNull()?.description?.let {
                        showMessage(it)
                    }
                }
            }
            input(dateOfBornTxt, "date_of_born") {
                isNotEmpty().description(R.string.empty_field)
            }
            input(ageTxt, "age") {
                isNotEmpty().description(R.string.empty_field)
            }
            input(phoneTxt, "phone") {
                isNotEmpty().description(R.string.empty_field)
            }
            input(emailTxt, "email") {
                isNotEmpty().description(R.string.empty_field)
            }
            submitWith(saveBtn) { it ->
                if (it.success()) {
                    user?.apply {
                        name = nameTxt.text.toString()
                        role = when (roleSpn.selectedItemPosition) {
                            1 -> KEY_ROLE_RR
                            2 -> KEY_ROLE_LAB
                            3 -> KEY_ROLE_PHARMACIST
                            else -> role
                        }
                        upkId = hospitalSpn.selectedItemId.toInt()
                        phone = phoneTxt.text.toString()
                        email = emailTxt.text.toString()
                    }
                    user?.let {
                        viewModel.updateProfile(it)
                    }
                }
            }
        }
    }
}