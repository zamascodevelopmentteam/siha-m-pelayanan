package hi.studio.msiha.ui

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.utils.ThemeSwitcher
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.default_toolbar.*
import org.koin.android.ext.android.inject
import java.util.*


class MainActivity : BaseActivity(), NavController.OnDestinationChangedListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    private val sp: SharedPreferences by inject()

    override fun setView(): Int {
        return R.layout.activity_main
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
//        supportActionBar?.apply {
//            setBackgroundDrawable(
//                ColorDrawable(
//                    ContextCompat.getColor(
//                        this@MainActivity, when (AppSIHA.instance.getUser()?.role) {
//                            KEY_ROLE_RR -> R.color.rr_theme
//                            KEY_ROLE_LAB -> R.color.lab_theme
//                            KEY_ROLE_PHARMACIST -> R.color.pharmacy_theme
//                            else -> 0
//                        }
//                    )
//                )
//            )
//            val red = Random().nextInt(255)
//            val green = Random().nextInt(255)
//            val blue = Random().nextInt(255)
//        }
        setupNavController()
    }

    private fun setupNavController() {
        val privilege = AppSIHA.instance.getPrivilege()
        privilege?.let {
            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.homeFragment,
                    R.id.pharmacistDashboardFragment,
                    R.id.profileFragment,
                    R.id.visitFragment,
                    R.id.todayVisitFragment,
                    R.id.patientFragment,
                    R.id.pharmacistStockFragment
                )
            )
            navController = Navigation.findNavController(this, R.id.homeNavHost)
            val graph = navController.navInflater.inflate(R.navigation.home_rr_nav_graph)
            graph.startDestination = when (AppSIHA.instance.getUser()?.role) {
                KEY_ROLE_RR, KEY_ROLE_LAB, KEY_ROLE_PHARMACIST -> R.id.homeFragment
                else -> 0
            }
            navController.setGraph(graph, intent.extras)
            homeBottomNav.menu.clear()
            homeBottomNav.inflateMenu(it.menu)
            setupActionBarWithNavController(navController, appBarConfiguration)
            homeBottomNav.setupWithNavController(navController)
            navController.addOnDestinationChangedListener(this)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setupNavController()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    override fun setTitle(title: CharSequence?) {
        super.setTitle(title)
        supportActionBar?.title = title
    }

    override fun setTitle(titleId: Int) {
        super.setTitle(titleId)
        supportActionBar?.setTitle(titleId)
    }

    private fun showFab(show: Boolean, src: Int = R.drawable.ic_add) {
        actionBtn.setImageResource(src)
        val params = actionBtn.layoutParams as CoordinatorLayout.LayoutParams
        params.bottomMargin = if (homeBottomNav.visibility == View.VISIBLE) {
            resources.getDimensionPixelSize(R.dimen._48sdp)
        } else {
            0
        }
        actionBtn.layoutParams = params
        if (show) {
            actionBtn.show()
        } else {
            actionBtn.hide()
        }
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when (destination.id) {
            R.id.medicineDetailFragment,
            R.id.patientRecipeFragment -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.GONE
                showFab(false)
            }
            R.id.patientFragment -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.VISIBLE
                showFab(
                    when (AppSIHA.instance.getUser()?.role) {
                        KEY_ROLE_RR -> true
                        else -> false
                    }
                )
            }
            R.id.homeFragment -> {
                defaultToolbar.visibility = View.GONE
                homeBottomNav.visibility = View.VISIBLE
                showFab(false)
            }
            R.id.visitFragment -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.VISIBLE
                showFab(true)
            }
            R.id.todayVisitFragment->{
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.VISIBLE
                showFab(when (AppSIHA.instance.getUser()?.role) {
                    KEY_ROLE_PHARMACIST -> false
                    else -> true
                })
            }
            R.id.patientSearchFragment -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.GONE
                showFab(true, R.drawable.ic_qr_code)
            }
            R.id.historyVisitFragment ->{
                homeBottomNav.visibility = View.GONE
                showFab(false)
            }
            R.id.visitPlanFragment ->{
                homeBottomNav.visibility = View.GONE
                showFab(false)
            }
            R.id.pharmacistDashboardFragment -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.VISIBLE
                showFab(false, R.drawable.ic_qr_code)
            }
            R.id.pharmacistStockFragment -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.VISIBLE
                showFab(false)
            }
            R.id.addMedicineFragment -> {
                defaultToolbar.visibility = View.GONE
                homeBottomNav.visibility = View.GONE
                showFab(false)
            }
            else -> {
                defaultToolbar.visibility = View.VISIBLE
                homeBottomNav.visibility = View.VISIBLE
                showFab(false)
            }
        }
    }
}
