package hi.studio.msiha.ui.home.patient.add

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.FEMALE
import hi.studio.msiha.data.constant.MALE
import hi.studio.msiha.data.constant.TYPE_HIV_NEGATIVE
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.ui.home.patient.add.form.PMODataFragment
import hi.studio.msiha.ui.home.patient.add.form.PersonalDataFragment
import hi.studio.msiha.utils.extention.*
import kotlinx.android.synthetic.main.fragment_add_patient.*
import kotlinx.android.synthetic.main.fragment_form_personal.*
import kotlinx.android.synthetic.main.fragment_form_pmo.*
import kotlinx.android.synthetic.main.fragment_patient_personal_data.*
import org.jetbrains.anko.alert
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.random.Random

class AddPatientFragment : BaseFragment() {
    private val viewModel by viewModel<AddPatientViewModel>()
    private var userBundle = Bundle()
    private val handler = Handler()
    private val searchRunnable = SearchRunnable()
    var gender = 0
    var isODHA = false
    private var isNikFound = false
    private var currentText = ""
    private var page = 0
    private var calendarSelectedDate: String? = null

    companion object {
        const val KEY_PERSONAL_ID = "personal_id"
        const val KEY_PERSONAL_HOSPITAL_ID = "personal_hospital_id"
        const val KEY_PERSONAL_NIK = "personal_nik"
        const val KEY_PERSONAL_NAME = "personal_name"
        const val KEY_PERSONAL_DOB = "personal_dob"
        const val KEY_PERSONAL_ADDRESS = "personal_address"
        const val KEY_PERSONAL_DOMICILE = "personal_domicile_address"
        const val KEY_PERSONAL_AGE = "personal_age"
        const val KEY_PERSONAL_GENDER = "personal_gender"
        const val KEY_PERSONAL_PHONE = "personal_phone"
        const val KEY_PERSONAL_EMAIL = "personal_email"
        const val KEY_PERSONAL_TYPE = "patient_type"
        const val KEY_PMO_NAME = "pmo_name"
        const val KEY_PMO_RELATION = "pmo_relation"
        const val KEY_PMO_PHONE = "pmo_phone"
        const val KEY_PMO_EMAIL = "pmo_email"
    }

    override fun setView(): Int {
        return R.layout.fragment_add_patient
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        nikClearBtn.setOnClickListener {
            nikTxt.isEnabled = true
            nikTxt.setText("")
            isNikFound = false
            it.visibility = View.GONE
        }

        nikTxt.doAfterTextChanged {
            if (requireParentFragment() is AddPatientFragment
                && !currentText.equals(it.toString(), true)
                && it.toString().isNotBlank()
            ) {
                currentText = it.toString()
                nikLoading.visibility = View.VISIBLE
                (requireParentFragment() as AddPatientFragment).searchByNIK(currentText)
            }
            nikClearBtn.visibility = if (it.toString().isBlank()) {
                View.GONE
            } else {
                if (nikLoading.visibility == View.GONE) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }

        dateOfBornTxt.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, db: String, ds: String ->
                ageTxt.setText(calendar.getAge().toString())
                calendarSelectedDate = db
                dateOfBornTxt.setText(ds)
            }
        }

        generateNewNik()
        nikTxt.setOnFocusChangeListener { _, _ ->
            generateNewNik()
        }

        odhaStatusSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                isODHA = position == 1
            }
        }
        nextBtn.setOnClickListener {
            if (layoutPreview.visibility == View.VISIBLE) {
                val result = validate()
                if (result.success()) {
                    requireActivity().alert("Daftarkan pasien?") {
                        positiveButton("Ya") {
                            if(isODHA && phoneTxt.text.isNullOrEmpty()){
                                Toast.makeText(context,"Pasien ODHA wajib mengisi No telp",Toast.LENGTH_SHORT).show()
                            }else {
                                if(nikTxt.text?.length==0 || nikTxt.text?.length==16) {
                                    viewModel.createPatient(toPatientBundle())
                                }else{
                                    Toast.makeText(context,"NIK harus 16 digit atau kosong",Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                        negativeButton("Batal") { it.dismiss() }
                    }.show()
                } else {
                    page = 0
                    updatePage()
                }
            } else {
                if(!ageTxt.text.isNullOrEmpty()){
                    if (!isODHA) {
                        page = 2
                    } else {
                        page++
                    }
                    updatePage()
                }else{
                    Toast.makeText(context,"Tanggal Lahir wajib di isi",Toast.LENGTH_SHORT).show()
                }

            }
        }

        prevBtn.setOnClickListener {
            if (!isODHA) {
                page = 0
            } else {
                page--
            }
            updatePage()
        }
    }

    private fun updatePage() {
        when (page) {
            0 -> {
                layoutPersonalData.visibility = View.VISIBLE
                layoutPMO.visibility = View.GONE
                layoutPreview.visibility = View.GONE
                prevBtn.visibility = View.GONE
                prevBtn.text = "Kembali"
                nextBtn.text = "Lanjut"
            }
            1 -> {
                layoutPersonalData.visibility = View.GONE
                layoutPMO.visibility = View.VISIBLE
                layoutPreview.visibility = View.GONE
                prevBtn.visibility = View.VISIBLE
                prevBtn.text = "Kembali"
                nextBtn.text = "Konfirmasi"
            }
            2 -> {
                layoutPersonalData.visibility = View.GONE
                layoutPMO.visibility = View.GONE
                layoutPreview.visibility = View.VISIBLE
                prevBtn.visibility = View.VISIBLE
                prevBtn.text = "Ubah"
                nextBtn.text = "Daftarkan"
                showPreviewPatientData()
            }
        }
    }

    private fun generateNewNik() {
        if (!nikTxt.hasFocus() && nikTxt.text.isNullOrBlank()) {
            val random = Random(System.currentTimeMillis())
            val newNik = "99${random.nextInt(99999)}"
            //nikTxt.setText(newNik)
            isNikFound = false
            nikTxt.isEnabled = true
            nikClearBtn.visibility = View.GONE
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.apply {
            isLoading.observe(this@AddPatientFragment, Observer {
                (requireActivity() as PatientFormActivity).showLoading(it)
            })

            isError.observe(this@AddPatientFragment, Observer {
                //(requireActivity() as PatientFormActivity).showError(it)
                Toast.makeText(activity,it.localizedMessage,Toast.LENGTH_LONG).show()
            })

            patientResult.observe(this@AddPatientFragment, Observer { it ->
                if (it == null) {
                    userBundle = Bundle()
                    updateUserData(null)
                } else {
                    val existSheet = PatientExistBottomSheetFragment(it)
                    existSheet.setCallback {
                        existSheet.dismiss()
                        if (it == null) {
                            updateUserData(null)
                        } else {
                            userBundle = Bundle().apply {
                                it.apply {
                                    putInt(KEY_PERSONAL_ID, id ?: 0)
                                    putInt(KEY_PERSONAL_HOSPITAL_ID, upkId ?: 0)
                                    putString(KEY_PERSONAL_NIK, nik)
                                    putString(KEY_PERSONAL_NAME, name)
                                    putString(KEY_PERSONAL_DOB, "01/11/1990")
                                    putString(KEY_PERSONAL_AGE, "28")
                                    putString(KEY_PERSONAL_GENDER, MALE)
                                    putString(KEY_PERSONAL_ADDRESS, addressKTP)
                                    putString(KEY_PERSONAL_DOMICILE, addressKTP)
                                    putString(KEY_PERSONAL_PHONE, phone)
                                }
                            }
                            updateUserData(userBundle)
                        }
                    }
                    existSheet.show(childFragmentManager, "exist")
                }
            })

            createPatientResult.observe(this@AddPatientFragment, Observer {
                (requireActivity() as PatientFormActivity).showMessage(it)
                requireActivity().finish()
            })

            updatePatientResult.observe(this@AddPatientFragment, Observer {
                (requireActivity() as PatientFormActivity).showMessage(it)
                requireActivity().finish()
            })
        }
    }

    private fun updateUserData(bundle: Bundle?) {
        bundle?.apply {
            val nik = this[KEY_PERSONAL_NIK] as String?
            nikTxt.setText(nik?.toForm())
            if (nik == null) {
                generateNewNik()
            } else {
                nikTxt.isEnabled = false
                nikClearBtn.visibility = View.VISIBLE
            }
            nameTxt.setText(this[KEY_PERSONAL_NAME] as String?)
            addressCardIdTxt.setText(this[KEY_PERSONAL_ADDRESS] as String?)
            addressDomicileTxt.setText(this[KEY_PERSONAL_DOMICILE] as String?)
            dateOfBornTxt.showDate(this[KEY_PERSONAL_DOB] as Long)
            ageTxt.setText(this[KEY_PERSONAL_AGE] as String?)
            addressDomicileTxt.setText(this[KEY_PERSONAL_DOMICILE] as String?)
            addressCardIdTxt.setText(this[KEY_PERSONAL_ADDRESS] as String?)
            genderSpn.selectItem((this[KEY_PERSONAL_GENDER] as String?)?.fromForm())
            phoneTxt.setText(this[KEY_PERSONAL_PHONE] as String?)
            emailTxt.setText(this[KEY_PERSONAL_EMAIL] as String?)
        }
    }

    private fun toPatientBundle(): Bundle {

        return userBundle.apply {
            putInt(KEY_PERSONAL_ID, id)
            putInt(KEY_PERSONAL_HOSPITAL_ID, AppSIHA.instance.getUser()?.upkId ?: 0)
            putString(KEY_PERSONAL_NIK, nikTxt.text.toString())
            putString(KEY_PERSONAL_NAME, nameTxt.text.toString())
            putString(KEY_PERSONAL_DOB, calendarSelectedDate.toString())
            putInt(KEY_PERSONAL_AGE, ageTxt.text.toString().toInt())
            putString(
                KEY_PERSONAL_GENDER, when (genderSpn.selectedItemPosition) {
                    0 -> MALE
                    else -> FEMALE
                }
            )
            putString(KEY_PERSONAL_ADDRESS, addressCardIdTxt.text.toString())
            putString(KEY_PERSONAL_DOMICILE, addressDomicileTxt.text.toString())
            putString(KEY_PERSONAL_PHONE, phoneTxt.text.toString())
            putString(KEY_PERSONAL_EMAIL, emailTxt.text.toString())
            putString(
                KEY_PERSONAL_TYPE, if (odhaStatusSpn.selectedItemPosition == 1) {
                    TYPE_ODHA
                } else {
                    TYPE_HIV_NEGATIVE
                }
            )
            putString(KEY_PMO_NAME, pmoNameTxt.text.toString())
            //putString(KEY_PMO_EMAIL, pmoEmailTxt.text.toString())
            putString(KEY_PMO_RELATION, pmoRelationShipTXt.text.toString())
            putString(KEY_PMO_PHONE, pmoPhoneTxt.text.toString())
        }
    }

    private fun validate(): FormResult {
        val form = form {
//            input(R.id.nikTxt, KEY_PERSONAL_NIK) {
//                length().atLeast(0)
//                length().exactly(16)
//            }
            input(R.id.nameTxt, KEY_PERSONAL_NAME) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.addressCardIdTxt, KEY_PERSONAL_ADDRESS) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.addressDomicileTxt, KEY_PERSONAL_DOMICILE) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.dateOfBornTxt, KEY_PERSONAL_DOB) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.ageTxt, KEY_PERSONAL_AGE, true) {
                isNotEmpty().description(R.string.empty_field)
            }
//            spinner(R.id.genderSpn, KEY_PERSONAL_GENDER) {
//                selection().greaterThan(0).description("Harap pilih jenis kelamin")
//                onErrors { _, errors ->
//                    errors.firstOrNull()?.let {
//                        showMessage(it.description)
//                    }
//                }
//            }
            input(R.id.phoneTxt, KEY_PERSONAL_PHONE) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.pmoNameTxt, KEY_PMO_NAME, !isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.pmoPhoneTxt, KEY_PMO_PHONE, !isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.pmoRelationShipTXt, KEY_PMO_RELATION, !isODHA) {
                isNotEmpty().description(R.string.empty_field)
            }
        }
        return form.validate()
    }

    fun searchByNIK(nik: String) {
        searchRunnable.query = nik
        handler.removeCallbacks(searchRunnable)
        handler.postDelayed(searchRunnable, 500)
    }

    fun getUserBundle(): Bundle? {
        return Bundle()
    }

    private fun showPreviewPatientData() {
        toPatientBundle().apply {
            val nik = getString(PersonalDataFragment.KEY_PERSONAL_NIK)
            val name = getString(PersonalDataFragment.KEY_PERSONAL_NAME)
            val address = getString(PersonalDataFragment.KEY_PERSONAL_ADDRESS)
            val dob = getString(PersonalDataFragment.KEY_PERSONAL_DOB)
            val age = getInt(PersonalDataFragment.KEY_PERSONAL_AGE, 0)
            val domicile = getString(PersonalDataFragment.KEY_PERSONAL_DOMICILE)
            val gender = getString(PersonalDataFragment.KEY_PERSONAL_GENDER)
            val phone = getString(PersonalDataFragment.KEY_PERSONAL_PHONE)
            val email = getString(PersonalDataFragment.KEY_PERSONAL_EMAIL)
            val patientType = getString(PersonalDataFragment.KEY_PERSONAL_TYPE)
            val pmoName = getString(PMODataFragment.KEY_PMO_NAME)
            val pmoEmail = getString(PMODataFragment.KEY_PMO_EMAIL)
            val pmoRelation = getString(PMODataFragment.KEY_PMO_RELATION)
            val pmoPhone = getString(PMODataFragment.KEY_PMO_PHONE)

            textPersonalDataNik.text = nik
            textPersonalDataName.text = name
            textPersonalDataAddress.text = address
            textPersonalDataCurrentAddress.text = domicile

            textPersonalDataAge.text = "${dob.toDateFormat()} ($age tahun)"

            textPersonalDataGender.text = gender
            textPersonalDataPhone.text = phone
            textPersonalDataEmail.text = email
            if(isODHA) {
                pmoWrapper.visibility=View.VISIBLE
                textNamaPmo.text = pmoName
                textHubunganPmo.text = pmoRelation
                textNoTelpPMO.text = pmoPhone
            }else{
                pmoWrapper.visibility=View.GONE
            }
        }
    }

    interface OnAddPatientParentListener {
        fun onGetUserByNIK(bundle: Bundle?) {}

        fun onPreviewData(bundle: Bundle?) {}
    }

    inner class SearchRunnable : Runnable {
        var query: String = ""
        override fun run() {
            viewModel.getUserByNIK(query)
        }
    }
}