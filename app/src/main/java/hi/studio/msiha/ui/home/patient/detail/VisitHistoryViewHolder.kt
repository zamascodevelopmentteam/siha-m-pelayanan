package hi.studio.msiha.ui.home.patient.detail

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.extention.toDateFormat
import kotlinx.android.synthetic.main.item_visit_history.*

class VisitHistoryViewHolder(private val context: Context, view: View,private val isOdha:Boolean) :
    BaseViewHolder<VisitHistory>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: VisitHistory,
        callback: BaseAdapter.OnItemClick<VisitHistory>?
    ) {
        item.apply {
            when (AppSIHA.instance.getUser()?.role) {
                KEY_ROLE_RR -> {
                    visitDescLbl.text =
                        "Kunjungan ke - ${item.ordinal}\n(${item.visitDate.toDateFormat()})"
                }
                KEY_ROLE_LAB -> {
                    val status = if (checkOutDate != null) {
                        "Selesai"
                    } else {
                        "Dalam Kunjungan"
                    }
                    visitDescLbl.text = "Kunjungan ke - ${item.ordinal}\n(${status})"
                    if (checkOutDate == null && (testVl!=null || testIms!=null || testHiv!=null)) {
                        visitPreviewBtn.apply {
                            setBackgroundResource(R.drawable.button_bg_primary)
                            setTextColor(Color.WHITE)
                            text = "Tambah"
                        }
                    } else if(checkOutDate == null && (testVl==null && testIms==null && testHiv==null)){
                        visitPreviewBtn.apply {
                            setBackgroundResource(R.drawable.button_bg_primary)
                            setTextColor(Color.WHITE)
                            text = "Ubah"
                        }
                    } else if(checkOutDate!=null){
                        visitPreviewBtn.apply {
                            setBackgroundResource(R.drawable.button_bg_primary_outline)
                            setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                            text = "Lihat"
                        }
                    }
                }
                KEY_ROLE_PHARMACIST -> {
                    visitDescLbl.text =
                        "Kunjungan ke - ${item.ordinal}\n${item.visitDate.toDateFormat()}"
                    Log.d("tag","status patient : ${item.patient?.name} - ${item.patient?.statusPatient}")
                    if(!isOdha){
                        visitPreviewBtn.visibility=View.GONE
                    }else{
                        visitPreviewBtn.visibility=View.VISIBLE
                    }
                    if(item.checkOutDate==null) {
                        visitPreviewBtn.apply {
                            setBackgroundResource(R.drawable.button_bg_primary)
                            setTextColor(Color.WHITE)
                            text = "Ubah"
                        }
                    }else{
                        visitPreviewBtn.apply {
                            setBackgroundResource(R.drawable.button_bg_primary_outline)
                            setTextColor(Color.DKGRAY)
                            text = "Lihat"
                        }
                    }
                }
            }
        }

        visitPreviewBtn.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}