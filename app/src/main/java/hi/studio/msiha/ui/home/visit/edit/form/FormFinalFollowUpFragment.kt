package hi.studio.msiha.ui.home.visit.edit.form

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.ui.home.visit.edit.VisitEditFragment
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.toDate
import hi.studio.msiha.utils.extention.toDateFormat
import hi.studio.msiha.utils.extention.toFormat
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_follow_up.*
import java.text.SimpleDateFormat
import java.util.*

class FormFinalFollowUpFragment : BaseFragment(), WizardStep {
    private var bundle = Bundle()

    companion object {
        const val KEY_FINAL_FOLLOW_UP = "final_follow_up"
        const val KEY_DATE = "follow_up_date"

        @JvmStatic
        fun newInstance() =
            FormFinalFollowUpFragment().apply {
            }
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_follow_up
    }

    private fun setFollowUpdate(followUp:String,date:String){
        Hawk.put("follow_up",followUp)
        Hawk.put("follow_up_date",date)
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setFollowUpdate("","")
        if(Hawk.get<VisitDetail>(VisitDetailRRFragment.KEY_VISIT_DETAIL)!=null) {
            val visitDetail: VisitDetail? = Hawk.get(VisitDetailRRFragment.KEY_VISIT_DETAIL)
            if(visitDetail?.checkOutDate!=null){
                dateLbl.isEnabled=false
                followUpSpn.isEnabled=false
            }
            if(!visitDetail?.treatment?.akhirFollowUp.isNullOrEmpty()){

                val arrays = context?.resources?.getStringArray(R.array.follow_up)
                Log.d("follow up ","error : ${visitDetail?.treatment?.akhirFollowUp}")
                val idx = arrays?.indexOf(visitDetail?.treatment?.akhirFollowUp)?:-1
                if(idx>0)followUpSpn.setSelection(idx)
                when(idx){
                    1 -> {
                        dateLayout.visibility = View.VISIBLE
                        dateTitleLbl.text = "Tanggal Rujuk Keluar"
                        if(visitDetail?.treatment?.tglRujukKeluar!=null) {
                            val tglRujukKeluar =
                                visitDetail?.treatment?.tglRujukKeluar.toDate().toFormat()
                            dateLbl.text = tglRujukKeluar
                            dateLbl.tag = visitDetail?.treatment?.tglRujukKeluar
                            setFollowUpdate(
                                "Rujuk Keluar",
                                visitDetail?.treatment?.tglRujukKeluar ?: ""
                            )
                        }
                    }
                    2 -> {
                        dateLayout.visibility = View.VISIBLE
                        dateTitleLbl.text = "Tanggal Meninggal"
                        val tglMeninggal=visitDetail?.treatment?.tglMeninggal.toDate().toFormat()
                        dateLbl.text=tglMeninggal
                        dateLbl.tag=visitDetail?.treatment?.tglMeninggal
                        setFollowUpdate("Meninggal",visitDetail?.treatment?.tglRujukKeluar?:"")
                    }
                    3 -> {
                        dateLayout.visibility = View.VISIBLE
                        dateTitleLbl.text = "Tanggal Berhenti ARV"
                        val tglBerhentiArv=visitDetail?.treatment?.tglBerhentiArv.toDate().toFormat()
                        dateLbl.text=tglBerhentiArv
                        dateLbl.tag=visitDetail?.treatment?.tglBerhentiArv
                        setFollowUpdate("Berhenti ARV",visitDetail?.treatment?.tglRujukKeluar?:"")
                    }
                    else -> {
                        dateLayout.visibility = View.GONE
                        setFollowUpdate("",visitDetail?.treatment?.tglRujukKeluar?:"")
                    }
                }
            }
        }
//        val visitHistory = (parentFragment as VisitEditFragment).visitHistory
//        visitHistory?.let {
//            dateLbl.text = it.visitDate.toDateFormat()
//            dateLbl.tag = it.visitDate
//        }
        followUpSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Hawk.put("follow_up",followUpSpn.selectedItem.toString())
                when (position) {
                    1 -> {
                        dateLayout.visibility = View.VISIBLE
                        dateTitleLbl.text = "Tanggal Rujuk Keluar"

                    }
                    2 -> {
                        dateLayout.visibility = View.VISIBLE
                        dateTitleLbl.text = "Tanggal Meninggal"
                    }
                    3 -> {
                        dateLayout.visibility = View.VISIBLE
                        dateTitleLbl.text = "Tanggal Berhenti ARV"
                    }
                    else -> {
                        Hawk.put("follow_up_date",null)
                        dateLayout.visibility = View.GONE
                    }
                }
            }
        }

        dateLbl.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, s: String, s1: String ->
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                val date = calendar.time
                calendar.timeZone=TimeZone.getTimeZone("UTC")
                dateLbl.tag=sdf.format(date)
                dateLbl.text = s1
                setFollowUpdate(followUpSpn.selectedItem.toString(),sdf.format(date))
            }
        }
    }

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        bundle.apply {
            putString(KEY_FINAL_FOLLOW_UP, followUpSpn.selectedItem.toString())
            putString(KEY_DATE, dateLbl.tag as String?)
        }
        if (dateLbl.tag == null && followUpSpn.selectedItemPosition > 0) {
            showMessage("Tanggal belum diisi")
            return false
        }
        return true
    }
}