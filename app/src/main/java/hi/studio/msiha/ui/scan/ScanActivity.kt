package hi.studio.msiha.ui.scan

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import kotlinx.android.synthetic.main.default_toolbar.*

class ScanActivity : BaseActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        const val KEY_SCAN_TYPE = "scan_type"
        const val TYPE_PATIENT = 0
        const val TYPE_VISIT = 1
        const val TYPE_RECIPE = 2
        const val TYPE_PATIENT_DETAIL = 3
    }

    private lateinit var navController: NavController

    override fun setView(): Int {
        return R.layout.activity_scan
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        setupNavController()
    }

    private fun setupNavController() {
        navController = Navigation.findNavController(this, R.id.scanNavHost)
        navController.setGraph(R.navigation.scan_nav_graph, intent.extras)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.patientPreviewFragment -> {
                    defaultToolbar.visibility = View.GONE
                }
                else -> {
                    defaultToolbar.visibility = View.VISIBLE
                }
            }
        }
        appBarConfiguration = AppBarConfiguration.Builder()
            .setFallbackOnNavigateUpListener {
                onBackPressed()
                return@setFallbackOnNavigateUpListener false
            }
            .build()
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }
}