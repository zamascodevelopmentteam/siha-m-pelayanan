package hi.studio.msiha.ui.home.recipe

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.Recipe
import kotlin.reflect.KProperty

class RecipeAdapter : BaseAdapter<Recipe>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_recipe
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return RecipeViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: Recipe,
        callback: OnItemClick<Recipe>?
    ) {
        if (holder is RecipeViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<Recipe>,
        new: MutableList<Recipe>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}