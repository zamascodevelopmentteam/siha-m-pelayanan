package hi.studio.msiha.ui.home.treatment

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.utils.extention.fromForm
import hi.studio.msiha.utils.extention.getSelectedContent
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_lab_result.*

class FormLabResultFragment : BaseFragment(), WizardStep {
    private var result: FormResult? = null
    private var condomGiven = ""

    companion object {
        const val KEY_LAB_RESULT = "lab_result"
        const val KEY_LAB_RESULT_DESCRIPTION = "lab_result_desc"
        const val KEY_CONDOM_GIVEN = "condom_given"
        const val KEY_CONDOM_AMOUNT = "total_condom"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            FormLabResultFragment().apply {
                arguments = bundle
            }
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_lab_result
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        displayData(arguments)
    }

    override var value: Bundle
        get() = Bundle().apply {
            result?.apply {
                putString(KEY_LAB_RESULT, this[KEY_LAB_RESULT]?.asString())
                putString(KEY_LAB_RESULT_DESCRIPTION, this[KEY_LAB_RESULT_DESCRIPTION]?.asString())
                putString(KEY_CONDOM_GIVEN, this[KEY_CONDOM_GIVEN]?.asString())
                putInt(KEY_CONDOM_AMOUNT, this[KEY_CONDOM_AMOUNT]?.asInt() ?: 0)
            }
        }
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(
                R.id.labResultTxt,
                KEY_LAB_RESULT
            ) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(
                R.id.descTxt,
                KEY_LAB_RESULT_DESCRIPTION
            ) {
                isNotEmpty().description(R.string.empty_field)
            }
        }
        result = form.validate()
        result?.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.condomGivenSpn -> showMessage(
                            description
                        )
                    }
                }
            } else {
                condomGiven = condomGivenSpn.getSelectedContent()
            }
        }
        return result?.success() ?: false
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            labResultTxt.setText(getString(KEY_LAB_RESULT, "").fromForm())
            descTxt.setText(getString(KEY_LAB_RESULT_DESCRIPTION, "").fromForm())
            condomGivenSpn.selectItem(getString(KEY_CONDOM_GIVEN))
            condomAmountTxt.setText(getInt(KEY_CONDOM_AMOUNT, 0).toString().fromForm())
        }
    }
}