package hi.studio.msiha.ui.home.patient.detail

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.domain.model.PrescriptionMedicine
import kotlin.reflect.KProperty


class RecipeRRAdapter(private val clickable:Boolean) : BaseAdapter<PrescriptionMedicine>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_arv
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return PrecriptionViewHolder(view,clickable)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: PrescriptionMedicine,
        callback: OnItemClick<PrescriptionMedicine>?
    ) {
        (holder as PrecriptionViewHolder).onBind(index, count, item, callback)
    }

    override fun compareDiffUtil(prop: KProperty<*>, old: MutableList<PrescriptionMedicine>, new: MutableList<PrescriptionMedicine>) {
        autoNotify(old, new) { o, n ->
            o.medicineId == n.medicineId
        }
    }
}