package hi.studio.msiha.ui.home.visit.edit.form

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import kotlinx.android.synthetic.main.activity_otp.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class OtpFragment : BaseFragment() {
    private val viewModel by viewModel<OtpViewModel>()
    private var patient: Patient? = null
    private var otpId: Int = 0
    private val timer = TimerRunnable()
    private var counter: Long = 300000L
    private val timeFormat = SimpleDateFormat("mm:ss", Locale.getDefault())
    private val handler = Handler()
    
    companion object {
        const val KEY_PATIENT = "patient"
        const val KEY_OTP_ID = "otp_id"
        const val DURATION = 1000L
    }

    override fun setView(): Int {
        return R.layout.activity_otp
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            patient = getParcelable(KEY_PATIENT)
            otpId = getInt(KEY_OTP_ID, 0)
            otpTitle.text =
                "Silahkan cek nomor telepon pasien\n" +
                        "dan masukkan kode OTP yang telah kami\n" +
                        "kirimkan. Pastikan hanya Anda yang mengetahui\n" +
                        "kode OTP ini."
            if(otpId==10){
                backBtn.visibility=View.VISIBLE
                backBtn.setOnClickListener {
                    findNavController().navigateUp()
                    findNavController().navigateUp()
                }
            }else{
                backBtn.visibility=View.GONE
            }
        }

        observeViewModel()

        resendBtn.setOnClickListener {
            patient?.apply {
                id?.let {
                    observeOtp()
                    viewModel.getOtp(it)
                }
            }
            handler.postDelayed(timer,
                DURATION
            )
        }

        otpView.setOtpCompletionListener {
            viewModel.submitOtp(patient?.id?:0,it)
        }

        continueBtn.setOnClickListener {
            findNavController().navigate(R.id.ODHASuccessFragment)
        }

        handler.postDelayed(timer,
            DURATION
        )
    }
    
    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(timer)
    }

    private fun observeOtp(){
        viewModel.apply{
            getOtpResult.observe(viewLifecycleOwner, Observer {
                otpId = it.id
                getOtpResult.removeObservers(viewLifecycleOwner)
            })
        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@OtpFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@OtpFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            submitOtpResult.observe(this@OtpFragment, Observer {
                findNavController().navigate(R.id.ODHASuccessFragment)
//                it?.let {
//                       continueBtn.isEnabled = true
//                       resendBtn.visibility = View.GONE
//                        otpResendLbl.visibility = View.GONE
//                        handler.removeCallbacks(timer)
//                }
            })
        }
    }

    inner class TimerRunnable : Runnable {
        override fun run() {
            resendBtn?.isEnabled = false
            counter -= 1000 //in millis
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = counter
            if (calendar[Calendar.SECOND] == 0 && calendar[Calendar.MINUTE] == 0) {
                resendBtn.isEnabled = true
                resendBtn.text = "Kirim Ulang"
                handler.removeCallbacks(this)
            } else {
                resendBtn?.text = timeFormat.format(calendar.time)
                handler.postDelayed(timer,
                    DURATION
                )
            }
        }
    }
}