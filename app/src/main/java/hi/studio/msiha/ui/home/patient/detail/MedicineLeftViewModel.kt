package hi.studio.msiha.ui.home.patient.detail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.request.MedicineLeftRequest
import hi.studio.msiha.data.datasource.remote.response.DefaultResponse
import hi.studio.msiha.data.datasource.remote.response.VisitAllDetailResponse
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Prescription
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class MedicineLeftViewModel(private val getVisit: GetVisit,private val getRecipe:GetRecipe) :BaseViewModel(){

    val getPreviousTreatmentResult = MutableLiveData<VisitAllDetailResponse>().toSingleEvent()
    val getLastGivenPresc = MutableLiveData<Prescription>().toSingleEvent()
    val updateSisaObatResult = MutableLiveData<DefaultResponse>().toSingleEvent()

    fun getPreviousTreatment(visitId:Int){
        isLoading.postValue(true)
        getVisit.getPreviousTreatment(visitId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> getPreviousTreatmentResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }

    fun getLastGivenPrescription(patientId:Int){
        isLoading.postValue(true)
        getRecipe.getLastGivenMedicine(patientId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getLastGivenPresc.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }


    fun updateSisaObat(visitId:Int,medicineLeftRequest: MedicineLeftRequest){
        isLoading.postValue(true)
        getVisit.updateSisaObat(visitId,medicineLeftRequest){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> updateSisaObatResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)

            }
        }
    }
}