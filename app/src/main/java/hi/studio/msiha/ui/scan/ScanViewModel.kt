package hi.studio.msiha.ui.scan

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.response.CreateVisitResponse
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.RecipeDetail
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class ScanViewModel(
    private val getUser: GetUser,
    private val getVisit: GetVisit,
    private val getRecipe: GetRecipe,
    private val getPatient: GetPatient
) : BaseViewModel() {
    val userResult = MutableLiveData<User>().toSingleEvent()
    val patientDetailResult = MutableLiveData<User>().toSingleEvent()
    val visitResult = MutableLiveData<Visit>().toSingleEvent()
    val visitErrorResult = MutableLiveData<Throwable>().toSingleEvent()
    val giveRecipeResult = MutableLiveData<String>().toSingleEvent()
    val recipeDetailResult = MutableLiveData<RecipeDetail>().toSingleEvent()
    val patientResult = MutableLiveData<Patient>().toSingleEvent()
    val createVisitResult = MutableLiveData<CreateVisitResponse>().toSingleEvent()

    fun getProfile(id: Int, mode: Int) {
        isLoading.postValue(true)
        getUser.getProfile(id) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> {
                    when (mode) {
                        ScanFragment.MODE_USER_PROFILE -> userResult.postValue(it.left)
                        ScanFragment.MODE_PATIENT_DETAIL -> patientDetailResult.postValue(it.left)
                    }
                }
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun giveRecipe(visitId: Int, patientId: Int, recipeId: Int) {
        isLoading.postValue(true)
        getVisit.giveRecipe(visitId, recipeId, patientId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> giveRecipeResult.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                    visitErrorResult.postValue(it.right)
                }
            }
        }
    }

    fun getRecipeDetail(idRecipe: Int) {
        isLoading.postValue(true)
        getRecipe.getRecipeDetail(idRecipe) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> recipeDetailResult.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }

    fun getPatientByNIK(nik: String) {
        isLoading.postValue(true)
        getPatient.getPatientByNIK(nik) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> patientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

//
//    fun createTreatment(pattien){
//
//    }

}