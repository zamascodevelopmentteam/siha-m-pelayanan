package hi.studio.msiha.ui.home.pharmacist.stock

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import hi.studio.msiha.ui.home.pharmacist.stock.medicinelist.MedicineListFragment


class PharmacistStockPagerAdapter(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int  = 2

    override fun getItem(i: Int): Fragment {
        return when(i){
            0 -> MedicineListFragment()
            else -> ReagenListFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when(position){
            0 ->"ARV"
            else ->"Non ARV"
        }
    }
}