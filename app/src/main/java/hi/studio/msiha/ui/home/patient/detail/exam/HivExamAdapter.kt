package hi.studio.msiha.ui.home.patient.detail.exam

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.TestHiv
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.detail.VisitHistoryViewHolder
import kotlin.reflect.KProperty

class HivExamAdapter:
    BaseAdapter<HivTestHistory>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_exam_history
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return HivExamListViewHolder(context, view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: HivTestHistory,
        callback: OnItemClick<HivTestHistory>?
    ) {
        if (holder is HivExamListViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<HivTestHistory>,
        new: MutableList<HivTestHistory>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}