package hi.studio.msiha.ui.auth.register

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : BaseFragment() {
    private val viewModel by viewModel<RegisterViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_register
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@RegisterFragment, Observer {

            })

            isError.observe(this@RegisterFragment, Observer {

            })

            registerResult.observe(this@RegisterFragment, Observer {

            })
        }
    }
}