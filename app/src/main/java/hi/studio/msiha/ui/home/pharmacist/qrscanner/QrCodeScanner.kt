package hi.studio.msiha.ui.home.pharmacist.qrscanner


import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.zxing.Result
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_qr_code_scanner.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class QrCodeScanner : BaseFragment(),ZXingScannerView.ResultHandler {

    private lateinit var mScannerView: ZXingScannerView
    private var isCaptured = false

    override fun setView(): Int {
        return R.layout.fragment_qr_code_scanner
    }


    override fun initView(view: View, savedInstanceState: Bundle?) {
        initScannerView()
        val user = AppSIHA.instance.getUser()
        imgBackButton.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onStart() {
        mScannerView.startCamera()
        doRequestPermission()
        super.onStart()
    }

    override fun onPause() {
        mScannerView.stopCamera()
        super.onPause()
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context?.let {
                if (checkSelfPermission(it,Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA), 100)
                }
            }
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            100 -> {
                initScannerView()
            }
            else -> {
                /* nothing to do in here */
            }
        }
    }

    override fun handleResult(rawResult: Result?) {
        Timber.d("result ${rawResult.toString()}")
    }



    private fun initScannerView() {
        mScannerView = ZXingScannerView(activity)
        mScannerView.setAutoFocus(true)
        mScannerView.setResultHandler(this)
        scannerLayout.addView(mScannerView)
    }


}
