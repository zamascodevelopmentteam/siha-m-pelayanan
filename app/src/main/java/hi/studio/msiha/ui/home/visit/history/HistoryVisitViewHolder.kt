package hi.studio.msiha.ui.home.visit.history

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.extention.toDateFormat
import kotlinx.android.synthetic.main.item_today_visit.view.*


class HistoryVisitViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<VisitHistory>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: VisitHistory,
        callback: BaseAdapter.OnItemClick<VisitHistory>?
    ) {
        item.apply {

            //TODO handle kasus data lengkap dan kurang lengkap
            view.tvPatientName.text = this.patient?.name
            view.tvVisitOrdinal.text="Kunjungan ke-${this.ordinal}"
            view.tvVisitDate.text=this.visitDate.toDateFormat()
            var inTransit = if(this.isOnTransit==null){
                "Reguler"
            }else{
                if(!this.isOnTransit){
                    "Reguler"
                }else{
                    "Transit"
                }
            }
            if(this.checkOutDate.isNullOrEmpty()){
                view.tvVisitStatus.text="Status : Dalam kunjungan - $inTransit "
            }else{
                view.tvVisitStatus.text="Status : Selesai - $inTransit"
            }
//            when (AppSIHA.instance.getUser()?.role) {
//                KEY_ROLE_PHARMACIST -> showDataCompletionStatus(this.isCompletedForPharma?:false,view)
//                KEY_ROLE_RR -> showDataCompletionStatus(this.isCompletedForRR?:false,view)
//                else -> showDataCompletionStatus(this.isCompletedForLab?:false,view)
//            }
            showDataCompletionStatus(this.isComplete?:false,view)

        }

        view.setOnClickListener {
            //callback?.onClick(it, item)
        }
    }

    private fun showDataCompletionStatus(isComplete:Boolean,view: View){
        if(isComplete){
            view.tvDataReqComplete.visibility= View.VISIBLE
            view.tvDataReqNotComplete.visibility= View.INVISIBLE
        }else{
            view.tvDataReqComplete.visibility= View.INVISIBLE
            view.tvDataReqNotComplete.visibility= View.VISIBLE
        }
    }
}