package hi.studio.msiha.ui.home.patient.detail

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment.Companion.KEY_VISIT_HISTORY
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.fragment_rr_recipe.*
import kotlinx.android.synthetic.main.fragment_visit_detail_rr.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.Exception

class RecipeRRFragment : BaseFragment() {

    private lateinit var adapter:RecipeRRAdapter
    private var visitDetail:VisitDetail? = null
    private var visitHistory:VisitHistory? = null
    private val viewModel by viewModel<VisitDetailViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_rr_recipe
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        Log.d("huff","fragment rr resep opened")
        arguments?.let{
            visitDetail = it.getParcelable(VisitDetailRRFragment.KEY_VISIT_DETAIL)
            visitHistory= it.getParcelable(KEY_VISIT_HISTORY)
        }
        setupPrescriptionList()
        when {
            visitDetail!=null -> {
                Log.d("test","visit detail not null")
                populateView()
            }
            visitHistory!=null -> {
                Log.d("test","visit detail null but visitHistory not null")
                viewModel.getVisitDetailByVisitId(visitHistory?.id?:0)
                observeViewModel()
            }
            else -> {
                Log.d("test","visit detail & bisit history  null")
                showEmptyLayout()
            }
        }
        Log.d("huff","fragment rr resep opened ${visitDetail?.treatment?.prescription?.prescriptionMedicines?.size}")
    }


    private fun observeViewModel() {
            viewModel.apply {
                isLoading.observe(this@RecipeRRFragment, Observer {
                    try {
                        (requireActivity() as PatientDetailActivity).showLoading(it)
                    } catch (e: Exception) {
                        (requireActivity() as MainActivity).showLoading(it)
                    }
                })

                isError.observe(this@RecipeRRFragment, Observer {
                    try {
                        (requireActivity() as PatientDetailActivity).showError(it)
                    } catch (e: Exception) {
                        (requireActivity() as MainActivity).showError(it)
                    }
                })

                isMessage.observe(this@RecipeRRFragment, Observer {
                    try {
                        (requireActivity() as PatientDetailActivity).showMessage(it)
                    } catch (e: Exception) {
                        (requireActivity() as MainActivity).showMessage(it)
                    }
                })

                getVisitDetailResult.observe(viewLifecycleOwner,Observer{
                    visitDetail=it
                    Log.d("test","it : $it")
                    if(visitDetail!=null)
                        populateView()
                    else
                        showEmptyLayout()
                })
            }
    }
    private fun populateView(){
        if(visitDetail!!.treatment!=null) {
            if (visitDetail!!.treatment!!.prescription != null) {
                if (visitDetail!!.treatment!!.prescription!!.prescriptionMedicines!=null) {
                    val prescriptionMedicine = visitDetail!!.treatment!!.prescription!!.prescriptionMedicines
                    Log.d("prescription","prescription medicines ${prescriptionMedicine?.size}")
                    if(prescriptionMedicine!!.isNotEmpty()) {
                        hideEmptyLayout()
                        if(visitDetail!!.treatment!!.obatArv1Data!=null){
                            val medName =visitDetail!!.treatment!!.obatArv1Data!!.name
                            prescriptionMedicine[0].medicineName=medName
                        }
                        if(visitDetail!!.treatment!!.obatArv2Data!=null){
                            prescriptionMedicine[1].medicineName=visitDetail!!.treatment!!.obatArv2Data!!.name
                        }
                        if(visitDetail!!.treatment!!.obatArv3Data!=null){
                            prescriptionMedicine[2].medicineName=visitDetail!!.treatment!!.obatArv3Data!!.name
                        }
                        try{
                            if(prescriptionMedicine[0].medicineName==null){
                                showEmptyLayout()
                            }else{
                                adapter.addAll(visitDetail!!.treatment!!.prescription!!.prescriptionMedicines!!)
                            }
                        }catch(e:Exception){
                            showEmptyLayout()
                        }
                    }else{
                        Log.d("prescription","prescription medicine null")
                        showEmptyLayout()
                    }
                }else{
                    Log.d("prescription","prescription medicines null")
                    showEmptyLayout()
                }
            }else{

                Log.d("prescription","prescription null")
                showEmptyLayout()
            }
        }
    }
    private fun showEmptyLayout(){
        rvPrescription.visibility=View.INVISIBLE
        emptyWrapper.visibility=View.VISIBLE
    }

    private fun hideEmptyLayout(){
        rvPrescription.visibility=View.VISIBLE
        emptyWrapper.visibility=View.INVISIBLE
    }

    private fun setupPrescriptionList(){
            adapter = RecipeRRAdapter(false)
            rvPrescription.apply {
                layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
                addItemDecoration(
                    DividerItemDecoration(
                        requireContext(),
                        DividerItemDecoration.VERTICAL
                    )
                )
                adapter = this@RecipeRRFragment.adapter
            }
    }
}