package hi.studio.msiha.ui.home.patient

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_patient_lab.view.*

class PatientLabViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<User>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: User,
        callback: BaseAdapter.OnItemClick<User>?
    ) {
        item.apply {
            view.userNameLbl.text = name
            view.userAvatarImg.showFromUrl(avatar, context)
            view.userNIKLbl.text = nik
            view.hivStatusLbl.text = "HIV: -"
            view.sifilisStatusLbl.text = "Sifilis: -"
        }

        view.editExamBtn.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}