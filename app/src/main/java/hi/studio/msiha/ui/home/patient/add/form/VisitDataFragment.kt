package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.extention.getSelectedContent
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.extention.showCalendar
import hi.studio.msiha.utils.extention.showDate
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_form_visit.*

class VisitDataFragment : BaseFragment(), WizardStep,
    AddPatientFragment.OnAddPatientParentListener {
    private var result: FormResult? = null
    private var bornStatus = false
    private var pregnantStatus = ""
    private var pregnantAge = 0
    private var hivStatus = false
    private var deliveryMethod = ""
    private var childBornDate: String? = null

    companion object {
        const val KEY_VISIT_PREGNANT_STATUS = "visit_pregnant_status"
        const val KEY_VISIT_PREGNANT_AGE = "visit_pregnant_age"
        const val KEY_VISIT_HIV_STATUS = "visit_hiv_status"
        const val KEY_VISIT_DELIVERY_METHOD = "visit_delivery_method"
        const val KEY_VISIT_DELIVERY_DATE = "visit_delivery_date"
        const val KEY_VISIT_BORN_STATUS = "visit_born_status"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            VisitDataFragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = Bundle().apply {
            result?.apply {
                if (success()) {
                    putString(KEY_VISIT_PREGNANT_STATUS, pregnantStatus)
                    putInt(KEY_VISIT_PREGNANT_AGE, pregnantAge)
                    putBoolean(KEY_VISIT_HIV_STATUS, hivStatus)
                    putString(KEY_VISIT_DELIVERY_METHOD, deliveryMethod)
                    putString(KEY_VISIT_DELIVERY_DATE, childBornDate)
                    putBoolean(KEY_VISIT_BORN_STATUS, bornStatus)
                }
            }
        }
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            spinner(R.id.pregnantStatusSpn, KEY_VISIT_PREGNANT_STATUS) {
                //                selection().greaterThan(0).description("Harap pilih status kehamilan")
            }
            spinner(R.id.pregnantAgeSpn, KEY_VISIT_PREGNANT_AGE) {
                //                selection().greaterThan(0).description("Harap pilih umur kehamilan")
            }
            spinner(R.id.hivStatusSpn, KEY_VISIT_HIV_STATUS) {
                //                selection().greaterThan(0).description("Harap pilih status HIV")
            }
            spinner(R.id.deliveryMethodSpn, KEY_VISIT_DELIVERY_METHOD) {
                //                selection().greaterThan(0).description("Harap pilih status persalinan")
            }
            input(R.id.deliveryDateTxt, KEY_VISIT_DELIVERY_DATE, optional = true) {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(R.id.bornStatusSpn, KEY_VISIT_DELIVERY_METHOD) {
                //                selection().greaterThan(0).description("Harap pilih status persalinan")
            }
        }
        result = form.validate()
        result?.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.pregnantStatusSpn,
                        R.id.pregnantAgeSpn,
                        R.id.hivStatusSpn,
                        R.id.deliveryMethodSpn,
                        R.id.bornStatusSpn
                        -> showMessage(description)
                    }
                }
            } else {
                bornStatus = bornStatusSpn.selectedItem == 1
                pregnantStatus = pregnantStatusSpn.getSelectedContent()
                pregnantAge = if (pregnantAgeSpn.getSelectedContent().isBlank()) {
                    0
                } else {
                    pregnantAgeSpn.getSelectedContent().toInt()
                }
                hivStatus = hivStatusSpn.selectedItemPosition == 1
                deliveryMethod = deliveryMethodSpn.getSelectedContent()
                childBornDate = deliveryDateTxt.tag as String? ?: "2012-12-12"
            }
        }
        return result?.success() ?: false
    }

    override fun setView(): Int {
        return R.layout.fragment_form_visit
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if (arguments == null) {
            if (requireParentFragment() is AddPatientFragment) {
                val bundle = (requireParentFragment() as AddPatientFragment).getUserBundle()
                displayData(bundle)
            }
        } else {
            displayData(arguments)
        }
        deliveryDateTxt.setOnClickListener {
            deliveryDateTxt.showCalendar()
        }
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            val pregnantStatus = getString(KEY_VISIT_PREGNANT_STATUS)
            val pregnantAge = getString(KEY_VISIT_PREGNANT_AGE)
            val hivStatus = getString(KEY_VISIT_HIV_STATUS)
            val deliveryMethod = getString(KEY_VISIT_DELIVERY_METHOD)
            val deliveryDate = getLong(KEY_VISIT_DELIVERY_DATE)
            val bornStatus = getString(KEY_VISIT_BORN_STATUS)
            pregnantStatusSpn.selectItem(pregnantStatus)
            pregnantAgeSpn.selectItem(pregnantAge)
            hivStatusSpn.selectItem(hivStatus)
            deliveryMethodSpn.selectItem(deliveryMethod)
            deliveryDateTxt.showDate(deliveryDate)
            bornStatusSpn.selectItem(bornStatus)
        }
        val gender = (requireParentFragment() as AddPatientFragment).gender
        if (gender == 1) {
            pregnancyInfoLayout.visibility = View.GONE
            pregnancyInfoLayout2.visibility = View.GONE
        } else {
            pregnancyInfoLayout.visibility = View.VISIBLE
            pregnancyInfoLayout2.visibility = View.VISIBLE
        }
    }

    override fun onGetUserByNIK(bundle: Bundle?) {
        displayData(bundle)
    }
}