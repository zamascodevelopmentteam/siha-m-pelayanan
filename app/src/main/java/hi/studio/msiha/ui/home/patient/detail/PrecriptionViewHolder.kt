package hi.studio.msiha.ui.home.patient.detail

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.MedicineInstruction
import hi.studio.msiha.domain.model.PrescriptionMedicine
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_arv.*


class PrecriptionViewHolder(view: View, val clickable:Boolean) : BaseViewHolder<PrescriptionMedicine>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: PrescriptionMedicine,
        callback: BaseAdapter.OnItemClick<PrescriptionMedicine>?
    ) {
        item.apply {
            arvImg.showFromUrl(null)
            arvNameLbl.text = "${this.medicineName}"
            arvDaysLbl.text = "${this.jumlahHari} Hari"
            arvStockLbl.text = "${this.amount}"
            arvNotesLbl.text= this.notes
        }
        if(clickable){
            editBtn.visibility= View.VISIBLE
            editBtn.setOnClickListener {
                callback?.onClick(it, item, index)
            }
        }else{
            editBtn.visibility= View.INVISIBLE
        }

    }
}