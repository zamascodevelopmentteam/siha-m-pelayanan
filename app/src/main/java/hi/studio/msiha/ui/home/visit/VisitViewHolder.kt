package hi.studio.msiha.ui.home.visit

import android.content.Context
import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.data.constant.TYPE_HIV_POSITIF
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.extention.showFromUrl
import kotlinx.android.synthetic.main.item_patient.*
import kotlinx.android.synthetic.main.item_patient.view.*

class VisitViewHolder(private val context: Context, private val view: View) :
    BaseViewHolder<Patient>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: Patient,
        callback: BaseAdapter.OnItemClick<Patient>?
    ) {
        item.apply {
            view.nameLbl.text = name
            view.nikLbl.text = nik
            view.userAvatarImg.showFromUrl(null)
            val status = when (statusPatient) {
                TYPE_HIV_POSITIF -> "HIV Positif"
                TYPE_ODHA -> "Pasien ODHA"
                else -> "HIV Negatif"
            }
            hivStatusLbl.text = "Status: $status"
        }

        view.setOnClickListener {
            callback?.onClick(it, item)
        }
    }
}