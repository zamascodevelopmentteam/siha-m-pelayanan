package hi.studio.msiha.ui.home.profile.edit

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetHospital
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class EditProfileViewModel(
    private val getHospital: GetHospital,
    private val getUser: GetUser
) : BaseViewModel() {
    val hospitalsResult = MutableLiveData<List<Hospital>>().toSingleEvent()
    val updatedProfileResult = MutableLiveData<User>().toSingleEvent()
    val profileResult = MutableLiveData<User>().toSingleEvent()

    fun getHospitals() {
        isLoading.postValue(true)
        getHospital.getHospitals {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> hospitalsResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun updateProfile(user: User) {
        isLoading.postValue(true)
        getUser.updateProfile(user) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> updatedProfileResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getProfile(id: Int) {
        isLoading.postValue(true)
        getUser.getProfile(id) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> profileResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}