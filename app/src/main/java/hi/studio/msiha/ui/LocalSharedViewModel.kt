package hi.studio.msiha.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.model.Arv
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitOverview
import hi.studio.msiha.utils.toSingleEvent

class LocalSharedViewModel : BaseViewModel() {
    private val VisitOverview = MutableLiveData<VisitOverview>().toSingleEvent()
    private val VisitData = MutableLiveData<VisitData>().toSingleEvent()
    private val arvData = MutableLiveData<Arv>().toSingleEvent()

    fun setArv(data: Arv) {
        arvData.postValue(data)
    }

    fun getArv(): LiveData<Arv> {
        return arvData
    }

    fun setOverview(data: VisitOverview) {
        VisitOverview.postValue(data)
    }

    fun getOverview(): LiveData<VisitOverview> {
        return VisitOverview
    }

    fun setVisitData(data: VisitData) {
        VisitData.postValue(data)
    }

    fun getVisitData(): LiveData<VisitData> {
        return VisitData
    }
}