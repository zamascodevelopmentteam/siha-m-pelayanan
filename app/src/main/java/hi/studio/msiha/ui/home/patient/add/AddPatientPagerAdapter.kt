package hi.studio.msiha.ui.home.patient.add

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import hi.studio.msiha.ui.home.patient.add.form.PMODataFragment
import hi.studio.msiha.ui.home.patient.add.form.PersonalDataFragment
import hi.studio.msiha.ui.home.patient.add.form.PreviewPatientDataFragment
import hi.studio.msiha.utils.wizard.WizardAdapter
import hi.studio.msiha.utils.wizard.WizardStep

class AddPatientPagerAdapter(fm: FragmentManager, val bundle: Bundle? = null) : WizardAdapter(fm) {

    override fun setData(): MutableList<WizardStep> {
        return mutableListOf(
            PersonalDataFragment.newInstance(bundle),
            PMODataFragment.newInstance(bundle),
            PreviewPatientDataFragment.newInstance(bundle)
        )
    }
}