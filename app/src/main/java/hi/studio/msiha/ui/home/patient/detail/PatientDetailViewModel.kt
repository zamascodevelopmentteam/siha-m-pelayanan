package hi.studio.msiha.ui.home.patient.detail

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.response.CreateVisitResponse
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Otp
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class PatientDetailViewModel(
    private val getPatient: GetPatient,
    private val getVisit: GetVisit,
    private val getUser: GetUser
) : BaseViewModel() {
    val visitHistoriesResult = MutableLiveData<List<VisitHistory>>().toSingleEvent()
    val visitResult = MutableLiveData<String>().toSingleEvent()
    val createVisitResult = MutableLiveData<CreateVisitResponse>().toSingleEvent()
    val getPatientByNikResult = MutableLiveData<Patient>().toSingleEvent()
    val getOtpResult = MutableLiveData<Otp>().toSingleEvent()


    fun getOtp(idPatient: Int) {
        isLoading.postValue(true)
        getUser.getOtp(idPatient) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getOtpResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getPatientByNik(nik:String){
        getPatient.getPatientByNIK(nik){
            when(it){
                is Either.Left -> getPatientByNikResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getVisitHistories(idPatient: Int,type:String) {
        isLoading.postValue(true)
        getPatient.getVisitHistories(idPatient,type) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> visitHistoriesResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createVisitOdha(ordinal: Int, idPatient: Int, date: String) {
        isLoading.postValue(true)
        getVisit.createVisitODHA(ordinal, idPatient, date) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> visitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createVisitByPatientId(patientId:Int){
        isLoading.postValue(true)
        getVisit.createGeneralVisit(patientId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}