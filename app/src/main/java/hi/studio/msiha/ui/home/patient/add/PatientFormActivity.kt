package hi.studio.msiha.ui.home.patient.add

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import kotlinx.android.synthetic.main.default_toolbar.*

class PatientFormActivity : BaseActivity() {
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        const val KEY_EDIT_MODE = "edit_mode"
    }

    override fun setView(): Int {
        return R.layout.activity_patient_form
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        setupNavController()
        intent.extras?.apply {
            if (getBoolean(KEY_EDIT_MODE, false)) {
                supportActionBar?.title = "Ubah Data Pasien"
            } else {
                supportActionBar?.title = "Daftar Pasien"
            }
        }
    }

    private fun setupNavController() {
        navController = Navigation.findNavController(this, R.id.patientFormNavHost)
        navController.setGraph(R.navigation.patient_form_nav_graph, intent.extras)
        appBarConfiguration = AppBarConfiguration.Builder()
            .setFallbackOnNavigateUpListener {
                onBackPressed()
                return@setFallbackOnNavigateUpListener false
            }
            .build()
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}