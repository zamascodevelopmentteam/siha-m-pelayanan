package hi.studio.msiha.ui.home.pharmacist.recipe

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.interactor.GetRecipe
import hi.studio.msiha.domain.interactor.GetUser
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.RecipeDetail
import hi.studio.msiha.domain.model.RecipeMedicine
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class PatientRecipeViewModel(
    private val getUser: GetUser,
    private val getPharmacist: GetPharmacist,
    private val getRecipe: GetRecipe
) : BaseViewModel() {
    val profileResult = MutableLiveData<User>().toSingleEvent()
    val confirmMedicineResult = MutableLiveData<Pair<RecipeMedicine, Int>>().toSingleEvent()
    val recipeDetailResult = MutableLiveData<RecipeDetail>().toSingleEvent()
    val confirmRecipeResult = MutableLiveData<String>().toSingleEvent()
    val medicineDetailResult = MutableLiveData<MedicineRsp>().toSingleEvent()

    fun getProfile(userId: Int) {
        getUser.getProfile(userId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> profileResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun confirmMedicine(recipeMedicine: RecipeMedicine, position: Int) {
        isLoading.postValue(true)
        getPharmacist.confirmMedicine(recipeMedicine.medicineId) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> {
                    isMessage.postValue(it.left)
                    confirmMedicineResult.postValue(Pair(recipeMedicine, position))
                }
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getRecipeDetail(idRecipe: Int) {
        isLoading.postValue(true)
        getRecipe.getRecipeDetail(idRecipe) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> recipeDetailResult.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }

    fun confirmRecipe(idRecipe: Int) {
        isLoading.postValue(true)
        getRecipe.confirmRecipe(idRecipe) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> confirmRecipeResult.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }

    fun getMedicineDetail(id: Int) {
        isLoading.postValue(true)
        getPharmacist.getMedicineDetail(id) {
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> medicineDetailResult.postValue(it.left)
                is Either.Right -> {
                    isError.postValue(it.right)
                }
            }
        }
    }
}