package hi.studio.msiha.ui.home.patient.detail.exam

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.KEY_IMS_RESULT
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.detail.ExamViewModel
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailFragment
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment.Companion.KEY_VISIT_DETAIL
import hi.studio.msiha.ui.home.patient.detail.exam.reagen.ReagenChooseActivity
import kotlinx.android.synthetic.main.fragment_exam_ims.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class IMSExamFragment : BaseFragment() {
    private val viewModel by viewModel<ExamViewModel>()
    private var qtyReagen:Int = 0

    companion object {
        const val RC_CHOOSE_REAGEN = 507
        const val KEY_REAGEN_IMS = "reagen_ims"
        const val KEY_IMS_TEST_RESULT = "ims_result"
        const val KEY_SYPHILIS_TEST_RESULT = "syphilis_result"
        const val KEY_IS_IMS_TEST = "is_ims_test"
        const val KEY_IS_SYPHILIS_TEST = "is_ims_test"
    }

    override fun setView(): Int {
        return R.layout.fragment_exam_ims
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        testImsSpn.onItemSelectedListener = dropdownListener
        testSyphilisSpn.onItemSelectedListener = dropdownListener
        reagenLbl.setOnClickListener(reagenChooseListener)
        populateView()
        saveBtn.setOnClickListener {
            val visitHistory: VisitHistory =
                arguments?.get(PatientDetailFragment.KEY_VISIT_HISTORY) as VisitHistory
            val ims = IMSExamRequest(
                ordinal = visitHistory.ordinal,
                visitId = visitHistory.id,
                testIms = testImsSpn.selectedItemPosition == 1,
                namaReagen = reagenLbl.tag as Int?,
                qty = qtyReagen,
                //nameReagen = reagenLbl.text.toString(),
                resultTestIms = testImsResultSpn.selectedItemPosition == 1,
                testSifilis = testSyphilisSpn.selectedItemPosition == 1,
                resultTestSifilis = testSyphilisResultSpn.selectedItemPosition == 1
            )
            Hawk.put(KEY_IMS_RESULT,ims)
            viewModel.updateIMSExam(ims)
        }
    }

    private fun populateView(){
        var ims = if(Hawk.contains(KEY_IMS_RESULT)){
            Hawk.get(KEY_IMS_RESULT)
        }else if(Hawk.contains(KEY_VISIT_DETAIL)){
            Hawk.get<VisitDetail>(KEY_VISIT_DETAIL).testIms
        }else{
            null
        }

        if(ims!=null) {
            Log.d("test", "ims $ims")
            if (ims != null) populateView(ims)
        }
    }

    private fun populateView(ims:IMSExamRequest){
        if(ims.testIms!=null){
            if(ims.testIms){
                testImsSpn.setSelection(1)
            }else{
                testImsSpn.setSelection(0)
            }

            imsTestLayout.visibility = if (ims.testIms) {
                View.GONE
            } else {
                View.VISIBLE
            }

            sifilisReagenWrapper.visibility= if (ims.testIms) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }

        if(ims.resultTestIms!=null){
            if(ims.resultTestIms){
                testImsResultSpn.setSelection(1)
            }else{
                testImsResultSpn.setSelection(0)
            }
        }

        if(ims.testSifilis!=null){
            if(ims.testSifilis){
                testSyphilisSpn.setSelection(1)
            }else{
                testSyphilisSpn.setSelection(0)
            }
        }

        if(ims.resultTestSifilis!=null){
            if(ims.resultTestSifilis){
                testSyphilisResultSpn.setSelection(1)
            }else{
                testSyphilisResultSpn.setSelection(0)
            }
        }

        if(ims.namaReagenData!=null) reagenLbl.text = ims.namaReagenData!!.name

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@IMSExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@IMSExamFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            examResult.observe(this@IMSExamFragment, Observer {
                findNavController().navigateUp()
            })
        }
    }


    private val dropdownListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ) {
            when (parent?.id) {
                R.id.testImsSpn -> {
                    imsTestLayout.visibility = if (position == 0) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }

                }
                R.id.testSyphilisSpn -> {
                    syphilisTestLayout.visibility = if (position == 0) {
                        View.GONE
                    } else {
                        if(qtyReagen!=0) {
                            View.VISIBLE
                        }else{
                            View.GONE
                        }
                    }
                    sifilisReagenWrapper.visibility= if (position==0) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }
                }
            }
        }
    }

    private val reagenChooseListener = View.OnClickListener {
        Hawk.put(HIVExamFragment.KEY_REAGEN_TYPE,"is_sifilis")
        startActivityForResult(
            Intent(requireContext(), ReagenChooseActivity::class.java)
                .putExtra("type", KEY_REAGEN_IMS),
            RC_CHOOSE_REAGEN
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            data?.apply {
                val medicineName = getStringExtra("name")
                val medicineId = getIntExtra("id", 0)
                qtyReagen= getIntExtra("qty", 0)
                if(medicineName!=null){
                    syphilisTestLayout.visibility=View.VISIBLE
                }else{
                    syphilisTestLayout.visibility=View.GONE
                }
                updateButton(
                    when (requestCode) {
                        RC_CHOOSE_REAGEN -> reagenLbl
                        else -> null
                    }, medicineId, "$medicineName($qtyReagen)"
                )
            }
        }
    }

    private fun updateButton(view: AppCompatTextView?, id: Int, name: String) {
        view?.apply {
            text = name
            tag = id
        }
    }
}