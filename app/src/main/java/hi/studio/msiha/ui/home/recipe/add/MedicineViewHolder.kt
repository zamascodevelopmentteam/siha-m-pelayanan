package hi.studio.msiha.ui.home.recipe.add

import android.view.View
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.RecipeMedicine
import kotlinx.android.synthetic.main.item_recipe_medicine.view.*

class MedicineViewHolder(private val view: View) : BaseViewHolder<RecipeMedicine>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: RecipeMedicine,
        callback: BaseAdapter.OnItemClick<RecipeMedicine>?
    ) {
        view.nameLbl.text = item.name
        view.ruleLbl.text = item.rule
        view.amountLbl.text = "${item.amount} Tablet"
        view.editBtn.setOnClickListener {
            callback?.onClick(it, item, index)
        }
        view.deleteBtn.setOnClickListener {
            callback?.onClick(it, item, index)
        }
    }
}