package hi.studio.msiha.ui.home.pharmacist.order.createorder

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetPharmacist
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class CreateOrderViewModel(private val getPharmacist: GetPharmacist):BaseViewModel(){
    val getMedicineListResult = MutableLiveData<List<MedicineRsp>>().toSingleEvent()


    fun getListMedicine(type:String){
        getPharmacist.getListMedicine(type,0,100){
            when (it) {
                is Either.Left -> getMedicineListResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }
}