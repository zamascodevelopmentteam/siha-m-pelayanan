package hi.studio.msiha.ui.auth.login

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.auth.AuthActivity
import kotlinx.android.synthetic.main.fragment_login.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment() {
    private val viewModel by viewModel<LoginViewModel>()

    override fun setView(): Int {
        return R.layout.fragment_login
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        loginBtn.setOnClickListener {
            login()
        }

        loginNewAccountLbl.setOnClickListener {
            NavHostFragment.findNavController(this@LoginFragment)
                .navigate(R.id.registerFragment, Bundle())
        }

        loginForgotPasswordTxt.setOnClickListener {
            NavHostFragment.findNavController(this@LoginFragment)
                .navigate(R.id.resetPasswordFragment, Bundle())
        }

        loginPasswordTxt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                login()
            }
            return@setOnEditorActionListener true
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@LoginFragment, Observer {
                (requireActivity() as AuthActivity).showLoading(it)
            })

            isError.observe(this@LoginFragment, Observer {
                (requireActivity() as AuthActivity).showError(it)
            })

            loginResult.observe(this@LoginFragment, Observer {
                observeLoginResult(it)
            })
        }
    }

    private fun login() {
        if (validate()) {
            viewModel.login(loginNikTxt.text.toString(), loginPasswordTxt.text.toString())
        }
    }

    private fun validate(): Boolean {
        loginNikTxt.error = null
        loginPasswordTxt.error = null

        if (loginNikTxt.text.isBlank()) {
            loginNikTxt.error = getString(R.string.empty_field)
            return false
        }

        if (loginPasswordTxt.text.isBlank()) {
            loginPasswordTxt.error = getString(R.string.empty_field)
            return false
        }
        return true
    }

    private fun observeLoginResult(result: AuthResult) {
        AppSIHA.instance.setToken(result.accessToken)
        AppSIHA.instance.setRefreshToken(result.refreshToken)
        AppSIHA.instance.setUser(result.user)
        if (AppSIHA.instance.isAllow()) {
            requireActivity().startActivity(
                requireContext().intentFor<MainActivity>()
                    .singleTop()
                    .clearTop()
                    .clearTask()
            )
            requireActivity().finish()
        } else {
            (requireActivity() as AuthActivity).showImportantMessage("Akses Dilarang")
        }
    }
}