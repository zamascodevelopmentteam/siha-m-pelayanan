package hi.studio.msiha.ui.home.visit.search

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.response.CreateVisitResponse
import hi.studio.msiha.data.datasource.remote.response.VisitHistoryResponse
import hi.studio.msiha.domain.interactor.GetPatient
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class PatientSearchViewModel(
    private val getPatient: GetPatient,
    private val getVisit: GetVisit
) : BaseViewModel() {
    val patientResult = MutableLiveData<Patient?>().toSingleEvent()
    val createVisitResult = MutableLiveData<CreateVisitResponse?>().toSingleEvent()

    fun getPatientByNIK(nik: String) {
        getPatient.getPatientByNIK(nik) {
            when (it) {
                is Either.Left -> patientResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createVisit(patientId:Int){
        getVisit.createGeneralVisit(patientId){
            when(it){
                is Either.Left->createVisitResult.postValue(it.left)
                is Either.Right->isError.postValue(it.right)
            }
        }
    }


}