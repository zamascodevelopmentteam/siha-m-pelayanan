package hi.studio.msiha.ui.home.visit.todayvisit

import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.data.datasource.remote.response.CreateVisitResponse
import hi.studio.msiha.domain.interactor.GetVisit
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class TodayVisitViewModel(private val getVisit: GetVisit):BaseViewModel(){
    val getTodayVisitResult = MutableLiveData<List<VisitHistory>>().toSingleEvent()
    val createVisitResult = MutableLiveData<CreateVisitResponse>().toSingleEvent()

    fun getTodayVisit(page: Int, limit: Int) {
        isLoading.postValue(true)
        getVisit.getTodayVisit{
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getTodayVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getTodayVisitWithKeyword(keyword:String){
        isLoading.postValue(true)
        getVisit.getTodayVisitWithKeyword(keyword){
            isLoading.postValue(false)
            when (it) {
                is Either.Left -> getTodayVisitResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun createVisit(patientId:Int){
        isLoading.postValue(true)
        getVisit.createGeneralVisit(patientId){
            isLoading.postValue(false)
            when(it){
                is Either.Left -> createVisitResult.postValue(it.left)
                is Either.Right->isError.postValue(it.right)
            }
        }
    }
}