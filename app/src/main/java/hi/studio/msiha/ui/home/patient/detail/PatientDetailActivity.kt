package hi.studio.msiha.ui.home.patient.detail

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.appbar.AppBarLayout
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import kotlinx.android.synthetic.main.default_toolbar.*

class PatientDetailActivity : BaseActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navController: NavController

    companion object {
        const val KEY_PATIENT = "patient"
        const val KEY_VISIT = "visit"
        const val KEY_MODE = "mode"
        const val MODE_VISIT = 1
        const val MODE_PATIENT = 2
    }

    override fun setView(): Int {
        return R.layout.activity_patient_detail
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        setupNavController()
    }

    private fun setupNavController() {
        navController = Navigation.findNavController(this, R.id.visitDetailNavHost)
        intent?.apply {
            navController.setGraph(R.navigation.patient_detail_nav_graph, intent.extras)
        }
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.ODHASuccessFragment -> {
                    supportActionBar?.hide()
                }
                R.id.visitEditFragment, R.id.addVisitFragment -> {
                    enableToolbarScroll(false)
                }
                else -> {
                    enableToolbarScroll(true)
                }
            }
        }
        appBarConfiguration = AppBarConfiguration.Builder()
            .setFallbackOnNavigateUpListener {
                onBackPressed()
                return@setFallbackOnNavigateUpListener false
            }
            .build()
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    private fun enableToolbarScroll(enable: Boolean) {
        val param = defaultToolbar.layoutParams as AppBarLayout.LayoutParams
        param.scrollFlags = if (enable) {
            AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
        } else {
            0
        }
    }
}