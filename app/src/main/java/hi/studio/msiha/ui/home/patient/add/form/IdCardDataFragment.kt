package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.extention.fromForm
import hi.studio.msiha.utils.extention.getSelectedContent
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_form_card_id.*

class IdCardDataFragment : BaseFragment(), WizardStep,
    AddPatientFragment.OnAddPatientParentListener {
    private var result: FormResult? = null
    private var province = ""
    private var city = ""
    private var subDistrict = ""

    companion object {
        const val KEY_ADDRESS = "address"
        const val KEY_PROVINCE = "province"
        const val KEY_CITY = "city"
        const val KEY_SUB_DISTRICT = "sub_district"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            IdCardDataFragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = Bundle().apply {
            result?.apply {
                if (success()) {
                    putString(KEY_ADDRESS, this[KEY_ADDRESS]?.asString())
                    putString(KEY_PROVINCE, province)
                    putString(KEY_CITY, city)
                    putString(KEY_SUB_DISTRICT, subDistrict)
                }
            }
        }
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(R.id.address_card_id_txt, KEY_ADDRESS) {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(R.id.province_spn, KEY_PROVINCE) {
                selection().greaterThan(0).description("Anda belum memilih provinsi")
            }
            spinner(R.id.city_spn, KEY_CITY) {
                selection().greaterThan(0).description("Anda belum memilih provinsi")
            }
            spinner(R.id.sub_district_spn, KEY_SUB_DISTRICT) {
                selection().greaterThan(0).description("Anda belum memilih provinsi")
            }
        }
        result = form.validate()
        result?.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.province_spn,
                        R.id.city_spn,
                        R.id.sub_district_spn -> showMessage(
                            description
                        )
                    }
                }
            } else {
                province = province_spn.getSelectedContent()
                city = city_spn.getSelectedContent()
                subDistrict = sub_district_spn.getSelectedContent()
            }
        }
        return result?.success() ?: false
    }

    override fun setView(): Int {
        return R.layout.fragment_form_card_id
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if (arguments == null) {
            if (requireParentFragment() is AddPatientFragment) {
                val bundle = (requireParentFragment() as AddPatientFragment).getUserBundle()
                displayData(bundle)
            }
        } else {
            displayData(arguments)
        }
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            val address = getString(KEY_ADDRESS)
            val province = getString(KEY_PROVINCE)
            val city = getString(KEY_CITY)
            val subDistrict = getString(KEY_SUB_DISTRICT)
            address_card_id_txt.setText(address?.fromForm())
            province_spn.selectItem(province)
            city_spn.selectItem(city)
            sub_district_spn.selectItem(subDistrict)
        }
    }

    override fun onGetUserByNIK(bundle: Bundle?) {
        displayData(bundle)
    }
}
