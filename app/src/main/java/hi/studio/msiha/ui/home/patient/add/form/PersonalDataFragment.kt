package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.core.widget.doAfterTextChanged
import com.afollestad.vvalidator.form
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.data.constant.TYPE_HIV_NEGATIVE
import hi.studio.msiha.data.constant.TYPE_ODHA
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.extention.*
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_form_personal.*
import timber.log.Timber
import kotlin.random.Random

class PersonalDataFragment : BaseFragment(), WizardStep,
    AddPatientFragment.OnAddPatientParentListener {
    private val bundle = Bundle()
    private var isNikFound = false
    private var currentText = ""

    companion object {
        const val KEY_PERSONAL_NIK = "personal_nik"
        const val KEY_PERSONAL_NAME = "personal_name"
        const val KEY_PERSONAL_DOB = "personal_dob"
        const val KEY_PERSONAL_ADDRESS = "personal_address"
        const val KEY_PERSONAL_DOMICILE = "personal_domicile_address"
        const val KEY_PERSONAL_AGE = "personal_age"
        const val KEY_PERSONAL_GENDER = "personal_gender"
        const val KEY_PERSONAL_PHONE = "personal_phone"
        const val KEY_PERSONAL_EMAIL = "personal_email"
        const val KEY_PERSONAL_TYPE = "patient_type"
        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            PersonalDataFragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = bundle
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(R.id.nikTxt, KEY_PERSONAL_NIK) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.nameTxt, KEY_PERSONAL_NAME) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.addressCardIdTxt, KEY_PERSONAL_ADDRESS) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.addressDomicileTxt, KEY_PERSONAL_DOMICILE) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.dateOfBornTxt, KEY_PERSONAL_DOB) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.ageTxt, KEY_PERSONAL_AGE, optional = true) {
                isNotEmpty().description(R.string.empty_field)
            }
            spinner(R.id.genderSpn, KEY_PERSONAL_GENDER) {
                selection().greaterThan(0).description("Harap pilih jenis kelamin")
            }
            input(R.id.phoneTxt, KEY_PERSONAL_PHONE) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(R.id.emailTxt, KEY_PERSONAL_EMAIL) {
                isNotEmpty().description(R.string.empty_field)
            }
        }
        val result = form.validate()
        result.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.genderSpn -> showMessage(description)
                    }
                }
            } else {
                bundle.apply {
                    putString(KEY_PERSONAL_NIK, result[KEY_PERSONAL_NIK]?.asString())
                    putString(KEY_PERSONAL_NAME, result[KEY_PERSONAL_NAME]?.asString())
                    putString(KEY_PERSONAL_ADDRESS, result[KEY_PERSONAL_ADDRESS]?.asString())
                    putString(KEY_PERSONAL_DOB, dateOfBornTxt.tag as String?)
                    putInt(KEY_PERSONAL_AGE, result[KEY_PERSONAL_AGE]?.asInt() ?: 0)
                    putString(KEY_PERSONAL_DOMICILE, result[KEY_PERSONAL_DOMICILE]?.asString())
                    putString(KEY_PERSONAL_GENDER, genderSpn.selectedItem as String)
                    putString(KEY_PERSONAL_PHONE, result[KEY_PERSONAL_PHONE]?.asString())
                    putString(KEY_PERSONAL_EMAIL, result[KEY_PERSONAL_EMAIL]?.asString())
                    putString(
                        KEY_PERSONAL_TYPE, when (odhaStatusSpn.selectedItemPosition) {
                            0 -> TYPE_ODHA
                            else -> TYPE_HIV_NEGATIVE
                        }
                    )
                }
            }
        }
        return result.success()
    }

    override fun setView(): Int {
        return R.layout.fragment_form_personal
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if (arguments == null) {
            if (requireParentFragment() is AddPatientFragment) {
                val bundle = (requireParentFragment() as AddPatientFragment).getUserBundle()
                displayData(bundle)
            }
        } else {
            displayData(arguments)
        }

        nikClearBtn.setOnClickListener {
            nikTxt.isEnabled = true
            nikTxt.setText("")
            isNikFound = false
            it.visibility = View.GONE
        }

        nikTxt.doAfterTextChanged {
            if (requireParentFragment() is AddPatientFragment
                && !currentText.equals(it.toString(), true)
                && it.toString().isNotBlank()
            ) {
                currentText = it.toString()
                nikLoading.visibility = View.VISIBLE
                (requireParentFragment() as AddPatientFragment).searchByNIK(currentText)
            }
            nikClearBtn.visibility = if (it.toString().isBlank()) {
                View.GONE
            } else {
                if (nikLoading.visibility == View.GONE) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        }

        dateOfBornTxt.setOnClickListener {
            dateOfBornTxt.showCalendar {
                ageTxt.setText(it.getAge().toString())
            }
        }

        nikTxt.setOnFocusChangeListener { _, _ ->
            generateNewNik()
        }

        genderSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                (requireParentFragment() as AddPatientFragment).gender = position
            }
        }

        odhaStatusSpn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
            }
        }
    }

    private fun generateNewNik() {
        if (!nikTxt.hasFocus() && nikTxt.text.isNullOrBlank()) {
            val random = Random(System.currentTimeMillis())
            val newNik = "99${random.nextInt(99999)}"
            nikTxt.setText(newNik)
            isNikFound = false
            nikTxt.isEnabled = false
            nikClearBtn.visibility = View.VISIBLE
        }
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            Timber.d(bundle.toString())
            val nik = getString(KEY_PERSONAL_NIK)
            val name = getString(KEY_PERSONAL_NAME)
            val address = getString(KEY_PERSONAL_ADDRESS)
            val domicile = getString(KEY_PERSONAL_DOMICILE)
            val dob = getLong(KEY_PERSONAL_DOB)
            val age = getString(KEY_PERSONAL_AGE, "0")
            val gender = getString(KEY_PERSONAL_GENDER)
            val phone = getString(KEY_PERSONAL_PHONE)
            val email = getString(KEY_PERSONAL_EMAIL)
            nikTxt.setText(nik?.fromForm())
            if (nik == null) {
                generateNewNik()
            } else {
                nikTxt.isEnabled = false
                nikClearBtn.visibility = View.VISIBLE
            }
            nameTxt.setText(name?.fromForm())
            addressCardIdTxt.setText(address?.fromForm())
            dateOfBornTxt.showDate(dob)
            ageTxt.setText(age)
            addressDomicileTxt.setText(domicile?.fromForm())
            genderSpn.selectItem(gender?.fromForm())
            phoneTxt.setText(phone?.fromForm())
        }
    }

    override fun onGetUserByNIK(bundle: Bundle?) {
        nikLoading.visibility = View.GONE
        isNikFound = bundle != null
        displayData(bundle)
    }
}