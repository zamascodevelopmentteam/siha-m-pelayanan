package hi.studio.msiha.ui.home.dashboard

import android.util.Log
import androidx.lifecycle.MutableLiveData
import hi.studio.msiha.base.BaseViewModel
import hi.studio.msiha.domain.interactor.GetHospital
import hi.studio.msiha.domain.model.Statistic
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.toSingleEvent

class HomeViewModel(private val stat: GetHospital) : BaseViewModel() {
    val statisticResult = MutableLiveData<Statistic>().toSingleEvent()
    val todayVisitResult = MutableLiveData<List<TodayVisit>>().toSingleEvent()

    fun getStatistic() {
        stat.getStatistic {
            when (it) {
                is Either.Left -> statisticResult.postValue(it.left)
                is Either.Right -> isError.postValue(it.right)
            }
        }
    }

    fun getTodayVisit() {
        stat.getTodayVisit {
            when (it) {
                is Either.Left -> todayVisitResult.postValue(it.left)
                is Either.Right -> Log.d("error","testing")//isError.postValue(it.right)
            }
        }
    }
}