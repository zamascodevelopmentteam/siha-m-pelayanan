package hi.studio.msiha.ui.home.pharmacist.stock.medicinedetail


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineStocks
import hi.studio.msiha.domain.model.Reagen
import hi.studio.msiha.utils.extention.getSelectedContent
import kotlinx.android.synthetic.main.fragment_medicine_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class MedicineDetailFragment : BaseFragment() {

    private lateinit var medicine: ArvStock
    private val viewModel by viewModel<MedicineDetailViewModel>()
    private var isEditable = false
    private var type: String = "cure"
    private var hospitalId: Int = 0

    override fun setView(): Int {
        return R.layout.fragment_medicine_detail
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        val bundle = arguments
        if (bundle != null) {
            medicine = bundle.getParcelable("medicine")
        }
        val user = AppSIHA.instance.getUser()
        user?.let {
            hospitalId = user.upkId ?: 0
        }
        populateView()
        toggleEditableField()
        handleEditButton()
        handleActionButton()
    }

    override fun onResume() {
        super.onResume()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@MedicineDetailFragment, Observer {

            })

            isError.observe(this@MedicineDetailFragment, Observer {

            })

            updateMedicineResult.observe(this@MedicineDetailFragment, Observer {
                isEditable = false
                toggleEditableField()
                populateView()
            })
        }
    }

    private fun handleActionButton() {
        buttonCancelEdit.setOnClickListener {
            isEditable = false
            toggleEditableField()
            populateView()
        }

        buttonEditMedicine.setOnClickListener {
            val med = Medicine(
                id = medicine.id ?: 0,
                name = editTextMedicineName.text.toString(),
                codeName = editTextMedicineKode.text.toString(),
                type = if (type == "cure") {
                    editTextMedicineType.text.toString()
                } else {
                    spinnerReagen.getSelectedContent()
                },
                picture = "",
                medicineType = type.toUpperCase(),
                ingredients = editTextKandungan.text.toString(),
                usage = editTextPenggunaan.text.toString(),
                medicineStocks = MedicineStocks(
                    cupboardCode = editTextCabinetCode.text.toString(),
                    stock = editTextStock.text.toString().toInt(),
                    hospitalId = hospitalId
                ),
                createdAt = "",
                updatedAt = ""
            )
            viewModel.updateMedicine(med)
//            medicine = med.toMedicineRsp()
        }
    }

    private fun handleEditButton() {
        imgEditButton.setOnClickListener {
            isEditable = true
            toggleEditableField()
        }
    }

    private fun populateView() {
        editTextMedicineName.setText(medicine.name)
        editTextMedicineKode.setText(medicine.codename)
        editTextStock.setText("${medicine.stockQuantity}")
        type = medicine.medicineType?.toLowerCase().toString()
        if (medicine.medicineType?.toLowerCase() == "cure") {
            spinnerReagen.visibility = View.GONE
            editTextMedicineType.visibility = View.VISIBLE
            editTextMedicineType.setText(medicine.medicineType)
        } else {
            editTextMedicineType.visibility = View.GONE
            spinnerReagen.visibility = View.VISIBLE
            val reagenList = resources.getStringArray(R.array.reagen_list)
            val pos = reagenList.indexOf(medicine.medicineType)
            if (pos == -1) {
                spinnerReagen.setSelection(0)
            } else {
                spinnerReagen.setSelection(pos)
            }
        }
    }

    private fun toggleEditableField() {
        if (type == "cure") {
            requireActivity().title = "Detail Data Obat "
        } else {
            requireActivity().title = "Detail Data Reagen"
        }
        var title = ""
        if (isEditable && type == "cure") {
            title = "Edit Data Obat"
        } else if (isEditable && type == "reagen") {
            title = "Edit Data Reagen"
        } else if (!isEditable && type == "cure") {
            title = "Detail Data Obat"
        } else if (!isEditable && type == "reagen") {
            title = "Detail Data Reagen"
        }
        requireActivity().title = title
        editTextMedicineName.isEnabled = isEditable
        editTextMedicineKode.isEnabled = isEditable
        editTextCabinetCode.isEnabled = isEditable
        editTextStock.isEnabled = isEditable
        editTextKandungan.isEnabled = isEditable
        editTextPenggunaan.isEnabled = isEditable
        editTextMedicineType.isEnabled = isEditable
        spinnerReagen.isEnabled = isEditable
        if (isEditable) {
            buttonEditMedicine.visibility = View.VISIBLE
            buttonCancelEdit.visibility = View.VISIBLE
            imgEditButton.visibility = View.GONE
        } else {
            buttonEditMedicine.visibility = View.GONE
            buttonCancelEdit.visibility = View.GONE
            imgEditButton.visibility = View.VISIBLE
        }
    }
}
