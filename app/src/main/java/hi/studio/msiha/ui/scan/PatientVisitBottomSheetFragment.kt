package hi.studio.msiha.ui.scan

import android.os.Bundle
import android.view.View
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseBottomSheetFragment
import kotlinx.android.synthetic.main.pop_up_pasient_visit.*

class PatientVisitBottomSheetFragment : BaseBottomSheetFragment() {
    private var callback: () -> Unit = {}

    override fun setView(): Int {
        return R.layout.pop_up_pasient_visit
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        continueBtn.setOnClickListener {
            callback()
        }
    }

    fun setCallback(callback: () -> Unit) {
        this.callback = callback
    }
}