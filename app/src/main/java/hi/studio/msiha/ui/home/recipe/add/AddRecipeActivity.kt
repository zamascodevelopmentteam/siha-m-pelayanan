package hi.studio.msiha.ui.home.recipe.add

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.vvalidator.form
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseActivity
import hi.studio.msiha.data.constant.KEY_ROLE_LAB
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.data.constant.KEY_ROLE_RR
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.RecipeMedicine
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.utils.extention.showDatePicker
import kotlinx.android.synthetic.main.activity_add_recipe.*
import kotlinx.android.synthetic.main.default_toolbar.*
import kotlinx.android.synthetic.main.popup_add_medicine.view.*
import org.jetbrains.anko.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class AddRecipeActivity : BaseActivity() {
    private val viewModel by viewModel<AddRecipeViewModel>()
    private var medicines = listOf<MedicineRsp>()
    private lateinit var medicineAdapter: MedicineAdapter
    private var patient: User? = null

    override fun setView(): Int {
        return R.layout.activity_add_recipe
    }

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(defaultToolbar)
        supportActionBar?.apply {
            title = "Tambah Resep"
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        intent?.apply {
            patient = getParcelableExtra(PatientDetailActivity.KEY_PATIENT) as User?
            patient?.apply {
                nameTxt.setText(name)
                nikTxt.setText(nik)
                nameTxt.isEnabled = false
                nikTxt.isEnabled = false
            }
        }

        initRecipeMedicinesAdapter()
        dateLbl.setOnClickListener {
            showDatePicker(this) { cal: Calendar, dbDate: String, displayDate: String ->
                dateLbl.text = displayDate
                dateLbl.tag = cal.timeInMillis
            }
        }

        addMedicineBtn.setOnClickListener {
            showPopUp()
        }

        saveBtn.setOnClickListener {
            val form = form {
                input(nameTxt, "name") {
                    isNotEmpty().description(R.string.empty_field)
                }
                input(nikTxt, "nik") {
                    isNotEmpty().description(R.string.empty_field)
                }
                input(notesTxt, "notes") {
                    isNotEmpty().description(R.string.empty_field)
                }
            }
            val result = form.validate()
            result.apply {
                if (success() && medicineAdapter.itemCount > 0 && dateLbl.tag != null) {
                    val nik = this["nik"]?.asString() ?: ""
                    val notes = this["notes"]?.asString() ?: ""
                    val date = dateLbl.tag as Long
                    viewModel.createRecipe(
                        nik,
                        date,
                        notes,
                        medicineAdapter.all.map {
                            RecipeMedicine(it.medicineId, it.name, it.rule, it.amount)
                        }
                    )
                } else {
                    if (medicineAdapter.itemCount == 0) {
                        toast("Anda belum menambahkan obat")
                    } else if (dateLbl.tag == null) {
                        toast("Anda belum memilih tanggal resep")
                    }
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onStart() {
        super.onStart()
        observeViewModel()
        //fixme next implement pagination on spinner
        viewModel.loadMedicine(0, 2000)
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@AddRecipeActivity, Observer {
                showLoading(it)
            })

            isError.observe(this@AddRecipeActivity, Observer {
                showError(it)
            })

            medicinesResult.observe(this@AddRecipeActivity, Observer {
                medicines = it
            })

            addRecipeResult.observe(this@AddRecipeActivity, Observer {
                showMessage(it)
                onBackPressed()
            })
        }
    }

    private fun initRecipeMedicinesAdapter() {
        medicineAdapter = MedicineAdapter()
        medicineList.apply {
            layoutManager =
                LinearLayoutManager(this@AddRecipeActivity, RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    this@AddRecipeActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = medicineAdapter
        }
        medicineAdapter.itemCallback { v: View, item: RecipeMedicine, position: Int ->
            when (v.id) {
                R.id.editBtn -> {
                    showPopUp(position, item)
                }
                R.id.deleteBtn -> {
                    medicineAdapter.remove(position)
                }
            }
        }
    }

    private fun initMedicinesAdapter(data: List<MedicineRsp>): ArrayAdapter<MedicineRsp> {
        return object : ArrayAdapter<MedicineRsp>(
            this,
            android.R.layout.simple_list_item_1,
            android.R.id.text1,
            data
        ) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent)
                val text1 = view.findViewById(android.R.id.text1) as TextView
                data[position].apply {
                    text1.text = name
                }
                return view
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val text1 = view.findViewById(android.R.id.text1) as TextView
                data[position].apply {
                    text1.text = name
                }
                return view
            }

            override fun getItemId(position: Int): Long {
                return data[position].id.toLong()
            }

            override fun getItem(position: Int): MedicineRsp? {
                return data[position]
            }

            override fun getCount(): Int {
                return data.size
            }
        }
    }

    private fun showPopUp(index: Int = 0, recipeMedicine: RecipeMedicine? = null) {
        val popUpView = LayoutInflater.from(this)
            .inflate(R.layout.popup_add_medicine, null)
        popUpView.medicinesSpn.adapter = initMedicinesAdapter(medicines)

        recipeMedicine?.apply {
            medicines.forEachIndexed { index, medicineRsp ->
                if (medicineRsp.id == recipeMedicine.medicineId) {
                    popUpView.medicinesSpn.setSelection(index)
                }
            }
            popUpView.doseTxt.setText("$amount")
            popUpView.instructionTxt.setText(rule)
        }

        val form = form {
            spinner(popUpView.medicinesSpn, "medicines") {
                selection().greaterThan(-1)
            }
            input(popUpView.doseTxt, "dose") {
                isNotEmpty().description(R.string.empty_field)
            }
            input(popUpView.instructionTxt, "instruction") {
                isNotEmpty().description(R.string.empty_field)
            }
        }

        val popup = AlertDialog.Builder(this).create()
        popup.setView(popUpView)
        popup.setButton(DialogInterface.BUTTON_POSITIVE, "Tambah") { _: DialogInterface, _: Int -> }
        popup.setButton(
            DialogInterface.BUTTON_NEGATIVE,
            "Batal"
        ) { d: DialogInterface, _: Int -> d.dismiss() }
        popup.setCancelable(false)
        popup.show()
        popup.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener {
            val result = form.validate()
            result.apply {
                if (success()) {
                    val medicine = popUpView.medicinesSpn.selectedItem as MedicineRsp
                    val data = RecipeMedicine(
                        medicine.id,
                        medicine.name,
                        result["instruction"]?.asString(),
                        result["dose"]?.asInt() ?: 0
                    )
                    if (recipeMedicine == null) {
                        medicineAdapter.add(data)
                    } else {
                        medicineAdapter.update(data, index)
                    }
                    popup.dismiss()
                } else {
                    if (hasErrors()) {
                        errors().forEach {
                            when (it.name) {
                                "medicines" -> toast("Anda belum memilih obat")
                            }
                        }
                    }
                }
            }
        }
    }
}