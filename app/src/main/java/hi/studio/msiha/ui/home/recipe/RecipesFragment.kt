package hi.studio.msiha.ui.home.recipe

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.data.constant.KEY_PAGE_LIMIT
import hi.studio.msiha.data.constant.KEY_ROLE_PHARMACIST
import hi.studio.msiha.domain.model.Recipe
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.ui.home.recipe.add.AddRecipeActivity
import kotlinx.android.synthetic.main.fragment_recipes.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class RecipesFragment : BaseFragment() {
    private val viewModel by viewModel<RecipeViewModel>()
    private lateinit var recipeAdapter: RecipeAdapter
    private var limit = KEY_PAGE_LIMIT
    private var patient: User? = null

    override fun setView(): Int {
        return R.layout.fragment_recipes
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            patient = getParcelable(PatientDetailActivity.KEY_PATIENT) as User?
        }
        initRecipeMedicinesAdapter()

        addRecipeBtn.visibility = when (AppSIHA.instance.getUser()?.role) {
            KEY_ROLE_PHARMACIST -> View.VISIBLE
            else -> View.GONE
        }
        addRecipeBtn.setOnClickListener {
            requireActivity().startActivity<AddRecipeActivity>(
                Pair(
                    PatientDetailActivity.KEY_PATIENT,
                    patient
                )
            )
        }
    }

    override fun onStart() {
        super.onStart()
        patient?.let { it ->
            it.id?.let {
                viewModel.getRecipes(it, 0, limit)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@RecipesFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@RecipesFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            recipesResult.observe(this@RecipesFragment, Observer {
                recipeAdapter.addAll(it)
            })
        }
    }

    private fun initRecipeMedicinesAdapter() {
        recipeAdapter = RecipeAdapter()
        recipeList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = recipeAdapter
        }
        recipeAdapter.itemCallback { v: View, item: Recipe, _: Int ->
            //            NavHostFragment.findNavController(this)
//                .navigate(R.id.recipeDetailFragment, arguments?.apply {
//                    putParcelable(RecipeDetailFragment.KEY_RECIPE_DETAIL, item)
//                })
        }
        recipeList.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
                limit++
                patient?.let { it ->
                    it.id?.let {
                        viewModel.getRecipes(it, 0, limit)
                    }
                }
            }
        })
    }
}