package hi.studio.msiha.ui.home.pharmacist.dashboard.orderdetail


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.utils.extention.textToImageEncode
import kotlinx.android.synthetic.main.fragment_detail_order.*
import java.text.SimpleDateFormat
import java.util.*
import android.widget.ArrayAdapter


/**
 * A simple [Fragment] subclass.
 */
class DetailOrderFragment : BaseFragment() {

    private lateinit var order:Order

    override fun setView(): Int {
        return R.layout.fragment_detail_order
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        val user = AppSIHA.instance.getUser()
        user?.let {

        }
        handleArgument()
        generateQrCode()
        setupListView()
        imgBackButton.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun generateQrCode(){
        val image = order.id.toString().textToImageEncode()
        imgQrCode.setImageBitmap(image)
        populateView()
    }

    private fun populateView(){
        textInvoice.text=order.invoiceNumber
        val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm")
        val dateTime = formatter.format(Date(order.transactionDate)).split(" ")

        textDate.text=dateTime[0]
        textTime.text=dateTime[1]
    }

    private fun handleArgument(){
        val bundle = arguments
        if (bundle != null) {
            order = bundle.getParcelable("order")
        }
    }

    private fun setupListView(){
        val adapter = DetailInfoAdapter(context, order.orderMedicines)
        listViewMedicineInfo.adapter=adapter
    }


}
