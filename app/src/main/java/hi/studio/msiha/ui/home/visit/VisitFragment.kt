package hi.studio.msiha.ui.home.visit

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.base.ScrollListener
import hi.studio.msiha.data.constant.KEY_PAGE_LIMIT
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.MainActivity
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_visit.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class VisitFragment : BaseFragment() {
    private val viewModel by viewModel<VisitViewModel>()
    private lateinit var adapter: VisitAdapter
    private var limit = KEY_PAGE_LIMIT

    override fun setView(): Int {
        return R.layout.fragment_visit
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setupAdapter()
        requireActivity().actionBtn.setOnClickListener {
            findNavController().navigate(R.id.patientSearchFragment)
        }
        refreshLayout.setOnRefreshListener {
            limit = KEY_PAGE_LIMIT
            viewModel.getVisits(0, limit)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getVisits(0, limit)
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@VisitFragment, Observer {
                refreshLayout.post {
                    refreshLayout.isRefreshing = it
                }
            })

            isError.observe(this@VisitFragment, Observer {
                (requireActivity() as MainActivity).showError(it)
            })

            visitsResult.observe(this@VisitFragment, Observer {
                adapter.addAll(it)
            })
        }
    }

    private fun setupAdapter() {
        adapter = VisitAdapter()
        visitList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = this@VisitFragment.adapter
        }
        adapter.itemCallback { _: View, patient: Patient, _: Int ->
            requireActivity().startActivity<PatientDetailActivity>(
                Pair(
                    PatientDetailActivity.KEY_PATIENT,
                    patient
                ),
                Pair(
                    PatientDetailActivity.KEY_MODE,
                    PatientDetailActivity.MODE_VISIT
                )
            )
        }
        visitList.addOnScrollListener(object : ScrollListener() {
            override fun onLoadMore() {
                limit += KEY_PAGE_LIMIT
                viewModel.getVisits(0, limit)
            }
        })
    }

}