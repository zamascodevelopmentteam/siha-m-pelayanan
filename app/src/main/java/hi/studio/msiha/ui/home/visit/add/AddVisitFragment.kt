package hi.studio.msiha.ui.home.visit.add

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity
import hi.studio.msiha.utils.extention.showDatePicker
import hi.studio.msiha.utils.extention.toFormat
import kotlinx.android.synthetic.main.fragment_add_visit.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class AddVisitFragment : BaseFragment() {
    private val viewModel by viewModel<AddVisitViewModel>()
    private var patient: Patient? = null
    private var ordinal = 0

    companion object {
        const val KEY_ORDINAL = "ordinal"
    }

    override fun setView(): Int {
        return R.layout.fragment_add_visit
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        arguments?.apply {
            patient = getParcelable(PatientDetailActivity.KEY_PATIENT)
            ordinal = getInt(KEY_ORDINAL) + 1
            ordinalLbl.text = "Kunjungan ke-$ordinal"
        }

        val calendar = Calendar.getInstance()
        dateLbl.text = calendar.time.toFormat()
        dateLbl.tag = calendar.time.toFormat("yyyy-MM-dd")

        dateLbl.setOnClickListener {
            showDatePicker(requireContext()) { calendar: Calendar, displayDate: String, dbDate: String ->
                dateLbl.text = displayDate
                dateLbl.tag = dbDate
            }
        }

        saveBtn.setOnClickListener {
            patient?.let {
                viewModel.addVisit(ordinal, it.id ?: 0, dateLbl.tag as String)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            isLoading.observe(this@AddVisitFragment, Observer {
                (requireActivity() as PatientDetailActivity).showLoading(it)
            })

            isError.observe(this@AddVisitFragment, Observer {
                (requireActivity() as PatientDetailActivity).showError(it)
            })

            addVisitResult.observe(this@AddVisitFragment, Observer {
                findNavController().navigateUp()
            })
        }
    }
}