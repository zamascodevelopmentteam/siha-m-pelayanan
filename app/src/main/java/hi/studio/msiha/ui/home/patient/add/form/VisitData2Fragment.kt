package hi.studio.msiha.ui.home.patient.add.form

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.utils.extention.getSelectedContent
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_form_visit2.*

class VisitData2Fragment : BaseFragment(), WizardStep,
    AddPatientFragment.OnAddPatientParentListener {
    private var result: FormResult? = null
    private var babyArt = ""
    private var babyCot = ""
    private var breastfeeding = false
    private var breastfeedingAlt = false
    private var specialGroup = ""
    private var riskFactor = ""
    private var riskGroup = ""
    private var psychosocial = ""

    companion object {
        const val KEY_VISIT_PRO_BABY_ART = "visit_pro_baby_art"
        const val KEY_VISIT_PRO_BABY_COT = "visit_pro_baby_cot"
        const val KEY_VISIT_BREASTFEEDING = "visit_breastfeeding"
        const val KEY_VISIT_BREASTFEEDING_ALT = "visit_breastfeeding_alternative"
        const val KEY_VISIT_SPECIAL_GROUP = "visit_special_group"
        const val KEY_VISIT_RISK_FACTOR = "visit_risk_factor"
        const val KEY_VISIT_RISK_GROUP = "visit_risk_group"
        const val KEY_VISIT_PSYCHOSOCIAL = "visit_psychosocial"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            VisitData2Fragment().apply {
                arguments = bundle
            }
    }

    override var value: Bundle
        get() = Bundle().apply {
            result?.apply {
                if (success()) {
                    putString(KEY_VISIT_PRO_BABY_ART, babyArt)
                    putString(KEY_VISIT_PRO_BABY_COT, babyCot)
                    putBoolean(KEY_VISIT_BREASTFEEDING, breastfeeding)
                    putBoolean(KEY_VISIT_BREASTFEEDING_ALT, breastfeedingAlt)
                    putString(KEY_VISIT_SPECIAL_GROUP, specialGroup)
                    putString(KEY_VISIT_RISK_FACTOR, riskFactor)
                    putString(KEY_VISIT_RISK_GROUP, riskGroup)
                    putString(KEY_VISIT_PSYCHOSOCIAL, psychosocial)
                }
            }
        }
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            spinner(R.id.prophylacticBabyArtSpn, KEY_VISIT_PRO_BABY_ART) {
                //                selection().greaterThan(0).description("Harap pilih Provilaksis Art")
            }
            spinner(R.id.prophylacticBabyCotriSpn, KEY_VISIT_PRO_BABY_COT) {
                //                selection().greaterThan(0).description("Harap pilih Provilaksis Art")
            }
            spinner(R.id.breastfeedingSpn, KEY_VISIT_BREASTFEEDING) {
                //                selection().greaterThan(0).description("Harap pilih Pemberian ASI")
            }
            spinner(R.id.breastfeedingAlternativeSpn, KEY_VISIT_BREASTFEEDING_ALT) {
                //                selection().greaterThan(0).description("Harap pilih Pemberian PASI")
            }
            spinner(R.id.specialGroupSpn, KEY_VISIT_SPECIAL_GROUP) {
                //                selection().greaterThan(0).description("Harap pilih Kelompok Khusus")
            }
            spinner(R.id.riskFactorSpn, KEY_VISIT_RISK_FACTOR) {
                //                selection().greaterThan(0).description("Harap pilih faktor resiko")
            }
            spinner(R.id.riskGroupSpn, KEY_VISIT_RISK_GROUP) {
                //                selection().greaterThan(0).description("Harap pilih kelompok beresiko")
            }
            checkable(R.id.faskesRadio, KEY_VISIT_PSYCHOSOCIAL) {
            }
            checkable(R.id.lsmRadio, KEY_VISIT_PSYCHOSOCIAL) {
            }
        }
        result = form.validate()
        result?.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.prophylacticBabyArtSpn,
                        R.id.prophylacticBabyCotriSpn,
                        R.id.breastfeedingSpn,
                        R.id.breastfeedingAlternativeSpn,
                        R.id.specialGroupSpn,
                        R.id.riskFactorSpn,
                        R.id.riskGroupSpn
                        -> showMessage(description)
                    }
                }
            } else {
                babyArt = prophylacticBabyArtSpn.getSelectedContent()
                babyCot = prophylacticBabyCotriSpn.getSelectedContent()
                breastfeeding = breastfeedingSpn.selectedItemPosition == 1
                breastfeedingAlt = breastfeedingAlternativeSpn.selectedItemPosition == 1
                specialGroup = specialGroupSpn.getSelectedContent()
                riskFactor = riskFactorSpn.getSelectedContent()
                riskGroup = riskGroupSpn.getSelectedContent()
                psychosocial = when {
                    faskesRadio.isChecked -> faskesRadio.text.toString()
                    lsmRadio.isChecked -> lsmRadio.text.toString()
                    else -> ""
                }
            }
        }
        return result?.success() ?: false
    }

    override fun setView(): Int {
        return R.layout.fragment_form_visit2
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        if (arguments == null) {
            if (requireParentFragment() is AddPatientFragment) {
                val bundle = (requireParentFragment() as AddPatientFragment).getUserBundle()
                displayData(bundle)
            }
        } else {
            displayData(arguments)
        }
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            val babyArt = getString(KEY_VISIT_PRO_BABY_ART)
            val babyCot = getString(KEY_VISIT_PRO_BABY_COT)
            val breastfeeding = getBoolean(KEY_VISIT_BREASTFEEDING)
            val breastfeedingAlt = getBoolean(KEY_VISIT_BREASTFEEDING_ALT)
            val specialGroup = getString(KEY_VISIT_SPECIAL_GROUP)
            val riskFactor = getString(KEY_VISIT_RISK_FACTOR)
            val riskGroup = getString(KEY_VISIT_RISK_GROUP)
            val psychosocial = getString(KEY_VISIT_PSYCHOSOCIAL)
            prophylacticBabyArtSpn.selectItem(babyArt)
            prophylacticBabyCotriSpn.selectItem(babyCot)
            breastfeedingSpn.setSelection(
                if (breastfeeding) {
                    1
                } else {
                    0
                }
            )
            breastfeedingAlternativeSpn.setSelection(
                if (breastfeedingAlt) {
                    1
                } else {
                    0
                }
            )
            specialGroupSpn.selectItem(specialGroup)
            riskFactorSpn.selectItem(riskFactor)
            riskGroupSpn.selectItem(riskGroup)
            if (psychosocial.equals(faskesRadio.text.toString(), true)) {
                faskesRadio.isChecked
            } else if (psychosocial.equals(lsmRadio.text.toString(), true)) {
                lsmRadio.isChecked
            }
        }
        val gender = (requireParentFragment() as AddPatientFragment).gender
        if (gender == 1) {
            pregnancyInfoLayout.visibility = View.GONE
        } else {
            pregnancyInfoLayout.visibility = View.VISIBLE
        }
    }

    override fun onGetUserByNIK(bundle: Bundle?) {
        displayData(bundle)
    }
}