package hi.studio.msiha.ui.home.treatment

import android.os.Bundle
import android.view.View
import com.afollestad.vvalidator.form
import com.afollestad.vvalidator.form.FormResult
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.utils.extention.fromForm
import hi.studio.msiha.utils.extention.getSelectedContent
import hi.studio.msiha.utils.extention.selectItem
import hi.studio.msiha.utils.wizard.WizardStep
import kotlinx.android.synthetic.main.fragment_visit_history_prevention.*

class FormPreventionFragment : BaseFragment(), WizardStep {
    private var result: FormResult? = null
    private var medicineGiven = ""

    companion object {
        const val KEY_MEDICINE_GIVEN = "key_medicine_given"
        const val KEY_FINAL_RESULT = "final_result"
        const val KEY_ARV_LEFT = "arv_left"

        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            FormPreventionFragment().apply {
                arguments = bundle
            }
    }

    override fun setView(): Int {
        return R.layout.fragment_visit_history_prevention
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        displayData(arguments)
    }

    override var value: Bundle
        get() = Bundle().apply {
            result?.apply {
                putString(KEY_MEDICINE_GIVEN, medicineGiven)
                putString(KEY_FINAL_RESULT, this[KEY_FINAL_RESULT]?.asString())
                putInt(KEY_ARV_LEFT, this[KEY_ARV_LEFT]?.asInt() ?: 0)
            }
        }
        set(value) {}

    override fun invalidateStep(): Boolean {
        val form = form {
            input(
                R.id.finalResultTxt,
                KEY_FINAL_RESULT
            ) {
                isNotEmpty().description(R.string.empty_field)
            }
            input(
                R.id.medicineLeftTxt,
                KEY_FINAL_RESULT
            ) {
                isNotEmpty().description(R.string.empty_field)
            }
        }
        result = form.validate()
        result?.apply {
            if (hasErrors()) {
                errors().firstOrNull()?.apply {
                    when (id) {
                        R.id.medicineGivenSpn -> showMessage(
                            description
                        )
                    }
                }
            } else {
                medicineGiven = medicineGivenSpn.getSelectedContent()
            }
        }
        return result?.success() ?: false
    }

    private fun displayData(bundle: Bundle?) {
        bundle?.apply {
            medicineGivenSpn.selectItem(getString(KEY_MEDICINE_GIVEN))
            finalResultTxt.setText(getString(KEY_FINAL_RESULT, "").fromForm())
            medicineLeftTxt.setText(getInt(KEY_ARV_LEFT, 0).toString().fromForm())
        }
    }
}