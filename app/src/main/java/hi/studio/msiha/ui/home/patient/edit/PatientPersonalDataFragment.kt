package hi.studio.msiha.ui.home.patient.edit

import android.os.Bundle
import android.util.Log
import android.view.View
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseFragment
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.ui.home.patient.detail.PatientDetailActivity.Companion.KEY_PATIENT
import kotlinx.android.synthetic.main.fragment_patient_personal_data.*

class PatientPersonalDataFragment : BaseFragment() {

    companion object {
        @JvmStatic
        fun newInstance(bundle: Bundle? = null) =
            PatientPersonalDataFragment().apply {
                arguments = bundle
            }
    }

    private var patient: Patient? = null

    override fun setView(): Int {
        return R.layout.fragment_patient_personal_data
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        patient = arguments?.getParcelable(KEY_PATIENT)
        if(Hawk.contains(KEY_PATIENT)){
            Log.d("test","patient exist")
            patient=Hawk.get(KEY_PATIENT)
        }else{
            Log.d("test","patient not exist")
        }
        fillPatientPersonalInfo()
    }

    private fun fillPatientPersonalInfo() {
        patient?.run {
            textPersonalDataNik.text = nik
            textPersonalDataName.text = name
            textPersonalDataAddress.text = addressKTP
            textPersonalDataCurrentAddress.text = addressDomicile
            textPersonalDataAge.text="$dateBirth"
            textPersonalDataGender.text = gender
            textPersonalDataPhone.text = phone
            textPersonalDataEmail.text = email
        }
    }
}
