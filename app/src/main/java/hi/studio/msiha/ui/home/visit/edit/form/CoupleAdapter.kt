package hi.studio.msiha.ui.home.visit.edit.form

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import kotlin.reflect.KProperty

class CoupleAdapter : BaseAdapter<ODHAVisitRequest.Couple>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_couple
    }

    override fun itemViewHolder(
        context: Context,
        view: View,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return CoupleViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        index: Int,
        count: Int,
        item: ODHAVisitRequest.Couple,
        callback: OnItemClick<ODHAVisitRequest.Couple>?
    ) {
        if (holder is CoupleViewHolder) {
            holder.onBind(index, count, item, callback)
        }
    }

    override fun compareDiffUtil(
        prop: KProperty<*>,
        old: MutableList<ODHAVisitRequest.Couple>,
        new: MutableList<ODHAVisitRequest.Couple>
    ) {
        autoNotify(old, new) { o, n ->
            o.id == n.id
        }
    }
}