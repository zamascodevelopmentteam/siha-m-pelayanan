package hi.studio.msiha.ui.home.pharmacist.order

import android.view.View
import com.bumptech.glide.request.RequestOptions
import hi.studio.msiha.R
import hi.studio.msiha.base.BaseAdapter
import hi.studio.msiha.base.BaseViewHolder
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.utils.GlideApp
import kotlinx.android.synthetic.main.item_farmasi_stok_obat.view.*

class OrderListViewHolder(private val view: View) : BaseViewHolder<Order>(view) {
    override fun onBind(
        index: Int,
        count: Int,
        item: Order,
        callback: BaseAdapter.OnItemClick<Order>?
    ) {
        val requestOptions = RequestOptions()
        requestOptions.circleCrop()
        GlideApp.with(view)
            .load(R.drawable.ic_pill)
            .apply(requestOptions)
            .into(view.imgMedicine)
        view.textMedicineName.text= item.orderMedicines?.get(0)?.name ?: ""
        view.textMedicineType.text=item.to
        view.textMedicineStock.text="Jumlah : ${item.amount}"
        view.layoutWrapper.setOnClickListener {
            callback?.onClick(view,item)
        }
    }
}