package hi.studio.msiha.utils.extention

import android.content.Context
import android.widget.ImageView
import hi.studio.msiha.BuildConfig
import hi.studio.msiha.R
import hi.studio.msiha.utils.GlideApp

fun ImageView.showFromUrl(url: String?, context: Context? = null) {
    val imgUrl = url?.let {
        if (it.contains("http", true)
            || it.contains("https", true)
        ) {
            it
        } else {
            BuildConfig.BASE_URL + it
        }
    }
    GlideApp.with(context ?: this.context)
        .load(imgUrl)
        .placeholder(R.color.color_bbb_gray)
        .error(R.color.color_bbb_gray)
        .into(this)
}