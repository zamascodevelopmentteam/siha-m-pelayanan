package hi.studio.msiha.utils.extention

import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

fun String?.toDateFormat(
    fromFormat: String = "yyyy-MM-dd",
    toFormat: String = "MMM dd, yyyy"
): String? {
    try {
        val from = SimpleDateFormat(fromFormat, Locale.getDefault()).parse(this)
        return SimpleDateFormat(toFormat, Locale.getDefault()).format(from)
    } catch (e: Exception) {
        Timber.e(e)
    }
    return null
}

fun String?.toTimestamp(format: String = "yyyy-MM-dd"): Long? {
    try {
        val date = SimpleDateFormat(format, Locale.getDefault()).parse(this)
        return date.time
    } catch (e: Exception) {
        Timber.e(e)
    }
    return null
}