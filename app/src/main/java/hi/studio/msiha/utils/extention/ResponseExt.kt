package hi.studio.msiha.utils.extention

import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber

fun Response<*>.getError(): Throwable {
    val errorBody = errorBody()?.string() ?: ""
    return Throwable(
        try {
            val json = JSONObject(errorBody)
            Log.d("rsp","get message success ${json.getString("message")}")
            json.getString("message")

        } catch (e: JSONException) {
            Log.d("rsp","get message failed")
            Timber.e(e)
            if(code()==500){
                "kesalahan pada server"
            }else {
                errorBody
            }
        }
    )
}

/**
 * https://futurestud.io/tutorials/retrofit-2-simple-error-handling
 * 1xx: Informational - Request received, continuing process
 * 2xx: Success - The action was successfully received, understood, and accepted
 * 3xx: Redirection - Further action must be taken in order to complete the request
 * 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
 * 5xx: Server Error - The server failed to fulfill an apparently valid request [1]
 */
fun <T> Response<T>.parse(
    success: (res: T) -> Unit,
    error: (e: Throwable) -> Unit = {}
) {
    if (isSuccessful) {
        //Error code from 200 - 299
        body()?.let {
            success(it)
        } ?: kotlin.run {
            Timber.d("Parse Error API")
            error(getError())
        }
    } else {
        //Error code from 400 - 599
        Timber.d("error code: ${code()}")
        Timber.d("error message: ${body().toString()}")
        error(
            when (code()) {
                404 -> Throwable("Data tidak ditemukan")
                500 -> getError()
                else -> getError()
            }
        )
    }
}