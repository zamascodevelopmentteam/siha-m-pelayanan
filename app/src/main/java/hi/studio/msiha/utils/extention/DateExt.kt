package hi.studio.msiha.utils.extention

import android.text.format.DateUtils
import com.orhanobut.hawk.Hawk
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

fun Date.isToday(timestamp: Long = 0L): Boolean {
    val newTime = if (timestamp == 0L) time else timestamp
    return DateUtils.isToday(newTime)
}

fun Date.isTomorrow(timestamp: Long = 0L): Boolean {
    val newTime = if (timestamp == 0L) time else timestamp
    return DateUtils.isToday(newTime - DateUtils.DAY_IN_MILLIS)
}

fun Date.isYesterday(timestamp: Long = 0L): Boolean {
    val newTime = if (timestamp == 0L) time else timestamp
    return DateUtils.isToday(newTime + DateUtils.DAY_IN_MILLIS)
}

fun Date.isBefore(timestamp: Long = 0L): Boolean {
    val date = if (timestamp == 0L) {
        Calendar.getInstance().time
    } else {
        Date(timestamp)
    }
    return before(date)
}

fun Date.isAfter(timestamp: Long = 0L): Boolean {
    val date = if (timestamp == 0L) {
        Calendar.getInstance().time
    } else {
        Date(timestamp)
    }
    return after(date)
}

fun Date?.toFormat(format: String = "MMM dd, yyyy"): String {
    try {
        val dateFormat = SimpleDateFormat(format, Locale.getDefault())
        return dateFormat.format(this)
    } catch (e: Exception) {
        Timber.e(e)
    }
    return ""
}

fun String?.toDate():Date{
    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    return dateFormat.parse(this)
}