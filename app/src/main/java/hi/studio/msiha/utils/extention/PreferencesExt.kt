package hi.studio.msiha.utils.extention

import android.content.SharedPreferences
import com.google.gson.GsonBuilder

private val gson = GsonBuilder().create()

fun SharedPreferences.clear() {
    this.edit().clear().apply()
}

fun <T> SharedPreferences.putObject(key: String, o: T) {
    val data = gson.toJson(o)
//    Timber.d("save object: %s", data)
    edit().apply {
        this.putString(key, data)
        this.apply()
    }
}

fun <T> SharedPreferences.getObject(key: String, c: Class<T>): T? {
    val data = getString(key, null)
//    Timber.d("get object: %s", data)
    data?.let {
        return gson.fromJson(data, c)
    }
    return null
}