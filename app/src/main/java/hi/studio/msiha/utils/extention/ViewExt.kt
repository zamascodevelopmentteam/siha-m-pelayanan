package hi.studio.msiha.utils.extention

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.util.Log
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import hi.studio.msiha.R
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

fun EditText.showCalendar(
    dbFormat: String = "yyyy-MM-dd",
    displayFormat: String = "dd/MM/yyyy",
    callback: (calendar: Calendar) -> Unit = {}
) {
    isFocusable = false
    showDatePicker(context, dbFormat, displayFormat) { calendar: Calendar, s: String, s2: String ->
        setText(s2)
        tag = s
        callback(calendar)
    }
}

fun showDatePicker(
    context: Context,
    dbFormat: String = "yyyy-MM-dd",
    displayFormat: String = "MMM dd, yyyy",
    callback: (calendar: Calendar, dbDate: String, displayDate: String) -> Unit
) {
    val displayDateFormat = SimpleDateFormat(displayFormat, Locale.getDefault())
    val dbDateFormat = SimpleDateFormat(dbFormat, Locale.getDefault())
    val calendar = Calendar.getInstance()
    val picker = DatePickerDialog(
        context, { _, year, month, day ->
            calendar.set(year, month, day)
            val selectedTimeInMillis = calendar.timeInMillis
            val systemTimeInMillis = Calendar.getInstance().timeInMillis
            if(selectedTimeInMillis<=systemTimeInMillis){
                callback(
                    calendar,
                    dbDateFormat.format(calendar.time),
                    displayDateFormat.format(calendar.time)
                )
            }else{
                Toast.makeText(context,"Tidak dapat memilih tanggal di masa depan",Toast.LENGTH_SHORT).show();
            }

        },
        calendar[Calendar.YEAR],
        calendar[Calendar.MONTH],
        calendar[Calendar.DAY_OF_MONTH]
    )
    picker.datePicker.maxDate=System.currentTimeMillis()
    picker.show()
}

fun Calendar.getAge(): Int {
    val today = Calendar.getInstance()
    var age = today.get(Calendar.YEAR) - get(Calendar.YEAR)
    if (today.get(Calendar.DAY_OF_YEAR) < get(Calendar.DAY_OF_YEAR)) {
        age -= 1
    }
    return age
}

fun Spinner.selectItem(item: String?) {
    for (i in 0 until count) {
        Log.d("test","select item ${getItemAtPosition(i).toString()} ${item}")
        if (getItemAtPosition(i).toString().equals(item, true)) {
            Log.d("set selection","selection $i")
            setSelection(i)
            break
        }
    }
}

fun Spinner.getSelectedContent(): String {
    if (selectedItemPosition > 0) {
        return selectedItem as String
    }
    return ""
}

/**
 * Extention only for PatientFormPreview
 */
fun TextView.setValue(title: String, value: String?) {
    val color = ContextCompat.getColor(context, R.color.charcoal_grey)
    val newValue = value ?: ""
    text = spannable {
        title.toSpan() + " : " +
                newValue.toSpan()
                    .color(color)
    }
}

fun TextView.showDate(timestamp: Long, format: String = "dd-MM-yyyy") {
    try {
        if (timestamp != 0L) {
            val date = Date(timestamp)
            val formatter = SimpleDateFormat(format, Locale.getDefault())
            text = formatter.format(date)
            tag = timestamp.toFormatDate("yyyy-MM-dd")
        }
    } catch (e: Exception) {
        Timber.e(e)
    }
}

fun Long.toFormatDate(format: String = "dd-MM-yyyy"): String? {
    try {
        if (this != 0L) {
            val date = Date(this)
            val formatter = SimpleDateFormat(format, Locale.getDefault())
            return formatter.format(date)
        }
    } catch (e: Exception) {
        Timber.e(e)
    }
    return null
}

fun String?.toForm(): String? = this?.toLowerCase(Locale.getDefault())

@SuppressLint("DefaultLocale")
fun String.fromForm(): String? = capitalize()
