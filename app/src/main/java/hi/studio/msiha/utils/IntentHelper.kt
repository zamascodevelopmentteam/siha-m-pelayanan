package hi.studio.msiha.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import java.net.URLEncoder

fun sendWhatsapp(context: Context, phone: String, text: String) {
    try {
        val packageManager = context.packageManager
        val i = Intent(Intent.ACTION_VIEW)
        val url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode(
            text,
            "UTF-8"
        )
        i.setPackage("com.whatsapp")
        i.data = Uri.parse(url)
        if (i.resolveActivity(packageManager) != null) {
            context.startActivity(i)
        } else {
            Toast.makeText(context, "Whatsapp not installed", Toast.LENGTH_SHORT).show()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}