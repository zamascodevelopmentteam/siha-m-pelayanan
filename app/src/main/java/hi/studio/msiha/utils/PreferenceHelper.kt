package hi.studio.msiha.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object PreferenceHelper {
    fun defaultPrefs(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    fun customPrefs(context: Context, name: String): SharedPreferences =
        context.getSharedPreferences(name, Context.MODE_PRIVATE)

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit { it.putString(key, value) }
            is Boolean -> edit { it.putBoolean(key, value) }
            is Int -> edit { it.putInt(key, value) }
            is Float -> edit { it.putFloat(key, value) }
            is Long -> edit { it.putLong(key, value) }
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    inline operator fun <reified T : Any> SharedPreferences.get(
        key: String,
        default: T? = null
    ): T? {
        return when (T::class) {
            String::class -> getString(key, default as? String) as T?
            Boolean::class -> getBoolean(key, default as? Boolean ?: false) as T?
            Int::class -> getInt(key, default as? Int ?: -1) as T?
            Float::class -> getFloat(key, default as? Float ?: -1f) as T?
            Long::class -> getLong(key, default as? Long ?: -1L) as T?
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }


}