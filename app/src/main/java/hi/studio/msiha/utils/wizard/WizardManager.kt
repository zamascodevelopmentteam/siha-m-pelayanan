package hi.studio.msiha.utils.wizard

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager

class WizardManager(
    private val pager: ViewPager,
    private val prevBtn: View,
    private val nextBtn: View
) {
    var currentPosition = 0
    private var callback: (position: Int, bundle: Bundle) -> Unit =
        { _: Int, _: Bundle -> }

    fun build(): WizardManager {
        disableScroll()
        pager.onPageChange {
            currentPosition = it
            configureButton(it)
        }
        prevBtn.setOnClickListener {
            prev()
        }
        nextBtn.setOnClickListener {
            next()
        }
        return this
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun disableScroll() {
        //fixme can be replace using custom view pager
        pager.setOnTouchListener { _, _ -> true }
    }

    fun move(step: Int) {
        pager.setCurrentItem(pager.currentItem + step, true)
    }

    fun next() {
        if (validateStep(currentPosition)) {
            move(1)
        }
    }

    fun prev() {
        move(-1)
    }

    private fun isLast(step: Int): Boolean = step + 1 == pager.adapter?.count
    private fun isFirst(position: Int): Boolean = position == 0

    private fun configureButton(position: Int) {
        callback(
            position, if (isLast(position)) {
                getResult()
            } else {
                Bundle()
            }
        )
    }

    private fun validateStep(position: Int): Boolean {
        val adapter = pager.adapter as WizardAdapter
        val step = adapter.getWizardStep(position)
        return step.invalidateStep()
    }

    fun validate() {
        val adapter = pager.adapter as WizardAdapter
        for (i in 0 until adapter.count) {
            validateStep(i)
        }
    }

    fun getResult(): Bundle {
        val adapter = pager.adapter as WizardAdapter
        return Bundle().apply {
            for (i in 0 until adapter.count) {
                putAll(adapter.getWizardStep(i).value)
            }
        }
    }

    fun setOnWizardChangeListener(callback: (position: Int, bundle: Bundle) -> Unit): WizardManager {
        this.callback = callback
        return this
    }
}

abstract class WizardAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var steps = mutableListOf<WizardStep>()

    abstract fun setData(): MutableList<WizardStep>

    init {
        steps = this.setData()
    }

    override fun getItem(position: Int): Fragment = steps[position] as Fragment

    override fun getCount(): Int {
        return steps.size
    }

    fun getWizardStep(position: Int): WizardStep {
        return steps[position]
    }
}

interface WizardStep {
    var value: Bundle
    fun invalidateStep(): Boolean
}

fun ViewPager.onPageChange(callback: (page: Int) -> Unit) {
    this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            callback(position)
        }
    })
}