package hi.studio.msiha.utils

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import hi.studio.msiha.R
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.MedicineInstruction
import kotlinx.android.synthetic.main.bottom_sheet_medicine_instruction.*
import kotlinx.android.synthetic.main.bottom_sheet_medicine_instruction.view.*
import kotlinx.android.synthetic.main.bottom_sheet_medicine_instruction.view.etJumlahObat


interface AddMedInstruction{
    fun addPrescription(medInstruction: MedicineInstruction)
}
class AddMedInstructionBottomSheet(
    private val arvStock:ArvStock,
    private val addMedInstruction: AddMedInstruction): BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.bottom_sheet_medicine_instruction, container, false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        populateView(v)
        return v
    }

    private fun populateView(view:View){
        view.textNamaBarangVal.text=arvStock.name
        view.textJenisBarangVal.text=arvStock.medicineType
        view.textJumlahStokVal.text="${arvStock.stockQuantity}"
        view.btnSimpan.setOnClickListener {
            if(isValid()){
                Log.d("testing","value1 ${arvStock.id}")
                Log.d("testing","value2 ${arvStock.name}")
                Log.d("testing","value3 ${arvStock.stockUnitType}")
                Log.d("testing","value4 ${arvStock.codename}")
                Log.d("testing","value5 ${view.etJumlahObat.text.toString()}")
                Log.d("testing","value5 ${view.etJumlahHari.text.toString()}")
                Log.d("testing","value5 ${view.etAturanMinum.text.toString()}")
                val medicineInstruction = MedicineInstruction(
                    arvStock.id?:0,
                    arvStock.name?:"",
                    view.etJumlahObat.text.toString().toInt(),
                    arvStock.stockUnitType?:"",
                    view.etAturanMinum.text.toString(),
                    arvStock.codename?:"",
                    view.etJumlahHari.text.toString().toInt()
                )
                addMedInstruction.addPrescription(medicineInstruction)
            }else{
                Toast.makeText(context,"Semua field wajib di isi",Toast.LENGTH_SHORT).show()
            }
        }

        view.icClose.setOnClickListener {
            this.dismiss()
        }
    }

    private fun isValid(): Boolean {
        var valid=false
        if(etJumlahObat.text.toString()!="0" && etJumlahHari.text.toString()!="0")valid=true
        return valid
    }
}