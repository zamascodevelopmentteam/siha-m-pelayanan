package hi.studio.msiha.di

import hi.studio.msiha.ui.LocalSharedViewModel
import hi.studio.msiha.ui.auth.login.LoginViewModel
import hi.studio.msiha.ui.auth.register.RegisterViewModel
import hi.studio.msiha.ui.home.dashboard.HomeViewModel
import hi.studio.msiha.ui.home.patient.PatientViewModel
import hi.studio.msiha.ui.home.patient.add.AddPatientViewModel
import hi.studio.msiha.ui.home.patient.detail.*
import hi.studio.msiha.ui.home.patient.detail.exam.reagen.ReagenChooseViewModel
import hi.studio.msiha.ui.home.patient.edit.EditPatientDataViewModel
import hi.studio.msiha.ui.home.pharmacist.dashboard.PharmacistDashboardViewModel
import hi.studio.msiha.ui.home.pharmacist.order.createorder.CreateOrderViewModel
import hi.studio.msiha.ui.home.pharmacist.order.createorderdetail.CreateOrderDetailViewModel
import hi.studio.msiha.ui.home.pharmacist.recipe.PatientRecipeViewModel
import hi.studio.msiha.ui.home.pharmacist.recipe.arv.ArvViewModel
import hi.studio.msiha.ui.home.pharmacist.recipe.medicine.ChooseArvViewModel
import hi.studio.msiha.ui.home.pharmacist.stock.addmedicine.AddMedicineViewModel
import hi.studio.msiha.ui.home.pharmacist.stock.medicinedetail.MedicineDetailViewModel
import hi.studio.msiha.ui.home.pharmacist.stock.medicinelist.MedicineListViewModel
import hi.studio.msiha.ui.home.profile.ProfileViewModel
import hi.studio.msiha.ui.home.profile.edit.EditProfileViewModel
import hi.studio.msiha.ui.home.recipe.RecipeViewModel
import hi.studio.msiha.ui.home.recipe.add.AddRecipeViewModel
import hi.studio.msiha.ui.home.recipe.detail.RecipeDetailViewModel
import hi.studio.msiha.ui.home.visit.VisitViewModel
import hi.studio.msiha.ui.home.visit.add.AddVisitViewModel
import hi.studio.msiha.ui.home.visit.edit.VisitEditViewModel
import hi.studio.msiha.ui.home.visit.edit.form.OtpViewModel
import hi.studio.msiha.ui.home.visit.history.HistoryVisitViewModel
import hi.studio.msiha.ui.home.visit.preview.VisitPreviewViewModel
import hi.studio.msiha.ui.home.visit.search.PatientSearchViewModel
import hi.studio.msiha.ui.home.visit.todayvisit.TodayVisitViewModel
import hi.studio.msiha.ui.home.visit.visitplan.VisitPlanViewModel
import hi.studio.msiha.ui.scan.ScanViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        LoginViewModel(get())
    }

    viewModel {
        RegisterViewModel(get())
    }

    viewModel {
        HomeViewModel(get())
    }

    viewModel {
        ProfileViewModel(get(), get())
    }
    viewModel {
        PatientViewModel(get())
    }
    viewModel {
        ScanViewModel(get(), get(), get(), get())
    }
    viewModel {
        AddPatientViewModel(get())
    }
    viewModel {
        VisitViewModel(get())
    }
    viewModel {
        PharmacistDashboardViewModel(get(), get())
    }
    viewModel {
        AddMedicineViewModel(get())
    }
    viewModel {
        MedicineListViewModel(get())
    }
    viewModel {
        MedicineDetailViewModel(get())
    }
    viewModel {
        CreateOrderViewModel(get())
    }
    viewModel {
        CreateOrderDetailViewModel(get())
    }
    viewModel {
        OtpViewModel(get(), get())
    }
    viewModel {
        VisitEditViewModel(get())
    }
    viewModel {
        AddRecipeViewModel(get(), get())
    }
    viewModel {
        RecipeViewModel(get())
    }
    viewModel {
        RecipeDetailViewModel(get())
    }
    viewModel {
        PatientRecipeViewModel(get(), get(), get())
    }
    viewModel {
        EditProfileViewModel(get(), get())
    }
    viewModel {
        VisitPreviewViewModel(get())
    }
    viewModel {
        ReagenChooseViewModel(get())
    }
    viewModel {
        PatientSearchViewModel(get(),get())
    }
    viewModel {
        ODHAViewModel(get(), get())
    }
    viewModel {
        PatientDetailViewModel(get(), get(),get())
    }
    viewModel {
        ExamViewModel(get(),get())
    }
    viewModel {
        EditPatientDataViewModel(get())
    }
    viewModel {
        AddVisitViewModel(get())
    }
    viewModel {
        VisitDetailViewModel(get(),get())
    }
    viewModel {
        LocalSharedViewModel()
    }
    viewModel {
        ChooseArvViewModel(get())
    }
    viewModel {
        ArvViewModel(get(),get())
    }

    viewModel{
        TodayVisitViewModel(get())
    }

    viewModel{
        MedicineLeftViewModel(get(),get())
    }

    viewModel{
        VisitPlanViewModel(get())
    }

    viewModel{
        HistoryVisitViewModel(get())
    }
}