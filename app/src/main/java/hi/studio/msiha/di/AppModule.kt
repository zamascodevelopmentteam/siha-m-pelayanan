package hi.studio.msiha.di

import hi.studio.msiha.data.constant.KEY_PREF
import hi.studio.msiha.utils.PreferenceHelper
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {
    single {
        PreferenceHelper.customPrefs(androidContext(), KEY_PREF)
    }
}