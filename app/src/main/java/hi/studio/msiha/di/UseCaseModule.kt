package hi.studio.msiha.di

import hi.studio.msiha.domain.interactor.*
import org.koin.dsl.module

val useCaseModule = module {
    single {
        GetAuth(get())
    }
    single {
        GetHospital(get())
    }
    single {
        GetUser(get())
    }
    single {
        GetPatient(get())
    }
    single {
        GetVisit(get())
    }
    single{
        GetPharmacist(get())
    }
    single {
        GetRecipe(get())
    }
    single {
        GetExam(get())
    }
}