package hi.studio.msiha.di

import hi.studio.msiha.data.repositoryImpl.*
import org.koin.dsl.module

val repoModule = module {
    single {
        AuthRepositoryImpl(get())
    }
    single {
        HospitalRepositoryImpl(get())
    }
    single {
        UserRepositoryImpl(get())
    }
    single {
        PatientRepositoryImpl(get())
    }
    single {
        VisitRepositoryImpl(get())
    }
    single{
        PharmacistRepositoryImpl(get(),get())
    }
    single {
        RecipeRepositoryImpl(get())
    }
    single {
        ExamRepositoryImpl(get())
    }
}