package hi.studio.msiha.di

import com.google.gson.GsonBuilder
import com.readystatesoftware.chuck.ChuckInterceptor
import hi.studio.msiha.data.datasource.remote.NetworkManager
import hi.studio.msiha.data.datasource.remote.api.APIManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val remoteModule = module {
    single {
        GsonBuilder().setLenient().create()
    }

    single {
        NetworkManager.provideOkHttp(ChuckInterceptor(androidContext()))
    }

    single {
        NetworkManager.provideRetrofit(get(), get())
    }

    single {
        APIManager.provideAuthAPI(get())
    }

    single {
        APIManager.provideHospitalAPI(get())
    }

    single {
        APIManager.provideUserAPI(get())
    }

    single {
        APIManager.providePatientAPI(get())
    }

    single {
        APIManager.provideVisitAPI(get())
    }

    single {
        APIManager.provideMedicineAPI(get())
    }

    single {
        APIManager.provideOrderAPI(get())
    }

    single {
        APIManager.provideRecipeAPI(get())
    }

    single {
        APIManager.provideExamAPI(get())
    }
}