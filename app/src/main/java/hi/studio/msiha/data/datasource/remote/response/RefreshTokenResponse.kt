package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class RefreshTokenResponse(
    @SerializedName("data")
    val data: RefreshTokenDataResponse?
) : CommonResponse()

data class RefreshTokenDataResponse(
    @SerializedName("refreshToken")
    val refreshToken: String? = "",
    @SerializedName("token")
    val token: String? = ""
)