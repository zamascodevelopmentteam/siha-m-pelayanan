package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class ExamResponse(
    @SerializedName("data")
    val data: ExamDataResponse? = null
) : CommonResponse() {
    data class ExamDataResponse(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("examOrdinal")
        val examOrdinal: String? = null,
        @SerializedName("examRequestDate")
        val examRequestDate: String? = null,
        @SerializedName("examDate")
        val examDate: String? = null,
        @SerializedName("receiveResultDate")
        val receiveResultDate: String? = null,
        @SerializedName("reasonTestHiv")
        val reasonTestHiv: String? = null,
        @SerializedName("hivTestType")
        val hivTestType: String? = null,
        @SerializedName("hivExaminationMethod")
        val hivExaminationMethod: String? = null,
        @SerializedName("reagen1")
        val reagen1: String? = null,
        @SerializedName("nameReagen1")
        val nameReagen1: String? = null,
        @SerializedName("resultReagen1")
        val resultReagen1: String? = null,
        @SerializedName("reagen2")
        val reagen2: String? = null,
        @SerializedName("nameReagen2")
        val nameReagen2: String? = null,
        @SerializedName("resultReagen2")
        val resultReagen2: String? = null,
        @SerializedName("reagen3")
        val reagen3: String? = null,
        @SerializedName("nameReagen3")
        val nameReagen3: String? = null,
        @SerializedName("resultReagen3")
        val resultReagen3: String? = null,
        @SerializedName("nameReagenNat")
        val nameReagenNat: String? = null,
        @SerializedName("resultTestNat")
        val resultTestNat: String? = null,
        @SerializedName("nameReagenPcr")
        val nameReagenPcr: String? = null,
        @SerializedName("resultTestPcr")
        val resultTestPcr: String? = null,
        @SerializedName("nameReagenElisa")
        val nameReagenElisa: String? = null,
        @SerializedName("resultTestElisa")
        val resultTestElisa: String? = null,
        @SerializedName("nameReagenWesternBlot")
        val nameReagenWesternBlot: String? = null,
        @SerializedName("resultTestWesternBlot")
        val resultTestWesternBlot: String? = null,
        @SerializedName("conclusionResultTestHiv")
        val conclusionResultTestHiv: String? = null,
        @SerializedName("hivResult")
        val hivResult: String? = null,
        @SerializedName("sifilisResult")
        val sifilisResult: String? = null,
        @SerializedName("testLab")
        val testLab: String? = null,
        @SerializedName("resultTestLab")
        val resultTestLab: String? = null,
        @SerializedName("rprIms")
        val rprIms: Int? = null,
        @SerializedName("imsResult")
        val imsResult: String? = null,
        @SerializedName("createdAt")
        val createdAt: String? = null,
        @SerializedName("updatedAt")
        val updatedAt: String? = null,
        @SerializedName("visitId")
        val visitId: Int? = null,
        @SerializedName("UserId")
        val userId: Int? = null,
        @SerializedName("hospitalId")
        val hospitalId: Int? = null,

        @SerializedName("vlTestType")
        val vlTestType: String? = null,
        @SerializedName("vlReagen")
        val vlReagen: String? = null,
        @SerializedName("vlReagenAmount")
        val vlReagenAmount: Int? = null,
        @SerializedName("vlTestResult")
        val vlTestResult: Int? = null
    )
}

