package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class TodayVisitResponse(
    @SerializedName("data")
    val data: List<TodayVisitDataResponse>? = listOf()
) : CommonResponse()

data class TodayVisitDataResponse(
    @SerializedName("ordinal")
    val ordinal: String? = "",
    @SerializedName("User")
    val user: UserResponse?
)