package hi.studio.msiha.data.datasource.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.ArvStock
import kotlinx.android.parcel.Parcelize

@Parcelize
class TestHiv (
    val id:Int?,
    @SerializedName("ordinal")
    val ordinal: Int? = null,
    @SerializedName("visitId")
    val visitId: Int? = null,
    @SerializedName("kelompokPopulasi")
    val kelompokPopulasi: String? = null,
    @SerializedName("lsmPenjangkau")
    val lsmPenjangkau: String? = null,
    @SerializedName("alasanTest")
    val aasanTest: String? = null,
    @SerializedName("tanggalTest")
    val tanggalTest: String? = null,
    @SerializedName("jenisTest")
    val jenisTest: String? = null,
    @SerializedName("namaReagen1")
    val namaReagen1: Int? = null,
    @SerializedName("hasilTestR1")
    val hasilTest1: String? = null,
    @SerializedName("namaReagen2")
    val namaReagen2: Int? = null,
    @SerializedName("hasilTestR2")
    val hasilTest2: String? = null,
    @SerializedName("namaReagen3")
    val namaReagen3: Int? = null,
    @SerializedName("hasilTestR3")
    val hasilTest3: String? = null,
    @SerializedName("namaReagenDna")
    val namaReagenDna: Int? = null,
    @SerializedName("hasilTestDna")
    val hasilTestDna: String? = null,
    @SerializedName("qtyReagenDna")
    val qtyReagenDna: Int? = null,
    @SerializedName("namaReagenRna")
    val namaReagenRna: Int? = null,
    @SerializedName("hasilTestRna")
    val hasilTestRna: String? = null,
    @SerializedName("qtyReagenRna")
    val qtyReagenRna: Int? = null,
    @SerializedName("namaReagenElisa")
    val namaReagenElisa: Int? = null,
    @SerializedName("hasilTestElisa")
    val hasilTestElisa: String? = null,
    @SerializedName("qtyReagenElisa")
    val qtyReagenElisa: Int? = null,
    @SerializedName("rujukanLabLanjut")
    val rujukanLabLanjut: String? = null,
    @SerializedName("namaReagenWb")
    val namaReagenWb: Int? = null,
    @SerializedName("hasilTestWb")
    val hasilTestWb: String? = null,
    @SerializedName("kesimpulanHiv")
    val kesimpulanHiv: String? = null,
    val namaReagenR1Data:ArvStock?,
    val namaReagenR2Data: ArvStock?,
    val namaReagenR3Data: ArvStock?,
    val namaReagenDnaData: ArvStock?,
    val namaReagenRnaData: ArvStock?,
    val namaReagenNatData:ArvStock?,
    val namaReagenElisaData: ArvStock?,
    val namaReagenWbData: ArvStock?,
    val hasilTestNat:String?=null
) : Parcelable