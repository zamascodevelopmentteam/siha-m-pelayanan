package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.ODHARequest
import hi.studio.msiha.data.datasource.remote.request.RegisterPatientRequest
import hi.studio.msiha.data.datasource.remote.request.StatusPatientRequest
import hi.studio.msiha.data.datasource.remote.response.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface PatientAPI {
    @GET("private/upk/patients")
    fun getPatientAsync(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Deferred<Response<PatientsResponse>>

    @POST("private/patient/create")
    fun createPatientAsync(
        @Body data: RegisterPatientRequest
    ): Deferred<Response<CreatePatientResponse>>

    @GET("private/patient/{id}/visits")
    fun getVisitHistoriesAsync(
        @Path("id") idPatient: Int,
        @Query("listType") listType:String
    ): Deferred<Response<VisitHistoriesResponse>>

    //@PUT("private/patient/update/{id}")
    @PUT("private/patient/{id}")
    fun updatePatientAsync(
        @Path("id") patientId: Int,
        @Body data: RegisterPatientRequest
    ): Deferred<Response<DefaultResponse>>

    @PUT("private/patient/{id}/status")
    fun activatePatientAsync(
        @Path("id") patientId: Int,
        @Body data: StatusPatientRequest
    ): Deferred<Response<DefaultResponse>>

    @GET("private/patient/{id}/recipes")
    fun getRecipesAsync(
        @Path("id") patientId: Int,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Deferred<Response<RecipesResponse>>

    //@PUT("private/patient/update/odha/{id}")
    @POST("private/patient/{id}/odha")
    fun updateToODHA(
        @Path("id") patientId: Int,
        @Body request: ODHARequest
    ): Deferred<Response<DefaultResponse>>

    @GET("private/patient/{nik}/nik")
    fun getPatientByNikAsync(@Path("nik") nik: String): Deferred<Response<PatientResponse>>


    @GET("private/patient/{id}")
    fun getPatientByIdAsync(@Path("id") id: Int): Deferred<Response<PatientResponse>>

}