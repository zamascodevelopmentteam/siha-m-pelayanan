package hi.studio.msiha.data.datasource.remote

import com.google.gson.GsonBuilder
import hi.studio.msiha.AppSIHA
import hi.studio.msiha.data.constant.KEY_HEADER
import hi.studio.msiha.data.datasource.remote.api.AuthAPI
import hi.studio.msiha.data.datasource.remote.request.RefreshTokenRequest
import hi.studio.msiha.utils.extention.getError
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import timber.log.Timber

class TokenAuthenticator : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        Timber.d("Response code: " + response.code)
        return when (response.code) {
            401 -> requestNewToken(response)
            else -> createRequest(response)
        }
    }

    private fun requestNewToken(originalResponse: Response): Request? {
        val refreshToken = AppSIHA.instance.getRefreshToken()
        refreshToken?.let { it ->
            val requestToken = RefreshTokenRequest(
                it,
                AppSIHA.instance.getUser()?.id.toString()
            )
            Timber.d(requestToken.toString())

            val client = NetworkManager.provideOkHttp()
            client?.let {
                val retrofit = NetworkManager.provideRetrofit(it, GsonBuilder().create())
                val api = retrofit?.create(AuthAPI::class.java)
                try {
                    val response = api?.refreshAsync(requestToken)?.execute()
                    response?.apply {
                        Timber.d("response fetched")
                        if (isSuccessful) {
                            Timber.d("response success")
                            body()?.apply {
                                Timber.d("Refresh Token Updated")
                                AppSIHA.instance.setRefreshToken(data?.refreshToken ?: "")
                                AppSIHA.instance.setToken(data?.token ?: "")
                                return createRequest(originalResponse)
                            } ?: run {
                                Timber.e(getError())
                                AppSIHA.instance.logout()
                                return null
                            }
                        } else {
                            Timber.e(getError())
                            AppSIHA.instance.logout()
                            return null
                        }
                    }
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }
        AppSIHA.instance.clearData()
        return null
    }

    private fun createRequest(response: Response): Request? {
        val token = AppSIHA.instance.getToken()
        token?.let {
            return response.request.newBuilder()
                .header(KEY_HEADER, it)
                .build()
        }
        AppSIHA.instance.logout()
        return null
    }
}