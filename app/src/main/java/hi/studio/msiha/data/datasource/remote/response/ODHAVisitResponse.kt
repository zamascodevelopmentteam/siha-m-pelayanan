package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class ODHAVisitResponse(
    @SerializedName("data")
    val data: Data
) : CommonResponse() {
    data class Data(
        @SerializedName("ordinal")
        val ordinal: Int? = null,
        @SerializedName("patientId")
        val patientId: Int? = null,
        @SerializedName("treatementStartDate")
        val treatementStartDate: String? = null,
        @SerializedName("noRegNas")
        val noRegNas: String? = null,
        @SerializedName("tglKonfirmasiHivPos")
        val tglKonfirmasiHivPos: String? = null,
        @SerializedName("tglKunjungan")
        val tglKunjungan: String? = null,
        @SerializedName("tglRujukMasuk")
        val tglRujukMasuk: String? = null,
        @SerializedName("kelompokPopulasi")
        val kelompokPopulasi: String? = null,
        @SerializedName("statusTb")
        val statusTb: String? = null,
        @SerializedName("tglPengobatanTb")
        val tglPengobatanTb: String? = null,
        @SerializedName("statusFungsional")
        val statusFungsional: String? = null,
        @SerializedName("stadiumKlinis")
        val stadiumKlinis: String? = null,
        @SerializedName("ppk")
        val ppk: Boolean? = null,
        @SerializedName("tglPemberianPpk")
        val tglPemberianPpk: String? = null,
        @SerializedName("statusTbTpt")
        val statusTbTpt: String? = null,
        @SerializedName("tglPemberianObat")
        val tglPemberianObat: String? = null,
        @SerializedName("obatArv1")
        val obatArv1: Int? = null,
        @SerializedName("jmlHrObatArv1")
        val jmlHrObatArv1: Int? = null,
        @SerializedName("obatArv2")
        val obatArv2: Int? = null,
        @SerializedName("jmlHrObatArv2")
        val jmlHrObatArv2: Int? = null,
        @SerializedName("obatArv3")
        val obatArv3: Int? = null,
        @SerializedName("jmlHrObatArv3")
        val jmlHrObatArv3: Int? = null,
        @SerializedName("lsmPenjangkauId")
        val lsmPenjangkauId: Int? = null,
        @SerializedName("namaReagen")
        val namaReagen: Int? = null,
        @SerializedName("jmlReagen")
        val jmlReagen: Int? = null,
        @SerializedName("hasilLab")
        val hasilLab: Int? = null,
        @SerializedName("hasilLabLain")
        val hasilLabLain: Int? = null,
        @SerializedName("notifikasiPasangan")
        val notifikasiPasangan: String? = null
    )
}