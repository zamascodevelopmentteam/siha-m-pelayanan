package hi.studio.msiha.data.repositoryImpl

import hi.studio.msiha.data.datasource.remote.api.ExamAPI
import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import hi.studio.msiha.data.datasource.remote.response.HivTestHistory
import hi.studio.msiha.data.datasource.remote.response.ImsTestHistory
import hi.studio.msiha.data.datasource.remote.response.VlTestHistory
import hi.studio.msiha.data.mapper.ExamMapper
import hi.studio.msiha.domain.model.Exam
import hi.studio.msiha.domain.repository.ExamRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class ExamRepositoryImpl(private val api: ExamAPI) : ExamRepository {

    override fun getImsHistoryByVisit(
        visitId: Int,
        callback: (Either<ArrayList<ImsTestHistory>?, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getImsExamHistory(visitId).await()
                .parse({
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getVlHistoryByVisit(
        visitId: Int,
        callback: (Either<ArrayList<VlTestHistory>?, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getVlExamHistory(visitId).await()
                .parse({
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getHivHistoryByVisit(
        visitId: Int,
        callback: (Either<ArrayList<HivTestHistory>?, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getHivExamHistory(visitId).await()
                .parse({
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }



    override fun createHIVExam(
        visitId:Int,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createHIVExam(visitId).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createIMSExam(
        visitId:Int,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createIMSExam(visitId).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createVLExam(
        visitId: Int,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createVLCExam(visitId).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateHIVExam(
        id: Int,
        request: HIVExamRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateHIVExam(id, request).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateIMSExam(
        id: Int,
        request: IMSExamRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateIMSExam(id, request).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateVLExam(
        id: Int,
        request: VLCExamRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateVLCExam(id, request).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getExamByVisit(idVisit: Int, callback: (Either<Exam?, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getExamByVisit(idVisit).await()
                .parse(
                    {
                        callback(Either.Left(ExamMapper.toExam(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getExam(id: Int, callback: (Either<Exam?, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getExam(id).await()
                .parse(
                    {
                        callback(Either.Left(ExamMapper.toExam(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }
}