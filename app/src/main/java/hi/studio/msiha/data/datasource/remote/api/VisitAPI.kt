package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.*
import hi.studio.msiha.data.datasource.remote.response.*
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitHistory
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface VisitAPI {
    @POST("private/visits")
    fun createVisitHIVNegativeAsync(
        @Body request: VisitHIVNegativeRequest
    ): Deferred<Response<VisitResponse>>

    @POST("private/visits")
    fun createVisitODHAAsync(
        @Body request: ODHAVisitRequest
    ): Deferred<Response<DefaultResponse>>

    @PUT("/private/visit/{visitId}/treatment")
    fun saveTreatmentData(
        @Path("visitId") visitId: Int,
        @Body visitData:VisitData
    ):Deferred<Response<DefaultResponse>>

    @POST("/private/visit/{visitId}/treatment")
    fun createTreatment(
        @Path("visitId") visitId: Int
    ):Deferred<Response<DefaultResponse>>

    @POST("private/visits/{patientId}")
    fun createVisitGeneral(
        @Path("patientId") patientId:Int
    ): Deferred<Response<CreateVisitResponse>>

    @PUT("private/visits/{id}")
    fun updateVisitDetail(
        @Path("id") id: Int,
        @Body request: EditVisitRequest
    ): Deferred<Response<DefaultResponse>>

    @PUT("private/visit-ikhtishar/{id}")
    fun updateIkhtisar(
        @Path("id") id: Int,
        @Body request: IkhtisarRequest
    ): Deferred<Response<DefaultResponse>>

    @GET("private/visits/{id}")
    fun getIkhtisar(
        @Path("id") visitId: Int
    ): Deferred<Response<IkhtisarResponse>>

    @POST("private/visits/{id}/recipe-given")
    fun giveRecipe(
        @Path("id") visitId: Int,
        @Body request: GiveRecipeRequest
    ): Deferred<Response<DefaultResponse>>

    @GET("private/visits/{id}")
    fun getVisitDetail(
        @Path("id") visitId: Int
    ): Deferred<Response<VisitResponse>>

    @GET("private/upk/visits")
    fun getAllVisitByUpk(
        @Query("limit") limit:Int,
        @Query("page") page:Int,
        @Query("listType") type:String
    ):Deferred<Response<AllVisitResponse>>

    @GET("private/upk/visits")
    fun getAllVisitByUpkWithKeyword(
        @Query("limit") limit:Int,
        @Query("keyword") keyword:String,
        @Query("page") page:Int,
        @Query("listType") type:String
    ):Deferred<Response<AllVisitResponse>>

    @GET("private/upk/visits")
    fun getPlannedVisit(
        @Query("limit") limit:Int,
        @Query("page") page:Int,
        @Query("listType") type:String
    ):Deferred<Response<AllVisitResponse>>

    @GET("private/visits/{visitId}")
    fun getAllDetailVisit(
        @Path("visitId") visitId:Int
    ):Deferred<Response<VisitAllDetailResponse>>

    @POST("private/visit/{visitId}/checkout")
    fun endVisit(
        @Path("visitId") visitId:Int
    ):Deferred<Response<DefaultResponse>>

    @GET("private/visit/{visitId}/previous-treatment")
    fun getPreviousTreatment(
        @Path("visitId") visitId:Int
    ):Deferred<Response<VisitAllDetailResponse>>

    @PUT("private/visit/{visitId}/sisa-obat")
    fun updateSisaObat(
        @Path("visitId") visitId: Int,
        @Body medicineLeftRequest: MedicineLeftRequest
    ):Deferred<Response<DefaultResponse>>
}