package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Prescription

data class VisitHistoryResponse(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("ordinal")
    val ordinal: Int? = null,
    @SerializedName("visitType")
    val visitType: String? = null,
    @SerializedName("visitDate")
    val visitDate: String? = null,
    @SerializedName("checkOutDate")
    val checkOutDate: String? = null,
    @SerializedName("createdBy")
    val createdBy: Int? = null,
    @SerializedName("updatedBy")
    val updatedBy: String? = null,
    @SerializedName("createdAt")
    val createdAt: String? = null,
    @SerializedName("updatedAt")
    val updatedAt: String? = null,
    @SerializedName("deletedAt")
    val deletedAt: String? = null,
    @SerializedName("UserId")
    val userId: Int? = null,
    @SerializedName("upkId")
    val upkId: Int? = null,
    @SerializedName("patientId")
    val patientId: Int? = null,
    @SerializedName("patient")
    val patient: Patient?=null,
    val prescription: Prescription
)