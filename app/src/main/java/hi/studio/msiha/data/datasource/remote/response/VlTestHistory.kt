package hi.studio.msiha.data.datasource.remote.response

import android.os.Parcelable
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import kotlinx.android.parcel.Parcelize

@Parcelize
class VlTestHistory (
    val id:Int?,
    val ordinal:Int?,
    val visitType:String?,
    val visitDate:String?,
    val checkoutDate:String,
    val userRrId:Int?,
    val createdBy:Int?,
    val updatedBy:Int,
    val createdAt:String?,
    val updatedAt:String?,
    val deleteAt:String?,
    val upkId:Int,
    val patientId:Int,
    val testVL: VLCExamRequest
) : Parcelable