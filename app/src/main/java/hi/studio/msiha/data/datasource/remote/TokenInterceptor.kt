package hi.studio.msiha.data.datasource.remote

import hi.studio.msiha.AppSIHA
import hi.studio.msiha.data.constant.KEY_HEADER
import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = AppSIHA.instance.getToken()
        val original = chain.request()
        val builder = original.newBuilder()
        if (token != null) {
            builder.header(KEY_HEADER, token)
        }
        val request = builder.build()
        return chain.proceed(request)
    }
}