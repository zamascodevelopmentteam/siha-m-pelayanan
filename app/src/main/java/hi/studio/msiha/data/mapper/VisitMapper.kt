package hi.studio.msiha.data.mapper

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.request.CoupleRequest
import hi.studio.msiha.data.datasource.remote.request.EditVisitRequest
import hi.studio.msiha.data.datasource.remote.request.ODHAVisitRequest
import hi.studio.msiha.data.datasource.remote.response.VisitResponse
import hi.studio.msiha.domain.model.Couple
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.ui.home.patient.detail.VisitDetailRRFragment
import hi.studio.msiha.ui.home.patient.overview.OverviewFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormCoupleFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormFinalFollowUpFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormTBCFragment
import hi.studio.msiha.ui.home.visit.edit.form.FormVisitInfoFragment
import hi.studio.msiha.utils.extention.fromForm

object VisitMapper {

    fun toVisit(response: VisitResponse.Data?): Visit {
        response?.apply {
            return Visit(
                id,
                ordinal,
                visitType,
                visitDate,
                createdBy,
                createdAt,
                updatedAt,
                userId,
                upkId,
                patientId
            )
        }
        return Visit()
    }

    fun toEditVisitRequest(bundle: Bundle): EditVisitRequest {
        bundle.apply {
            val visitDate = this[FormVisitInfoFragment.KEY_VISIT_DATE] as Long?

            val followUp = this[FormFinalFollowUpFragment.KEY_FINAL_FOLLOW_UP] as String?
            val followUpDate = this[FormFinalFollowUpFragment.KEY_DATE] as Long?

            val tbStatus = this[FormTBCFragment.KEY_STATUS_TB] as String?
            val ppkDate = this[FormTBCFragment.KEY_PPK_DATE] as Long?
            val ppk = this[FormTBCFragment.KEY_PPK] as Boolean?
            val clinicalStadium = this[OverviewFragment.KEY_CLINICAL_STADIUM] as String?
            val functionalStatus = this[OverviewFragment.KEY_FUNCTIONAL_STATUS] as String?
            val therapyDate = this[FormTBCFragment.KEY_THERAPY_DATE] as Long?
            val therapyStatus = this[FormTBCFragment.KEY_THERAPY_STATUS] as String?
            val treatmentDate = this[FormTBCFragment.KEY_TREATMENT_DATE] as Long?

            val notificationStatus = this[FormCoupleFragment.KEY_NOTIFICATION_STATUS] as String?
            val couples = this[FormCoupleFragment.KEY_COUPLES] as List<Couple>

            return EditVisitRequest(
                clinicalStadium = clinicalStadium?.fromForm(),
                followUp = followUp?.fromForm(),
                functionalStatus = functionalStatus?.fromForm(),
                tbStatus = tbStatus?.fromForm(),
                visitDate = visitDate,
                ppk = ppk,
                dateStartGiveTreatmentPreventionTb = therapyDate,
                dateStartGiveppk = ppkDate,
                dateStartTreatmentTb = treatmentDate,
                statusTreatmentPreventionTb = therapyStatus,
                statusNotif = notificationStatus,
                couples = couples.map {
                    CoupleRequest(
                        name = it.name,
                        age = it.age,
                        relationship = it.relationship,
                        gender = it.gender
                    )
                }
            )
        }
    }

    fun toODHAVisitRequest(bundle: Bundle): ODHAVisitRequest {
        bundle.apply {
            val ordinal = this[VisitDetailRRFragment.KEY_ORDINAL] as Int?
            val patientId = this[VisitDetailRRFragment.KEY_PATIENT_ID] as Int?

            val date = this[VisitDetailRRFragment.KEY_DATE] as String?

            val visitDate = this[FormVisitInfoFragment.KEY_VISIT_DATE] as String?
            val referInDate = this[FormVisitInfoFragment.KEY_REFER_IN_DATE] as String?
            val confirmDate = this[FormVisitInfoFragment.KEY_CONFIRM_DATE] as String?
            val fasyankesDate = this[FormVisitInfoFragment.KEY_FASYANKES] as String?
            val lsmDate = this[FormVisitInfoFragment.KEY_LSM] as String?
            val populationDate = this[FormVisitInfoFragment.KEY_POPULATION] as String?

            val followUp = this[FormFinalFollowUpFragment.KEY_FINAL_FOLLOW_UP] as String?
            val followUpDate = this[FormFinalFollowUpFragment.KEY_DATE] as String?

            val tbStatus = this[FormTBCFragment.KEY_STATUS_TB] as String?
            val ppkDate = this[FormTBCFragment.KEY_PPK_DATE] as String?
            val ppk = this[FormTBCFragment.KEY_PPK] as Boolean?
            val clinicalStadium = this[OverviewFragment.KEY_CLINICAL_STADIUM] as String?
            val functionalStatus = this[OverviewFragment.KEY_FUNCTIONAL_STATUS] as String?
            val therapyDate = this[FormTBCFragment.KEY_THERAPY_DATE] as String?
            val therapyStatus = this[FormTBCFragment.KEY_THERAPY_STATUS] as String?
            val treatmentDate = this[FormTBCFragment.KEY_TREATMENT_DATE] as String?

            val notificationStatus = this[FormCoupleFragment.KEY_NOTIFICATION_STATUS] as String?
            val couples = this[FormCoupleFragment.KEY_COUPLES] as List<Couple>

            return ODHAVisitRequest(
                ordinal,
                patientId,
                treatmentDate
            )
        }
    }
}