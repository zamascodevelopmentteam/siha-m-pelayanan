package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class RegisterPatientRequest(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("nik")
    val nik: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("addressKTP")
    val addressKTP: String? = null,
    @SerializedName("addressDomicile")
    val addressDomicile: String? = null,
    @SerializedName("dateBirth")
    val dateBirth: String? = null,
    @SerializedName("gender")
    val gender: String? = null,
    @SerializedName("phone")
    val phone: String? = null,
    @SerializedName("statusPatient")
    val statusPatient: String? = null,
    @SerializedName("weight")
    val weight: Int? = null,
    @SerializedName("height")
    val height: Int? = null,
    @SerializedName("namaPmo")
    val namaPmo: String? = null,
    @SerializedName("hubunganPmo")
    val hubunganPmo: String? = null,
    @SerializedName("noHpPmo")
    val noHpPmo: String? = null,
    @SerializedName("UserId")
    val userId: Int? = null,
    @SerializedName("upkId")
    val upkId: Int? = null,
    val email:String?=null
)