package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class StatisticResponse(
    @SerializedName("data")
    val data: StatisticDataResponse?
) : CommonResponse()

data class StatisticDataResponse(
    @SerializedName("visitEstimation")
    val visitEstimation: Int? = 0,
    @SerializedName("visitThisDay")
    val visitThisDay: Int? = 0,
    @SerializedName("visitThisMonth")
    val visitThisMonth: Int? = 0,
    @SerializedName("examThisDay")
    val examThisDay: Int? = 0
)