package hi.studio.msiha.data.datasource.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Couple
import hi.studio.msiha.domain.model.Prescription
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ODHAVisitRequest(
    @SerializedName("ordinal")
    val ordinal: Int? = null,
    @SerializedName("patientId")
    val patientId: Int? = null,
    @SerializedName("date")
    val date: String? = null,
    @SerializedName("treatmentStartDate")
    val treatementStartDate: String? = null,
    @SerializedName("noRegNas")
    val noRegNas: String? = null,
    @SerializedName("tglKonfirmasiHivPos")
    var tglKonfirmasiHivPos: String? = null,
    @SerializedName("tglKunjungan")
    var tglKunjungan: String? = null,
    @SerializedName("tglRujukMasuk")
    val tglRujukMasuk: String? = null,
    @SerializedName("kelompokPopulasi")
    val kelompokPopulasi: String? = null,
    @SerializedName("statusTb")
    val statusTb: String? = null,
    @SerializedName("tglPengobatanTb")
    val tglPengobatanTb: String? = null,
    @SerializedName("statusFungsional")
    val statusFungsional: String? = null,
    @SerializedName("stadiumKlinis")
    val stadiumKlinis: String? = null,
    @SerializedName("ppk")
    val ppk: Boolean? = null,
    @SerializedName("tglPemberianPpk")
    val tglPemberianPpk: String? = null,
    @SerializedName("statusTbTpt")
    val statusTbTpt: String? = null,
    @SerializedName("tglPemberianObat")
    val tglPemberianObat: String? = null,
    @SerializedName("obatArv1")
    val obatArv1: Int? = null,
    @SerializedName("jmlHrObatArv1")
    val jmlHrObatArv1: Int? = null,
    @SerializedName("sisaObatArv1")
    val sisaObatArv1: Int? = null,
    @SerializedName("obatArv2")
    val obatArv2: Int? = null,
    @SerializedName("jmlHrObatArv2")
    val jmlHrObatArv2: Int? = null,
    @SerializedName("sisaObatArv2")
    val sisaObatArv2: Int? = null,
    @SerializedName("obatArv3")
    val obatArv3: Int? = null,
    @SerializedName("jmlHrObatArv3")
    val jmlHrObatArv3: Int? = null,
    @SerializedName("sisaObatArv3")
    val sisaObatArv3: Int? = null,
    @SerializedName("lsmPenjangkauId")
    val lsmPenjangkauId: Int? = null,
    @SerializedName("namaReagen")
    val namaReagen: Int? = null,
    @SerializedName("jmlReagen")
    val jmlReagen: Int? = null,
    @SerializedName("hasilLab")
    val hasilLab: Int? = null,
    @SerializedName("hasilLabLain")
    val hasilLabLain: Int? = null,
    @SerializedName("notifikasiPasangan")
    var notifikasiPasangan: String? = null,
    @SerializedName("couples")
    var couples: List<Couple?>? = null,
    val obatArv1Data:ArvStock?=null,
    val obatArv2Data:ArvStock?=null,
    val obatArv3Data:ArvStock?=null,
    var height:String?=null,
    var weight:String?=null,
    val prescription:Prescription?=null,
    var lsmPenjangkau:String?=null,
    var tglPemberianTbTpt:String?=null,
    var akhirFollowUp:String?=null,
    var tglRujukKeluar:String?=null,
    var tglMeninggal:String?=null,
    var tglBerhentiArv:String?=null,
    var isOnTransit:Boolean?=null
) : Parcelable {
    @Parcelize
    data class Couple(
        val id: Int?=0,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("age")
        val age: Int? = null,
        @SerializedName("gender")
        val gender: String? = null,
        val relationship: String?=null
    ) : Parcelable{
        fun toKopel():hi.studio.msiha.domain.model.Couple{
            return hi.studio.msiha.domain.model.Couple(id?:0,name?:"",age?:0,gender?:"",relationship?:"")
        }
    }
}