package hi.studio.msiha.data.datasource.remote.request

class IkhtisarRequest(
    val visitDate: String,
    val regnas:String
)