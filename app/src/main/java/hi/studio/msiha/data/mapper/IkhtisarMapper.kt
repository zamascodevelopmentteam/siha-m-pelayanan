package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.request.IkhtisarRequest
import hi.studio.msiha.data.datasource.remote.response.IkhtisarDataResponse

object IkhtisarMapper {
    fun toIkhtisar(response: IkhtisarDataResponse?):IkhtisarRequest{
        response?.apply {
            return IkhtisarRequest(
                visitDate,
                regnas
            )
        }
        return IkhtisarRequest("", "")
    }
}