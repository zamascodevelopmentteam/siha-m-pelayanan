package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class GiveRecipeRequest(
    @SerializedName("recipeId")
    val recipeId: Int = 0,
    @SerializedName("patientId")
    val patientId: Int = 0
)