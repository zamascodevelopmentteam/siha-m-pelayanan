package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("nik")
    val nik: String? = null,
    @SerializedName("isActive")
    val isActive: Boolean? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("role")
    val role: String? = null,
    @SerializedName("logisticrole")
    val logisticrole: String? = null,
    @SerializedName("avatar")
    val avatar: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("phone")
    val phone: String? = null,
    @SerializedName("created_by")
    val createdBy: Int? = null,
    @SerializedName("updated_by")
    val updatedBy: Int? = null,
    @SerializedName("createdAt")
    val createdAt: String? = null,
    @SerializedName("updatedAt")
    val updatedAt: String? = null,
    @SerializedName("provinceId")
    val provinceId: Int? = null,
    @SerializedName("upkId")
    val upkId: Int? = null,
    @SerializedName("sudinKabKotumId")
    val sudinKabKotumId: Int? = null
)