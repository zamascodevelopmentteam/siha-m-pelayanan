package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class RecipesResponse(
    @SerializedName("data")
    val data: List<RecipeResponse>? = emptyList()
) : CommonResponse()