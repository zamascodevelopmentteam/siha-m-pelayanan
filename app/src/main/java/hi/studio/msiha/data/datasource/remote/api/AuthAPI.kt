package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.LoginRequest
import hi.studio.msiha.data.datasource.remote.request.RefreshTokenRequest
import hi.studio.msiha.data.datasource.remote.request.RegisterRequest
import hi.studio.msiha.data.datasource.remote.response.AuthResponse
import hi.studio.msiha.data.datasource.remote.response.LogoutResponse
import hi.studio.msiha.data.datasource.remote.response.RefreshTokenResponse
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthAPI {
    @POST("public/auth/login")
    fun loginAsync(@Body req: LoginRequest): Deferred<Response<AuthResponse>>

    @POST("public/auth/register")
    fun registerAsync(@Body req: RegisterRequest): Deferred<Response<AuthResponse>>

    @POST("private/auth/logout")
    fun logoutAsync(
        @Query("fcmToken") fcmToken:String
    ): Deferred<Response<LogoutResponse>>

    @POST("public/auth/refresh")
    fun refreshAsync(@Body request: RefreshTokenRequest): Call<RefreshTokenResponse>
}