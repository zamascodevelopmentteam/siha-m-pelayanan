package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.VisitDetail
import hi.studio.msiha.domain.model.VisitHistory

data class VisitAllDetailResponse(
    @SerializedName("data")
    val data: VisitDetail
) : CommonResponse()