package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class VisitHIVNegativeRequest(
    @SerializedName("ordinal")
    val ordinal: Int,
    @SerializedName("patientId")
    val idPatient: Int,
    @SerializedName("date")
    val date: String? = null
)