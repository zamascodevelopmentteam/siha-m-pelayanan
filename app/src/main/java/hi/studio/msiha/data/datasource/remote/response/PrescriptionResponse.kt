package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Prescription

data class PrescriptionResponse(
    @SerializedName("data")
    val data: Prescription
) : CommonResponse()