package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class HivTestHistoryResponse(
    @SerializedName("data")
    val data: ArrayList<HivTestHistory>
) : CommonResponse()