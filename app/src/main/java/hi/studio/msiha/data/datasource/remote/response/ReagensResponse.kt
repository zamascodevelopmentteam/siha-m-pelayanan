package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class ReagensResponse(
    @SerializedName("data")
    val data: List<ReagenResponse>? = null
) : CommonResponse()