package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class CreatePatientResponse(
    @SerializedName("data")
    val data: PatientResponse?
) : CommonResponse()
