package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.MedicineResponse
import hi.studio.msiha.data.datasource.remote.response.MedicineListResponse
import hi.studio.msiha.data.datasource.remote.response.OrderListResponse
import hi.studio.msiha.data.datasource.remote.response.OrderResponse
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.Order

object MedicineMapper {
    fun toListOrder(data: OrderListResponse?): List<Order> = data?.data?.map {
        Order(
            it.id ?: 0,
            it.invoiceNumber ?: "",
            it.transactionDate ?: 0,
            it.confirm ?: true,
            it.to?: "",
            it.amount?:0,
            it.createdAt ?: "",
            it.updatedAt ?: "",
            it.hospitalId ?: 0,
            it.orderMedicines
        )
    } ?: listOf()

    fun toMedicine(data:MedicineResponse):MedicineRsp {
        data.apply {
            return this.data
        }
    }

    fun toMedicineList(data:MedicineListResponse):List<MedicineRsp> = data.data

    fun toOrder(it: OrderResponse): Order {
            return it.data
    }
}