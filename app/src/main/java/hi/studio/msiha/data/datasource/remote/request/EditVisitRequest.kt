package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName


data class EditVisitRequest(
    @SerializedName("clinicalStadium")
    val clinicalStadium: String? = null,
    @SerializedName("dateStartGiveTreatmentPreventionTb")
    val dateStartGiveTreatmentPreventionTb: Long? = null,
    @SerializedName("dateStartGiveppk")
    val dateStartGiveppk: Long? = null,
    @SerializedName("dateStartTreatmentTb")
    val dateStartTreatmentTb: Long? = null,
    @SerializedName("followUp")
    val followUp: String? = null,
    @SerializedName("functionalStatus")
    val functionalStatus: String? = null,
    @SerializedName("kbMethod")
    val kbMethod: String? = null, // null
    @SerializedName("lsmPenjangkkau")
    val lsmPenjangkau: String? = null,
    @SerializedName("populationGroup")
    val populationGroup: String? = null,
    @SerializedName("ppk")
    val ppk: Boolean? = null,
    @SerializedName("statusTb")
    val statusTb: String? = null,
    @SerializedName("statusTreatmentPreventionTb")
    val statusTreatmentPreventionTb: String? = null,
    @SerializedName("tbStatus")
    val tbStatus: String? = null,
    @SerializedName("visitDate")
    val visitDate: Long? = null,
    @SerializedName("statusNotifCouple")
    val statusNotif: String? = null,
    @SerializedName("couples")
    val couples: List<CoupleRequest>
)

data class CoupleRequest(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("age")
    val age: Int? = null,
    @SerializedName("gender")
    val gender: String? = null,
    @SerializedName("relationship")
    val relationship: String? = null
)