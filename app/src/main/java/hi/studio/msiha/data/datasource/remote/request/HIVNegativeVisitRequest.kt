package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class HIVNegativeVisitRequest(
    @SerializedName("hospitalId")
    val idHospital: Int,
    @SerializedName("patientId")
    val idPatient: Int
)