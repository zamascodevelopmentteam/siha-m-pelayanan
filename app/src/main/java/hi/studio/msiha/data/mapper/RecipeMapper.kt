package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.request.RecipeMedicineRequest
import hi.studio.msiha.data.datasource.remote.request.RecipeRequest
import hi.studio.msiha.data.datasource.remote.response.MedicineResponse
import hi.studio.msiha.data.datasource.remote.response.RecipeDetailDataResponse
import hi.studio.msiha.data.datasource.remote.response.RecipeMedicineResponse
import hi.studio.msiha.data.datasource.remote.response.RecipeResponse
import hi.studio.msiha.domain.model.*

object RecipeMapper {
    fun toAddRecipe(
        nik: String,
        date: Long,
        notes: String,
        medicines: List<RecipeMedicine>
    ): RecipeRequest {
        return RecipeRequest(nik, date, notes, toListRecipeMedicineRequest(medicines))
    }

    fun toListRecipeMedicineRequest(medicines: List<RecipeMedicine>): List<RecipeMedicineRequest> =
        medicines.map {
            RecipeMedicineRequest(it.medicineId, it.rule, it.amount)
        }

    fun toRecipes(response: List<RecipeResponse>?): List<Recipe> = response?.map {
        Recipe(it.id, it.recipeNo ?: "", it.medicineTotal)
    } ?: run {
        emptyList<Recipe>()
    }

    fun toRecipeDetail(response: RecipeDetailDataResponse?): RecipeDetail? {
        response?.apply {
            return RecipeDetail(
                id,
                recipeNo ?: "",
                notes ?: "",
                givenAt ?: "",
                userId,
                toListRecipeMedicine(medicines)
            )
        }
        return null
    }

    fun toListRecipeMedicine(response: List<RecipeMedicineResponse>?): List<RecipeMedicine> =
        response?.map {
            toRecipeMedicine(it)
        } ?: run {
            emptyList<RecipeMedicine>()
        }

    fun toRecipeMedicine(response: RecipeMedicineResponse): RecipeMedicine {
        response.apply {
            return RecipeMedicine(medicineId, "", rule, amount, toMedicine(medicine))
        }
    }

    fun toMedicine(response: MedicineResponse?): MedicineRsp? {
        response?.apply {
            return MedicineRsp(
                id,
                name ?: "",
                codeName ?: "",
                type ?: "",
                picture,
                medicineType ?: "",
                ingredients,
                usage,
                "",
                "",
                medicineStock ?: MedicineStocks()
            )
        }
        return null
    }
}