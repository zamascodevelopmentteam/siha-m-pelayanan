package hi.studio.msiha.data.datasource.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.NamaReagenData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IMSExamRequest(
    @SerializedName("ordinal")
    val ordinal: Int? = null,
    @SerializedName("visitId")
    val visitId: Int? = null,
    @SerializedName("ditestIms")
    val testIms: Boolean? = null,
    @SerializedName("namaReagen")
    val namaReagen: Int? = null,
    @SerializedName("qtyReagen")
    val qty: Int? = null,
    @SerializedName("hasilTestIms")
    val resultTestIms: Boolean? = null,
    @SerializedName("ditestSifilis")
    val testSifilis: Boolean? = null,
    @SerializedName("hasilTestSifilis")
    val resultTestSifilis: Boolean? = null,
    var namaReagenData:NamaReagenData?=null
) : Parcelable