package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class RecipeDetailResponse(
    @SerializedName("data")
    val data: RecipeDetailDataResponse
) : CommonResponse()

data class RecipeDetailDataResponse(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("recipeNumber")
    val recipeNo: String? = "",
    @SerializedName("notes")
    val notes: String? = "",
    @SerializedName("givenAt")
    val givenAt: String? = "",
    @SerializedName("UserId")
    val userId: Int = 0,
    @SerializedName("recipeMedicines")
    val medicines: List<RecipeMedicineResponse>? = emptyList()
)