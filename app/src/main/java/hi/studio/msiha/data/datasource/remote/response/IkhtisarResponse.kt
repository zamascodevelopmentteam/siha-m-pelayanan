package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName


data class IkhtisarResponse(
    @SerializedName("data")
    val data: IkhtisarDataResponse
) : CommonResponse()

data class IkhtisarDataResponse(
    val visitDate:String,
    val regnas:String
)