package hi.studio.msiha.data.repositoryImpl

import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.constant.KEY_FCM_TOKEN
import hi.studio.msiha.data.datasource.remote.api.AuthAPI
import hi.studio.msiha.data.datasource.remote.request.RegisterRequest
import hi.studio.msiha.data.mapper.AuthMapper
import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.domain.repository.AuthRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class AuthRepositoryImpl(private val api: AuthAPI) : AuthRepository {
    override fun logout(callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.logoutAsync(Hawk.get(KEY_FCM_TOKEN?:"")).await()
                .parse(
                    { it ->
                        it.message?.let {
                            callback(Either.Left(it))
                        }
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun register(
        nik: String,
        name: String,
        password: String,
        regnas: String,
        role: String,
        avatar: String,
        callback: (Either<AuthResult, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.registerAsync(RegisterRequest(avatar, name, nik, password, regnas, role)).await()
                .parse(
                    {
                        callback(Either.Left(AuthMapper.toUser(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun login(
        nik: String,
        pass: String,
        callback: (Either<AuthResult, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.loginAsync(AuthMapper.toLoginRequest(nik, pass)).await()
                .parse(
                    {
                        callback(Either.Left(AuthMapper.toUser(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun forgotPassword(nik: String) {
    }
}