package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class DefaultResponse(
    @SerializedName("data")
    val data: Any?
) : CommonResponse()