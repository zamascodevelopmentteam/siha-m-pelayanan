package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.Order

data class OrderResponse(
    @SerializedName("data")
    val data: Order
) : CommonResponse()