package hi.studio.msiha.data.datasource.remote

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.data.datasource.remote.response.CommonResponse
import hi.studio.msiha.domain.model.MedicineRsp


data class MedicineResponse(
    @SerializedName("data")
    val data: MedicineRsp
) : CommonResponse()