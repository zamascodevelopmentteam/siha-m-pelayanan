package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.OtpRequest
import hi.studio.msiha.data.datasource.remote.request.ProfileRequest
import hi.studio.msiha.data.datasource.remote.response.OtpResponse
import hi.studio.msiha.data.datasource.remote.response.ProfileResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface UserAPI {
    @GET("private/users/{id}")
    fun getProfileAsync(@Path("id") id: Int): Deferred<Response<ProfileResponse>>

    @GET("private/users")
    fun getUserByNikAsync(@Query("nik") nik: String): Deferred<Response<ProfileResponse>>

    @GET("private/patient/{id}/otp")
    fun getOtpAsync(@Path("id") userId: Int): Deferred<Response<OtpResponse>>

    @POST("private/patient/{id}/otp")
    fun submitOtpAsync(
        @Body request: OtpRequest,
        @Path("id") otpId: Int
    ): Deferred<Response<OtpResponse>>

    @PUT("private/users/{id}")
    fun updateProfileAsync(
        @Path("id") id: Int,
        @Body request: ProfileRequest
    ): Deferred<Response<ProfileResponse>>
}