package hi.studio.msiha.data.repositoryImpl

import hi.studio.msiha.data.datasource.remote.api.PatientAPI
import hi.studio.msiha.data.datasource.remote.request.ODHARequest
import hi.studio.msiha.data.datasource.remote.request.RegisterPatientRequest
import hi.studio.msiha.data.datasource.remote.request.StatusPatientRequest
import hi.studio.msiha.data.mapper.PatientMapper
import hi.studio.msiha.data.mapper.RecipeMapper
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Recipe
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.domain.repository.PatientRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class PatientRepositoryImpl(private val api: PatientAPI) : PatientRepository {
    override fun getPatientById(nik: Int, callback: (Either<Patient?, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getPatientByIdAsync(nik).await()
                .parse(
                    {
                        callback(Either.Left(PatientMapper.toPatient(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getPatientByNik(nik: String, callback: (Either<Patient?, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getPatientByNikAsync(nik).await()
                .parse(
                    {
                        callback(Either.Left(PatientMapper.toPatient(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateToODHA(
        id: Int,
        request: ODHARequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateToODHA(id, request).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getRecipes(
        idPatient: Int,
        page: Int,
        limit: Int,
        callback: (Either<List<Recipe>, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getRecipesAsync(idPatient, page, limit).await()
                .parse(
                    {
                        callback(Either.Left(RecipeMapper.toRecipes(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun activatePatient(
        id: Int,
        password: String,
        active: Boolean,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.activatePatientAsync(id, StatusPatientRequest(active, password)).await()
                .parse(
                    { it ->
                        it.message?.let {
                            callback(Either.Left(it))
                        }
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updatePatient(
        id: Int,
        request: RegisterPatientRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updatePatientAsync(id, request).await()
                .parse(
                    { it ->
                        it.message?.let {
                            callback(Either.Left(it))
                        }
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getVisitHistory(
        idVisit: Int,
        type:String,
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getVisitHistoriesAsync(idVisit,type).await()
                .parse(
                    {
                        callback(Either.Left(PatientMapper.toVisitHistories(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createPatient(
        request: RegisterPatientRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createPatientAsync(request).await()
                .parse(
                    { it ->
                        it.message?.let {
                            callback(Either.Left(it))
                        }
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getPatients(
        page: Int,
        limit: Int,
        callback: (Either<List<Patient>, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getPatientAsync(page, limit).await()
                .parse(
                    {
                        callback(Either.Left(PatientMapper.toListPatient(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }
}