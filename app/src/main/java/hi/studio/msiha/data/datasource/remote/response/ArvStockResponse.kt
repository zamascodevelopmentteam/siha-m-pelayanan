package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.ArvStock


data class ArvStockResponse(
    @SerializedName("data")
    val data: List<ArvStock>
) : CommonResponse()