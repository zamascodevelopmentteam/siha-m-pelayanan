package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.ExamRequest
import hi.studio.msiha.data.datasource.remote.request.HIVExamRequest
import hi.studio.msiha.data.datasource.remote.request.IMSExamRequest
import hi.studio.msiha.data.datasource.remote.request.VLCExamRequest
import hi.studio.msiha.data.datasource.remote.response.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ExamAPI {
    @POST("private/examination")
    fun createExam(
        @Body request: ExamRequest
    ): Deferred<Response<DefaultResponse>>

    @GET("private/examination/{id}")
    fun getExam(
        @Path("id") id: Int
    ): Deferred<Response<ExamResponse>>

    @GET("private/examination/{id}/visit")
    fun getExamByVisit(
        @Path("id") idVisit: Int
    ): Deferred<Response<ExamResponse>>

    @PUT("private/examination/hiv/{id}")
    fun updateHIVExam(
        @Path("id") idVisit: Int,
        @Body request: HIVExamRequest
    ): Deferred<Response<DefaultResponse>>

    @PUT("private/examination/ims/{id}")
    fun updateIMSExam(
        @Path("id") idVisit: Int,
        @Body request: IMSExamRequest
    ): Deferred<Response<DefaultResponse>>

    @PUT("private/examination/vl/{id}")
    fun updateVLCExam(
        @Path("id") idVisit: Int,
        @Body request: VLCExamRequest
    ): Deferred<Response<DefaultResponse>>

    @POST("private/examination/hiv/{visitId}")
    fun createHIVExam(
        @Path("visitId") visitId:Int
    ): Deferred<Response<DefaultResponse>>

    @POST("private/examination/ims/{visitId}")
    fun createIMSExam(
        @Path("visitId") visitId:Int
    ): Deferred<Response<DefaultResponse>>

    @POST("private/examination/vl/{visitId}")
    fun createVLCExam(
        @Path("visitId") visitId:Int
    ): Deferred<Response<DefaultResponse>>

    @GET("private/examination/hiv/patient/{patientId}")
    fun getHivExamHistory(
        @Path("patientId") visitId: Int
    ):Deferred<Response<HivTestHistoryResponse>>

    @GET("private/examination/ims/patient/{patientId}")
    fun getImsExamHistory(
        @Path("patientId") visitId: Int
    ):Deferred<Response<ImsTestHistoryResponse>>

    @GET("private/examination/vl/patient/{patientId}")
    fun getVlExamHistory(
        @Path("patientId") visitId: Int
    ):Deferred<Response<VlTestHistoryResponse>>
}