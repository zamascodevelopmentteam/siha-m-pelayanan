package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.response.*
import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.Statistic
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.domain.model.User

object HospitalMapper {
    fun toStatistic(response: StatisticDataResponse?): Statistic {
        response?.apply {
            return Statistic(
                visitEstimation ?: 0,
                visitThisDay ?: 0,
                visitThisMonth ?: 0,
                examThisDay ?: 0
            )
        }
        return Statistic()
    }

    fun toTodayVisitList(response: TodayVisitResponse): List<TodayVisit> = response.data?.map {
        toTodayVisit(it) ?: TodayVisit()
    } ?: listOf()

    fun toTodayVisit(response: TodayVisitDataResponse?): TodayVisit? {
        response?.apply {
            return TodayVisit(
                ordinal ?: "",
                UserMapper.toUser(user) ?: User()
            )
        }
        return null
    }

    fun toHospital(response: HospitalResponse?): Hospital {
        response?.apply {
            if (data != null) {
                return Hospital(
                    data.id,
                    data.name,
                    data.createdAt,
                    data.updatedAt
                )
            }
        }
        return Hospital(0, "", "", "")
    }

    fun toHospitals(response: HospitalsResponse): List<Hospital> = response.data?.map {
        Hospital(it.id ?: 0, it.name ?: "", "", "")
    } ?: emptyList()
}