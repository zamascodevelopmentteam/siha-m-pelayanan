package hi.studio.msiha.data.mapper

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.request.ExamRequest
import hi.studio.msiha.data.datasource.remote.response.ExamResponse
import hi.studio.msiha.domain.model.Exam
import hi.studio.msiha.ui.home.patient.detail.ExamFragment
import hi.studio.msiha.ui.home.patient.detail.exam.HIVExamFragment
import hi.studio.msiha.ui.home.patient.detail.exam.IMSExamFragment
import hi.studio.msiha.ui.home.patient.detail.exam.VLCExamFragment

object ExamMapper {
    fun toExamRequest(bundle: Bundle): ExamRequest {
        bundle.apply {
            return ExamRequest(
                type = this[ExamFragment.KEY_EXAM_TYPE] as? String,
                userId = this[ExamFragment.KEY_ID] as? Int,
                hospitalId = this[ExamFragment.KEY_HOSPITAL_ID] as? Int,
                examOrdinal = this[ExamFragment.KEY_ORDINAL] as? Int,
                examDate = this[ExamFragment.KEY_EXAM_CHECK] as? Long,
                examRequestDate = this[ExamFragment.KEY_EXAM_REQUEST] as? Long,
                receiveResultDate = this[ExamFragment.KEY_EXAM_RESULT] as? Long,
                hivExaminationMethod = this[HIVExamFragment.KEY_TEST_METHOD] as? String,
                populationGroup = this[HIVExamFragment.KEY_POPULATION] as? String,
                lsmPenjangkau = this[HIVExamFragment.KEY_LSM] as? String,
                reasonCheck = this[HIVExamFragment.KEY_REASON] as? String,
                reagen1 = this[HIVExamFragment.KEY_REAGEN_R1] as? Int,
                reagen1Result = this[HIVExamFragment.KEY_REAGEN_R1_RESULT] as? String,
                reagen2 = this[HIVExamFragment.KEY_REAGEN_R2] as? Int,
                reagen2Result = this[HIVExamFragment.KEY_REAGEN_R2_RESULT] as? String,
                reagen3 = this[HIVExamFragment.KEY_REAGEN_R3] as? Int,
                reagen3Result = this[HIVExamFragment.KEY_REAGEN_R3_RESULT] as? String,
                reagenNat = this[HIVExamFragment.KEY_REAGEN_NAT] as? Int,
                reagenNatResult = this[HIVExamFragment.KEY_REAGEN_NAT_RESULT] as? String,
                reagenPcr = this[HIVExamFragment.KEY_REAGEN_PCR] as? Int,
                reagenPcrResult = this[HIVExamFragment.KEY_REAGEN_PCR_RESULT] as? String,
                reagenElisa = this[HIVExamFragment.KEY_REAGEN_ELISA] as? Int,
                reagenElisaResult = this[HIVExamFragment.KEY_REAGEN_ELISA_RESULT] as? String,
                reagenBlot = this[HIVExamFragment.KEY_REAGEN_WESTERN] as? Int,
                reagenBlotResult = this[HIVExamFragment.KEY_REAGEN_WESTERN_RESULT] as? String,
                hivResult = this[HIVExamFragment.KEY_TEST_RESULT] as? Boolean,
                conclusionHivResult = this[HIVExamFragment.KEY_TEST_RESULT_CONCLUSION] as? String,
                syphilisResult = this[IMSExamFragment.KEY_SYPHILIS_TEST_RESULT] as? String,
                imsResult = this[IMSExamFragment.KEY_IMS_TEST_RESULT] as? String,
                testIMS = this[IMSExamFragment.KEY_IS_IMS_TEST] as? Boolean,
                testSyphilis = this[IMSExamFragment.KEY_IS_SYPHILIS_TEST] as? Boolean,
                rprIms = this[IMSExamFragment.KEY_REAGEN_IMS] as? Int,
                vlTestType = this[VLCExamFragment.KEY_TEST_TYPE] as? String,
                vlReagen = this[VLCExamFragment.KEY_REAGEN] as? String,
                vlReagenAmount = this[VLCExamFragment.KEY_REAGEN_AMOUNT] as? Int,
                vlTestResult = this[VLCExamFragment.KEY_TEST_RESULT] as? Int
            )
        }
    }

    fun toExam(response: ExamResponse.ExamDataResponse?): Exam? {
        response?.apply {
            return Exam(
                id,
                examOrdinal,
                examRequestDate,
                examDate,
                receiveResultDate,
                reasonTestHiv,
                hivTestType,
                hivExaminationMethod,
                reagen1,
                nameReagen1,
                resultReagen1,
                reagen2,
                nameReagen2,
                resultReagen2,
                reagen3,
                nameReagen3,
                resultReagen3,
                nameReagenNat,
                resultTestNat,
                nameReagenPcr,
                resultTestPcr,
                nameReagenElisa,
                resultTestElisa,
                nameReagenWesternBlot,
                resultTestWesternBlot,
                conclusionResultTestHiv,
                hivResult,
                sifilisResult,
                testLab,
                resultTestLab,
                rprIms,
                imsResult,
                createdAt,
                updatedAt,
                visitId,
                userId,
                hospitalId,
                vlTestType,
                vlReagen,
                vlReagenAmount,
                vlTestResult
            )
        }
        return null
    }
}