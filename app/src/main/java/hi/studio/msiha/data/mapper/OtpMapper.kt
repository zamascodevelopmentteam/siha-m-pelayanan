package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.response.OtpDataResponse
import hi.studio.msiha.domain.model.Otp

object OtpMapper {
    fun toOtp(response: OtpDataResponse?): Otp {
        response?.apply {
            return Otp(
                otpId ?: 0,
                valid ?: false
            )
        }
        return Otp()
    }
}