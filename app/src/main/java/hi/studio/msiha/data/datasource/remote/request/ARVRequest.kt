package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class ARVRequest(
    @SerializedName("visitId")
    val visitId: Int? = null,
    @SerializedName("dateGiven")
    val dateGiven: String? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("medicines")
    val arvs: List<Arv?>? = null
) {
    data class Arv(
        @SerializedName("medicine_id")
        val id: Int? = null,
        @SerializedName("jumlah_hari")
        val days: Int? = null,
        @SerializedName("stock_qty")
        val qty: Int? = null
    )
}