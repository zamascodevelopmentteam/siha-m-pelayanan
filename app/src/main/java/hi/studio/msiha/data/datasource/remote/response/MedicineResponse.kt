package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.MedicineStocks

data class MedicineResponse(
    @SerializedName("codeName")
    val codeName: String? = "", // null
    @SerializedName("id")
    val id: Int = 0, // 1
    @SerializedName("ingredients")
    val ingredients: String? = "", // null
    @SerializedName("medicineType")
    val medicineType: String? = "", // null
    @SerializedName("name")
    val name: String? = "", // null
    @SerializedName("picture")
    val picture: String? = "", // null
    @SerializedName("type")
    val type: String? = "", // null
    @SerializedName("usage")
    val usage: String? = "", // null
    @SerializedName("medicineStock")
    val medicineStock: MedicineStocks?
)