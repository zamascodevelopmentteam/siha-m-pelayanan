package hi.studio.msiha.data.repositoryImpl

import hi.studio.msiha.data.datasource.remote.api.MedicineAPI
import hi.studio.msiha.data.datasource.remote.api.OrderAPI
import hi.studio.msiha.data.datasource.remote.request.FarmasiOrderRequest
import hi.studio.msiha.data.mapper.MedicineMapper
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineRsp
import hi.studio.msiha.domain.model.Order
import hi.studio.msiha.domain.repository.PharmacistRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class PharmacistRepositoryImpl(private val api: MedicineAPI, private val orderAPI: OrderAPI) :
    PharmacistRepository {
    override fun getMedicineDetail(id: Int, callback: (Either<MedicineRsp, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getMedicineDetail(id).await()
                .parse(
                    {
                        callback(Either.Left(MedicineMapper.toMedicine(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun confirmMedicine(id: Int, callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.confirmMedicine(id).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createOrder(
        request: FarmasiOrderRequest,
        callback: (Either<Order, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            orderAPI.createOrder(request).await()
                .parse(
                    {
                        callback(Either.Left(MedicineMapper.toOrder(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateMedicine(
        request: Medicine,
        callback: (Either<MedicineRsp, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateMedicine(request.id?:0, request).await()
                .parse(
                    {
                        callback(Either.Left(MedicineMapper.toMedicine(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getListMedicine(
        type: String,
        page: Int,
        limit: Int,
        callback: (Either<List<MedicineRsp>, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getListMedicine(type, limit, page).await()
                .parse(
                    {
                        callback(Either.Left(MedicineMapper.toMedicineList(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }


    override fun createMedicine(
        request: Medicine,
        callback: (Either<MedicineRsp, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createMedicine(request).await()
                .parse(
                    {
                        callback(Either.Left(MedicineMapper.toMedicine(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getListReagen(
        page: Int,
        limit: Int,
        callback: (Either<List<Order>, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            //api.getPharmacistDashboardList(limit,page,true,"REAGEN").await()
            api.getListOrder(limit, page, true, "REAGEN").await()
                .parse(
                    {
                        callback(Either.Left(MedicineMapper.toListOrder(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

}