package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName


data class ReagenResponse(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("code_name")
    val codeName: String? = null,
    @SerializedName("medicine_type")
    val medicineType: String? = null,
    @SerializedName("stock_unit_type")
    val stockUnitType: String? = null,
    @SerializedName("package_unit_type")
    val packageUnitType: String? = null,
    @SerializedName("stock_qty")
    val sum: String? = null
)
