package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class OtpRequest(
    @SerializedName("otpCode")
    val code: String
)