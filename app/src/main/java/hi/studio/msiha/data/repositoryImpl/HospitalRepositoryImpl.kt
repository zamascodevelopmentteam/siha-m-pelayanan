package hi.studio.msiha.data.repositoryImpl

import hi.studio.msiha.data.datasource.remote.api.HospitalAPI
import hi.studio.msiha.data.mapper.HospitalMapper
import hi.studio.msiha.data.mapper.PatientMapper
import hi.studio.msiha.domain.model.Hospital
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.Statistic
import hi.studio.msiha.domain.model.TodayVisit
import hi.studio.msiha.domain.repository.HospitalRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class HospitalRepositoryImpl(private val api: HospitalAPI) : HospitalRepository {
    override fun getHospitals(callback: (Either<List<Hospital>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getHospitals().await()
                .parse(
                    {
                        callback(Either.Left(HospitalMapper.toHospitals(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getHospitalDetail(id: Int, callback: (Either<Hospital, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getHospitalById(id).await()
                .parse(
                    {
                        callback(Either.Left(HospitalMapper.toHospital(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getVisits(
        page: Int,
        limit: Int,
        callback: (Either<List<Patient>, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getVisitListAsync(page, limit).await()
                .parse(
                    {
                        callback(Either.Left(PatientMapper.toListPatient(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getStatistic(callback: (Either<Statistic, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getStatisticAsync().await()
                .parse(
                    {
                        callback(Either.Left(HospitalMapper.toStatistic(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getTodayVisit(callback: (Either<List<TodayVisit>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getTodayVisitAsync().await()
                .parse(
                    {
                        callback(Either.Left(HospitalMapper.toTodayVisitList(it)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }


}