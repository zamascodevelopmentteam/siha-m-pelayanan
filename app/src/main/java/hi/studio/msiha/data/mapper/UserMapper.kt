package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.request.ProfileRequest
import hi.studio.msiha.data.datasource.remote.response.UserResponse
import hi.studio.msiha.domain.model.User

object UserMapper {
    fun toUser(response: UserResponse?): User? {
        response?.apply {
            return User(
                id,
                nik,
                isActive,
                name,
                role,
                logisticrole,
                avatar,
                email,
                phone,
                createdBy,
                updatedBy,
                createdAt,
                updatedAt,
                provinceId,
                upkId,
                sudinKabKotumId
            )
        }
        return null
    }

    fun toListUser(data: List<UserResponse>?): List<User> = data?.map {
        User(
            it.id,
            it.nik,
            it.isActive,
            it.name,
            it.role,
            it.logisticrole,
            it.avatar,
            it.email,
            it.phone,
            it.createdBy,
            it.updatedBy,
            it.createdAt,
            it.updatedAt,
            it.provinceId,
            it.upkId,
            it.sudinKabKotumId
        )
    } ?: listOf()

    fun toUserProfileRequest(user: User): ProfileRequest {
        user.apply {
            return ProfileRequest(
                name,
                phone
            )
        }
    }
}