package hi.studio.msiha.data.datasource.remote

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import hi.studio.msiha.BuildConfig
import hi.studio.msiha.data.constant.KEY_HEADER
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

object NetworkManager {

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        val logger = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.d(message)
            }
        })
        logger.level = HttpLoggingInterceptor.Level.BODY
        logger.redactHeader(KEY_HEADER)
        return logger
    }

    fun provideOkHttp(chuckInterceptor: ChuckInterceptor? = null): OkHttpClient? {
        val client = OkHttpClient.Builder()
        client.addInterceptor(getLoggingInterceptor())
        client.addInterceptor(NetworkInterceptor())
        client.addInterceptor(TokenInterceptor())
//        chuckInterceptor?.let {
//            client.addInterceptor(it)
//        }
        client.authenticator(TokenAuthenticator())
        client.connectTimeout(30, TimeUnit.SECONDS)
        client.readTimeout(30, TimeUnit.SECONDS)
        client.writeTimeout(30, TimeUnit.SECONDS)
        return client.build()
    }

    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit? {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(okHttpClient)
            .build()
    }
}