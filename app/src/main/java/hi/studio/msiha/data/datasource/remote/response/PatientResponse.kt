package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.User

data class PatientResponse(
    @SerializedName("data")
    val data: Data? = null
) : CommonResponse() {
    data class Data(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("nik")
        val nik: String? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("addressKTP")
        val addressKTP: String? = null,
        @SerializedName("addressDomicile")
        val addressDomicile: String? = null,
        @SerializedName("dateBirth")
        val dateBirth: String? = null,
        @SerializedName("gender")
        val gender: String? = null,
        @SerializedName("phone")
        val phone: String? = null,
        @SerializedName("statusPatient")
        val statusPatient: String? = null,
        @SerializedName("weight")
        val weight: Int? = null,
        @SerializedName("height")
        val height: Int? = null,
        @SerializedName("namaPmo")
        val namaPmo: String? = null,
        @SerializedName("hubunganPmo")
        val hubunganPmo: String? = null,
        @SerializedName("noHpPmo")
        val noHpPmo: String? = null,
        @SerializedName("createdBy")
        val createdBy: Int? = null,
        @SerializedName("createdAt")
        val createdAt: String? = null,
        @SerializedName("updatedAt")
        val updatedAt: String? = null,
        @SerializedName("UserId")
        val userId: Int? = null,
        @SerializedName("upkId")
        val upkId: Int? = null,
        @SerializedName("email")
        val email: String? = null,
        @SerializedName("user")
        val user: User?=null
    )
}