package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.VisitHistory


data class CreateVisitResponse(
    @SerializedName("data")
    val data: VisitHistory
) : CommonResponse()