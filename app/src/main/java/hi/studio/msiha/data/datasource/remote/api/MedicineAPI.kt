package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.MedicineResponse
import hi.studio.msiha.data.datasource.remote.response.DefaultResponse
import hi.studio.msiha.data.datasource.remote.response.MedicineListResponse
import hi.studio.msiha.data.datasource.remote.response.OrderListResponse
import hi.studio.msiha.domain.model.Medicine
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface MedicineAPI{
    @GET("private/medicine/order")
    fun getListOrder(
        @Query("limit") limit:Int,
        @Query("page") page:Int,
        @Query("confirm") confirm:Boolean,
        @Query("medicineType") type:String
    ): Deferred<Response<OrderListResponse>>

    @GET("private/orders")
    fun getPharmacistDashboardList(
        @Query("limit") limit:Int,
        @Query("page") page:Int,
        @Query("confirm") confirm:Boolean,
        @Query("medicineType") type:String
    ): Deferred<Response<OrderListResponse>>

    @GET("private/medicines/{type}")
    fun getListMedicine(
        @Path("type") type:String,
        @Query("limit") limit:Int,
        @Query("page") page:Int
    ):Deferred<Response<MedicineListResponse>>

    @POST("private/medicine/create")
    fun createMedicine(
        @Body data: Medicine
    ):Deferred<Response<MedicineResponse>>

    @PUT("private/medicine/{id}")
    fun updateMedicine(
        @Path("id") id:Int,
        @Body data:Medicine
    ):Deferred<Response<MedicineResponse>>

    @PUT("private/medicine/confirm/{id}")
    fun confirmMedicine(
        @Path("id") id: Int
    ): Deferred<Response<DefaultResponse>>

    @GET("private/medicine/detail/{id}")
    fun getMedicineDetail(
        @Path("id") id: Int
    ): Deferred<Response<MedicineResponse>>
}