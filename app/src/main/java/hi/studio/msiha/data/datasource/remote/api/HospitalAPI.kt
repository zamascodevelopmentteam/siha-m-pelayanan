package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.response.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface HospitalAPI {
    @GET("private/hospitals/1/statistic")
    fun getStatisticAsync(): Deferred<Response<StatisticResponse>>

    @GET("private/hospitals/1/today-visits")
    fun getTodayVisitAsync(): Deferred<Response<TodayVisitResponse>>

    @GET("private/visits")
    fun getVisitListAsync(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Deferred<Response<PatientsResponse>>

    @GET("private/hospitals/{id}")
    fun getHospitalById(
        @Path("id") id: Int
    ): Deferred<Response<HospitalResponse>>

    @GET("private/hospitals")
    fun getHospitals(): Deferred<Response<HospitalsResponse>>
}