package hi.studio.msiha.data.datasource.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.NamaReagenData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VLCExamRequest(
    @SerializedName("ordinal")
    val ordinal: Int? = null,
    @SerializedName("testVlCd4Type")
    val testType: String? = null,
    @SerializedName("namaReagen")
    val nameReagen1: Int? = null,
    @SerializedName("qtyReagen")
    val qty: Int? = -1,
    @SerializedName("hasilTestVlCd4")
    val resultTest: String? = null,
    @SerializedName("visitId")
    val visitId: Int? = null,
    val namaReagenData: NamaReagenData?=null
) : Parcelable