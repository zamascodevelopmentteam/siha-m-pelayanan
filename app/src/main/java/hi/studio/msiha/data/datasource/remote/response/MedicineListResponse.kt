package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.Medicine
import hi.studio.msiha.domain.model.MedicineRsp

data class MedicineListResponse(
    @SerializedName("data")
    val data: ArrayList<MedicineRsp>
) : CommonResponse()