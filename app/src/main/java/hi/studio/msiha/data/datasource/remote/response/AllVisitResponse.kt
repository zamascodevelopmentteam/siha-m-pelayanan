package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory

data class AllVisitResponse(
    @SerializedName("data")
    val data: List<VisitHistory>
) : CommonResponse()
