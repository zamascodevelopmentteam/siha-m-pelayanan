package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("nik")
    val nik: String,
    @SerializedName("password")
    val password: String,
    val fcmToken:String
)