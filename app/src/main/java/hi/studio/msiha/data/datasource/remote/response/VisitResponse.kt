package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class VisitResponse(
    @SerializedName("data")
    val data: Data
) : CommonResponse() {
    data class Data(
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("ordinal")
        val ordinal: Int? = null,
        @SerializedName("visitType")
        val visitType: String? = null,
        @SerializedName("visitDate")
        val visitDate: String? = null,
        @SerializedName("createdBy")
        val createdBy: Int? = null,
        @SerializedName("createdAt")
        val createdAt: String? = null,
        @SerializedName("updatedAt")
        val updatedAt: String? = null,
        @SerializedName("UserId")
        val userId: Int? = null,
        @SerializedName("upkId")
        val upkId: Int? = null,
        @SerializedName("patientId")
        val patientId: Int? = null
    )
}
