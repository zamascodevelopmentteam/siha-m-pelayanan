package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class VlTestHistoryResponse(
    @SerializedName("data")
    val data: ArrayList<VlTestHistory>
) : CommonResponse()