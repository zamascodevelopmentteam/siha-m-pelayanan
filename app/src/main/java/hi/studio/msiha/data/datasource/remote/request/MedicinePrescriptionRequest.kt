package hi.studio.msiha.data.datasource.remote.request

import hi.studio.msiha.domain.model.ArvStock

class MedicinePrescriptionRequest (
    val notes:String?,
    val statusPaduan:String,
    val medicines:List<ArvStock>
)