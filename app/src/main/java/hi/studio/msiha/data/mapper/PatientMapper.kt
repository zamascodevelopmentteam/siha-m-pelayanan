package hi.studio.msiha.data.mapper

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.request.RegisterPatientRequest
import hi.studio.msiha.data.datasource.remote.response.PatientResponse
import hi.studio.msiha.data.datasource.remote.response.VisitHistoriesResponse
import hi.studio.msiha.data.datasource.remote.response.VisitHistoryResponse
import hi.studio.msiha.domain.model.Patient
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.ui.home.patient.add.AddPatientFragment
import hi.studio.msiha.ui.home.patient.add.form.PersonalDataFragment
import hi.studio.msiha.utils.extention.toForm
import timber.log.Timber

object PatientMapper {
    fun toRequest(bundle: Bundle): RegisterPatientRequest {
        bundle.apply {
            val nik = getString(AddPatientFragment.KEY_PERSONAL_NIK).toForm()
            val name = getString(AddPatientFragment.KEY_PERSONAL_NAME)
            val address = getString(AddPatientFragment.KEY_PERSONAL_ADDRESS)
            val dob = getString(PersonalDataFragment.KEY_PERSONAL_DOB).toForm()
            val domicile = getString(AddPatientFragment.KEY_PERSONAL_DOMICILE)
            val gender = getString(AddPatientFragment.KEY_PERSONAL_GENDER).toForm()
            val phone = getString(AddPatientFragment.KEY_PERSONAL_PHONE)
            val email = getString(AddPatientFragment.KEY_PERSONAL_EMAIL)
            val patientType = getString(AddPatientFragment.KEY_PERSONAL_TYPE)
            val pmoName = getString(AddPatientFragment.KEY_PMO_NAME)
            val pmoEmail = getString(AddPatientFragment.KEY_PMO_EMAIL)
            val pmoRelation = getString(AddPatientFragment.KEY_PMO_RELATION)
            val pmoPhone = getString(AddPatientFragment.KEY_PMO_PHONE)
            return RegisterPatientRequest(
                gender = gender,
                nik = nik,
                name = name,
                dateBirth = dob,
                phone = phone,
                addressDomicile = domicile,
                addressKTP = address,
                statusPatient = patientType,
                namaPmo = pmoName,
                noHpPmo = pmoPhone,
                hubunganPmo = pmoRelation,
                email=email
            )
        }
    }

    fun toRequest(patient: Patient): RegisterPatientRequest {
        patient.apply {
            return RegisterPatientRequest(
                gender = gender,
                nik = nik,
                name = name,
                dateBirth = dateBirth,
                phone = phone,
                addressDomicile = addressDomicile,
                addressKTP = addressKTP,
                statusPatient = statusPatient,
                namaPmo = namaPmo,
                noHpPmo = noHpPmo,
                hubunganPmo = hubunganPmo,
                email = email
            )
        }
    }

    fun toPatient(response: PatientResponse.Data?): Patient? {
        response?.apply {
            Timber.d(response.toString())
            return Patient(
                id,
                nik,
                name,
                addressKTP,
                addressDomicile,
                dateBirth,
                gender,
                phone,
                statusPatient,
                weight,
                height,
                namaPmo,
                hubunganPmo,
                noHpPmo,
                createdBy,
                createdAt,
                updatedAt,
                userId,
                upkId,
                email,
                user
            )
        }
        return null
    }

    fun toListPatient(response: List<PatientResponse.Data>?): List<Patient> = response?.map {
        Patient(
            it.id,
            it.nik,
            it.name,
            it.addressKTP,
            it.addressDomicile,
            it.dateBirth,
            it.gender,
            it.phone,
            it.statusPatient,
            it.weight,
            it.height,
            it.namaPmo,
            it.hubunganPmo,
            it.noHpPmo,
            it.createdBy,
            it.createdAt,
            it.updatedAt,
            it.userId,
            it.upkId,
            it.email,
            it.user
        )
    } ?: run {
        listOf<Patient>()
    }

    fun toVisitHistories(response: VisitHistoriesResponse?): List<VisitHistory> =
        response?.data?.map {
            toVisitHistory(it)
        } ?: listOf()

    fun toVisitHistory(response: VisitHistoryResponse?): VisitHistory {
        response?.apply {
            return VisitHistory(
                id,
                ordinal,
                visitType,
                visitDate,
                checkOutDate,
                createdBy,
                updatedBy,
                createdAt,
                updatedAt,
                deletedAt,
                userId,
                upkId,
                patientId,
                prescription = prescription
            )
        }
        return VisitHistory()
    }
}