package hi.studio.msiha.data.datasource.remote.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class HivTestHistory (
val id:Int?,
val ordinal:Int?,
val visitType:String?,
val visitDate:String?,
val checkoutDate:String,
val userRrId:Int?,
val createdBy:Int?,
val updatedBy:Int,
val createdAt:String?,
val updatedAt:String?,
val deleteAt:String?,
val upkId:Int?,
val patientId:Int?,
val testHiv: TestHiv?
) : Parcelable