package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.ARVRequest
import hi.studio.msiha.data.datasource.remote.request.MedicinePrescriptionRequest
import hi.studio.msiha.data.datasource.remote.request.RecipeRequest
import hi.studio.msiha.data.datasource.remote.response.*
import hi.studio.msiha.domain.model.ArvStock
import hi.studio.msiha.domain.model.Prescription
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface RecipeAPI {
    @POST("private/recipes")
    fun createRecipe(
        @Body recipe: RecipeRequest
    ): Deferred<Response<DefaultResponse>>

    @GET("private/recipes/{id}")
    fun getRecipeDetail(
        @Path("id") recipeId: Int
    ): Deferred<Response<RecipeDetailResponse>>

    @PUT("private/recipes/confirm/{id}")
    fun confirmRecipe(
        @Path("id") id: Int
    ): Deferred<Response<DefaultResponse>>

    @POST("private/recipes")
    fun createArvRecipe(
        @Body request: ARVRequest
    ): Deferred<Response<DefaultResponse>>

    @PUT("private/visit/{visitId}/prescription")
    fun createPrescription(
        @Path("visitId") visitId:Int,
        @Body request:MedicinePrescriptionRequest
    ):Deferred<Response<DefaultResponse>>

    @POST("private/visit/{visitId}/give-prescription")
    fun giveMedicine(
        @Path("visitId") visitId: Int
    ):Deferred<Response<DefaultResponse>>

    @GET("private/medicines/stock/NON_ARV")
    fun getReagenEmpty(
        //@Path("type") type: String,
        //@Path("exam") exam: String
    ): Deferred<Response<ReagensResponse>>

    @GET("private/medicines/stock/NON_ARV")
    fun getReagen(
        //@Path("type") type: String,
        @QueryMap exam:Map<String,String>
    ): Deferred<Response<ReagensResponse>>

    @GET("private/medicines/stock/ARV")
    fun getARVMedicineList():Deferred<Response<ArvStockResponse>>

    @GET("private/medicines/stock/NON_ARV")
    fun getReagenMedicineList():Deferred<Response<ArvStockResponse>>

    @GET("private/patient/{patientId}/last-given-prescription")
    fun getLastGivenPrescription(
        @Path("patientId") patientId:Int
    ):Deferred<Response<PrescriptionResponse>>
}