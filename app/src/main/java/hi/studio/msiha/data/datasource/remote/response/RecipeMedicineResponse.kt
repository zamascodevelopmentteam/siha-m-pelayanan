package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class RecipeMedicineResponse(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("rule")
    val rule: String? = "",
    @SerializedName("amount")
    val amount: Int = 0,
    @SerializedName("medicineId")
    val medicineId: Int = 0,
    @SerializedName("recipeId")
    val recipeId: Int = 0,
    @SerializedName("medicine")
    val medicine: MedicineResponse? = null
)