package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class OtpResponse(
    @SerializedName("data")
    val data: OtpDataResponse?
) : CommonResponse()

data class OtpDataResponse(
    @SerializedName("otpId")
    val otpId: Int? = 0,
    @SerializedName("valid")
    val valid: Boolean? = false
)