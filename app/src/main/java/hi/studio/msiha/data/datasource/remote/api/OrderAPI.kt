package hi.studio.msiha.data.datasource.remote.api

import hi.studio.msiha.data.datasource.remote.request.FarmasiOrderRequest
import hi.studio.msiha.data.datasource.remote.response.OrderResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface OrderAPI {


    @POST("private/orders")
    fun createOrder(
        @Body orderRequest: FarmasiOrderRequest
    ): Deferred<Response<OrderResponse>>
}
