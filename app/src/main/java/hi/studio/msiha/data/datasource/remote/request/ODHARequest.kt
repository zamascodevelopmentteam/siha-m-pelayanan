package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName


data class ODHARequest(
    @SerializedName("patientType")
    val patientType: String? = null,
    @SerializedName("email")
    val pmoEmail: String? = null,
    @SerializedName("phone")
    val phone: String? = null,
    @SerializedName("namaPmo")
    val pmoName: String? = null,
    @SerializedName("hubunganPmo")
    val pmoRelationship: String? = null,
    @SerializedName("noHpPmo")
    val pmoPhone: String? =""
)