package hi.studio.msiha.data.datasource.remote.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.NamaReagenData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HIVExamRequest(
    @SerializedName("ordinal")
    val ordinal: Int? = null,
    @SerializedName("visitId")
    val visitId: Int? = null,
    @SerializedName("kelompokPopulasi")
    val kelompokPopulasi: String? = null,
    @SerializedName("lsmPenjangkau")
    val lsmPenjangkau: String? = null,
    @SerializedName("alasanTest")
    val aasanTest: String? = null,
    @SerializedName("tanggalTest")
    val tanggalTest: String? = null,
    @SerializedName("jenisTest")
    val jenisTest: String? = null,
    @SerializedName("namaReagenR1")
    val namaReagen1: Int? = null,
    @SerializedName("hasilTestR1")
    val hasilTest1: String? = null,
    @SerializedName("qtyReagenR1")
    val qtyReagenR1: Int? = null,
    @SerializedName("namaReagenR2")
    val namaReagen2: Int? = null,
    @SerializedName("hasilTestR2")
    val hasilTest2: String? = null,
    @SerializedName("qtyReagenR2")
    val qtyReagenR2: Int? = null,
    @SerializedName("namaReagenR3")
    val namaReagen3: Int? = null,
    @SerializedName("hasilTestR3")
    val hasilTest3: String? = null,
    @SerializedName("namaReagenDna")
    val namaReagenDna: Int? = null,
    @SerializedName("qtyReagenR3")
    val qtyReagenR3: Int? = null,
    @SerializedName("hasilTestDna")
    val hasilTestDna: String? = null,
    @SerializedName("qtyReagenDna")
    val qtyReagenDna: Int? = null,
    @SerializedName("namaReagenRna")
    val namaReagenRna: Int? = null,
    @SerializedName("hasilTestRna")
    val hasilTestRna: String? = null,
    @SerializedName("qtyReagenRna")
    val qtyReagenRna: Int? = null,
    @SerializedName("namaReagenElisa")
    val namaReagenElisa: Int? = null,
    @SerializedName("hasilTestElisa")
    val hasilTestElisa: String? = null,
    @SerializedName("qtyReagenElisa")
    val qtyReagenElisa: Int? = null,
    @SerializedName("rujukanLabLanjut")
    val rujukanLabLanjut: String? = null,
    @SerializedName("namaReagenWb")
    val namaReagenWb: Int? = null,
    @SerializedName("qtyReagenWb")
    val qtyReagenWb: Int? = null,
    @SerializedName("hasilTestWb")
    val hasilTestWb: String? = null,
    @SerializedName("kesimpulanHiv")
    val kesimpulanHiv: String? = null,
    val namaReagenR1Data:NamaReagenData?=null,
    val namaReagenR2Data:NamaReagenData?=null,
    val namaReagenR3Data:NamaReagenData?=null,
    val namaReagenDnaData:NamaReagenData?=null,
    val namaReagenRnaData:NamaReagenData?=null,
    val namaReagenElisaData:NamaReagenData?=null,
    val namaReagenWbData:NamaReagenData?=null,
    val namaReagenNatData:NamaReagenData?=null,
    val namaReagenNat:Int?=null,
    @SerializedName("qtyReagenNat")
    val qtyReagenNat: Int? = null,
    val hasilTestNat: String? = null

) : Parcelable