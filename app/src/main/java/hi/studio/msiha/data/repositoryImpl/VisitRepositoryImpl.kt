package hi.studio.msiha.data.repositoryImpl

import android.os.Bundle
import hi.studio.msiha.data.datasource.remote.api.VisitAPI
import hi.studio.msiha.data.datasource.remote.request.*
import hi.studio.msiha.data.datasource.remote.response.*
import hi.studio.msiha.data.mapper.IkhtisarMapper
import hi.studio.msiha.data.mapper.VisitMapper
import hi.studio.msiha.domain.model.Visit
import hi.studio.msiha.domain.model.VisitData
import hi.studio.msiha.domain.model.VisitHistory
import hi.studio.msiha.domain.repository.VisitRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class VisitRepositoryImpl(private val api: VisitAPI) : VisitRepository {


    override fun createVisitODHA(
        ordinal: Int?,
        patientId: Int?,
        date: String?,
        treatementStartDate: String?,
        noRegNas: String?,
        tglKonfirmasiHivPos: String?,
        tglKunjungan: String?,
        tglRujukMasuk: String?,
        kelompokPopulasi: String?,
        statusTb: String?,
        tglPengobatanTb: String?,
        statusFungsional: String?,
        stadiumKlinis: String?,
        ppk: Boolean?,
        tglPemberianPpk: String?,
        statusTbTpt: String?,
        tglPemberianObat: String?,
        obatArv1: Int?,
        jmlHrObatArv1: Int?,
        obatArv2: Int?,
        jmlHrObatArv2: Int?,
        obatArv3: Int?,
        jmlHrObatArv3: Int?,
        lsmPenjangkauId: Int?,
        namaReagen: Int?,
        jmlReagen: Int?,
        hasilLab: Int?,
        hasilLabLain: Int?,
        notifikasiPasangan: String?,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createVisitODHAAsync(
                ODHAVisitRequest(
                    ordinal,
                    patientId,
                    date,
                    treatementStartDate,
                    noRegNas,
                    tglKonfirmasiHivPos,
                    tglKunjungan,
                    tglRujukMasuk,
                    kelompokPopulasi,
                    statusTb,
                    tglPengobatanTb,
                    statusFungsional,
                    stadiumKlinis,
                    ppk,
                    tglPemberianPpk,
                    statusTbTpt,
                    tglPemberianObat,
                    obatArv1,
                    jmlHrObatArv1,
                    0,
                    obatArv2,
                    jmlHrObatArv2,
                    0,
                    obatArv3,
                    jmlHrObatArv3,
                    0,
                    lsmPenjangkauId,
                    namaReagen,
                    jmlReagen,
                    hasilLab,
                    hasilLabLain,
                    notifikasiPasangan
                )
            )
                .await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getIkhtisar(idVisit: Int, callback: (Either<IkhtisarRequest, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getIkhtisar(idVisit).await()
                .parse(
                    {
                        callback(Either.Left(IkhtisarMapper.toIkhtisar(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateIkhtisar(
        ordinal: Int,
        visit: IkhtisarRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateIkhtisar(ordinal, visit).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getVisitDetail(idVisit: Int, callback: (Either<Visit, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getVisitDetail(idVisit).await()
                .parse(
                    {
                        callback(Either.Left(VisitMapper.toVisit(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun giveRecipe(
        visitId: Int,
        recipeId: Int,
        patientId: Int,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.giveRecipe(visitId, GiveRecipeRequest(recipeId, patientId)).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun updateVisitDetail(
        id: Int,
        bundle: Bundle,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateVisitDetail(id, VisitMapper.toEditVisitRequest(bundle)).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createVisitHIVNegative(
        ordinal: Int,
        idPatient: Int,
        date: String,
        callback: (Either<Visit, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createVisitHIVNegativeAsync(VisitHIVNegativeRequest(ordinal, idPatient, date))
                .await()
                .parse(
                    {
                        callback(Either.Left(VisitMapper.toVisit(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getTodayVisit(callback: (Either<List<VisitHistory>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO){
            api.getAllVisitByUpk(100,0,"TODAY")
                .await()
                .parse({
                    it
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getTodayVisitWithKeyword(
        keyword:String,
        callback: (Either<List<VisitHistory>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO){
            api.getAllVisitByUpkWithKeyword(100,keyword,0,"TODAY")
                .await()
                .parse({
                    it
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getPlanVisit(callback: (Either<List<VisitHistory>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO){
            api.getPlannedVisit(100,0,"RENCANA_KUNJUNGAN")
                .await()
                .parse({
                    it
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getVisitHistory(callback: (Either<List<VisitHistory>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO){
            api.getAllVisitByUpk(100,0,"HISTORY")
                .await()
                .parse({
                    it
                    callback(Either.Left(it.data))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun createGeneralVisit(
        patientId: Int,
        callback: (Either<CreateVisitResponse, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createVisitGeneral(patientId).await()
                .parse({
                    callback(Either.Left(it))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getVisitAllDetail(
        visitId: Int,
        callback: (Either<VisitAllDetailResponse, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getAllDetailVisit(visitId).await()
                .parse({
                    callback(Either.Left(it))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun createTreatment(visitId: Int, callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.createTreatment(visitId).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },{
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun saveTreatment(
        visitId: Int,
        visitData:VisitData,
        callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.saveTreatmentData(visitId,visitData).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },{
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun endVisit(visitId: Int, callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.endVisit(visitId).await()
                .parse({
                    callback(Either.Left(it.message?:""))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun getPreviousTreatment(
        visitId: Int,
        callback: (Either<VisitAllDetailResponse, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getPreviousTreatment(visitId).await()
                .parse({
                    callback(Either.Left(it))
                },{
                    callback(Either.Right(it))
                })
        }
    }

    override fun updateSisaObat(
        visitId: Int,
        medicineLeftRequest: MedicineLeftRequest,
        callback: (Either<DefaultResponse, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateSisaObat(visitId,medicineLeftRequest).await()
                .parse(
                    {
                        callback(Either.Left(it))
                    },{
                        callback(Either.Right(it))
                    }
                )
        }
    }

}