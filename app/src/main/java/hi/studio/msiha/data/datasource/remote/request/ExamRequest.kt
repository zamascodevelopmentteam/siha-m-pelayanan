package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName


data class ExamRequest(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("userId")
    val userId: Int? = null,
    @SerializedName("hospitalId")
    val hospitalId: Int? = null,
    @SerializedName("examDate")
    val examDate: Long? = null,
    @SerializedName("examOrdinal")
    val examOrdinal: Int? = null,
    @SerializedName("examRequestDate")
    val examRequestDate: Long? = null,
    @SerializedName("receiveResultDate")
    val receiveResultDate: Long? = null,

    @SerializedName("populationGroup")
    val populationGroup: String? = null,
    @SerializedName("lsmPenjangkau")
    val lsmPenjangkau: String? = null,
    @SerializedName("reasonCheckHiv")
    val reasonCheck: String? = null,

    @SerializedName("typeTestHiv")
    val hivExaminationMethod: String? = null,
    @SerializedName("nameReagen1")
    val reagen1: Int? = null,
    @SerializedName("resultReagen1")
    val reagen1Result: String? = null,
    @SerializedName("nameReagen2")
    val reagen2: Int? = null,
    @SerializedName("resultReagen2")
    val reagen2Result: String? = null,
    @SerializedName("nameReagen3")
    val reagen3: Int? = null,
    @SerializedName("resultReagen3")
    val reagen3Result: String? = null,
    @SerializedName("nameReagenNat")
    val reagenNat: Int? = null,
    @SerializedName("resiulTestNat")
    val reagenNatResult: String? = null,
    @SerializedName("reagenPcr")
    val reagenPcr: Int? = null,
    @SerializedName("resultTestPcr")
    val reagenPcrResult: String? = null,
    @SerializedName("nameReagenElisa")
    val reagenElisa: Int? = null,
    @SerializedName("resultTestElisa")
    val reagenElisaResult: String? = null,
    @SerializedName("reagenBlot")
    val reagenBlot: Int? = null,
    @SerializedName("resultTestWesternBlot")
    val reagenBlotResult: String? = null,

    @SerializedName("hivResult")
    val hivResult: Boolean? = null,
    @SerializedName("conclusionResultTestHiv")
    val conclusionHivResult: String? = null,

    @SerializedName("testSifilis")
    val testSyphilis: Boolean? = null,
    @SerializedName("sifilisResult")
    val syphilisResult: String? = null,
    @SerializedName("testIms")
    val testIMS: Boolean? = null,
    @SerializedName("imsResult")
    val imsResult: String? = null,
    @SerializedName("rprIms")
    val rprIms: Int? = null,

    //for vl exam
    @SerializedName("labTest")
    val vlTestType: String? = null,
    @SerializedName("reagenName")
    val vlReagen: String? = null,
    @SerializedName("mountReagen")
    val vlReagenAmount: Int? = null,
    @SerializedName("labResult")
    val vlTestResult: Int? = null
)