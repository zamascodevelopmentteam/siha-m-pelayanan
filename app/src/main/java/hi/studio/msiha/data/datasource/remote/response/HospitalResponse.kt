package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName
import hi.studio.msiha.domain.model.Hospital


data class HospitalResponse(
    @SerializedName("data")
    val data: Hospital?
) : CommonResponse()

data class HospitalDataResponse(
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("name")
    val name: String? = ""
)