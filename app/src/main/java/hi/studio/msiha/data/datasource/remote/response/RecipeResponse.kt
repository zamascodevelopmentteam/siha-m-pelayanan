package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class RecipeResponse(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("recipeNumber")
    val recipeNo: String? = "",
    @SerializedName("medicine_total")
    val medicineTotal: Int = 0
)