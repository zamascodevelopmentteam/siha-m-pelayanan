package hi.studio.msiha.data.mapper

import hi.studio.msiha.data.datasource.remote.response.ReagenResponse
import hi.studio.msiha.domain.model.Reagen

object ReagenMapper {
    fun toReagen(response: ReagenResponse?): Reagen? {
        response?.apply {
            return Reagen(id, name, codeName, medicineType, stockUnitType, packageUnitType, sum)
        }
        return null
    }

    fun toListReagen(response: List<ReagenResponse>?): List<Reagen> = response?.map {
        Reagen(
            it.id,
            it.name,
            it.codeName,
            it.medicineType,
            it.stockUnitType,
            it.packageUnitType,
            it.sum
        )
    } ?: run {
        listOf<Reagen>()
    }
}