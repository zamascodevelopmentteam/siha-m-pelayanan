package hi.studio.msiha.data.datasource.remote.request

import hi.studio.msiha.domain.model.Hospital

class FarmasiOrderRequest(
    val to:String,
    val hospitalId:Int,
    val medicineId:Int,
    val amount:Int
)