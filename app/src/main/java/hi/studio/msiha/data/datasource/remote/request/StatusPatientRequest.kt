package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName


data class StatusPatientRequest(
    @SerializedName("isActive")
    val isActive: Boolean = false, // true
    @SerializedName("password")
    val password: String = "" // password
)