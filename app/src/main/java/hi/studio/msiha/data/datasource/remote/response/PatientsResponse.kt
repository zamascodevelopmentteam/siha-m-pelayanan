package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class PatientsResponse(
    @SerializedName("data")
    val data: List<PatientResponse.Data>? = listOf()
) : CommonResponse()