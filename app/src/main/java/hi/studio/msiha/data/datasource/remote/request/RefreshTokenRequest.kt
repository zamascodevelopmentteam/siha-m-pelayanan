package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName


data class RefreshTokenRequest(
    @SerializedName("refreshToken")
    val refreshToken: String = "", // 1b45c4ef-469d-4d6d-9448-48d81a6e5016
    @SerializedName("userId")
    val userId: String = "" // 2
)