package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class RecipeMedicineRequest(
    @SerializedName("medicineId")
    val medicineId: Int = 0,
    @SerializedName("rule")
    val rule: String? = "",
    @SerializedName("amount")
    val amount: Int = 0
)