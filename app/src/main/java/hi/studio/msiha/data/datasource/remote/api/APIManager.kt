package hi.studio.msiha.data.datasource.remote.api

import retrofit2.Retrofit

object APIManager {
    fun provideAuthAPI(retrofit: Retrofit): AuthAPI {
        return retrofit.create(AuthAPI::class.java)
    }

    fun provideHospitalAPI(retrofit: Retrofit): HospitalAPI {
        return retrofit.create(HospitalAPI::class.java)
    }

    fun provideUserAPI(retrofit: Retrofit): UserAPI {
        return retrofit.create(UserAPI::class.java)
    }

    fun providePatientAPI(retrofit: Retrofit): PatientAPI {
        return retrofit.create(PatientAPI::class.java)
    }

    fun provideVisitAPI(retrofit: Retrofit): VisitAPI {
        return retrofit.create(VisitAPI::class.java)
    }

    fun provideMedicineAPI(retrofit: Retrofit): MedicineAPI {
        return retrofit.create(MedicineAPI::class.java)
    }

    fun provideOrderAPI(retrofit: Retrofit): OrderAPI {
        return retrofit.create(OrderAPI::class.java)
    }

    fun provideRecipeAPI(retrofit: Retrofit): RecipeAPI {
        return retrofit.create(RecipeAPI::class.java)
    }

    fun provideExamAPI(retrofit: Retrofit): ExamAPI {
        return retrofit.create(ExamAPI::class.java)
    }
}