package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class PagingResponse(
    @SerializedName("page")
    val page: Int? = 0,
    @SerializedName("size")
    val size: Int? = 0,
    @SerializedName("total")
    val total: Int? = 0
)