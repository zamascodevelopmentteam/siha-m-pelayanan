package hi.studio.msiha.data.mapper

import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.constant.KEY_FCM_TOKEN
import hi.studio.msiha.data.datasource.remote.request.LoginRequest
import hi.studio.msiha.data.datasource.remote.response.AuthResponse
import hi.studio.msiha.domain.model.AuthResult
import hi.studio.msiha.domain.model.User

object AuthMapper {
    fun toUser(response: AuthResponse): AuthResult {
        response.data.apply {
            return AuthResult(
                token ?: "",
                refreshToken ?: "",
                UserMapper.toUser(user) ?: User()
            )
        }
    }

    fun toLoginRequest(nik: String, pass: String): LoginRequest {
        return LoginRequest(nik, pass, Hawk.get(KEY_FCM_TOKEN)?:"")
    }
}