package hi.studio.msiha.data.repositoryImpl

import hi.studio.msiha.data.datasource.remote.api.UserAPI
import hi.studio.msiha.data.datasource.remote.request.OtpRequest
import hi.studio.msiha.data.mapper.OtpMapper
import hi.studio.msiha.data.mapper.UserMapper
import hi.studio.msiha.domain.model.Otp
import hi.studio.msiha.domain.model.User
import hi.studio.msiha.domain.repository.UserRepository
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class UserRepositoryImpl(private val api: UserAPI) : UserRepository {
    override fun updateProfile(user: User, callback: (Either<User, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.updateProfileAsync(user.id ?: 0, UserMapper.toUserProfileRequest(user)).await()
                .parse(
                    {
                        callback(Either.Left(UserMapper.toUser(it.data) ?: User()))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getOtp(idUser: Int, callback: (Either<Otp, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getOtpAsync(idUser).await()
                .parse(
                    {
                        callback(Either.Left(OtpMapper.toOtp(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun submitOtp(patientId: Int, code: String, callback: (Either<Otp, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.submitOtpAsync(OtpRequest(code), patientId).await()
                .parse(
                    {
                        callback(Either.Left(OtpMapper.toOtp(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getUserByNik(nik: String, callback: (Either<User?, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getUserByNikAsync(nik).await()
                .parse(
                    {
                        callback(Either.Left(UserMapper.toUser(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getProfile(id: Int, callback: (Either<User, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getProfileAsync(id).await()
                .parse(
                    {
                        callback(Either.Left(UserMapper.toUser(it.data) ?: User()))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }
}