package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("data")
    val data: Data
) : CommonResponse()

data class Data(
    @SerializedName("token")
    val token: String? = "",
    @SerializedName("refreshToken")
    val refreshToken: String? = "",
    @SerializedName("user")
    val user: UserResponse?
)