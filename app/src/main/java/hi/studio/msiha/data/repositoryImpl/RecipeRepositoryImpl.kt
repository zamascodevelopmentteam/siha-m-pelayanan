package hi.studio.msiha.data.repositoryImpl

import com.orhanobut.hawk.Hawk
import hi.studio.msiha.data.datasource.remote.api.RecipeAPI
import hi.studio.msiha.data.datasource.remote.request.ARVRequest
import hi.studio.msiha.data.datasource.remote.request.MedicinePrescriptionRequest
import hi.studio.msiha.data.mapper.ReagenMapper
import hi.studio.msiha.data.mapper.RecipeMapper
import hi.studio.msiha.domain.model.*
import hi.studio.msiha.domain.repository.RecipeRepository
import hi.studio.msiha.ui.home.patient.detail.exam.HIVExamFragment.Companion.KEY_REAGEN_TYPE
import hi.studio.msiha.utils.Either
import hi.studio.msiha.utils.extention.coroutineLaunch
import hi.studio.msiha.utils.extention.parse
import kotlinx.coroutines.Dispatchers

class RecipeRepositoryImpl(private val api: RecipeAPI) : RecipeRepository {
    override fun getLastGivenPrescription(
        patientId: Int,
        callback: (Either<Prescription, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getLastGivenPrescription(patientId).await()
                .parse(
                    {
                        callback(Either.Left(it.data))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun giveMedicine(visitId: Int, callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.giveMedicine(visitId).await()
                .parse(
                    {
                        callback(Either.Left(it.message?:""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createPrescription(
        medicinePrescriptionRequest: MedicinePrescriptionRequest,
        visitId: Int,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createPrescription(visitId,medicinePrescriptionRequest).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getArv(callback: (Either<List<ArvStock>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getARVMedicineList().await()
                .parse(
                    {
                        callback(Either.Left(it.data))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getReagen(callback: (Either<List<ArvStock>, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.getReagenMedicineList().await()
                .parse(
                    {
                        callback(Either.Left(it.data))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getReagens(
        type: String,
        exam: String,
        callback: (Either<List<Reagen>, Throwable>) -> Unit
    ) {
        if(Hawk.contains(KEY_REAGEN_TYPE)){
            var reagenType = Hawk.get<String>(KEY_REAGEN_TYPE)
            val reagens = mapOf(reagenType to "true")
            coroutineLaunch(Dispatchers.IO) {
                api.getReagen(reagens).await()
                    .parse(
                        {
                            callback(Either.Left(ReagenMapper.toListReagen(it.data)))
                        },
                        {
                            callback(Either.Right(it))
                        }
                    )
            }
        }else{
            coroutineLaunch(Dispatchers.IO) {
                api.getReagenEmpty().await()
                    .parse(
                        {
                            callback(Either.Left(ReagenMapper.toListReagen(it.data)))
                        },
                        {
                            callback(Either.Right(it))
                        }
                    )
            }
        }

    }

    override fun createArvRecipe(
        request: ARVRequest,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createArvRecipe(request).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun confirmRecipe(idRecipe: Int, callback: (Either<String, Throwable>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            api.confirmRecipe(idRecipe).await()
                .parse(
                    {
                        callback(Either.Left(it.message ?: ""))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun getRecipeDetail(
        idRecipe: Int,
        callback: (Either<RecipeDetail?, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.getRecipeDetail(idRecipe).await()
                .parse(
                    {
                        callback(Either.Left(RecipeMapper.toRecipeDetail(it.data)))
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }

    override fun createRecipe(
        nik: String,
        date: Long,
        notes: String,
        medicines: List<RecipeMedicine>,
        callback: (Either<String, Throwable>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            api.createRecipe(RecipeMapper.toAddRecipe(nik, date, notes, medicines)).await()
                .parse(
                    { it ->
                        it.message?.let {
                            callback(Either.Left(it))
                        }
                    },
                    {
                        callback(Either.Right(it))
                    }
                )
        }
    }
}