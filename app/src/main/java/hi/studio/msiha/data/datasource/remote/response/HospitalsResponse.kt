package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class HospitalsResponse(
    @SerializedName("data")
    val data: List<HospitalDataResponse>?
) : CommonResponse()