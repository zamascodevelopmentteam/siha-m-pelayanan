package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName


data class RegisterRequest(
    @SerializedName("avatar")
    val avatar: String = "", // https://www.w3schools.com/howto/img_avatar.png
    @SerializedName("name")
    val name: String = "", // Priagung Satyagama
    @SerializedName("nik")
    val nik: String = "", // 1234566781
    @SerializedName("password")
    val password: String = "", // passw0rd
    @SerializedName("regnas")
    val regnas: String = "", // 31241341
    @SerializedName("role")
    val role: String = "" // RR_STAFF
)