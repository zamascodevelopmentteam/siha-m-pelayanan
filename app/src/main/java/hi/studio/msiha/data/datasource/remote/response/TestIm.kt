package hi.studio.msiha.data.datasource.remote.response

import android.os.Parcelable
import hi.studio.msiha.domain.model.ArvStock
import kotlinx.android.parcel.Parcelize

@Parcelize
class TestIm(
    val id:Int?,
    val ordinal:Int?,
    val namaReagen:String?,
    val ditestIms:Boolean?,
    val qtyReagen:Int,
    val hasilTestIms:Boolean?,
    val ditestSifilis:Boolean?,
    val hasilTestSifilis:Boolean?,
    val createdBy:Int?,
    val updatedBy:Int?,
    val createdAt:String?,
    val updatedAt:String?,
    val visitId:Int?,
    val namaReagenData:ArvStock
) : Parcelable