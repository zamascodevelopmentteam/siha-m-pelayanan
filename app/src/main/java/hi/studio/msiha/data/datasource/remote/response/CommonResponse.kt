package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

abstract class CommonResponse(
    @SerializedName("status")
    val status: Int? = 0,
    @SerializedName("message")
    val message: String? = "unknown_message",
    @SerializedName("paging")
    val paging: PagingResponse? = PagingResponse()
)