package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class ProfileRequest(
    @SerializedName("dateBirth")
    val dateBirth: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("gender")
    val gender: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("phone")
    val phone: String? = null,
    @SerializedName("placeBirth")
    val placeBirth: String? = null,
    @SerializedName("role")
    val role: String? = null
)