package hi.studio.msiha.data.datasource.remote.request

import com.google.gson.annotations.SerializedName

data class RecipeRequest(
    @SerializedName("nik")
    val nik: String = "",
    @SerializedName("date")
    val date: Long = 0L,
    @SerializedName("notes")
    val notes: String? = "",
    @SerializedName("medicines")
    val medicines: List<RecipeMedicineRequest> = emptyList()
)