package hi.studio.msiha.data.datasource.remote

import hi.studio.msiha.AppSIHA
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import org.json.JSONObject
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class NetworkInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!AppSIHA.instance.isInternetAvailable()) {
            return createResponse(422, "Internet tidak terdeteksi", chain)
        }
        return try {
            val response = chain.proceed(chain.request())
            val type = response.body?.contentType()?.subtype
            check(type == "json") { "Tipe respon tidak dikenal" }
            response
        } catch (e: SocketTimeoutException) {
            Timber.e(e)
            createResponse(422, "Gagal terkoneksi ke server", chain)
        } catch (e: UnknownHostException) {
            Timber.e(e)
            createResponse(422, "Server tidak dapat diakses", chain)
        } catch (e: IllegalStateException) {
            Timber.e(e)
            createResponse(422, e.localizedMessage, chain)
        }
    }

    private fun createResponse(
        statusCode: Int,
        message: String,
        chain: Interceptor.Chain
    ): Response {
        val type = "application/json; charset=utf-8".toMediaTypeOrNull()
        val content = JSONObject().apply {
            put("message", message)
        }

        return Response.Builder()
            .protocol(Protocol.HTTP_1_1)
            .code(statusCode)
            .message("network error")
            .body(
                content.toString().toResponseBody(type)
            )
            .request(chain.request())
            .build()
    }
}