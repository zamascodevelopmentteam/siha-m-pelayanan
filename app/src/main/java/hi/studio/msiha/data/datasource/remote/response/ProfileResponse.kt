package hi.studio.msiha.data.datasource.remote.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("data")
    val data: UserResponse?
) : CommonResponse()